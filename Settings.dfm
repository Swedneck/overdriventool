object Form2: TForm2
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Settings'
  ClientHeight = 382
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 3
    Width = 284
    Height = 174
    Caption = 'General'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 0
    object Label2: TLabel
      Left = 16
      Top = 20
      Width = 154
      Height = 13
      Caption = 'Show additional info about GPU:'
    end
    object Label4: TLabel
      Left = 36
      Top = 142
      Width = 166
      Height = 13
      Caption = '(this may change current gpu_id'#39's)'
    end
    object CheckBox3: TCheckBox
      Left = 26
      Top = 39
      Width = 97
      Height = 17
      Caption = 'Bus number'
      TabOrder = 0
    end
    object CheckBox1: TCheckBox
      Left = 26
      Top = 85
      Width = 97
      Height = 17
      Caption = 'PNP string'
      TabOrder = 1
    end
    object CheckBox4: TCheckBox
      Left = 26
      Top = 62
      Width = 97
      Height = 17
      Caption = 'Adapter index'
      TabOrder = 2
    end
    object CheckBox5: TCheckBox
      Left = 16
      Top = 124
      Width = 253
      Height = 17
      Caption = 'Do not list unsupported GPU'#39's (requires restart)'
      TabOrder = 3
    end
    object CheckBox6: TCheckBox
      Left = 147
      Top = 62
      Width = 97
      Height = 17
      Caption = 'Friendly name'
      TabOrder = 4
    end
    object CheckBox7: TCheckBox
      Left = 147
      Top = 39
      Width = 97
      Height = 17
      Caption = 'Registry key'
      TabOrder = 5
    end
  end
  object Button1: TButton
    Left = 453
    Top = 348
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 1
    OnClick = Button1Click
  end
  object GroupBox2: TGroupBox
    Left = 5
    Top = 183
    Width = 284
    Height = 162
    Caption = 'I2C (for RX 400, some RX500 series)'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 2
    object Label1: TLabel
      Left = 24
      Top = 45
      Width = 250
      Height = 26
      Caption = 
        'It'#39's recommended to disable I2C when it'#39's not being '#13#10'used or no' +
        't supported.'
    end
    object Label3: TLabel
      Left = 33
      Top = 111
      Width = 244
      Height = 39
      Caption = 
        'On next program startup will be done scanning for '#13#10'supported VR' +
        'M controller.'#13#10'Use this if something in PC config was changed.'
    end
    object CheckBox2: TCheckBox
      Left = 15
      Top = 22
      Width = 162
      Height = 17
      Caption = 'Enable I2C (requires restart)'
      TabOrder = 0
    end
    object Button2: TButton
      Left = 32
      Top = 80
      Width = 97
      Height = 25
      Caption = 'Rescan lines'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 295
    Top = 3
    Width = 250
    Height = 141
    Caption = 'Troubleshooting'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 3
    object Label5: TLabel
      Left = 34
      Top = 36
      Width = 145
      Height = 13
      Caption = '(ADL_ERR_NOT_SUPPORTED)'
      OnMouseDown = Label5MouseDown
      OnMouseUp = Label5MouseUp
    end
    object CheckBox8: TCheckBox
      Left = 16
      Top = 19
      Width = 217
      Height = 17
      Caption = 'Hide errors with ErrorCode -8'
      TabOrder = 0
    end
    object CheckBox14: TCheckBox
      Left = 16
      Top = 79
      Width = 225
      Height = 17
      Caption = 'Treat values mismatch on apply as error'
      TabOrder = 1
    end
    object CheckBox15: TCheckBox
      Left = 16
      Top = 56
      Width = 185
      Height = 17
      Caption = 'Automatically reset before apply'
      TabOrder = 2
    end
  end
  object GroupBox4: TGroupBox
    Left = 295
    Top = 150
    Width = 250
    Height = 192
    Caption = 'Ini backup'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 4
    object Label9: TLabel
      Left = 34
      Top = 37
      Width = 164
      Height = 13
      Caption = 'something was changed in profiles'
      OnMouseDown = Label9MouseDown
      OnMouseUp = Label9MouseUp
    end
    object Label6: TLabel
      Left = 40
      Top = 111
      Width = 106
      Height = 13
      Caption = 'when ini has 0 profiles'
      Enabled = False
      OnMouseDown = Label6MouseDown
      OnMouseUp = Label6MouseUp
    end
    object SpinEdit1: TSpinEdit
      Left = 170
      Top = 66
      Width = 59
      Height = 22
      Enabled = False
      MaxValue = 9999
      MinValue = 1
      TabOrder = 0
      Value = 10
    end
    object CheckBox10: TCheckBox
      Left = 16
      Top = 22
      Width = 225
      Height = 17
      Caption = 'Automatically make .ini file backup when'
      TabOrder = 1
      OnClick = CheckBox10Click
    end
    object CheckBox9: TCheckBox
      Left = 16
      Top = 94
      Width = 217
      Height = 17
      Caption = 'Restore automatically from last backup'
      Enabled = False
      TabOrder = 2
      OnClick = CheckBox9Click
    end
    object Button3: TButton
      Left = 72
      Top = 153
      Width = 113
      Height = 25
      Caption = 'Make backup now'
      TabOrder = 3
      OnClick = Button3Click
    end
    object CheckBox11: TCheckBox
      Left = 32
      Top = 130
      Width = 215
      Height = 17
      Caption = 'Silently (without confirmation message)'
      Enabled = False
      TabOrder = 4
    end
    object CheckBox12: TCheckBox
      Left = 16
      Top = 68
      Width = 146
      Height = 17
      Caption = 'Limit number of backups to'
      Enabled = False
      TabOrder = 5
      OnClick = CheckBox12Click
    end
  end
end

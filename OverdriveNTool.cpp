//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"
#include <tchar.h>


//---------------------------------------------------------------------------
USEFORM("PowerPlay.cpp", Form3);
USEFORM("Settings.cpp", Form2);
USEFORM("Unit1.cpp", Form1);
USEFORM("ChangeFriendlyName.cpp", Form4);
USEFORM("Reorder.cpp", Form5);
//---------------------------------------------------------------------------
void ExecuteCustom(bool* CustomEnabled, String* CustomCommand, String* CustomGpu, String* CustomOryginalCommand, bool* cmd_messages)
{
Cmd.SetCustomToGPU(*CustomGpu, *CustomCommand, *CustomOryginalCommand, *cmd_messages);
*CustomEnabled=false;
*CustomCommand="";
*CustomGpu="";
*CustomOryginalCommand="";
}
//---------------------------------------------------------------------------
bool WaitParse(const String command, bool cmd_messages)
{
String str=command;
str=str.Delete(1,5);
int time;
if (TryStrToInt(str, time))
 {
  Sleep(time);
  return true;
 } else
  {
   Cmd.ErrorInfoShow("Wrong command: "+ command+"\x0d\x0a"+"\""+str+"\" cannot be used as wait time", cmd_messages, NULL, 0, false);
   return false;
  }
}
//---------------------------------------------------------------------------
int WINAPI _tWinMain(HINSTANCE, HINSTANCE, LPTSTR, int)
{
     try
	{
               INI.SetIniFileName();
               if (ParamCount()>0)
                 {
                   bool ShowGui=false;
                   bool HighGPUCount;
                   int retcode=-1;
                   //********************************
                   //If -wait is the first parameter don't touch Windows or ADL API
                   if (Pos("-wait",ParamStr(1))==1)
                    {
                     WaitParse(ParamStr(1), false);
                    }
                   //********************************
                   if (CheckIfDriverInstalled())
                    {
                      INI.LoadOptions(true);
                      if (INI.LoadProfiles()==0)
                       INI.RestoreFromBackup(true);
                      if (FindGPUs())
                        {
                          HighGPUCount=Cmd.IsHighGPUCount();
                          retcode=0;
                        } else
                          {
                            if (VerifyADL_Library()==-2)
                              {
                                blad("This application requires driver 18.12.2 or newer",NULL,false);
                              }
                            return retcode;
                          }
                    } else
                      {
                        return retcode;    //gdy driver niezainstalowany to tylko wychodzimy bez komunikatu
                      }
                   //********************************
                   bool cmd_messages=false;
                   bool CustomEnabled=false;
                   String CustomGpu="";
                   String CustomCommand="";
                   String CustomOryginalCommand="";
                   for (int j = 1; j<=ParamCount(); j++)
                     {
                      if (ParamStr(j)=="-consoleonly")
                        {
                          cmd_messages=true;
                          continue;
                        }
                      if (ParamStr(j).Length()>=3)
                        {
                         String str=ParamStr(j);
                         String par=str.SubString(1,2);
                         if ((par == "-p") || (par == "-c") || (par == "-r") || (par == "cp") || (par == "cm") || (par == "co") || (par == "-t"))
                          {
                           if (CustomEnabled)  //wywo�ujemy komend� dla CustomValues
                             ExecuteCustom(&CustomEnabled, &CustomCommand, &CustomGpu, &CustomOryginalCommand, &cmd_messages);
                           try
                            {
                             str.Delete(1,2);
                             int GpuInd=NULL;
                             bool AffectsAllGPUs=false;
                             int GPUsLength=1;
                             if (str.SubString(1,1)=="*")
                              {
                                AffectsAllGPUs=true;
                                GPUsLength=Cmd.GetGPULength();
                              }
                             if (AffectsAllGPUs==false)
                              {
                                int gsize=1;
                                if (HighGPUCount) gsize=2;
                                if (TryStrToInt(str.SubString(1,gsize),GpuInd)==false)
                                  {
                                    if (HighGPUCount==false)
                                      {
                                        Cmd.ErrorInfoShow("Wrong command: "+ ParamStr(j)+"\x0d\x0a"+"\""+str.SubString(1,gsize)+"\" cannot be used as [gpu_id]", cmd_messages, NULL, 0, false);
                                      } else
                                        {
                                          Cmd.ErrorInfoShow("Wrong command: "+ ParamStr(j)+"\x0d\x0a"+"For systems with more than 10 GPUs two digits must be used as [gpu_id] (00,01,02 ... 09,10,11,12,13 ...)", cmd_messages, NULL, 0, false);
                                        }
                                    retcode=-1;
                                    continue;
                                  }
                               if (!Cmd.CorrectGPU(GpuInd))
                                {
                                  Cmd.ErrorInfoShow("Wrong command - GPU with id="+IntToStr(GpuInd)+" not found", cmd_messages, NULL, 0, false);
                                  retcode=-1;
                                  continue;
                                }
                               if (!Cmd.SupportedGPU(GpuInd))
                                {
                                  Cmd.ErrorInfoShow("Wrong command - GPU with id="+IntToStr(GpuInd)+" is not supported", cmd_messages, NULL, 0, false);
                                  retcode=-1;
                                  continue;
                                }
                              }
                             int p;
                             if (par!="-r" && par!="-t")
                              {
                               if (HighGPUCount==false || AffectsAllGPUs)
                                {
                                 str.Delete(1,1);
                                } else
                                  {
                                   str.Delete(1,2);
                                  }

                               p=Cmd.FindProfileByName(str);
                               if (p==-1)
                                {
                                  Cmd.ErrorInfoShow("Wrong command - profile: \""+str+"\" not found", cmd_messages, NULL, 0, false);
                                  retcode=-1;
                                  continue;
                                }
                              }
                             if ((par == "-p") || (par == "-c"))    //apply
                              {
                                Cmd.InitializeI2C();
                                for (int i = 0; i < GPUsLength; i++)
                                 {
                                  if (AffectsAllGPUs)
                                   {
                                     if (!Cmd.SupportedGPU(i)) continue;
                                     GpuInd=i;
                                   }
                                  if (Cmd.SetProfileToGPU(p, GpuInd, cmd_messages))
                                    {
                                     if (par == "-c")
                                      {
                                        String s="";
                                        if (Cmd.GetGPULength()>1)
                                         {
                                           s=" to GPU "+IntToStr(GpuInd);
                                         }
                                        Cmd.ErrorInfoShow("Succesfully applied profile \""+str+"\""+s, cmd_messages, NULL, 1, false);
                                      }
                                    } else
                                       {
                                        Cmd.ErrorInfoShow("Failed to apply profile \""+str+"\" to GPU "+IntToStr(GpuInd), cmd_messages, NULL, 0, false);
                                        retcode=-1;
                                       }
                                 }
                              } else
                               {
                                 if (par == "-r")               //reset
                                  {
                                    Cmd.InitializeI2C();
                                    for (int i = 0; i < GPUsLength; i++)
                                     {
                                      if (AffectsAllGPUs)
                                       {
                                        if (!Cmd.SupportedGPU(i)) continue;
                                        GpuInd=i;
                                       }
                                      Reset(GpuInd, cmd_messages, true);
                                     }
                                  } else
                                   {
                                     if ((par == "cp") || (par == "cm") || (par == "co"))  //compare
                                       {
                                         Cmd.InitializeI2C();
                                         for (int i = 0; i < GPUsLength; i++)
                                          {
                                            if (AffectsAllGPUs)
                                             {
                                              if (!Cmd.SupportedGPU(i)) continue;
                                              GpuInd=i;
                                             }
                                            if (Cmd.CompareProfileWithGPU(p, GpuInd, par, cmd_messages)==false)
                                             {
                                              Cmd.ErrorInfoShow("Failed to compare values for GPU "+IntToStr(GpuInd)+" with profile \""+str+"\"", cmd_messages, NULL, 0, false);
                                              retcode=-1;
                                             }
                                          }
                                       } else
                                        {
                                          if (par == "-t")               //restart
                                            {
                                              //-------
                                              bool wait=false;
                                              //je�eli nast�pna komenda nie jest restartem to czekamy
                                              //je�eli to ostatnia komenda a pokazujemy gui to te� czekamy
                                              if (j<ParamCount())
                                               {
                                                if (Pos("-t", ParamStr(j+1))!=1)
                                                 wait=true;
                                               } else
                                                {
                                                 wait=ShowGui;
                                                }
                                              //-------

                                              for (int i = 0; i < GPUsLength; i++)
                                               {
                                                if (AffectsAllGPUs)
                                                  {
                                                    if (!Cmd.SupportedGPU(i)) continue;
                                                    GpuInd=i;
                                                  }
                                                Cmd.RestartGPU(GpuInd, cmd_messages, wait);
                                               }
                                            }
                                        }
                                   }
                               }
                            } catch(...)
        		      {
                                Cmd.ErrorInfoShow("Failed to parse command: "+ ParamStr(j), cmd_messages, NULL, 0, false);
                                retcode=-1;
         		      }
                          } else
                           {
                             if (ParamStr(j)=="-getcurrent")
                              {
                               if (CustomEnabled)  //wywo�ujemy komend� dla CustomValues
                                 ExecuteCustom(&CustomEnabled, &CustomCommand, &CustomGpu, &CustomOryginalCommand, &cmd_messages);
                               Cmd.PrintCurrentValues();
                              } else
                               if (ParamStr(j)=="-showgui")
                                {
                                 if (CustomEnabled)  //wywo�ujemy komend� dla CustomValues
                                   ExecuteCustom(&CustomEnabled, &CustomCommand, &CustomGpu, &CustomOryginalCommand, &cmd_messages);
                                 ShowGui=true;
                                } else

                               if (Pos("-wait",ParamStr(j))==1)    //wait
                                {
                                 if (j==1)       //ju� wywo�ana na pocz�tku
                                  continue;
                                 if (CustomEnabled)  //wywo�ujemy komend� dla CustomValues
                                  ExecuteCustom(&CustomEnabled, &CustomCommand, &CustomGpu, &CustomOryginalCommand, &cmd_messages);
                                 if (WaitParse(ParamStr(j), cmd_messages)==false)
                                  retcode=-1;
                                } else
                                 {
                                  String par=(ParamStr(j)).SubString(1,3);
                                  if (par == "-ac")
                                   {
                                     if (CustomEnabled)  //wywo�ujemy komend� dla CustomValues
                                       ExecuteCustom(&CustomEnabled, &CustomCommand, &CustomGpu, &CustomOryginalCommand, &cmd_messages);

                                     String str=ParamStr(j);
                                     str=str.Delete(1,3);
                                     CustomCommand="";
                                     CustomOryginalCommand=ParamStr(j);
                                     CustomGpu=str;
                                     CustomEnabled=true;
                                   } else
                                     if (CustomEnabled)
                                      {
                                       if (CustomCommand!="")
                                        CustomCommand+=Char(30);
                                       if (CustomOryginalCommand!="")
                                        CustomOryginalCommand+=" ";

                                       CustomCommand+=ParamStr(j);
                                       CustomOryginalCommand+=ParamStr(j);
                                       if (j==ParamCount())       //ostatni element listy
                                        {
                                         Cmd.SetCustomToGPU(CustomGpu, CustomCommand, CustomOryginalCommand, cmd_messages);
                                        }
                                      } else
                                         {
                                          Cmd.ErrorInfoShow("Wrong command: "+ ParamStr(j), cmd_messages, NULL, 0, false);
                                          retcode=-1;
                                         }
                               }
                           }
                        } else
                          {
                            Cmd.ErrorInfoShow("Wrong command: "+ ParamStr(j), cmd_messages, NULL, 0, false);
                            retcode=-1;
                          }
                     }
                   //********************************
                   if (ShowGui)
                    Cmd.ShowGUI(); else
                     return retcode;

                 }

               Application->Initialize();
               Application->MainFormOnTaskBar = true;
               Application->Title = "OverdriveNTool";
	       Application->CreateForm(__classid(TForm1), &Form1);
		Application->CreateForm(__classid(TForm2), &Form2);
		Application->CreateForm(__classid(TForm3), &Form3);
		Application->CreateForm(__classid(TForm5), &Form5);
		Application->Run();
	}
         catch (Exception &exception)
       	  {
           Application->ShowException(&exception);
	  }
     catch (...)
	{
               try
		   {
                      throw Exception("");
                   }
               catch (Exception &exception)
	       	   {
                     Application->ShowException(&exception);
		   }
	}
     return 0;
}
//---------------------------------------------------------------------------


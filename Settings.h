//---------------------------------------------------------------------------

#ifndef SettingsH
#define SettingsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Samples.Spin.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TCheckBox *CheckBox3;
	TCheckBox *CheckBox1;
	TCheckBox *CheckBox2;
	TLabel *Label1;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	TLabel *Label3;
	TGroupBox *GroupBox1;
	TGroupBox *GroupBox2;
	TCheckBox *CheckBox4;
	TCheckBox *CheckBox5;
	TLabel *Label4;
	TCheckBox *CheckBox6;
	TCheckBox *CheckBox7;
	TGroupBox *GroupBox3;
	TLabel *Label5;
	TGroupBox *GroupBox4;
	TSpinEdit *SpinEdit1;
	TLabel *Label9;
        TCheckBox *CheckBox8;
	TCheckBox *CheckBox10;
	TCheckBox *CheckBox9;
	TLabel *Label6;
	TButton *Button3;
	TCheckBox *CheckBox11;
	TCheckBox *CheckBox12;
	TCheckBox *CheckBox14;
	TCheckBox *CheckBox15;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall CheckBox10Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall CheckBox12Click(TObject *Sender);
	void __fastcall CheckBox9Click(TObject *Sender);
	void __fastcall Label5MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label5MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label9MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label9MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label6MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label6MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif

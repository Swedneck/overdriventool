object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Change friendly name'
  ClientHeight = 240
  ClientWidth = 549
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnResize = FormResize
  DesignSize = (
    549
    240)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 222
    Height = 13
    Caption = 'Empty name will delete FriendlyName property'
  end
  object ValueListEditor1: TValueListEditor
    Left = 8
    Top = 32
    Width = 533
    Height = 169
    Anchors = [akLeft, akTop, akRight, akBottom]
    DrawingStyle = gdsClassic
    Strings.Strings = (
      '')
    TabOrder = 0
    TitleCaptions.Strings = (
      'GPU'
      'Friendly name')
    ColWidths = (
      358
      169)
    RowHeights = (
      18
      18)
  end
  object Button1: TButton
    Left = 458
    Top = 207
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 340
    Top = 207
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = Button2Click
  end
end

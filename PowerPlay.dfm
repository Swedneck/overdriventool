object Form3: TForm3
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'SoftPowerPlayTable Editor'
  ClientHeight = 480
  ClientWidth = 629
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 412
    Top = 401
    Width = 192
    Height = 13
    Caption = 'Driver must be restarted to take effects'
    Visible = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 41
    Width = 613
    Height = 118
    Caption = 'PP_PhmSoftPowerPlayTable value'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 3
    object Label9: TLabel
      Left = 447
      Top = 57
      Width = 12
      Height = 13
      Caption = '...'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label2: TLabel
      Left = 447
      Top = 83
      Width = 12
      Height = 13
      Caption = '...'
    end
    object Label10: TLabel
      Left = 30
      Top = 98
      Width = 77
      Height = 13
      Caption = 'or open .reg file'
      OnMouseDown = Label10MouseDown
      OnMouseUp = Label10MouseUp
    end
    object RadioButton1: TRadioButton
      Left = 12
      Top = 56
      Width = 153
      Height = 17
      Caption = 'Use existing in the registry'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = RadioButton1Click
    end
    object RadioButton2: TRadioButton
      Left = 12
      Top = 82
      Width = 153
      Height = 17
      Caption = 'Create new using bios file'
      TabOrder = 1
      OnClick = RadioButton2Click
    end
    object Button1: TButton
      Left = 171
      Top = 80
      Width = 118
      Height = 21
      Caption = 'Open bios/reg file...'
      TabOrder = 2
      TabStop = False
      OnClick = Button1Click
    end
    object Button4: TButton
      Left = 171
      Top = 53
      Width = 118
      Height = 21
      Caption = 'Delete'
      TabOrder = 3
      TabStop = False
      OnClick = Button4Click
    end
    object Edit1: TEdit
      Left = 10
      Top = 20
      Width = 570
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 4
    end
    object Edit2: TEdit
      Left = 295
      Top = 80
      Width = 132
      Height = 21
      TabStop = False
      Color = clBtnFace
      TabOrder = 5
    end
    object Button7: TButton
      Left = 295
      Top = 53
      Width = 97
      Height = 21
      Caption = 'Export to .reg file'
      TabOrder = 6
      TabStop = False
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 583
      Top = 20
      Width = 22
      Height = 21
      Caption = '->'
      TabOrder = 7
      OnClick = Button8Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 202
    Top = 167
    Width = 185
    Height = 265
    Caption = 'Memory default'
    TabOrder = 1
    Visible = False
    object Label12: TLabel
      Left = 48
      Top = 16
      Width = 20
      Height = 13
      Caption = 'MHz'
    end
    object Label13: TLabel
      Left = 124
      Top = 16
      Width = 14
      Height = 13
      Caption = 'mV'
    end
    object Mem_P0: TLabel
      Left = 11
      Top = 38
      Width = 12
      Height = 13
      Caption = 'P0'
    end
    object Mem_P1: TLabel
      Left = 11
      Top = 66
      Width = 12
      Height = 13
      Caption = 'P1'
    end
    object Mem_P2: TLabel
      Left = 11
      Top = 94
      Width = 12
      Height = 13
      Caption = 'P2'
    end
    object Mem_P3: TLabel
      Left = 11
      Top = 122
      Width = 12
      Height = 13
      Caption = 'P3'
    end
    object Mem_P4: TLabel
      Left = 11
      Top = 150
      Width = 12
      Height = 13
      Caption = 'P4'
    end
    object Mem_P5: TLabel
      Left = 11
      Top = 178
      Width = 12
      Height = 13
      Caption = 'P5'
    end
    object Mem_P6: TLabel
      Left = 11
      Top = 206
      Width = 12
      Height = 13
      Caption = 'P6'
    end
    object Mem_P7: TLabel
      Left = 11
      Top = 234
      Width = 12
      Height = 13
      Caption = 'P7'
    end
    object Mem_Clock_0: TSpinEdit
      Left = 29
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 0
      Value = 0
    end
    object Mem_Vcc_0: TSpinEdit
      Left = 103
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
      OnChange = Mem_Vcc_0Change
    end
    object Mem_Clock_2: TSpinEdit
      Left = 29
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 4
      Value = 0
    end
    object Mem_Vcc_2: TSpinEdit
      Left = 103
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 5
      Value = 0
      OnChange = Mem_Vcc_2Change
    end
    object Mem_Vcc_5: TSpinEdit
      Left = 103
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 11
      Value = 0
      OnChange = Mem_Vcc_5Change
    end
    object Mem_Clock_6: TSpinEdit
      Left = 29
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 12
      Value = 0
    end
    object Mem_Vcc_6: TSpinEdit
      Left = 103
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 13
      Value = 0
      OnChange = Mem_Vcc_6Change
    end
    object Mem_Clock_1: TSpinEdit
      Left = 29
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
    end
    object Mem_Vcc_1: TSpinEdit
      Left = 103
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 3
      Value = 0
      OnChange = Mem_Vcc_1Change
    end
    object Mem_Vcc_3: TSpinEdit
      Left = 103
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 7
      Value = 0
      OnChange = Mem_Vcc_3Change
    end
    object Mem_Clock_3: TSpinEdit
      Left = 29
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 6
      Value = 0
    end
    object Mem_Clock_4: TSpinEdit
      Left = 29
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 8
      Value = 0
    end
    object Mem_Vcc_4: TSpinEdit
      Left = 103
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 9
      Value = 0
      OnChange = Mem_Vcc_4Change
    end
    object Mem_Clock_5: TSpinEdit
      Left = 29
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 10
      Value = 0
    end
    object Mem_Clock_7: TSpinEdit
      Left = 29
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 14
      Value = 0
    end
    object Mem_Vcc_7: TSpinEdit
      Left = 103
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 15
      Value = 0
      OnChange = Mem_Vcc_7Change
    end
  end
  object GroupBox3: TGroupBox
    Left = 8
    Top = 167
    Width = 185
    Height = 265
    Caption = 'GPU default'
    TabOrder = 0
    Visible = False
    object Label3: TLabel
      Left = 48
      Top = 16
      Width = 20
      Height = 13
      Caption = 'MHz'
    end
    object Label4: TLabel
      Left = 124
      Top = 16
      Width = 14
      Height = 13
      Caption = 'mV'
    end
    object GPU_P0: TLabel
      Left = 11
      Top = 38
      Width = 12
      Height = 13
      Caption = 'P0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object GPU_P1: TLabel
      Left = 11
      Top = 66
      Width = 12
      Height = 13
      Caption = 'P1'
    end
    object GPU_P2: TLabel
      Left = 11
      Top = 94
      Width = 12
      Height = 13
      Caption = 'P2'
    end
    object GPU_P3: TLabel
      Left = 11
      Top = 122
      Width = 12
      Height = 13
      Caption = 'P3'
    end
    object GPU_P4: TLabel
      Left = 11
      Top = 150
      Width = 12
      Height = 13
      Caption = 'P4'
    end
    object GPU_P5: TLabel
      Left = 11
      Top = 178
      Width = 12
      Height = 13
      Caption = 'P5'
    end
    object GPU_P6: TLabel
      Left = 11
      Top = 206
      Width = 12
      Height = 13
      Caption = 'P6'
    end
    object GPU_P7: TLabel
      Left = 11
      Top = 234
      Width = 12
      Height = 13
      Caption = 'P7'
    end
    object GPU_Clock_0: TSpinEdit
      Left = 29
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 0
      Value = 0
    end
    object GPU_Vcc_0: TSpinEdit
      Left = 103
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
      OnChange = GPU_Vcc_0Change
    end
    object GPU_Clock_2: TSpinEdit
      Left = 29
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 4
      Value = 0
    end
    object GPU_Vcc_2: TSpinEdit
      Left = 103
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 5
      Value = 0
      OnChange = GPU_Vcc_2Change
    end
    object GPU_Vcc_5: TSpinEdit
      Left = 103
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 11
      Value = 0
      OnChange = GPU_Vcc_5Change
    end
    object GPU_Clock_6: TSpinEdit
      Left = 29
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 12
      Value = 0
    end
    object GPU_Vcc_6: TSpinEdit
      Left = 103
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 13
      Value = 0
      OnChange = GPU_Vcc_6Change
    end
    object GPU_Clock_1: TSpinEdit
      Left = 29
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
    end
    object GPU_Vcc_1: TSpinEdit
      Left = 103
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 3
      Value = 0
      OnChange = GPU_Vcc_1Change
    end
    object GPU_Vcc_3: TSpinEdit
      Left = 103
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 7
      Value = 0
      OnChange = GPU_Vcc_3Change
    end
    object GPU_Clock_3: TSpinEdit
      Left = 29
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 6
      Value = 0
    end
    object GPU_Clock_4: TSpinEdit
      Left = 29
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 8
      Value = 0
    end
    object GPU_Vcc_4: TSpinEdit
      Left = 103
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 9
      Value = 0
      OnChange = GPU_Vcc_4Change
    end
    object GPU_Clock_5: TSpinEdit
      Left = 29
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 10
      Value = 0
    end
    object GPU_Clock_7: TSpinEdit
      Left = 29
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 14
      Value = 0
    end
    object GPU_Vcc_7: TSpinEdit
      Left = 103
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 5
      MaxValue = 99999
      MinValue = 0
      ParentFont = False
      TabOrder = 15
      Value = 0
      OnChange = GPU_Vcc_7Change
    end
  end
  object GroupBox4: TGroupBox
    Left = 396
    Top = 167
    Width = 225
    Height = 114
    Caption = 'Limits'
    TabOrder = 2
    Visible = False
    object Label5: TLabel
      Left = 12
      Top = 23
      Width = 102
      Height = 13
      Caption = 'Max GPU Freq. (Mhz)'
    end
    object Label6: TLabel
      Left = 12
      Top = 51
      Width = 120
      Height = 13
      Caption = 'Max Memory Freq. (Mhz)'
    end
    object Label7: TLabel
      Left = 12
      Top = 79
      Width = 110
      Height = 13
      Caption = 'Max Power Target (%)'
    end
    object Max_GPU: TSpinEdit
      Left = 140
      Top = 19
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 0
      Value = 0
    end
    object Max_Mem: TSpinEdit
      Left = 140
      Top = 47
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
    end
    object Max_Power: TSpinEdit
      Left = 140
      Top = 75
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
    end
  end
  object Button2: TButton
    Left = 514
    Top = 443
    Width = 90
    Height = 27
    Caption = 'Close'
    TabOrder = 4
    TabStop = False
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 378
    Top = 443
    Width = 90
    Height = 27
    Caption = 'Save'
    TabOrder = 5
    TabStop = False
    Visible = False
    OnClick = Button3Click
  end
  object GroupBox5: TGroupBox
    Left = 8
    Top = 5
    Width = 613
    Height = 30
    Caption = 'GPU'
    TabOrder = 6
    object Label1: TLabel
      Left = 32
      Top = 10
      Width = 16
      Height = 13
      Caption = '....'
    end
  end
  object Button5: TButton
    Left = 412
    Top = 287
    Width = 90
    Height = 25
    Caption = 'Undo'
    TabOrder = 7
    TabStop = False
    Visible = False
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 242
    Top = 443
    Width = 90
    Height = 27
    Caption = 'Save to .reg file'
    TabOrder = 8
    TabStop = False
    OnClick = Button6Click
  end
  object OpenDialog1: TOpenDialog
    Left = 447
    Top = 340
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.reg'
    Filter = '.reg|*.reg'
    Left = 536
    Top = 336
  end
end

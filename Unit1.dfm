object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'OverdriveNTool'
  ClientHeight = 482
  ClientWidth = 598
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object InfoLabel: TLabel
    Left = 194
    Top = 196
    Width = 5
    Height = 19
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label25: TLabel
    Left = 8
    Top = 435
    Width = 88
    Height = 13
    Caption = 'Target Temp. ('#176'C)'
    Visible = False
  end
  object Label23: TLabel
    Left = 8
    Top = 454
    Width = 72
    Height = 13
    Caption = 'Minimum (RPM)'
    Visible = False
  end
  object Label24: TLabel
    Left = 139
    Top = 435
    Width = 76
    Height = 13
    Caption = 'Maximum (RPM)'
    Visible = False
  end
  object Label27: TLabel
    Left = 152
    Top = 460
    Width = 76
    Height = 13
    Caption = 'Max Temp. ('#176'C)'
    Visible = False
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 43
    Width = 185
    Height = 265
    Caption = 'GPU'
    TabOrder = 0
    object Label1: TLabel
      Left = 48
      Top = 16
      Width = 20
      Height = 13
      Caption = 'MHz'
    end
    object Label2: TLabel
      Left = 124
      Top = 16
      Width = 14
      Height = 13
      Caption = 'mV'
    end
    object GPU_P0: TLabel
      Left = 11
      Top = 38
      Width = 12
      Height = 13
      Caption = 'P0'
      OnClick = GPU_P0Click
    end
    object GPU_P1: TLabel
      Left = 11
      Top = 66
      Width = 12
      Height = 13
      Caption = 'P1'
      OnClick = GPU_P1Click
    end
    object GPU_P2: TLabel
      Left = 11
      Top = 94
      Width = 12
      Height = 13
      Caption = 'P2'
      OnClick = GPU_P2Click
    end
    object GPU_P3: TLabel
      Left = 11
      Top = 122
      Width = 12
      Height = 13
      Caption = 'P3'
      OnClick = GPU_P3Click
    end
    object GPU_P4: TLabel
      Left = 11
      Top = 150
      Width = 12
      Height = 13
      Caption = 'P4'
      OnClick = GPU_P4Click
    end
    object GPU_P5: TLabel
      Left = 11
      Top = 178
      Width = 12
      Height = 13
      Caption = 'P5'
      OnClick = GPU_P5Click
    end
    object GPU_P6: TLabel
      Left = 11
      Top = 206
      Width = 12
      Height = 13
      Caption = 'P6'
      OnClick = GPU_P6Click
    end
    object GPU_P7: TLabel
      Left = 11
      Top = 234
      Width = 12
      Height = 13
      Caption = 'P7'
      OnClick = GPU_P7Click
    end
    object GPU_Min_Label: TLabel
      Left = 11
      Top = 147
      Width = 71
      Height = 13
      Caption = 'Minimum (MHz)'
    end
    object GPU_Max_Label: TLabel
      Left = 11
      Top = 175
      Width = 75
      Height = 13
      Caption = 'Maximum (MHz)'
    end
    object GPU_Clock_0: TSpinEdit
      Left = 29
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 0
      Value = 0
      OnChange = GPU_Clock_0Change
    end
    object GPU_Vcc_0: TSpinEdit
      Left = 103
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
      OnChange = GPU_Vcc_0Change
    end
    object GPU_Clock_2: TSpinEdit
      Left = 29
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 4
      Value = 0
      OnChange = GPU_Clock_2Change
    end
    object GPU_Vcc_2: TSpinEdit
      Left = 103
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 5
      Value = 0
      OnChange = GPU_Vcc_2Change
    end
    object GPU_Vcc_5: TSpinEdit
      Left = 103
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 11
      Value = 0
      OnChange = GPU_Vcc_5Change
    end
    object GPU_Clock_6: TSpinEdit
      Left = 29
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 12
      Value = 0
      OnChange = GPU_Clock_6Change
    end
    object GPU_Vcc_6: TSpinEdit
      Left = 103
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 13
      Value = 0
      OnChange = GPU_Vcc_6Change
    end
    object GPU_Clock_1: TSpinEdit
      Left = 29
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
      OnChange = GPU_Clock_1Change
    end
    object GPU_Vcc_1: TSpinEdit
      Left = 103
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 3
      Value = 0
      OnChange = GPU_Vcc_1Change
    end
    object GPU_Vcc_3: TSpinEdit
      Left = 103
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 7
      Value = 0
      OnChange = GPU_Vcc_3Change
    end
    object GPU_Clock_3: TSpinEdit
      Left = 29
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 6
      Value = 0
      OnChange = GPU_Clock_3Change
    end
    object GPU_Clock_4: TSpinEdit
      Left = 29
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 8
      Value = 0
      OnChange = GPU_Clock_4Change
    end
    object GPU_Vcc_4: TSpinEdit
      Left = 103
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 9
      Value = 0
      OnChange = GPU_Vcc_4Change
    end
    object GPU_Clock_5: TSpinEdit
      Left = 29
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 10
      Value = 0
      OnChange = GPU_Clock_5Change
    end
    object GPU_Clock_7: TSpinEdit
      Left = 29
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 14
      Value = 0
      OnChange = GPU_Clock_7Change
    end
    object GPU_Vcc_7: TSpinEdit
      Left = 103
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 15
      Value = 0
      OnChange = GPU_Vcc_7Change
    end
    object GPU_Min: TSpinEdit
      Left = 103
      Top = 142
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 16
      Value = 0
      OnChange = GPU_MinChange
    end
    object GPU_Max: TSpinEdit
      Left = 103
      Top = 170
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 17
      Value = 0
      OnChange = GPU_MaxChange
    end
  end
  object ComboBox1: TComboBox
    Left = 8
    Top = 16
    Width = 485
    Height = 21
    Style = csDropDownList
    Ctl3D = True
    DropDownCount = 13
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    PopupMenu = FriendlyNameMenu
    TabOrder = 1
    TabStop = False
    OnChange = ComboBox1Change
  end
  object GroupBox2: TGroupBox
    Left = 202
    Top = 43
    Width = 185
    Height = 265
    Caption = 'Memory'
    TabOrder = 2
    object Label12: TLabel
      Left = 48
      Top = 16
      Width = 20
      Height = 13
      Caption = 'MHz'
    end
    object Label13: TLabel
      Left = 124
      Top = 16
      Width = 14
      Height = 13
      Caption = 'mV'
    end
    object Mem_P0: TLabel
      Left = 11
      Top = 38
      Width = 12
      Height = 13
      Caption = 'P0'
      OnClick = Mem_P0Click
    end
    object Mem_P1: TLabel
      Left = 11
      Top = 66
      Width = 12
      Height = 13
      Caption = 'P1'
      OnClick = Mem_P1Click
    end
    object Mem_P2: TLabel
      Left = 11
      Top = 94
      Width = 12
      Height = 13
      Caption = 'P2'
      OnClick = Mem_P2Click
    end
    object Mem_P3: TLabel
      Left = 11
      Top = 122
      Width = 12
      Height = 13
      Caption = 'P3'
      OnClick = Mem_P3Click
    end
    object Mem_P4: TLabel
      Left = 11
      Top = 150
      Width = 12
      Height = 13
      Caption = 'P4'
      OnClick = Mem_P4Click
    end
    object Mem_P5: TLabel
      Left = 11
      Top = 178
      Width = 12
      Height = 13
      Caption = 'P5'
      OnClick = Mem_P5Click
    end
    object Mem_P6: TLabel
      Left = 11
      Top = 206
      Width = 12
      Height = 13
      Caption = 'P6'
      OnClick = Mem_P6Click
    end
    object Mem_P7: TLabel
      Left = 11
      Top = 234
      Width = 12
      Height = 13
      Caption = 'P7'
      OnClick = Mem_P7Click
    end
    object Label20: TLabel
      Left = 11
      Top = 232
      Width = 58
      Height = 13
      Caption = 'Timing Level'
    end
    object Mem_Max_Label: TLabel
      Left = 11
      Top = 40
      Width = 75
      Height = 13
      Caption = 'Maximum (MHz)'
    end
    object Mem_Vcc_0: TSpinEdit
      Left = 103
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 1
      Value = 0
      OnChange = Mem_Vcc_0Change
    end
    object Mem_Max: TSpinEdit
      Left = 103
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 17
      Value = 0
      OnChange = Mem_MaxChange
    end
    object Mem_Clock_0: TSpinEdit
      Left = 29
      Top = 35
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 0
      Value = 0
      OnChange = Mem_Clock_0Change
    end
    object Mem_Clock_2: TSpinEdit
      Left = 29
      Top = 91
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 4
      Value = 0
      OnChange = Mem_Clock_2Change
    end
    object Mem_Vcc_2: TSpinEdit
      Left = 103
      Top = 90
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 5
      Value = 0
      OnChange = Mem_Vcc_2Change
    end
    object Mem_Vcc_5: TSpinEdit
      Left = 103
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 11
      Value = 0
      OnChange = Mem_Vcc_5Change
    end
    object Mem_Clock_6: TSpinEdit
      Left = 29
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 12
      Value = 0
      OnChange = Mem_Clock_6Change
    end
    object Mem_Vcc_6: TSpinEdit
      Left = 103
      Top = 203
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 13
      Value = 0
      OnChange = Mem_Vcc_6Change
    end
    object Mem_Clock_1: TSpinEdit
      Left = 29
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 2
      Value = 0
      OnChange = Mem_Clock_1Change
    end
    object Mem_Vcc_1: TSpinEdit
      Left = 103
      Top = 63
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 3
      Value = 0
      OnChange = Mem_Vcc_1Change
    end
    object Mem_Vcc_3: TSpinEdit
      Left = 103
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 7
      Value = 0
      OnChange = Mem_Vcc_3Change
    end
    object Mem_Clock_3: TSpinEdit
      Left = 29
      Top = 119
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 6
      Value = 0
      OnChange = Mem_Clock_3Change
    end
    object Mem_Clock_4: TSpinEdit
      Left = 29
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 8
      Value = 0
      OnChange = Mem_Clock_4Change
    end
    object Mem_Vcc_4: TSpinEdit
      Left = 103
      Top = 147
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 9
      Value = 0
      OnChange = Mem_Vcc_4Change
    end
    object Mem_Clock_5: TSpinEdit
      Left = 29
      Top = 175
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 10
      Value = 0
      OnChange = Mem_Clock_5Change
    end
    object Mem_Clock_7: TSpinEdit
      Left = 29
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 14
      Value = 0
      OnChange = Mem_Clock_7Change
    end
    object Mem_Vcc_7: TSpinEdit
      Left = 103
      Top = 231
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 15
      Value = 0
      OnChange = Mem_Vcc_7Change
    end
    object Mem_TimingBox: TComboBox
      Left = 82
      Top = 229
      Width = 85
      Height = 21
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      OnChange = Mem_TimingBoxChange
      Items.Strings = (
        '0 - Auto'
        '1 - Level 1'
        '2 - Level 2')
    end
  end
  object Button1: TButton
    Left = 305
    Top = 441
    Width = 100
    Height = 30
    Caption = 'Apply'
    TabOrder = 3
    TabStop = False
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 480
    Top = 441
    Width = 100
    Height = 30
    Caption = 'Close'
    TabOrder = 4
    TabStop = False
    OnClick = Button2Click
  end
  object Button4: TButton
    Left = 406
    Top = 280
    Width = 69
    Height = 25
    Caption = 'Reset'
    TabOrder = 5
    TabStop = False
    OnClick = Button4Click
  end
  object GroupBox3: TGroupBox
    Left = 395
    Top = 43
    Width = 195
    Height = 193
    Caption = 'Fan'
    TabOrder = 6
    OnDblClick = GroupBox3DblClick
    object Label26: TLabel
      Left = 12
      Top = 170
      Width = 95
      Height = 13
      Caption = 'Acoustic Limit (MHz)'
    end
    object Label3: TLabel
      Left = 56
      Top = 10
      Width = 12
      Height = 13
      Caption = #176'C'
    end
    object Label11: TLabel
      Left = 132
      Top = 10
      Width = 11
      Height = 13
      Caption = '%'
    end
    object Label14: TLabel
      Left = 19
      Top = 29
      Width = 12
      Height = 13
      Caption = 'P0'
    end
    object Label15: TLabel
      Left = 19
      Top = 54
      Width = 12
      Height = 13
      Caption = 'P1'
    end
    object Label16: TLabel
      Left = 19
      Top = 79
      Width = 12
      Height = 13
      Caption = 'P2'
    end
    object Label17: TLabel
      Left = 19
      Top = 104
      Width = 12
      Height = 13
      Caption = 'P3'
    end
    object Label18: TLabel
      Left = 19
      Top = 129
      Width = 12
      Height = 13
      Caption = 'P4'
    end
    object Label19: TLabel
      Left = 12
      Top = 153
      Width = 46
      Height = 13
      Caption = 'Zero RPM'
    end
    object Fan_Acoustic: TSpinEdit
      Left = 113
      Top = 165
      Width = 68
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = 0
      ParentFont = False
      TabOrder = 11
      Value = 0
      OnChange = Fan_AcousticChange
    end
    object Fan_Perc_2: TSpinEdit
      Left = 111
      Top = 75
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 5
      Value = 0
      OnChange = Fan_Perc_2Change
    end
    object Fan_Temp_0: TSpinEdit
      Left = 37
      Top = 25
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 0
      Value = 0
      OnChange = Fan_Temp_0Change
    end
    object Fan_Perc_0: TSpinEdit
      Left = 111
      Top = 25
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 1
      Value = 0
      OnChange = Fan_Perc_0Change
    end
    object Fan_Perc_1: TSpinEdit
      Left = 111
      Top = 50
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 3
      Value = 0
      OnChange = Fan_Perc_1Change
    end
    object Fan_Temp_1: TSpinEdit
      Left = 37
      Top = 50
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 2
      Value = 0
      OnChange = Fan_Temp_1Change
    end
    object Fan_Temp_2: TSpinEdit
      Left = 37
      Top = 75
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 4
      Value = 0
      OnChange = Fan_Temp_2Change
    end
    object Fan_Perc_3: TSpinEdit
      Left = 111
      Top = 100
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 7
      Value = 0
      OnChange = Fan_Perc_3Change
    end
    object Fan_Temp_3: TSpinEdit
      Left = 37
      Top = 100
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 6
      Value = 0
      OnChange = Fan_Temp_3Change
    end
    object Fan_Temp_4: TSpinEdit
      Left = 37
      Top = 125
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 8
      Value = 0
      OnChange = Fan_Temp_4Change
    end
    object Fan_Perc_4: TSpinEdit
      Left = 111
      Top = 125
      Width = 65
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -1
      ParentFont = False
      TabOrder = 9
      Value = 0
      OnChange = Fan_Perc_4Change
    end
    object ZeroRPMBox: TCheckBox
      Left = 64
      Top = 151
      Width = 38
      Height = 17
      Caption = 'OFF'
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 10
      OnClick = ZeroRPMBoxClick
    end
  end
  object GroupBox4: TGroupBox
    Left = 395
    Top = 236
    Width = 195
    Height = 42
    Caption = 'Power'
    TabOrder = 7
    object Label30: TLabel
      Left = 12
      Top = 16
      Width = 87
      Height = 13
      Caption = 'Power Target (%)'
    end
    object Power_Target: TSpinEdit
      Left = 113
      Top = 11
      Width = 68
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 4
      MaxValue = 9999
      MinValue = -9999
      ParentFont = False
      TabOrder = 0
      Value = 0
      OnChange = Power_TargetChange
    end
  end
  object GroupBox5: TGroupBox
    Left = 8
    Top = 370
    Width = 582
    Height = 55
    Caption = 'Profiles'
    TabOrder = 8
    object ComboBox2: TComboBox
      Left = 11
      Top = 17
      Width = 139
      Height = 24
      Style = csDropDownList
      DoubleBuffered = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentDoubleBuffered = False
      ParentFont = False
      TabOrder = 0
      TabStop = False
    end
    object Button5: TButton
      Left = 512
      Top = 16
      Width = 56
      Height = 25
      Caption = 'Rename'
      TabOrder = 1
      TabStop = False
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 406
      Top = 16
      Width = 45
      Height = 25
      Caption = 'New'
      TabOrder = 2
      TabStop = False
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 457
      Top = 16
      Width = 49
      Height = 25
      Caption = 'Delete'
      TabOrder = 3
      TabStop = False
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 156
      Top = 16
      Width = 70
      Height = 25
      Caption = 'Load'
      TabOrder = 4
      TabStop = False
      OnClick = Button8Click
    end
    object Button9: TButton
      Left = 232
      Top = 16
      Width = 72
      Height = 25
      Caption = 'Save'
      TabOrder = 5
      TabStop = False
      OnClick = Button9Click
    end
    object Button3: TButton
      Left = 345
      Top = 16
      Width = 55
      Height = 25
      Caption = 'Reorder'
      TabOrder = 6
      OnClick = Button3Click
    end
  end
  object Button10: TButton
    Left = 508
    Top = 16
    Width = 82
    Height = 21
    Caption = 'Refresh'
    TabOrder = 9
    TabStop = False
    OnClick = Button10Click
  end
  object GroupBox6: TGroupBox
    Left = 8
    Top = 314
    Width = 582
    Height = 50
    Caption = 'I2C'
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 10
    Visible = False
    object Label4: TLabel
      Left = 167
      Top = 20
      Width = 20
      Height = 13
      Caption = '0mV'
      Visible = False
    end
    object Label6: TLabel
      Left = 11
      Top = 20
      Width = 88
      Height = 13
      Caption = 'Offset (x6.25mV):'
      Visible = False
    end
    object Label5: TLabel
      Left = 242
      Top = 20
      Width = 21
      Height = 13
      Caption = 'LLC:'
      Visible = False
    end
    object Label7: TLabel
      Left = 288
      Top = 20
      Width = 20
      Height = 13
      Caption = 'OFF'
      Visible = False
    end
    object Label8: TLabel
      Left = 256
      Top = 20
      Width = 62
      Height = 13
      Caption = 'Not available'
      Visible = False
    end
    object Label9: TLabel
      Left = 324
      Top = 20
      Width = 56
      Height = 13
      Caption = 'Phase gain:'
      Visible = False
    end
    object Label10: TLabel
      Left = 444
      Top = 20
      Width = 68
      Height = 13
      Caption = 'Current scale:'
      Visible = False
    end
    object I2C_Offset: TSpinEdit
      Left = 102
      Top = 17
      Width = 56
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxValue = 48
      MinValue = -48
      ParentFont = False
      TabOrder = 0
      Value = 0
      Visible = False
      OnChange = I2C_OffsetChange
    end
    object I2C_LLC: TCheckBox
      Left = 269
      Top = 20
      Width = 15
      Height = 15
      TabOrder = 1
      Visible = False
      OnClick = I2C_LLCClick
    end
    object Edit1: TEdit
      Left = 382
      Top = 17
      Width = 56
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 6
      ParentFont = False
      TabOrder = 2
      Visible = False
      OnChange = Edit1Change
      OnKeyPress = Edit1KeyPress
    end
    object Edit2: TEdit
      Left = 514
      Top = 17
      Width = 54
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 2
      ParentFont = False
      TabOrder = 3
      Visible = False
      OnChange = Edit2Change
      OnKeyPress = Edit2KeyPress
    end
  end
  object Fan_Target: TSpinEdit
    Left = 78
    Top = 431
    Width = 68
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 4
    MaxValue = 9999
    MinValue = 0
    ParentFont = False
    TabOrder = 11
    Value = 0
    Visible = False
    OnChange = Fan_TargetChange
  end
  object Fan_Max: TSpinEdit
    Left = 194
    Top = 431
    Width = 68
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 4
    MaxValue = 9999
    MinValue = 0
    ParentFont = False
    TabOrder = 12
    Value = 0
    Visible = False
    OnChange = Fan_MaxChange
  end
  object Fan_Min: TSpinEdit
    Left = 78
    Top = 451
    Width = 68
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 4
    MaxValue = 9999
    MinValue = 0
    ParentFont = False
    TabOrder = 13
    Value = 0
    Visible = False
    OnChange = Fan_MinChange
  end
  object Power_Temp: TSpinEdit
    Left = 213
    Top = 454
    Width = 68
    Height = 23
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 4
    MaxValue = 9999
    MinValue = 0
    ParentFont = False
    TabOrder = 14
    Value = 0
    Visible = False
    OnChange = Power_TempChange
  end
  object FriendlyNameMenu: TPopupMenu
    Left = 312
    Top = 296
    object ChangeFriendlyName1: TMenuItem
      Caption = 'Change friendly name'
      Visible = False
      OnClick = ChangeFriendlyName1Click
    end
  end
  object FanTestMenu: TPopupMenu
    Left = 256
    Top = 296
    object SettoAutoMode1: TMenuItem
      Caption = 'Set to auto mode'
      OnClick = SettoAutoMode1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SetFansto01: TMenuItem
      Caption = 'Set manual to 0%'
      OnClick = SetFansto01Click
    end
    object SetFansto02: TMenuItem
      Caption = 'Set manual to 20%'
      OnClick = SetFansto02Click
    end
    object SetFansto301: TMenuItem
      Caption = 'Set manual to 30%'
      OnClick = SetFansto301Click
    end
    object Setmanualto701: TMenuItem
      Caption = 'Set manual to 70%'
      OnClick = Setmanualto701Click
    end
    object SetFansto1001: TMenuItem
      Caption = 'Set manual to 100%'
      OnClick = SetFansto1001Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Setcustomspeed1: TMenuItem
      Caption = 'Set custom speed'
      OnClick = Setcustomspeed1Click
    end
  end
  object PowerMenu: TPopupMenu
    OnPopup = PowerMenuPopup
    Left = 200
    Top = 296
    object PowerEfficiency1: TMenuItem
      Caption = 'Power Efficiency: OFF'
      OnClick = PowerEfficiency1Click
    end
    object Chill1: TMenuItem
      Caption = 'Chill: OFF'
      OnClick = Chill1Click
    end
  end
end

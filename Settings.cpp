//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "Settings.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
//Form1->Form2_Created=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm2::FormClose(TObject *Sender, TCloseAction &Action)
{
if (Form2->Visible)
 {
   INI.SaveOptions();
   Odnt.ChangeGPUDescription(true);
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button1Click(TObject *Sender)
{
Form2->Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button2Click(TObject *Sender)
{
INI.EraseI2CSection();
Button2->Enabled=false;
}
//---------------------------------------------------------------------------

void __fastcall TForm2::CheckBox10Click(TObject *Sender)
{
if (CheckBox10->Checked)
 {
   CheckBox9->Enabled=true;
   CheckBox12->Enabled=true;
   Label6->Enabled=true;
   if (CheckBox9->Checked) CheckBox11->Enabled=true;
   if (CheckBox12->Checked) SpinEdit1->Enabled=true;
 } else
    {
      CheckBox9->Enabled=false;
      CheckBox11->Enabled=false;
      CheckBox12->Enabled=false;
      Label6->Enabled=false;
      SpinEdit1->Enabled=false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button3Click(TObject *Sender)
{
INI.MakeIniBackup(true);
}
//---------------------------------------------------------------------------
void __fastcall TForm2::CheckBox12Click(TObject *Sender)
{
if (CheckBox12->Checked)
 {
   if (CheckBox10->Checked) SpinEdit1->Enabled=true;
 } else
    {
      SpinEdit1->Enabled=false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm2::CheckBox9Click(TObject *Sender)
{
if (CheckBox9->Checked)
 {
   if (CheckBox10->Checked) CheckBox11->Enabled=true;
 } else
    {
      CheckBox11->Enabled=false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Label5MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(CheckBox8->Handle, WM_LBUTTONDOWN, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Label5MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(CheckBox8->Handle, WM_LBUTTONUP, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Label9MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(CheckBox10->Handle, WM_LBUTTONDOWN, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Label9MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(CheckBox10->Handle, WM_LBUTTONUP, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Label6MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(CheckBox9->Handle, WM_LBUTTONDOWN, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Label6MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(CheckBox9->Handle, WM_LBUTTONUP, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------


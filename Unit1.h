//---------------------------------------------------------------------------
#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
//===============================
//#define WINVER 0x06010000
//#define _WIN32_WINNT 0x0601
//===============================
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Samples.Spin.hpp>
#include "adl_sdk/adl_sdk.h"
#include <psapi.h>
#include <System.IniFiles.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Menus.hpp>
#include <Setupapi.h>
#include <io.h>          // _open_osfhandle, dup2
#include <cstdio>        //printf

#define fileAnything 0xFFFFFFFF

int  VerifyADL_Library();
bool CheckIfDriverInstalled();
int  FindProfileByName(String Name);
bool FindGPUs();


void blad(String msg, int ErrCode, bool WindowsError);
bool InitializeADL();
void DeinitializeADL();
void Reset(int GpuInd, bool CmdMsg, bool from_cmd);


void SetFansPercentage(int GpuInd, int Value);
bool SetGetChillPowerEfficiency(bool Chill, int GpuInd, bool silent, int NewValue, bool* CurrentValue);

//------------------------------------------------------------------------------
//Helper structs for OverdriveN ExtSettings
//Get Default:
typedef struct _ADLODNCurveInfoPoint
{
 ADLODNExtSingleInitSetting Temp;
 ADLODNExtSingleInitSetting Percentage;
} ADLODNCurveInfoPoint;

typedef struct _ADLODNExtSettingsInfo
{
 ADLODNExtSingleInitSetting MemTimingLevel;
 ADLODNExtSingleInitSetting ZeroRPM;
 ADLODNExtSingleInitSetting AutoUVEngine;
 ADLODNExtSingleInitSetting AutoOCEngine;
 ADLODNExtSingleInitSetting AutoOCMemory;
 ADLODNCurveInfoPoint Point[5];
} ADLODNExtSettingsInfo;
//Get/Set Current:
typedef struct _ADLODNFanCurvePoint
{
 int Temp;
 int Percentage;
} ADLODNFanCurvePoint;

typedef struct _ADLODNExtSettings
{
 int MemTimingLevel;
 int ZeroRPM;
 int AutoUVEngine;
 int AutoOCEngine;
 int AutoOCMemory;
 ADLODNFanCurvePoint Point[5];
} ADLODNExtSettings;
//------------------------------------------------------------------------------
//Helper structs for Overdrive8
typedef struct _Odn8PStateSettings {
 int Clock;
 int Voltage;
} Odn8PStateSettings;

typedef struct _Odn8Settings {
 int GPU_Max;
 int GPU_Min;
 Odn8PStateSettings GPU_P[3];
 int Mem_Max;
 int Power_Target;
 int Fan_Min;
 int Fan_Acoustic;
 int Fan_Target;
 int Power_Temp;
 ADLODNExtSettings ExtSettings;
} Odn8Settings;
//------------------------------------------------------------------------------
class OverdriveN
 {
  private:
   bool CheckIfLimitsExceed(int GpuInd, ADLODNPerformanceLevelsX2 *pLevels, ADLODNFanControl* fan, ADLODNPowerLimitSetting* power, String* msg, bool SetToGpu);
   bool SetClocks(int GpuInd, bool SetToGpu, ADLODNPerformanceLevelsX2 *pLevels, const int size, String* errmsg, bool CmdMsg, bool silent);
  public:
   bool SetValues_GPUMem(int GpuInd, bool SetToGpu, ADLODNPerformanceLevelsX2 *pLevels, const int size, String errmsg, bool CmdMsg);
   bool SetValues_Fan(int GpuInd, bool ReplaceObsolete, int Fan_Min, int Fan_Max, int Fan_Target, int Fan_Acoustic, String errmsg, bool CmdMsg);
   bool SetValues_Power(int GpuInd, bool ReplaceObsolete, int Power_Temp, int Power_Target, String errmsg, bool CmdMsg);
   bool ExtSettings_Get(int GpuInd, ADLODNExtSettingsInfo *ExtInfo, ADLODNExtSettings *ExtCurrent, bool CmdMsg);
   bool ExtSettings_Set(int GpuInd, ADLODNExtSettings *ExtCurrent, bool CmdMsg);
   bool ExtSettings_Reset(int GpuInd, bool CmdMsg);

 } OverdriveN;
//------------------------------------------------------------------------------
class Overdrive8
{
  private:
   bool Od8Set(int GpuInd, bool Reset, Odn8Settings* Current, bool CmdMsg);
  public:
   bool GetValues(int GpuInd, ADLOD8SingleInitSetting* Default, Odn8Settings* Current, bool CmdMsg);
   bool SetValues(int GpuInd, Odn8Settings* Current, bool CmdMsg);
   bool Reset(int GpuInd, bool CmdMsg);
 //  void Test(int GpuInd);
} Overdrive8;
//------------------------------------------------------------------------------
class Cmd
 {
  private:
   bool CosoleAttached;
  public:
   //variable initialization:
   Cmd():CosoleAttached(false){ };   //czy do��czono ju� konsol� cmd.exe
   //=========================
   void InitializeI2C();
   bool AttachToConsole();
   void ErrorInfoShow(const String msg, bool console, int ErrCode, int typ, bool WindowsError);
   bool ReadCurrent(int GpuInd, bool CmdMsg);
   bool SetProfileToGPU(int prof, int GpuInd, bool CmdMsg);
   bool CompareProfileWithGPU(int prof,int GpuInd, String atype, bool CmdMsg);
   bool IsHighGPUCount();
   int  GetGPULength();
   bool SetCustomToGPU(String GpuStr, String cm, const String OryginalCommand, bool CmdMsg);
   void PrintCurrentValues();
   bool RestartGPU(int GpuInd, bool CmdMsg, bool WaitForEnd);
   void ShowGUI();
   bool CorrectGPU(int GpuInd);
   bool SupportedGPU(int GpuInd);
   int FindProfileByName(String Name);
 } Cmd;
//------------------------------------------------------------------------------
class I2C_
 {
  private:
   byte ConvertVoltageWrite(int GpuInd, int value);
   int ConvertVoltageRead(int GpuInd, int value);
   bool FindDevice(int GpuInd, int Line, int Address, int typ);
   int RegisterRead(int GpuInd,int iSize, int iLine, int iAddress, int iOffset);
   int RegisterWrite(int GpuInd,int iSize, int iLine, int iAddress, int iOffset, int Value);
  public:
   bool Linescanning;    //czy trwa skanowanie
   bool DoFullScan;
   bool UsingCommandLine;
   bool Scanned;         //przeprowadzono skanowanie
   void Scan(bool FullScan);
   void WriteSettings(int GpuInd, bool reset,int offset,int llc, String phasegain, String CurrentScale);
   void ReadSettings(int GpuInd,bool ShowOnForm);
 } I2C_;
//------------------------------------------------------------------------------
class INI
 {
  private:
   static String inifile;
   static String backupinifile;
   void FlushFileBuffer(String ini_name);

  public:
   void EraseI2CSection();
   int LoadProfiles();
   void SaveOptions();
   void LoadOptions(bool from_cmd);
   int SaveProfiles();
   void SetIniFileName();
   void RestoreFromBackup(bool from_cmd);
   inline void SetMainFormPosition();
   void SaveMainFormPosition();
   void MakeIniBackup(bool DisplayConfirmMsg);
   int LoadI2CSettings();
   int SaveI2CSettings();
   void SaveFanDeactivated();
 } INI;
//------------------------------------------------------------------------------
class Odnt
 {
  private:
  public:
   bool ReadGPUFriendlyName(bool fromsettings);
   bool ChangeGPUFriendlyName(int GpuInd, String NewName);
   void ChangeMultiGpuFriendlyNames();
   void ChangeGPUDescription(bool FromSettings);
   bool CheckIfAdmin();
   void ProfilesCompareAtStartup();
   String FormatGPUDisplayName(int GpuInd, bool IncludeFrName);
 } Odnt;
//------------------------------------------------------------------------------

typedef struct _I2CDataB {
   String Name;
   byte IdentSize;        //1 lub 2 bajty odczytu identyfikatora
   byte FixedLine;             // sta�a linia i address lub skanowanie gdy NULL
   byte FixedAddress;          // sta�y linia i address lub skanowanie gdy NULL
   byte IdentOffset1;
   WORD IdentData1;
   byte IdentOffset2;
   WORD IdentData2;
   WORD IdentData21;
   bool ConvertVoltage;      // false when normal signed byte, true when bit7 is used for negative voltage values
   byte VoltageOffset1;
   byte VoltageAddRegOffset;
   byte VoltageAddRegData;
   bool LLC_Enabled;
   byte LLCOffset;
   byte LLCdataON;
   byte LLCdataOFF;     //domy�lne
   bool Phase_Enabled;
   byte PhaseGainOffset1;
   byte PhaseGainOffset2;
   byte PhaseGainOffset3;
   bool Scale_Enabled;
   byte CurrentScaleOffset;
} I2CDataB;


const int I2cDBLength=2;

const I2CDataB I2cDB[I2cDBLength]={
  {"IR3567B", 1, NULL, NULL, 0x92, 0x43, 0x38, 0x01, 0x81, false, 0x8D, NULL, NULL, true, 0x38, 0x01, 0x81, true,  0x1E, 0x1F, 0x20, true,  0x4D }
, {"up9505",  1, NULL, NULL, 0x4A, 0x29, 0x49, 0x02, 0x03, true,  0x0C, 0x27, 0x1F, true, 0x28, 0x8C, 0x04, false, NULL, NULL, NULL, false, NULL }
 };


// identyfikator 2 bajty, offset, llc - 1 bajt
//sta�a kolejno��/nie zmienia� : 0-IR3567B, 1-up9505

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TSpinEdit *GPU_Clock_0;
	TGroupBox *GroupBox1;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *GPU_P0;
	TSpinEdit *GPU_Vcc_0;
	TComboBox *ComboBox1;
	TSpinEdit *GPU_Vcc_3;
	TSpinEdit *GPU_Clock_3;
	TLabel *GPU_P3;
	TLabel *GPU_P1;
	TSpinEdit *GPU_Clock_2;
	TSpinEdit *GPU_Vcc_2;
	TLabel *GPU_P2;
	TSpinEdit *GPU_Clock_4;
	TSpinEdit *GPU_Vcc_4;
	TSpinEdit *GPU_Clock_5;
	TSpinEdit *GPU_Vcc_5;
	TSpinEdit *GPU_Clock_6;
	TSpinEdit *GPU_Vcc_6;
	TSpinEdit *GPU_Clock_1;
	TSpinEdit *GPU_Vcc_1;
	TLabel *GPU_P4;
	TLabel *GPU_P5;
	TLabel *GPU_P6;
	TLabel *GPU_P7;
	TSpinEdit *GPU_Clock_7;
	TSpinEdit *GPU_Vcc_7;
	TGroupBox *GroupBox2;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Mem_P0;
	TLabel *Mem_P1;
	TLabel *Mem_P2;
	TLabel *Mem_P3;
	TLabel *Mem_P4;
	TLabel *Mem_P5;
	TLabel *Mem_P6;
	TLabel *Mem_P7;
	TSpinEdit *Mem_Clock_0;
	TSpinEdit *Mem_Vcc_0;
	TSpinEdit *Mem_Clock_2;
	TSpinEdit *Mem_Vcc_2;
	TSpinEdit *Mem_Vcc_5;
	TSpinEdit *Mem_Clock_6;
	TSpinEdit *Mem_Vcc_6;
	TSpinEdit *Mem_Clock_1;
	TSpinEdit *Mem_Vcc_1;
	TSpinEdit *Mem_Vcc_3;
	TSpinEdit *Mem_Clock_3;
	TSpinEdit *Mem_Clock_4;
	TSpinEdit *Mem_Vcc_4;
	TSpinEdit *Mem_Clock_5;
	TSpinEdit *Mem_Clock_7;
	TSpinEdit *Mem_Vcc_7;
	TButton *Button1;
	TButton *Button2;
	TButton *Button4;
	TGroupBox *GroupBox3;
	TLabel *Label23;
	TSpinEdit *Fan_Min;
	TLabel *Label24;
	TSpinEdit *Fan_Max;
	TLabel *Label25;
	TSpinEdit *Fan_Target;
	TGroupBox *GroupBox4;
	TLabel *Label27;
	TLabel *Label30;
	TSpinEdit *Power_Temp;
	TSpinEdit *Power_Target;
	TGroupBox *GroupBox5;
	TComboBox *ComboBox2;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TLabel *InfoLabel;
	TLabel *Label26;
	TSpinEdit *Fan_Acoustic;
	TGroupBox *GroupBox6;
	TSpinEdit *I2C_Offset;
	TLabel *Label4;
	TLabel *Label6;
	TLabel *Label5;
	TCheckBox *I2C_LLC;
	TLabel *Label7;
	TLabel *Label8;
	TEdit *Edit1;
	TEdit *Edit2;
	TLabel *Label9;
	TLabel *Label10;
	TPopupMenu *FriendlyNameMenu;
	TMenuItem *ChangeFriendlyName1;
	TPopupMenu *FanTestMenu;
	TMenuItem *SetFansto01;
	TMenuItem *SetFansto02;
	TMenuItem *SetFansto1001;
	TMenuItem *SetFansto301;
	TMenuItem *SettoAutoMode1;
	TMenuItem *N1;
	TMenuItem *Setmanualto701;
	TMenuItem *N2;
	TMenuItem *Setcustomspeed1;
	TPopupMenu *PowerMenu;
	TMenuItem *PowerEfficiency1;
	TMenuItem *Chill1;
	TSpinEdit *Fan_Perc_2;
	TLabel *Label3;
	TLabel *Label11;
	TLabel *Label14;
	TSpinEdit *Fan_Temp_0;
	TSpinEdit *Fan_Perc_0;
	TSpinEdit *Fan_Perc_1;
	TSpinEdit *Fan_Temp_1;
	TLabel *Label15;
	TLabel *Label16;
	TSpinEdit *Fan_Temp_2;
	TSpinEdit *Fan_Perc_3;
	TSpinEdit *Fan_Temp_3;
	TLabel *Label17;
	TLabel *Label18;
	TSpinEdit *Fan_Temp_4;
	TSpinEdit *Fan_Perc_4;
	TLabel *Label19;
	TCheckBox *ZeroRPMBox;
	TLabel *Label20;
	TComboBox *Mem_TimingBox;
	TSpinEdit *GPU_Min;
	TLabel *GPU_Min_Label;
	TSpinEdit *GPU_Max;
	TLabel *GPU_Max_Label;
	TSpinEdit *Mem_Max;
	TLabel *Mem_Max_Label;
	TButton *Button3;
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall GPU_Clock_7Change(TObject *Sender);
	void __fastcall GPU_Clock_6Change(TObject *Sender);
	void __fastcall GPU_Clock_5Change(TObject *Sender);
	void __fastcall GPU_Clock_4Change(TObject *Sender);
	void __fastcall GPU_Clock_3Change(TObject *Sender);
	void __fastcall GPU_Clock_2Change(TObject *Sender);
	void __fastcall GPU_Clock_1Change(TObject *Sender);
	void __fastcall GPU_Clock_0Change(TObject *Sender);
	void __fastcall GPU_Vcc_0Change(TObject *Sender);
	void __fastcall GPU_Vcc_1Change(TObject *Sender);
	void __fastcall GPU_Vcc_2Change(TObject *Sender);
	void __fastcall GPU_Vcc_3Change(TObject *Sender);
	void __fastcall GPU_Vcc_4Change(TObject *Sender);
	void __fastcall GPU_Vcc_5Change(TObject *Sender);
	void __fastcall GPU_Vcc_6Change(TObject *Sender);
	void __fastcall GPU_Vcc_7Change(TObject *Sender);
	void __fastcall Mem_Clock_0Change(TObject *Sender);
	void __fastcall Mem_Clock_1Change(TObject *Sender);
	void __fastcall Mem_Clock_2Change(TObject *Sender);
	void __fastcall Mem_Clock_3Change(TObject *Sender);
	void __fastcall Mem_Clock_4Change(TObject *Sender);
	void __fastcall Mem_Clock_5Change(TObject *Sender);
	void __fastcall Mem_Clock_6Change(TObject *Sender);
	void __fastcall Mem_Clock_7Change(TObject *Sender);
	void __fastcall Mem_Vcc_0Change(TObject *Sender);
	void __fastcall Mem_Vcc_1Change(TObject *Sender);
	void __fastcall Mem_Vcc_2Change(TObject *Sender);
	void __fastcall Mem_Vcc_3Change(TObject *Sender);
	void __fastcall Mem_Vcc_4Change(TObject *Sender);
	void __fastcall Mem_Vcc_5Change(TObject *Sender);
	void __fastcall Mem_Vcc_6Change(TObject *Sender);
	void __fastcall Mem_Vcc_7Change(TObject *Sender);
	void __fastcall Fan_MinChange(TObject *Sender);
	void __fastcall Fan_MaxChange(TObject *Sender);
	void __fastcall Fan_TargetChange(TObject *Sender);
	void __fastcall Fan_AcousticChange(TObject *Sender);
	void __fastcall Power_TempChange(TObject *Sender);
	void __fastcall Power_TargetChange(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall I2C_OffsetChange(TObject *Sender);
	void __fastcall I2C_LLCClick(TObject *Sender);
	void __fastcall GPU_P0Click(TObject *Sender);
	void __fastcall GPU_P1Click(TObject *Sender);
	void __fastcall GPU_P2Click(TObject *Sender);
	void __fastcall GPU_P3Click(TObject *Sender);
	void __fastcall GPU_P4Click(TObject *Sender);
	void __fastcall GPU_P5Click(TObject *Sender);
	void __fastcall GPU_P6Click(TObject *Sender);
	void __fastcall GPU_P7Click(TObject *Sender);
	void __fastcall Mem_P0Click(TObject *Sender);
	void __fastcall Mem_P1Click(TObject *Sender);
	void __fastcall Mem_P2Click(TObject *Sender);
	void __fastcall Mem_P3Click(TObject *Sender);
	void __fastcall Mem_P4Click(TObject *Sender);
	void __fastcall Mem_P5Click(TObject *Sender);
	void __fastcall Mem_P6Click(TObject *Sender);
	void __fastcall Mem_P7Click(TObject *Sender);
	void __fastcall Edit1KeyPress(TObject *Sender, System::WideChar &Key);
	void __fastcall Edit2KeyPress(TObject *Sender, System::WideChar &Key);
	void __fastcall Edit1Change(TObject *Sender);
	void __fastcall Edit2Change(TObject *Sender);
	void __fastcall GroupBox3DblClick(TObject *Sender);
	void __fastcall ChangeFriendlyName1Click(TObject *Sender);
	void __fastcall SettoAutoMode1Click(TObject *Sender);
	void __fastcall SetFansto01Click(TObject *Sender);
	void __fastcall SetFansto02Click(TObject *Sender);
	void __fastcall SetFansto301Click(TObject *Sender);
	void __fastcall SetFansto1001Click(TObject *Sender);
	void __fastcall Setmanualto701Click(TObject *Sender);
	void __fastcall Setcustomspeed1Click(TObject *Sender);
	void __fastcall PowerMenuPopup(TObject *Sender);
	void __fastcall PowerEfficiency1Click(TObject *Sender);
        void __fastcall PowerEfficiency1Chill1DisplayStatus(bool Chill, bool status);
	void __fastcall Chill1Click(TObject *Sender);
	void __fastcall Fan_Temp_0Change(TObject *Sender);
	void __fastcall Fan_Temp_1Change(TObject *Sender);
	void __fastcall Fan_Temp_2Change(TObject *Sender);
	void __fastcall Fan_Temp_3Change(TObject *Sender);
	void __fastcall Fan_Temp_4Change(TObject *Sender);
	void __fastcall Fan_Perc_0Change(TObject *Sender);
	void __fastcall Fan_Perc_1Change(TObject *Sender);
	void __fastcall Fan_Perc_2Change(TObject *Sender);
	void __fastcall Fan_Perc_3Change(TObject *Sender);
	void __fastcall Fan_Perc_4Change(TObject *Sender);
	void __fastcall ZeroRPMBoxClick(TObject *Sender);
	void __fastcall Mem_TimingBoxChange(TObject *Sender);
	void __fastcall GPU_MinChange(TObject *Sender);
	void __fastcall GPU_MaxChange(TObject *Sender);
	void __fastcall Mem_MaxChange(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
        void __fastcall ReorderProfiles();



private:
 MESSAGE void __fastcall wmSysCommand(TMessage& Message);
// MESSAGE void __fastcall wmEndMoveForm(TMessage& Message);

 BEGIN_MESSAGE_MAP
    VCL_MESSAGE_HANDLER(WM_SYSCOMMAND, TMessage, wmSysCommand)
 //   VCL_MESSAGE_HANDLER(WM_EXITSIZEMOVE, TMessage, wmEndMoveForm)
  END_MESSAGE_MAP( TForm )




private:	// User declarations


public:		// User declarations
       	__fastcall TForm1(TComponent* Owner);
        void __fastcall ShowHideControls(int GpuInd);
        void __fastcall ShowHideI2CControls(int GpuInd);
        void __fastcall ReadCurrent(int GpuInd);
        void __fastcall SetCurrent(int GpuInd);
        bool __fastcall CurrentToProfile(int i,int GpuInd);
        void __fastcall ProfileToCurrent(int i,int GpuInd);
        void __fastcall ValueChange(Byte typ,bool enabled,int value,TSpinEdit* s);
        void __fastcall EnableDisablePState(bool typ,TLabel* L,TSpinEdit* S1,TSpinEdit* S2);
        void __fastcall EnableDisableFanSection(bool enabled);
        void __fastcall OpenPowerPlayEditor();
        void __fastcall SetBoxesNotVisible();
        inline void __fastcall AddSystemMemuEntries();
        void __fastcall ShowHideInfoLabel(bool visible, int typ);
        bool Form4_Created; //ChangeFriendlyName
        bool Form5_Created; //About

     //   bool Form2_Created; //Settings
      //  bool Form3_Created; //PowerPlay
        double DpiMultiplerPix;
        int StartupPosLeft;
        int StartupPosTop;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif


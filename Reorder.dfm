object Form5: TForm5
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Reorder profiles'
  ClientHeight = 349
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object OK: TButton
    Left = 246
    Top = 308
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 0
    OnClick = OKClick
  end
  object Cancel: TButton
    Left = 106
    Top = 308
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = CancelClick
  end
  object ListView1: TListView
    Left = 16
    Top = 24
    Width = 245
    Height = 265
    Columns = <
      item
        AutoSize = True
      end>
    DragMode = dmAutomatic
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    TabOrder = 2
    ViewStyle = vsReport
    OnDragDrop = ListView1DragDrop
    OnDragOver = ListView1DragOver
  end
  object Button1: TButton
    Left = 273
    Top = 104
    Width = 54
    Height = 25
    Caption = 'UP'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 273
    Top = 143
    Width = 54
    Height = 25
    Caption = 'Down'
    TabOrder = 4
    OnClick = Button2Click
  end
end

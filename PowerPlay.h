//---------------------------------------------------------------------------

#ifndef PowerPlayH
#define PowerPlayH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Samples.Spin.hpp>
#include <Vcl.Dialogs.hpp>
#include <System.Win.Registry.hpp>
#include <System.IOUtils.hpp>

//---------------------------------------------------------------------------
void InitializePowerPlay(String DriverPath, String GpuStr, int MinVoltage, int MaxVoltage, String DevId);
int LoadPPTableFromRegistry(TMemoryStream *ms);
int DeterminePPTableRevision(TMemoryStream *ms);
void SetConstants(int typ,TMemoryStream* ms);
void ReadValuesFromPPTable(TMemoryStream* ms);
String PPTableTypeToDescription(byte typ);
bool ppRestartGpuUsingExternalExe();
bool ppRestartGPU(String DevID);
void ppRestartGPUPrompt();

//---------------------------------------------------------------------------
class TCustomSpinEdit;

class TForm3 : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TGroupBox *GroupBox1;
	TRadioButton *RadioButton1;
	TRadioButton *RadioButton2;
	TButton *Button1;
	TGroupBox *GroupBox2;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Mem_P0;
	TLabel *Mem_P1;
	TLabel *Mem_P2;
	TLabel *Mem_P3;
	TLabel *Mem_P4;
	TLabel *Mem_P5;
	TLabel *Mem_P6;
	TLabel *Mem_P7;
	TSpinEdit *Mem_Clock_0;
	TSpinEdit *Mem_Vcc_0;
	TSpinEdit *Mem_Clock_2;
	TSpinEdit *Mem_Vcc_2;
	TSpinEdit *Mem_Vcc_5;
	TSpinEdit *Mem_Clock_6;
	TSpinEdit *Mem_Vcc_6;
	TSpinEdit *Mem_Clock_1;
	TSpinEdit *Mem_Vcc_1;
	TSpinEdit *Mem_Vcc_3;
	TSpinEdit *Mem_Clock_3;
	TSpinEdit *Mem_Clock_4;
	TSpinEdit *Mem_Vcc_4;
	TSpinEdit *Mem_Clock_5;
	TSpinEdit *Mem_Clock_7;
	TSpinEdit *Mem_Vcc_7;
	TGroupBox *GroupBox3;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *GPU_P0;
	TLabel *GPU_P1;
	TLabel *GPU_P2;
	TLabel *GPU_P3;
	TLabel *GPU_P4;
	TLabel *GPU_P5;
	TLabel *GPU_P6;
	TLabel *GPU_P7;
	TSpinEdit *GPU_Clock_0;
	TSpinEdit *GPU_Vcc_0;
	TSpinEdit *GPU_Clock_2;
	TSpinEdit *GPU_Vcc_2;
	TSpinEdit *GPU_Vcc_5;
	TSpinEdit *GPU_Clock_6;
	TSpinEdit *GPU_Vcc_6;
	TSpinEdit *GPU_Clock_1;
	TSpinEdit *GPU_Vcc_1;
	TSpinEdit *GPU_Vcc_3;
	TSpinEdit *GPU_Clock_3;
	TSpinEdit *GPU_Clock_4;
	TSpinEdit *GPU_Vcc_4;
	TSpinEdit *GPU_Clock_5;
	TSpinEdit *GPU_Clock_7;
	TSpinEdit *GPU_Vcc_7;
	TGroupBox *GroupBox4;
	TLabel *Label5;
	TLabel *Label6;
	TLabel *Label7;
	TSpinEdit *Max_GPU;
	TSpinEdit *Max_Mem;
	TSpinEdit *Max_Power;
	TButton *Button2;
	TButton *Button3;
	TLabel *Label8;
	TEdit *Edit1;
	TButton *Button4;
	TLabel *Label9;
	TGroupBox *GroupBox5;
	TOpenDialog *OpenDialog1;
	TButton *Button5;
	TEdit *Edit2;
	TLabel *Label2;
	TButton *Button6;
	TButton *Button7;
	TSaveDialog *SaveDialog1;
	TLabel *Label10;
	TButton *Button8;
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall GPU_Vcc_0Change(TObject *Sender);
	void __fastcall GPU_Vcc_1Change(TObject *Sender);
	void __fastcall GPU_Vcc_2Change(TObject *Sender);
	void __fastcall GPU_Vcc_3Change(TObject *Sender);
	void __fastcall GPU_Vcc_4Change(TObject *Sender);
	void __fastcall GPU_Vcc_5Change(TObject *Sender);
	void __fastcall GPU_Vcc_6Change(TObject *Sender);
	void __fastcall GPU_Vcc_7Change(TObject *Sender);
	void __fastcall Mem_Vcc_0Change(TObject *Sender);
	void __fastcall Mem_Vcc_1Change(TObject *Sender);
	void __fastcall Mem_Vcc_2Change(TObject *Sender);
	void __fastcall Mem_Vcc_3Change(TObject *Sender);
	void __fastcall Mem_Vcc_4Change(TObject *Sender);
	void __fastcall Mem_Vcc_5Change(TObject *Sender);
	void __fastcall Mem_Vcc_6Change(TObject *Sender);
	void __fastcall Mem_Vcc_7Change(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall RadioButton1Click(TObject *Sender);
	void __fastcall RadioButton2Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Label10MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Label10MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y);
	void __fastcall Button8Click(TObject *Sender);

private:	// User declarations
public:		// User declarations
	__fastcall TForm3(TComponent* Owner);
        void __fastcall ShowHidePPControls(bool Found);
        void __fastcall ReadValues(TMemoryStream* ms);
        void __fastcall WriteValues(TMemoryStream* ms);
        void __fastcall GPUChangeShared(int ps, int value);
        void __fastcall MemChangeShared(int ps, int value);
        bool __fastcall TForm3::ValidateEnteredValues(bool CheckHighLowVoltages);
        void __fastcall EnableDisableRegistryLine(bool enable, String str);


};
//---------------------------------------------------------------------------
extern PACKAGE TForm3 *Form3;
//---------------------------------------------------------------------------
#endif





//############################ DEFINES BEGIN ###################################

#define MINIMUM_UNSUPPORTED_TABLE_REVISION_AVAILABLE_TO_EXTRACT_ONLY   5
#define MAXIMUM_UNSUPPORTED_TABLE_REVISION_AVAILABLE_TO_EXTRACT_ONLY   55

#define OFFSET_TO_POINTER_TO_ATOM_ROM_HEADER  0x00000048      //  Define offset to location of ROM header.

//bajty odr�niania wersji tabel
//=====================================================
#define ATOM_Tonga_TABLE_REVISION_TONGA              7           //1bajt // Polaris, Fiji
#define ATOM_Vega10_TABLE_REVISION_VEGA10            8           //1bajt  // Vega

#define SCLK_Dep_TABLE_REVISION_Polaris              1           //1bajt // Polaris
#define SCLK_Dep_TABLE_REVISION_Fiji                 0           //1bajt  //  Fiji

#define Fan_TABLE_REVISION_Polaris                   9           //1bajt //  Polaris
#define Fan_TABLE_REVISION_Fiji                      8           //1bajt  // Fiji

//=====================================================

#define POWERPLAYTABLE_8_TableSize                   0x5C        //2bajty // Vega
#define POWERPLAYTABLE_7_TableSize                   0x4D        //2bajty // Polaris, Fiji
#define POWERPLAYTABLE_TableSize_Offset              5           //odczyt rozmiaru
#define POWERPLAYTABLE_TableRevision_Offset          2           //odczyt rewizji

//=====================================================

#define POWERPLAYTABLE_7_MaxODEngineClock_Offset     23
#define POWERPLAYTABLE_7_MaxODMemoryClock_Offset     27
#define POWERPLAYTABLE_7_PowerControlLimit_Offset    31
#define POWERPLAYTABLE_8_MaxODEngineClock_Offset     21
#define POWERPLAYTABLE_8_MaxODMemoryClock_Offset     25
#define POWERPLAYTABLE_8_PowerControlLimit_Offset    29

#define POWERPLAYTABLE_7_MclkDependencyTable_Offset_to_pointer    43
#define POWERPLAYTABLE_7_SclkDependencyTable_Offset_to_pointer    45
#define POWERPLAYTABLE_7_VddcLookupTable_Offset_to_pointer        47
#define POWERPLAYTABLE_7_FanTableTable_Offset_to_pointer          37

#define POWERPLAYTABLE_8_GFXclkDependencyTable_Offset_to_pointer  58
#define POWERPLAYTABLE_8_MclkDependencyTable_Offset_to_pointer    56
#define POWERPLAYTABLE_8_VddcLookupTable_Offset_to_pointer        62
#define POWERPLAYTABLE_8_VddmemLookupTable_Offset_to_pointer      64



#define MCLK_Dependency_Record_7_Size                13
#define SCLK_Dependency_Record_7_Polaris_Size        15
#define SCLK_Dependency_Record_7_Fiji_Size           11
#define Voltage_Lookup_Record_7_Size                 8
#define Voltage_Lookup_Record_8_Size                 2

#define MCLK_Dependency_Record_8_Size                7
#define GFXclk_Dependency_Record_V1_8_Size           9
#define GFXclk_Dependency_Record_V2_8_Size           13
#define GFXclk_Dependency_Record_V1_8_Ident          0
#define GFXclk_Dependency_Record_V2_8_Ident          1



#define AllTables_NumEntries_Offset                  1       //odczyt ilo�ci record�w
#define AllTables_Record_Start_Offset                2       //gdzie zaczynaj� si� recordy

#define SCLK_Record_7_ucVddInd_Offset                0       //1 bajt do odczytu
#define SCLK_Record_7_ulSclk_Offset                  3       //4 bajty do odczytu
#define GFXclk_Record_8_ucVddInd_Offset              4       //1 bajt do odczytu
#define GFXclk_Record_8_ulSclk_Offset                0       //4 bajty do odczytu

#define MCLK_Record_7_ucVddInd_Offset                0       //1 bajt do odczytu
#define MCLK_Record_7_ulSclk_Offset                  7       //4 bajty do odczytu
#define MCLK_Record_8_ucVddInd_Offset                4       //1 bajt do odczytu
#define MCLK_Record_8_ulSclk_Offset                  0       //4 bajty do odczytu

#define Voltage_Record_7_8_Offset                    0       //1 bajt do odczytu


//############################# DEFINES END ####################################

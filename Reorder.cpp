//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Reorder.h"
#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm5 *Form5;
//---------------------------------------------------------------------------
__fastcall TForm5::TForm5(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm5::ListView1DragOver(TObject *Sender, TObject *Source, int X,
          int Y, TDragState State, bool &Accept)
{
Accept=Sender==ListView1;

}
//---------------------------------------------------------------------------
void UPDown(int oldindex, int newindex)
{
String Name=Form5->ListView1->Items->Item[newindex]->Caption;
String index=Form5->ListView1->Items->Item[newindex]->SubItems->Strings[0];

Form5->ListView1->Items->Item[newindex]->Caption=Form5->ListView1->Items->Item[oldindex]->Caption;
Form5->ListView1->Items->Item[newindex]->SubItems->Strings[0]=Form5->ListView1->Items->Item[oldindex]->SubItems->Strings[0];

Form5->ListView1->Items->Item[oldindex]->Caption=Name;
Form5->ListView1->Items->Item[oldindex]->SubItems->Strings[0]=index;

Form5->ListView1->ItemIndex=newindex;
}
//---------------------------------------------------------------------------

void __fastcall TForm5::Button1Click(TObject *Sender)
{
if (ListView1->ItemIndex==-1 || ListView1->ItemIndex==0)
 {
  return;
 }
UPDown(ListView1->ItemIndex, ListView1->ItemIndex-1);
}
//---------------------------------------------------------------------------

void __fastcall TForm5::Button2Click(TObject *Sender)
{
if (ListView1->ItemIndex==-1 || ListView1->ItemIndex==ListView1->Items->Count-1)
 {
  return;
 }
UPDown(ListView1->ItemIndex, ListView1->ItemIndex+1);
}
//---------------------------------------------------------------------------

void __fastcall TForm5::CancelClick(TObject *Sender)
{
Form5->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm5::OKClick(TObject *Sender)
{
Form1->ReorderProfiles();
Form5->Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm5::ListView1DragDrop(TObject *Sender, TObject *Source, int X, int Y)
{
if (Source==ListView1)
 {
  int index;
  if (ListView1->GetItemAt(X,Y)!=NULL)
   index=ListView1->GetItemAt(X,Y)->Index; else
    index=ListView1->Items->Count;

  TListItem* DragItem=ListView1->Items->Insert(index);
  DragItem->Assign(ListView1->Selected);
  ListView1->Selected->Delete();
 }
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "PowerPlay.h"
#include "Unit1.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;

typedef struct pConst {
 String RegKey;
 byte MaxGPU_Offset;
 byte MaxMemory_Offset;
 byte MaxPower_Offset;
 unsigned short GPU_Table_Offset;
 unsigned short Mem_Table_Offset;
 unsigned short VDDC_Table_Offset;
 unsigned short VDDMem_Table_Offset;
 byte MCLK_Record_Size;
 byte SCLK_Record_Size;                                    // wielko�� recordu MCLK
 byte Voltage_Record_Size;                                 // wielko�� recordu SCLK
 byte Tables_NumEntries_Offset;                            // odczyt ilo�ci record�w
 byte Record_Start_Offset;                                 // gdzie zaczynaj� si� recordy
 byte SCLK_Record_VDDInd_Offset;
 byte SCLK_Record_CLK_Offset;
 byte MCLK_Record_VddInd_Offset;
 byte MCLK_Record_CLK_Offset;
 byte Voltage_Record_Offset;
 byte GPU_NumPStates;
 byte Mem_NumPStates;
 int MinVoltage;
 int MaxVoltage;
 String DevId;
} pConst;
 pConst Const;

typedef struct PsItem {
 unsigned int Clock;              //u�ywane do undo all changes
 unsigned int Voltage;             //u�ywane do undo all changes
 unsigned short VoltageOffset;   //gotowy finalny offset do napi�cia dla danego PState (z tablicy napi��)
 unsigned short VoltageRecordNumber;
 unsigned short ClockOffset;     //offset do napi�cia dla danego PState
 DynamicArray <TSpinEdit*> Shared;       //powi�zane SpinEdity dla napi��
} PsItem;

DynamicArray <PsItem> GpuPs;
DynamicArray <PsItem> MemPs;

TMemoryStream *PPTable;

bool IsAdmin;

//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
	: TForm(Owner)
{
//Form1->Form3_Created=true;
}
//---------------------------------------------------------------------------
void __fastcall TForm3::EnableDisableRegistryLine(bool enable, String str)
{
Label9->Caption=str;
if (enable)
 {
    Button4->Enabled=true;
    Button7->Enabled=true;
   // Label8->Visible=true;
    RadioButton1->Enabled=true;
 } else
   {
    Button4->Enabled=false;
    Button7->Enabled=false;
    Label8->Visible=false;
    RadioButton1->Enabled=false;
    if (RadioButton1->Checked)
     {
      ShowHidePPControls(false);
      RadioButton2->Checked=true;
     }
   }
}
//---------------------------------------------------------------------------
String PPTableTypeToDescription(byte typ)
{
switch (typ)
 {
  case -1: return "not supported";
  case 0 : return "Fiji table found";
  case 1 : return "Polaris table found";
  case 2 : return "Vega table found";
  default: return "";
 }
}
//---------------------------------------------------------------------------
void ParseRegistryTable()
{

PPTable->Clear();
int an=LoadPPTableFromRegistry(PPTable);
if (an==-2)
 {
  Form3->EnableDisableRegistryLine(false, "invalid size");
  return;
 } else
  if (an==-1)
   {
     if (IsAdmin==false)
      {
        Form3->EnableDisableRegistryLine(false, "admin rights required");
      } else
       {
         Form3->EnableDisableRegistryLine(false, "not found");
       }
     return;
   }
//found:
int typ=DeterminePPTableRevision(PPTable);
if (typ==-1)
 {
   Form3->EnableDisableRegistryLine(true, PPTableTypeToDescription(typ));
   Form3->ShowHidePPControls(false);
   return;
 } else
  {
    Form3->EnableDisableRegistryLine(true, PPTableTypeToDescription(typ));
    SetConstants(typ,PPTable);
  }
Form3->RadioButton1->Checked=true;
ReadValuesFromPPTable(PPTable);
Form3->ReadValues(PPTable);
Form3->ShowHidePPControls(true);
}

//---------------------------------------------------------------------------
void InitializePowerPlay(String DriverPath, String GpuStr, int MinVoltage, int MaxVoltage, String DevId)
{
Form3->Edit1->Text="HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Class\\"+DriverPath+"\\PP_PhmSoftPowerPlayTable";
Form3->Label1->Caption=GpuStr;
Const.RegKey="SYSTEM\\CurrentControlSet\\Control\\Class\\"+DriverPath;
Const.MinVoltage=MinVoltage;
Const.MaxVoltage=MaxVoltage;
PPTable=new TMemoryStream;

Const.DevId=DevId;
IsAdmin=Odnt.CheckIfAdmin();
Form3->ShowHidePPControls(false);
//pr�ba odczytu z rejestru
ParseRegistryTable();
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button2Click(TObject *Sender)
{
Form3->Close();
}
//---------------------------------------------------------------------------
int ReadValueFromStream(TMemoryStream* ms, const int pos, const int psize)
{
if (psize==1)
 {
  byte answ;
  ms->Position=pos;
  ms->ReadBuffer(&answ,1);
  return answ;
 } else
   if (psize==2)
    {
     unsigned short answ;
     ms->Position=pos;
     ms->ReadBuffer(&answ,2);
     return answ;
    } else
      if (psize==4)
        {
         int answ;
         ms->Position=pos;
         ms->ReadBuffer(&answ,4);
         return answ;
        } else
         {
          ShowMessage("incorrect size");
          return NULL;
         }

}
//---------------------------------------------------------------------------
void WriteValueToStream(TMemoryStream* ms, const int pos, const int psize, int value)
{
if (psize==1)
 {
  byte val=value;
  ms->Position=pos;
  ms->WriteBuffer(&val,1);
 } else
   if (psize==2)
    {
     unsigned short val=value;
     ms->Position=pos;
     ms->WriteBuffer(&val,2);
    } else
      if (psize==4)
        {
         int val=value;
         ms->Position=pos;
         ms->WriteBuffer(&val,4);
        } else
         {
          ShowMessage("incorrect size");
         }
}
//---------------------------------------------------------------------------
//return:  (-1)-pozosta�e, 0-Fiji, 1-Polaris, 2-Vega
int DeterminePPTableRevision(TMemoryStream *ms)
{
int Found=-1;

int size=ReadValueFromStream(ms,POWERPLAYTABLE_TableSize_Offset,2);
int rev=ReadValueFromStream(ms,POWERPLAYTABLE_TableRevision_Offset,1);

if ((size==POWERPLAYTABLE_8_TableSize) && (rev==ATOM_Vega10_TABLE_REVISION_VEGA10))  //Vega
 {
   int GFXclkOffs=ReadValueFromStream(ms,POWERPLAYTABLE_8_GFXclkDependencyTable_Offset_to_pointer,2);
   int recIdent=ReadValueFromStream(ms,GFXclkOffs,1);
   if ((recIdent==GFXclk_Dependency_Record_V2_8_Ident) || (recIdent==GFXclk_Dependency_Record_V1_8_Ident))
    {
      Found=2;
    }

 } else
  {
     if ((size==POWERPLAYTABLE_7_TableSize) && (rev==ATOM_Tonga_TABLE_REVISION_TONGA))  //Polaris lub Fiji
      {
        int i=ReadValueFromStream(ms,POWERPLAYTABLE_7_FanTableTable_Offset_to_pointer,2);
        int fan_rev=ReadValueFromStream(ms,i,1);
        i=ReadValueFromStream(ms,POWERPLAYTABLE_7_SclkDependencyTable_Offset_to_pointer,2);
        int sclk_rev=ReadValueFromStream(ms,i,1);
        if ((fan_rev==Fan_TABLE_REVISION_Polaris) && (sclk_rev==SCLK_Dep_TABLE_REVISION_Polaris))  //Polaris
          {
            Found=1;
          } else
            if ((fan_rev==Fan_TABLE_REVISION_Fiji) && (sclk_rev==SCLK_Dep_TABLE_REVISION_Fiji))  //Fiji
            {
              Found=0;
            }
      }
  }

return Found;
}
//---------------------------------------------------------------------------
bool ValidPPTable(TMemoryStream *ms)
{
if ((ms->Size<150) || (ms->Size>2000))
 {
  return false;
 }
return true;
}

//---------------------------------------------------------------------------
bool ExtractPowerPlayTableFromBios(TMemoryStream* ms)
{

 //const int OFFSET_TO_POINTER_TO_ATOM_ROM_HEADER = 0x48;
int ATOMROMHeaderOffset=ReadValueFromStream(ms,OFFSET_TO_POINTER_TO_ATOM_ROM_HEADER,2);
int usMasterDataTableOffset=ReadValueFromStream(ms,ATOMROMHeaderOffset+32,2);
int PowerPlayInfoOffset=ReadValueFromStream(ms,usMasterDataTableOffset+34,2);
int PowerPlayInfoSize=ReadValueFromStream(ms,PowerPlayInfoOffset,2);

TMemoryStream *temp=new TMemoryStream;
ms->Position=PowerPlayInfoOffset;
temp->CopyFrom(ms,PowerPlayInfoSize);

ms->Clear();
temp->Position=0;
ms->CopyFrom(temp,PowerPlayInfoSize);
delete temp;
return true;
}
//---------------------------------------------------------------------------
void SetConstants(int typ,TMemoryStream* ms)
{
if ((typ==0) || (typ==1))  //Polaris Lub Fiji
 {
   //===
   if (typ==0)      //tylko dla fiji
    {
      Const.SCLK_Record_Size=SCLK_Dependency_Record_7_Fiji_Size;
    } else
       if (typ==1)      //tylko dla Polaris
        {
          Const.SCLK_Record_Size=SCLK_Dependency_Record_7_Polaris_Size;
        }
   //===
   Const.MaxGPU_Offset=POWERPLAYTABLE_7_MaxODEngineClock_Offset;
   Const.MaxMemory_Offset=POWERPLAYTABLE_7_MaxODMemoryClock_Offset;
   Const.MaxPower_Offset=POWERPLAYTABLE_7_PowerControlLimit_Offset;
   Const.GPU_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_7_SclkDependencyTable_Offset_to_pointer,2);
   Const.Mem_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_7_MclkDependencyTable_Offset_to_pointer,2);
   Const.VDDC_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_7_VddcLookupTable_Offset_to_pointer,2);
   Const.VDDMem_Table_Offset=Const.VDDC_Table_Offset;
   Const.MCLK_Record_Size=MCLK_Dependency_Record_7_Size;
   Const.Voltage_Record_Size=Voltage_Lookup_Record_7_Size;
   Const.SCLK_Record_VDDInd_Offset=SCLK_Record_7_ucVddInd_Offset;
   Const.SCLK_Record_CLK_Offset=SCLK_Record_7_ulSclk_Offset;
   Const.MCLK_Record_VddInd_Offset=MCLK_Record_7_ucVddInd_Offset;
   Const.MCLK_Record_CLK_Offset=MCLK_Record_7_ulSclk_Offset;

 } else
  if (typ==2)    //Vega
   {
     Const.MaxGPU_Offset=POWERPLAYTABLE_8_MaxODEngineClock_Offset;
     Const.MaxMemory_Offset=POWERPLAYTABLE_8_MaxODMemoryClock_Offset;
     Const.MaxPower_Offset=POWERPLAYTABLE_8_PowerControlLimit_Offset;
     Const.GPU_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_8_GFXclkDependencyTable_Offset_to_pointer,2);
     Const.Mem_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_8_MclkDependencyTable_Offset_to_pointer,2);
     Const.VDDC_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_8_VddcLookupTable_Offset_to_pointer,2);
     //Const.VDDMem_Table_Offset=ReadValueFromStream(ms,POWERPLAYTABLE_8_VddmemLookupTable_Offset_to_pointer,2);
     Const.VDDMem_Table_Offset=Const.VDDC_Table_Offset;
     int recIdent=ReadValueFromStream(ms,Const.GPU_Table_Offset,1);
     if (recIdent==GFXclk_Dependency_Record_V2_8_Ident)
      {
        Const.SCLK_Record_Size=GFXclk_Dependency_Record_V2_8_Size;
      } else
         if (recIdent==GFXclk_Dependency_Record_V1_8_Ident)
           {
             Const.SCLK_Record_Size=GFXclk_Dependency_Record_V1_8_Size;
           }
     Const.MCLK_Record_Size=MCLK_Dependency_Record_8_Size;
     Const.Voltage_Record_Size=Voltage_Lookup_Record_8_Size;
     Const.SCLK_Record_VDDInd_Offset=GFXclk_Record_8_ucVddInd_Offset;
     Const.SCLK_Record_CLK_Offset=GFXclk_Record_8_ulSclk_Offset;
     Const.MCLK_Record_VddInd_Offset=MCLK_Record_8_ucVddInd_Offset;
     Const.MCLK_Record_CLK_Offset=MCLK_Record_8_ulSclk_Offset;
   }

//Wsp�lne dla wszystkich:
Const.Tables_NumEntries_Offset=AllTables_NumEntries_Offset;
Const.Record_Start_Offset=AllTables_Record_Start_Offset;
Const.Voltage_Record_Offset=Voltage_Record_7_8_Offset;

Const.GPU_NumPStates=ReadValueFromStream(ms,Const.GPU_Table_Offset+Const.Tables_NumEntries_Offset,1);
Const.Mem_NumPStates=ReadValueFromStream(ms,Const.Mem_Table_Offset+Const.Tables_NumEntries_Offset,1);
}

//---------------------------------------------------------------------------
void ReadValuesFromPPTable(TMemoryStream* ms)
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
GpuPs.Length=Const.GPU_NumPStates;
MemPs.Length=Const.Mem_NumPStates;
//GPU
int clkoffs=Const.GPU_Table_Offset+Const.Record_Start_Offset+Const.SCLK_Record_CLK_Offset;
int volffs=Const.GPU_Table_Offset+Const.Record_Start_Offset+Const.SCLK_Record_VDDInd_Offset;
int vtableoffs=Const.VDDC_Table_Offset+Const.Record_Start_Offset+Const.Voltage_Record_Offset;

for (int i = 0; i < GpuPs.Length; i++)
{
  //Clock
  GpuPs[i].Clock=ReadValueFromStream(ms,clkoffs,4)/100;
  GpuPs[i].ClockOffset=clkoffs;
  //Voltages
  GpuPs[i].VoltageRecordNumber=ReadValueFromStream(ms,volffs,1);
  GpuPs[i].VoltageOffset=vtableoffs+(GpuPs[i].VoltageRecordNumber*Const.Voltage_Record_Size);
  GpuPs[i].Voltage=ReadValueFromStream(ms,GpuPs[i].VoltageOffset,2);

  clkoffs=clkoffs+Const.SCLK_Record_Size;
  volffs=volffs+Const.SCLK_Record_Size;
  GpuPs[i].Shared.Length=0;

}
//Memory
clkoffs=Const.Mem_Table_Offset+Const.Record_Start_Offset+Const.MCLK_Record_CLK_Offset;
volffs=Const.Mem_Table_Offset+Const.Record_Start_Offset+Const.MCLK_Record_VddInd_Offset;
vtableoffs=Const.VDDMem_Table_Offset+Const.Record_Start_Offset+Const.Voltage_Record_Offset;

for (int i = 0; i < MemPs.Length; i++)
{
  //Clock
  MemPs[i].Clock=ReadValueFromStream(ms,clkoffs,4)/100;
  MemPs[i].ClockOffset=clkoffs;
  //Voltages
  MemPs[i].VoltageRecordNumber=ReadValueFromStream(ms,volffs,1);
  MemPs[i].VoltageOffset=vtableoffs+(MemPs[i].VoltageRecordNumber*Const.Voltage_Record_Size);
  MemPs[i].Voltage=ReadValueFromStream(ms,MemPs[i].VoltageOffset,2);

  clkoffs=clkoffs+Const.MCLK_Record_Size;
  volffs=volffs+Const.MCLK_Record_Size;
  MemPs[i].Shared.Length=0;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Ustalamy powi�zania
//gpu
for (int i=0; i <GpuPs.Length; i++)
 {
  for (int j=i; j < GpuPs.Length-i; j++)
  {
    if (i!=j)
     {
       if (GpuPs[i].VoltageRecordNumber==GpuPs[j].VoltageRecordNumber)
       {
        GpuPs[i].Shared.Length++;
        GpuPs[i].Shared[GpuPs[i].Shared.Length-1]=(TSpinEdit*)Form3->FindComponent("GPU_Vcc_"+IntToStr(j));

        GpuPs[j].Shared.Length++;
        GpuPs[j].Shared[GpuPs[j].Shared.Length-1]=(TSpinEdit*)Form3->FindComponent("GPU_Vcc_"+IntToStr(i));
       }

     }
   }
 }

//mem
for (int i=0; i <MemPs.Length; i++)
 {
  for (int j=i; j < MemPs.Length-i; j++)
   {
    if (i!=j)
     {
       if (MemPs[i].VoltageRecordNumber==MemPs[j].VoltageRecordNumber)
        {
          MemPs[i].Shared.Length++;
          MemPs[i].Shared[MemPs[i].Shared.Length-1]=(TSpinEdit*)Form3->FindComponent("Mem_Vcc_"+IntToStr(j));

          MemPs[j].Shared.Length++;
          MemPs[j].Shared[MemPs[j].Shared.Length-1]=(TSpinEdit*)Form3->FindComponent("Mem_Vcc_"+IntToStr(i));
        }
     }
   }
 }
//gpu<->mem
for (int i=0; i <GpuPs.Length; i++)
{
 for (int j=0; j < MemPs.Length; j++)
  {
    if (GpuPs[i].VoltageRecordNumber==MemPs[j].VoltageRecordNumber)
      {
        GpuPs[i].Shared.Length++;
        GpuPs[i].Shared[GpuPs[i].Shared.Length-1]=(TSpinEdit*)Form3->FindComponent("Mem_Vcc_"+IntToStr(j));

        MemPs[j].Shared.Length++;
        MemPs[j].Shared[MemPs[j].Shared.Length-1]=(TSpinEdit*)Form3->FindComponent("Gpu_Vcc_"+IntToStr(i));
      }
  }
}


}
//---------------------------------------------------------------------------
void __fastcall TForm3::ReadValues(TMemoryStream* ms)
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//GPU
if (GpuPs.Length>=1)
 {
    GPU_Clock_0->Value=GpuPs[0].Clock;
    GPU_Vcc_0->Value=GpuPs[0].Voltage;
 }
if (GpuPs.Length>=2)
 {
    GPU_Clock_1->Value=GpuPs[1].Clock;
    GPU_Vcc_1->Value=GpuPs[1].Voltage;
 }
if (GpuPs.Length>=3)
 {
    GPU_Clock_2->Value=GpuPs[2].Clock;
    GPU_Vcc_2->Value=GpuPs[2].Voltage;
 }
if (GpuPs.Length>=4)
 {
    GPU_Clock_3->Value=GpuPs[3].Clock;
    GPU_Vcc_3->Value=GpuPs[3].Voltage;
 }
if (GpuPs.Length>=5)
 {
    GPU_Clock_4->Value=GpuPs[4].Clock;
    GPU_Vcc_4->Value=GpuPs[4].Voltage;
 }
if (GpuPs.Length>=6)
 {
    GPU_Clock_5->Value=GpuPs[5].Clock;
    GPU_Vcc_5->Value=GpuPs[5].Voltage;
 }
if (GpuPs.Length>=7)
 {
    GPU_Clock_6->Value=GpuPs[6].Clock;
    GPU_Vcc_6->Value=GpuPs[6].Voltage;
 }
if (GpuPs.Length>=8)
 {
    GPU_Clock_7->Value=GpuPs[7].Clock;
    GPU_Vcc_7->Value=GpuPs[7].Voltage;
 }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//memory
if (MemPs.Length>=1)
 {
    Mem_Clock_0->Value=MemPs[0].Clock;
    Mem_Vcc_0->Value=MemPs[0].Voltage;
 }
if (MemPs.Length>=2)
 {
    Mem_Clock_1->Value=MemPs[1].Clock;
    Mem_Vcc_1->Value=MemPs[1].Voltage;
 }
if (MemPs.Length>=3)
 {
    Mem_Clock_2->Value=MemPs[2].Clock;
    Mem_Vcc_2->Value=MemPs[2].Voltage;
 }
if (MemPs.Length>=4)
 {
    Mem_Clock_3->Value=MemPs[3].Clock;
    Mem_Vcc_3->Value=MemPs[3].Voltage;
 }
if (MemPs.Length>=5)
 {
    Mem_Clock_4->Value=MemPs[4].Clock;
    Mem_Vcc_4->Value=MemPs[4].Voltage;
 }
if (MemPs.Length>=6)
 {
    Mem_Clock_5->Value=MemPs[5].Clock;
    Mem_Vcc_5->Value=MemPs[5].Voltage;
 }
if (MemPs.Length>=7)
 {
    Mem_Clock_6->Value=MemPs[6].Clock;
    Mem_Vcc_6->Value=MemPs[6].Voltage;
 }
if (MemPs.Length>=8)
 {
    Mem_Clock_7->Value=MemPs[7].Clock;
    Mem_Vcc_7->Value=MemPs[7].Voltage;
 }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Others
Max_GPU->Value=ReadValueFromStream(ms,Const.MaxGPU_Offset,4)/100;
Max_Mem->Value=ReadValueFromStream(ms,Const.MaxMemory_Offset,4)/100;
Max_Power->Value=ReadValueFromStream(ms,Const.MaxPower_Offset,2);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//---------------------------------------------------------------------------
void __fastcall TForm3::WriteValues(TMemoryStream* ms)
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//GPU
if (GpuPs.Length>=1)
 {
   WriteValueToStream(ms,GpuPs[0].ClockOffset, 4, GPU_Clock_0->Value*100);
   WriteValueToStream(ms,GpuPs[0].VoltageOffset, 2, GPU_Vcc_0->Value);
 }
if (GpuPs.Length>=2)
 {
   WriteValueToStream(ms,GpuPs[1].ClockOffset, 4, GPU_Clock_1->Value*100);
   WriteValueToStream(ms,GpuPs[1].VoltageOffset, 2, GPU_Vcc_1->Value);
 }
if (GpuPs.Length>=3)
 {
   WriteValueToStream(ms,GpuPs[2].ClockOffset, 4, GPU_Clock_2->Value*100);
   WriteValueToStream(ms,GpuPs[2].VoltageOffset, 2, GPU_Vcc_2->Value);
 }
if (GpuPs.Length>=4)
 {
   WriteValueToStream(ms,GpuPs[3].ClockOffset, 4, GPU_Clock_3->Value*100);
   WriteValueToStream(ms,GpuPs[3].VoltageOffset, 2, GPU_Vcc_3->Value);
 }
if (GpuPs.Length>=5)
 {
   WriteValueToStream(ms,GpuPs[4].ClockOffset, 4, GPU_Clock_4->Value*100);
   WriteValueToStream(ms,GpuPs[4].VoltageOffset, 2, GPU_Vcc_4->Value);
 }
if (GpuPs.Length>=6)
 {
   WriteValueToStream(ms,GpuPs[5].ClockOffset, 4, GPU_Clock_5->Value*100);
   WriteValueToStream(ms,GpuPs[5].VoltageOffset, 2, GPU_Vcc_5->Value);
 }
if (GpuPs.Length>=7)
 {
   WriteValueToStream(ms,GpuPs[6].ClockOffset, 4, GPU_Clock_6->Value*100);
   WriteValueToStream(ms,GpuPs[6].VoltageOffset, 2, GPU_Vcc_6->Value);
 }
if (GpuPs.Length>=8)
 {
   WriteValueToStream(ms,GpuPs[7].ClockOffset, 4, GPU_Clock_7->Value*100);
   WriteValueToStream(ms,GpuPs[7].VoltageOffset, 2, GPU_Vcc_7->Value);
 }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//memory
if (MemPs.Length>=1)
 {
   WriteValueToStream(ms,MemPs[0].ClockOffset, 4, Mem_Clock_0->Value*100);
   WriteValueToStream(ms,MemPs[0].VoltageOffset, 2, Mem_Vcc_0->Value);
 }
if (MemPs.Length>=2)
 {
   WriteValueToStream(ms,MemPs[1].ClockOffset, 4, Mem_Clock_1->Value*100);
   WriteValueToStream(ms,MemPs[1].VoltageOffset, 2, Mem_Vcc_1->Value);
 }
if (MemPs.Length>=3)
 {
   WriteValueToStream(ms,MemPs[2].ClockOffset, 4, Mem_Clock_2->Value*100);
   WriteValueToStream(ms,MemPs[2].VoltageOffset, 2, Mem_Vcc_2->Value);
 }
if (MemPs.Length>=4)
 {
   WriteValueToStream(ms,MemPs[3].ClockOffset, 4, Mem_Clock_3->Value*100);
   WriteValueToStream(ms,MemPs[3].VoltageOffset, 2, Mem_Vcc_3->Value);
 }
if (MemPs.Length>=5)
 {
   WriteValueToStream(ms,MemPs[4].ClockOffset, 4, Mem_Clock_4->Value*100);
   WriteValueToStream(ms,MemPs[4].VoltageOffset, 2, Mem_Vcc_4->Value);
 }
if (MemPs.Length>=6)
 {
   WriteValueToStream(ms,MemPs[5].ClockOffset, 4, Mem_Clock_5->Value*100);
   WriteValueToStream(ms,MemPs[5].VoltageOffset, 2, Mem_Vcc_5->Value);
 }
if (MemPs.Length>=7)
 {
   WriteValueToStream(ms,MemPs[6].ClockOffset, 4, Mem_Clock_6->Value*100);
   WriteValueToStream(ms,MemPs[6].VoltageOffset, 2, Mem_Vcc_6->Value);
 }
if (MemPs.Length>=8)
 {
   WriteValueToStream(ms,MemPs[7].ClockOffset, 4, Mem_Clock_7->Value*100);
   WriteValueToStream(ms,MemPs[7].VoltageOffset, 2, Mem_Vcc_7->Value);
 }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Others
WriteValueToStream(ms, Const.MaxGPU_Offset, 4, Max_GPU->Value*100);
WriteValueToStream(ms, Const.MaxMemory_Offset, 4, Max_Mem->Value*100);
WriteValueToStream(ms, Const.MaxPower_Offset, 2, Max_Power->Value);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//---------------------------------------------------------------------------
bool ValidateVoltage(TSpinEdit *s,String name,bool CheckHighLowVoltages)
{
int val;
if (TryStrToInt(s->Text,val)==false)
 {
    blad("Incorrect voltage value entered in "+name, NULL, false);
    return false;
 } else
  {
    if (CheckHighLowVoltages)
     {
       if (val<Const.MinVoltage)
        {
          blad(name+ " voltage value too low (Minimum allowed: "+IntToStr(Const.MinVoltage)+" mV)", NULL, false);
         return false;
        } else
          if (((val>Const.MaxVoltage) && (val<0xFF00)) || (val>0xFFFF))
          {
            blad(name+ " voltage value too high (Maximum allowed: "+IntToStr(Const.MaxVoltage)+" mV)", NULL, false);
            return false;
          }
     }
  }
   

return true;
}
//---------------------------------------------------------------------------
bool ValidateClock(TSpinEdit *s,String name,bool CheckHighLowClocks)
{
int val;
if (TryStrToInt(s->Text,val)==false)
 {
    blad("Incorrect clock value entered in "+name, NULL, false);
    return false;
 } else
  {
    if (CheckHighLowClocks)
     {
       if (val<100)
        {
          //blad("Too low Clock value entered in "+name+"\x0d\x0a"+"(Minimum allowed: 100 MHz)",NULL);
          blad(name+ " clock value too low (Minimum allowed: 100 MHz)", NULL, false);
          return false;
        }
     }
  }
   
return true;
}

//---------------------------------------------------------------------------
bool __fastcall TForm3::ValidateEnteredValues(bool CheckHighLowValues)
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//GPU
if (GpuPs.Length>=1)
 {
   if (ValidateClock(GPU_Clock_0,"GPU P0",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_0,"GPU P0",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=2)
 {
   if (ValidateClock(GPU_Clock_1,"GPU P1",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_1,"GPU P1",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=3)
 {
   if (ValidateClock(GPU_Clock_2,"GPU P2",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_2,"GPU P2",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=4)
 {
   if (ValidateClock(GPU_Clock_3,"GPU P3",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_3,"GPU P3",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=5)
 {
   if (ValidateClock(GPU_Clock_4,"GPU P4",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_4,"GPU P4",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=6)
 {
   if (ValidateClock(GPU_Clock_5,"GPU P5",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_5,"GPU P5",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=7)
 {
   if (ValidateClock(GPU_Clock_6,"GPU P6",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_6,"GPU P6",CheckHighLowValues)==false) return false;
 }
if (GpuPs.Length>=8)
 {
   if (ValidateClock(GPU_Clock_7,"GPU P7",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(GPU_Vcc_7,"GPU P7",CheckHighLowValues)==false) return false;
 }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//memory
if (MemPs.Length>=1)
 {
   if (ValidateClock(Mem_Clock_0,"Memory P0",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_0,"Memory P0",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=2)
 {
   if (ValidateClock(Mem_Clock_1,"Memory P1",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_1,"Memory P1",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=3)
 {
   if (ValidateClock(Mem_Clock_2,"Memory P2",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_2,"Memory P2",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=4)
 {
   if (ValidateClock(Mem_Clock_3,"Memory P3",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_3,"Memory P3",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=5)
 {
   if (ValidateClock(Mem_Clock_4,"Memory P4",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_4,"Memory P4",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=6)
 {
   if (ValidateClock(Mem_Clock_5,"Memory P5",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_5,"Memory P5",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=7)
 {
   if (ValidateClock(Mem_Clock_6,"Memory P6",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_6,"Memory P6",CheckHighLowValues)==false) return false;
 }
if (MemPs.Length>=8)
 {
   if (ValidateClock(Mem_Clock_7,"Memory P7",CheckHighLowValues)==false) return false;
   if (ValidateVoltage(Mem_Vcc_7,"Memory P7",CheckHighLowValues)==false) return false;
 }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Others
if (ValidateClock(Max_GPU,"Max GPU Freq.",CheckHighLowValues)==false) return false;
if (ValidateClock(Max_Mem,"Max Memory Freq.",CheckHighLowValues)==false) return false;
int val;
if (TryStrToInt(Max_Power->Text,val)==false)
 {
    blad("Incorrect value entered in Max Power Target", NULL, false);
    return false;
 }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
return true;
}

//---------------------------------------------------------------------------
void __fastcall TForm3::ShowHidePPControls(bool found)
{
if (found)
 {
   //============
   //GPU
   if (Const.GPU_NumPStates>=1)
    {
     GPU_P0->Visible=true;
     GPU_Clock_0->Visible=true;
     GPU_Vcc_0->Visible=true;
    } else
       {
        GPU_P0->Visible=false;
        GPU_Clock_0->Visible=false;
        GPU_Vcc_0->Visible=false;
       }
   if (Const.GPU_NumPStates>=2)
    {
     GPU_P1->Visible=true;
     GPU_Clock_1->Visible=true;
     GPU_Vcc_1->Visible=true;
    } else
       {
        GPU_P1->Visible=false;
        GPU_Clock_1->Visible=false;
        GPU_Vcc_1->Visible=false;
       }
   if (Const.GPU_NumPStates>=3)
    {
     GPU_P2->Visible=true;
     GPU_Clock_2->Visible=true;
     GPU_Vcc_2->Visible=true;
    } else
       {
        GPU_P2->Visible=false;
        GPU_Clock_2->Visible=false;
        GPU_Vcc_2->Visible=false;
       }
   if (Const.GPU_NumPStates>=4)
    {
     GPU_P3->Visible=true;
     GPU_Clock_3->Visible=true;
     GPU_Vcc_3->Visible=true;
    } else
       {
        GPU_P3->Visible=false;
        GPU_Clock_3->Visible=false;
        GPU_Vcc_3->Visible=false;
       }
   if (Const.GPU_NumPStates>=5)
    {
     GPU_P4->Visible=true;
     GPU_Clock_4->Visible=true;
     GPU_Vcc_4->Visible=true;
    } else
       {
        GPU_P4->Visible=false;
        GPU_Clock_4->Visible=false;
        GPU_Vcc_4->Visible=false;
       }
   if (Const.GPU_NumPStates>=6)
    {
     GPU_P5->Visible=true;
     GPU_Clock_5->Visible=true;
     GPU_Vcc_5->Visible=true;
    } else
       {
        GPU_P5->Visible=false;
        GPU_Clock_5->Visible=false;
        GPU_Vcc_5->Visible=false;
       }
   if (Const.GPU_NumPStates>=7)
    {
     GPU_P6->Visible=true;
     GPU_Clock_6->Visible=true;
     GPU_Vcc_6->Visible=true;
    } else
       {
        GPU_P6->Visible=false;
        GPU_Clock_6->Visible=false;
        GPU_Vcc_6->Visible=false;
       }
   if (Const.GPU_NumPStates>=8)
    {
     GPU_P7->Visible=true;
     GPU_Clock_7->Visible=true;
     GPU_Vcc_7->Visible=true;
    } else
       {
        GPU_P7->Visible=false;
        GPU_Clock_7->Visible=false;
        GPU_Vcc_7->Visible=false;
       }
   //============
   //Memory
   if (Const.Mem_NumPStates>=1)
    {
     Mem_P0->Visible=true;
     Mem_Clock_0->Visible=true;
     Mem_Vcc_0->Visible=true;
    } else
       {
        Mem_P0->Visible=false;
        Mem_Clock_0->Visible=false;
        Mem_Vcc_0->Visible=false;
       }
   if (Const.Mem_NumPStates>=2)
    {
     Mem_P1->Visible=true;
     Mem_Clock_1->Visible=true;
     Mem_Vcc_1->Visible=true;
    } else
       {
        Mem_P1->Visible=false;
        Mem_Clock_1->Visible=false;
        Mem_Vcc_1->Visible=false;
       }
   if (Const.Mem_NumPStates>=3)
    {
     Mem_P2->Visible=true;
     Mem_Clock_2->Visible=true;
     Mem_Vcc_2->Visible=true;
    } else
       {
        Mem_P2->Visible=false;
        Mem_Clock_2->Visible=false;
        Mem_Vcc_2->Visible=false;
       }
   if (Const.Mem_NumPStates>=4)
    {
     Mem_P3->Visible=true;
     Mem_Clock_3->Visible=true;
     Mem_Vcc_3->Visible=true;
    } else
       {
        Mem_P3->Visible=false;
        Mem_Clock_3->Visible=false;
        Mem_Vcc_3->Visible=false;
       }
   if (Const.Mem_NumPStates>=5)
    {
     Mem_P4->Visible=true;
     Mem_Clock_4->Visible=true;
     Mem_Vcc_4->Visible=true;
    } else
       {
        Mem_P4->Visible=false;
        Mem_Clock_4->Visible=false;
        Mem_Vcc_4->Visible=false;
       }
   if (Const.Mem_NumPStates>=6)
    {
     Mem_P5->Visible=true;
     Mem_Clock_5->Visible=true;
     Mem_Vcc_5->Visible=true;
    } else
       {
        Mem_P5->Visible=false;
        Mem_Clock_5->Visible=false;
        Mem_Vcc_5->Visible=false;
       }
   if (Const.Mem_NumPStates>=7)
    {
     Mem_P6->Visible=true;
     Mem_Clock_6->Visible=true;
     Mem_Vcc_6->Visible=true;
    } else
       {
        Mem_P6->Visible=false;
        Mem_Clock_6->Visible=false;
        Mem_Vcc_6->Visible=false;
       }
   if (Const.Mem_NumPStates>=8)
    {
     Mem_P7->Visible=true;
     Mem_Clock_7->Visible=true;
     Mem_Vcc_7->Visible=true;
    } else
       {
        Mem_P7->Visible=false;
        Mem_Clock_7->Visible=false;
        Mem_Vcc_7->Visible=false;
       }
   //============
   GroupBox3->Visible=true;
   GroupBox2->Visible=true;
   GroupBox4->Visible=true;
   //Label8->Visible=true;
   if (Label1->Caption!="none")
    Button3->Visible=true; else
     Button3->Visible=false;
   Button5->Visible=true;
   Button6->Visible=true;
 } else
  {
    GroupBox3->Visible=false;
    GroupBox2->Visible=false;
    GroupBox4->Visible=false;
    //Label8->Visible=false;
    Button3->Visible=false;
    Button5->Visible=false;
    Button6->Visible=false;
    Edit2->Text="";
    Label2->Caption="";
   // Label9->Caption="";
  }
Label8->Visible=false;
}
//---------------------------------------------------------------------------
//-2 invalid size -1 value not found 0-ok
int LoadPPTableFromRegistry(TMemoryStream *ms)
{
int found=-1;
TRegistry* reg = new TRegistry(KEY_WRITE + KEY_READ);
reg->RootKey= HKEY_LOCAL_MACHINE;
if(reg->KeyExists(Const.RegKey))
 {
  if (reg->OpenKey(Const.RegKey, false))
   {
     if (reg->ValueExists("PP_PhmSoftPowerPlayTable"))
       {
         TMemoryStream *buf=new TMemoryStream;
         buf->Size=4096;
         int size=reg->ReadBinaryData("PP_PhmSoftPowerPlayTable",buf->Memory,buf->Size);
         if (size==ReadValueFromStream(buf,0,2))
          {
            buf->Position=0;
            ms->CopyFrom(buf,size);
            found=0;
          } else
           {
             found=-2;
           }
         buf->Free();
       }
     reg->CloseKey();
   }
 }
reg->Free();
return found;
}

//---------------------------------------------------------------------------
int SavePPTableToRegistry(TMemoryStream *ms)
{
int res=-1;
TRegistry* reg = new TRegistry(KEY_WRITE + KEY_READ);
reg->RootKey= HKEY_LOCAL_MACHINE;

if(reg->KeyExists(Const.RegKey))
 {
  if (reg->OpenKey(Const.RegKey, false))
        {
          reg->WriteBinaryData("PP_PhmSoftPowerPlayTable",ms->Memory,ms->Size);
          reg->CloseKey();
          res=0;
        }
 }
reg->Free();
return res;
}
//---------------------------------------------------------------------------
bool IsPossibleToSaveToReg(TMemoryStream *ms)
{
int size=ReadValueFromStream(ms,0,2);
if (size==ms->Size && size>150)
 {
  int rev=ReadValueFromStream(ms,POWERPLAYTABLE_TableRevision_Offset,1);
  if (rev>=MINIMUM_UNSUPPORTED_TABLE_REVISION_AVAILABLE_TO_EXTRACT_ONLY && rev<=MAXIMUM_UNSUPPORTED_TABLE_REVISION_AVAILABLE_TO_EXTRACT_ONLY)
   {
    return true;
   }
 }
return false;
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button1Click(TObject *Sender)
{
RadioButton2->Checked=true;
if (OpenDialog1->Execute())
 {
   //============== Odczyt
   PPTable->Clear();
   String str=TFile::ReadAllText(OpenDialog1->FileName);
   if ((Pos("\\Control\\Class\\", str)>0) && (Pos("PP_PhmSoftPowerPlayTable\"=hex:", str)>0))
    { //reg file
      String s=str;
      s=s.Delete(1,Pos("PP_PhmSoftPowerPlayTable\"=hex:", s)-1);
      s=Trim(StringReplace(s, "PP_PhmSoftPowerPlayTable\"=hex:", "", TReplaceFlags()));
      TStringList *sl=new TStringList;
      sl->Text=s;
      s="";
      for (int i = 0; i < sl->Count; i++)
       {
        sl->Strings[i]=Trim(sl->Strings[i]);
        if ((sl->Strings[i])[sl->Strings[i].Length()]==*L"\\")
         {
           s+=StringReplace(sl->Strings[i], ",\\", ",", TReplaceFlags());
         } else
          {
            s+=sl->Strings[i];
            break;
          }
       }
      sl->Delimiter=*L",";
      sl->QuoteChar=Char(0);
      sl->StrictDelimiter=true;
      sl->DelimitedText=s;
      int buf;
      bool correct;
      for (int i = 0; i < sl->Count; i++)
       {
        correct=false;
        if ((sl->Strings[i].Length()==1) || (sl->Strings[i].Length()==2))
         {
          if (TryStrToInt("0x"+sl->Strings[i], buf))
           {
            if (buf>=0x00 && buf<=0xFF)
             {
              PPTable->Position=i;
              PPTable->WriteBufferData(buf,1);
              correct=true;
             }
           }
         }
        if (!correct)
         {
           blad("Failed to read reg file, invalid data: \""+sl->Strings[i]+"\"", NULL,false);
           ShowHidePPControls(false);
           PPTable->Clear();
           delete sl;
           return;
         }
       }
      delete sl;
      if (PPTable->Size<ReadValueFromStream(PPTable, 0, 2))
       {
         blad("Failed to read .reg file, incorrect PP_PhmSoftPowerPlayTable data size", NULL, false);
         ShowHidePPControls(false);
         PPTable->Clear();
         return;
       }

      if (Label1->Caption=="none")
       {
        if ((Pos("]", str)> Pos("[HKEY_LOCAL_MACHINE\\SYSTEM\\",str)+60) && (Pos("[HKEY_LOCAL_MACHINE\\SYSTEM\\",str)<Pos("PP_PhmSoftPowerPlayTable\"=hex:", str)))
         {
           String s=str.SubString(Pos("HKEY_LOCAL_MACHINE\\SYSTEM\\", str), Pos("]", str)-Pos("HKEY_LOCAL_MACHINE\\SYSTEM\\", str));
           if (s.Length()<120)
            {
              Edit1->Text=s+"\\PP_PhmSoftPowerPlayTable";
              Const.RegKey=StringReplace(s, "HKEY_LOCAL_MACHINE\\", "", TReplaceFlags() << rfIgnoreCase);
            }
         }
       }
    } else
     {  //bios file
      TMemoryStream *ms=new TMemoryStream;
      ms->LoadFromFile(OpenDialog1->FileName);
      if (ms->Size % 0x10000)     //rozmiar musi by� podzielny przez 64KB
       {
         blad("Invalid filesize", NULL,false);
         ShowHidePPControls(false);
         ms->Free();
         return;
       }
      if (ExtractPowerPlayTableFromBios(ms))
       {
        ms->Position=0;
      //  PPTable->Clear();
        PPTable->CopyFrom(ms,ms->Size);
        ms->Free();
       } else
        {
          blad("Failed to read bios file", NULL, false);
          ShowHidePPControls(false);
          return;
        }
     }
   //============== weryfikacja
   if (ValidPPTable(PPTable)==false)
    {
      blad("Failed to read PowerPlayTable", NULL, false);
      ShowHidePPControls(false);
      return;
    }
   int typ=DeterminePPTableRevision(PPTable);
   Label2->Caption=PPTableTypeToDescription(typ);
   if (typ==-1)
    {
     ShowHidePPControls(false);
     if (IsPossibleToSaveToReg(PPTable))
      {
        Label2->Caption=PPTableTypeToDescription(typ);
        Edit2->Text=OpenDialog1->FileName;
        Edit2->SelStart=Edit2->Text.Length();
        Button6->Visible=true;
        blad("Unsupported PowerPlayTable, only save to .reg file is available", NULL, false);
      } else
       {
        blad("Unsupported PowerPlayTable", NULL, false);
       }
     return;
    } else
     {
       SetConstants(typ,PPTable);
     }
   ReadValuesFromPPTable(PPTable);
   ReadValues(PPTable);
   ShowHidePPControls(true);
   Edit2->Text=OpenDialog1->FileName;
   Edit2->SelStart=Edit2->Text.Length();
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::GPUChangeShared(int ps, int value)
{
if (GpuPs[ps].Shared.Length>0)
 {
  for (int i = 0; i < GpuPs[ps].Shared.Length; i++)
   {
    if (!GpuPs[ps].Shared[i]->Focused())
      GpuPs[ps].Shared[i]->Value=value;
   }

 }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::GPU_Vcc_0Change(TObject *Sender)
{
 GPUChangeShared(0, GPU_Vcc_0->Value);
}
//---------------------------------------------------------------------------
void __fastcall TForm3::GPU_Vcc_1Change(TObject *Sender)
{
 GPUChangeShared(1, GPU_Vcc_1->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GPU_Vcc_2Change(TObject *Sender)
{
 GPUChangeShared(2, GPU_Vcc_2->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GPU_Vcc_3Change(TObject *Sender)
{
//if (GPU_Vcc_3->Focused())
 GPUChangeShared(3, GPU_Vcc_3->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GPU_Vcc_4Change(TObject *Sender)
{
//if (GPU_Vcc_4->Focused())
 GPUChangeShared(4, GPU_Vcc_4->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GPU_Vcc_5Change(TObject *Sender)
{
//if (GPU_Vcc_5->Focused())
 GPUChangeShared(5, GPU_Vcc_5->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GPU_Vcc_6Change(TObject *Sender)
{
//if (GPU_Vcc_6->Focused())
 GPUChangeShared(6, GPU_Vcc_6->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::GPU_Vcc_7Change(TObject *Sender)
{
//if (GPU_Vcc_7->Focused())
 GPUChangeShared(7, GPU_Vcc_7->Value);
}
//---------------------------------------------------------------------------
void __fastcall TForm3::MemChangeShared(int ps, int value)
{
if (MemPs[ps].Shared.Length>0)
 {
  for (int i = 0; i < MemPs[ps].Shared.Length; i++)
   {
    if (!MemPs[ps].Shared[i]->Focused())
      MemPs[ps].Shared[i]->Value=value;
   }
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Mem_Vcc_0Change(TObject *Sender)
{
MemChangeShared(0, Mem_Vcc_0->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_1Change(TObject *Sender)
{
MemChangeShared(1, Mem_Vcc_1->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_2Change(TObject *Sender)
{
MemChangeShared(2, Mem_Vcc_2->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_3Change(TObject *Sender)
{
MemChangeShared(3, Mem_Vcc_3->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_4Change(TObject *Sender)
{
MemChangeShared(4, Mem_Vcc_4->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_5Change(TObject *Sender)
{
MemChangeShared(5, Mem_Vcc_5->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_6Change(TObject *Sender)
{
MemChangeShared(6, Mem_Vcc_6->Value);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Mem_Vcc_7Change(TObject *Sender)
{
MemChangeShared(7, Mem_Vcc_7->Value);
}
//---------------------------------------------------------------------------
//delete value
void __fastcall TForm3::Button4Click(TObject *Sender)
{
String str="Are you sure to delete \"PP_PhmSoftPowerPlayTable\" value from registry?";
if (MessageBox(0, str.w_str(), L"Confirmation", MB_ICONQUESTION|MB_YESNO)==7) return;

bool res=true;
TRegistry* reg = new TRegistry(KEY_WRITE + KEY_READ);
reg->RootKey= HKEY_LOCAL_MACHINE;

if(reg->KeyExists(Const.RegKey))
 {
  if (reg->OpenKey(Const.RegKey, false))
        {
          if (reg->ValueExists("PP_PhmSoftPowerPlayTable"))
           {
             res=reg->DeleteValue("PP_PhmSoftPowerPlayTable");
           }
          reg->CloseKey();
        }
 }
reg->Free();
if (res==false)
 {
   blad("Failed to delete PP_PhmSoftPowerPlayTable value", NULL, false);
 } else
    {
      EnableDisableRegistryLine(false, "not found");
      //Automatyczny restart
      ppRestartGPUPrompt();
    }
}
//---------------------------------------------------------------------------
//Automatyczny restart
//true - zrestartowano, false - nie zrestartowano
bool ppRestartGpuUsingExternalExe()       
{
String str="";
String path=ExtractFilePath(Application->ExeName);

if (FileExists(path+"devcon.exe",false))
 {
  str="devcon.exe";
 } else
  {
     if (IsWow64Process)
      {
        if (FileExists(path+"restart64.exe",false))
         {
           str="restart64.exe";
         }
     } else
       if (FileExists(path+"restart.exe",false))
        {
          str="restart.exe";
        }
  }

if (str!="")
 {
   //je�eli znaleziono to pytanie:
 //  String msg="Restart failed, use "+str+" to restart?";
 //  if (MessageBox(0, msg.w_str(), L"", MB_ICONQUESTION|MB_YESNO)==7) return false;
   String parameters;
   DWORD Flags;
   if (str=="devcon.exe")
    {
     String DevId=Const.DevId;
     DevId.Delete(1,Pos("VEN_",DevId)-1);
     DevId=DevId.SubString(1,Pos("\\",DevId)-1);
     if (DevId.Length()>12)
      {
          DevId="PCI\\"+DevId;
      } else
        {
           Const.DevId="";
           return false;
         }
      parameters="restart "+DevId;
      Flags=SW_HIDE;
    } else
     {
      parameters=NULL;
      Flags=SW_SHOWNORMAL;
     }

  if ((int)ShellExecute(0, L"open", str.w_str(), parameters.w_str(), path.w_str(), Flags)<=32)
   {
     blad("Failed to start process: "+str, NULL, false);
   } else
     {
       return true;
     }
 }
return false;
}

//---------------------------------------------------------------------------
bool ppRestartGPU(String DevID)          //wymaga praw administratora
{
GUID hidGuid= StringToGUID("{4d36e968-e325-11ce-bfc1-08002be10318}");

HDEVINFO hDevInfo = SetupDiGetClassDevs(&hidGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_PROFILE);
if (INVALID_HANDLE_VALUE == hDevInfo)
  {
    blad( "SetupDiGetClassDevs function failed", GetLastError(), true);
    return false;
  }

SP_DEVINFO_DATA* pspDevInfoData = (SP_DEVINFO_DATA*)HeapAlloc(GetProcessHeap(), 0, sizeof(SP_DEVINFO_DATA));
pspDevInfoData->cbSize = sizeof(SP_DEVINFO_DATA);
pspDevInfoData->ClassGuid=hidGuid;

for (int i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, pspDevInfoData); i++)
 {
   DWORD nSize = 0;
   TCHAR buf[MAX_PATH];
   if (!SetupDiGetDeviceInstanceId(hDevInfo, pspDevInfoData, buf, sizeof(buf), &nSize))
    {
      blad("SetupDiGetDeviceInstanceId function failed", GetLastError(), true);
      return false;
    } else
     {
       if (String(buf)==DevID)
        {
          if (SetupDiRestartDevices(hDevInfo, pspDevInfoData))
           {
             return true;
           } else
             {
               blad("SetupDiRestartDevices function failed", GetLastError(), true);
               return false;
             }
        }
     }
 }

return false;

}
//---------------------------------------------------------------------------
void ppRestartGPUPrompt()
{
//Automatyczny restart
String msg="Restart this GPU now?";
if (MessageBox(0, msg.w_str(), L"", MB_ICONQUESTION|MB_YESNO)==7)
 {
  Form3->Label8->Visible=true;
  return;
 }

bool res=false;
if (IsAdmin)
 {
  res=ppRestartGPU(Const.DevId);
 }
if (res==false)
 {
  res=ppRestartGpuUsingExternalExe();
 }

if (res)
 {
   Form3->Label8->Visible=false;
 } else
    {
     blad("Failed to restart GPU", NULL, false);
     Form3->Label8->Visible=true;
    }

}

//---------------------------------------------------------------------------
void __fastcall TForm3::Button3Click(TObject *Sender)
{
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
 if (IsAdmin==false)
  {
    blad("Administrator rights are required to save to registry", NULL, false);
    return;
  }

if (ValidateEnteredValues(true)==false)
 {
   return;
 }
WriteValues(PPTable);
SavePPTableToRegistry(PPTable);
if (RadioButton2->Checked)
 {
   EnableDisableRegistryLine(true, Label2->Caption);
 } else
  {
    EnableDisableRegistryLine(true, Label9->Caption);
  }

//Automatyczny restart
ppRestartGPUPrompt();
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Button5Click(TObject *Sender)
{
ReadValues(PPTable);
}
//---------------------------------------------------------------------------


void __fastcall TForm3::FormClose(TObject *Sender, TCloseAction &Action)
{
delete PPTable;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::RadioButton1Click(TObject *Sender)
{
Edit2->Text="";
Label2->Caption="";
ParseRegistryTable();
}
//---------------------------------------------------------------------------

void __fastcall TForm3::RadioButton2Click(TObject *Sender)
{
ShowHidePPControls(false);
}
//---------------------------------------------------------------------------
bool ExportPPTableToRegFile(TMemoryStream *ms,String key, String filename)
{
TStringStream *ss=new TStringStream(NULL,TEncoding::Unicode,false);
const String eol="\x0d\x0a";
String str="";
str="Windows Registry Editor Version 5.00"+eol+eol;
str=str+"[HKEY_LOCAL_MACHINE\\"+key+"]"+eol+"\"PP_PhmSoftPowerPlayTable\"=hex:";
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//pierwsza linia  (16 bajt�w)
byte b;
for (int i = 0; i < 16; i++)
 {
  b=ReadValueFromStream(ms,i,1);
  str=str+LowerCase(IntToHex(b,2))+",";
 }
str=str+"\\"+eol+"  ";
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//ka�da kolejna pe�na (25 bajt�w)
int m=0;
for (int i = 16; i < ms->Size; i++)
 {
  b=ReadValueFromStream(ms,i,1);
  if (i!=ms->Size-1)
   {
    str=str+LowerCase(IntToHex(b,2))+",";
   } else
     {
       str=str+LowerCase(IntToHex(b,2));
     }
  m=m+1;
  if (((m%25)==0) && (i!=ms->Size-1))
   {
     str=str+"\\"+eol+"  ";
   }
 }
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//ostatnia
str=str+eol+eol;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Pierwsze dwa bajty to Byte Order Mark (BOM), dodajemy je
ss->Size=2;
int buf=65279;
ss->WriteBuffer(&buf,2);
ss->Position=2;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ss->WriteString(str);
ss->SaveToFile(filename);
delete ss;
return (FileExists(filename));
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button6Click(TObject *Sender)
{
if (GroupBox3->Visible)
 {
  if (ValidateEnteredValues(false)==false)
   {
     return;
   }
 }

if (SaveDialog1->Execute())
 {
   if (GroupBox3->Visible)
    {
     WriteValues(PPTable);
    }
   ExportPPTableToRegFile(PPTable,Const.RegKey,SaveDialog1->FileName);
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button7Click(TObject *Sender)
{
TMemoryStream *ms=new TMemoryStream;
if (LoadPPTableFromRegistry(ms)==0)
 {
   //SaveDialog1->FileName="PP_PhmSoftPowerPlayTable.reg";
   if (SaveDialog1->Execute())
    {
      ExportPPTableToRegFile(ms,Const.RegKey,SaveDialog1->FileName);
    }
 } else
  {
    blad("Failed to read \"PP_PhmSoftPowerPlayTable\" from registry", NULL, false);
  }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Label10MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(RadioButton2->Handle, WM_LBUTTONDOWN, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Label10MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
SendMessage(RadioButton2->Handle, WM_LBUTTONUP, MK_LBUTTON, 0);
}
//---------------------------------------------------------------------------
void JumpTo_RegeditExeKey(String Key, const String Value)
{
void* Wow64FsEnableRedirection;
Wow64DisableWow64FsRedirection(&Wow64FsEnableRedirection); // wy�aczamy przekierowywania (dotyczy TYLKO aktualnego w�tku)
                                                           // dzi�ki temu otwierany regedit jest 64 bitowy
bool LastKeyMethod=false;

try
 {
  HWND hWin=FindWindow(L"RegEdit_RegEdit", NULL);
  if (hWin!=0)
   {
    SendMessage(hWin, WM_CLOSE, NULL, NULL);
    hWin=FindWindow(L"RegEdit_RegEdit", NULL);
   }

  //===write key method
  if (hWin==0 && IsAdmin==false)
   {
    TRegistry* reg = new TRegistry(KEY_WRITE + KEY_READ);
    try
     {
      reg->RootKey=HKEY_CURRENT_USER;
      String LastKey="Software\\Microsoft\\Windows\\CurrentVersion\\Applets\\Regedit";
      if(reg->KeyExists(LastKey))
       {
        if (reg->OpenKey(LastKey, false))
         {
          try
           {
            reg->WriteString("LastKey", Key);
            if (reg->ReadString("LastKey")==Key)
             LastKeyMethod=true;
           } __finally
            {
              reg->CloseKey();
            }
         }
       }
     } __finally
      {
       delete reg;
      }
   }
  //=========

  if (hWin==0)
   {
    TShellExecuteInfo* ExecInfo=new TShellExecuteInfo;
    memset(ExecInfo, 0, sizeof(TShellExecuteInfo));
    ExecInfo->cbSize=60;
    ExecInfo->fMask=SEE_MASK_NOCLOSEPROCESS;
    ExecInfo->lpVerb=L"open";
    ExecInfo->lpFile=L"regedit.exe";
    ExecInfo->nShow=1;
    ShellExecuteEx(ExecInfo);
    WaitForInputIdle(ExecInfo->hProcess, 200);
    hWin=FindWindow(L"RegEdit_RegEdit", NULL);
    delete ExecInfo;
    Sleep(25);
   }
  if (LastKeyMethod==false)
   {
    ShowWindow(hWin, SW_SHOWNORMAL);
    HWND hTree = FindWindowEx(hWin, 0, L"SysTreeView32", NULL);
    SetForegroundWindow(hTree);
    Sleep(25);
    for (int i = 0; i < 30; i++)
     {
      SendMessage(hTree, WM_KEYDOWN, VK_LEFT, 0);
     }
    SendMessage(hTree, WM_KEYDOWN, VK_RIGHT, 0);

    Sleep(25);
    for (int i = 1; i <= Key.Length(); i++)
     {
      if (Key[i]==*L"\\")
       SendMessage(hTree, WM_KEYDOWN, VK_RIGHT, 0); else
        SendMessage(hTree, WM_CHAR, (int)Key[i], 0);
      Sleep(0);
     }
   }
  if (Value!="")
   {
    HWND hList = FindWindowEx(hWin, 0, L"SysListView32", NULL);
    SetForegroundWindow(hList);
    Sleep(25);
    SendMessage(hList, WM_LBUTTONUP, MK_LBUTTON, 0);

    for (int i = 1; i <= Value.Length(); i++)
     {
      SendMessage(hList, WM_CHAR, (int)Value[i], 0);
     }
   }
 } __finally
   {
    Wow64RevertWow64FsRedirection(Wow64FsEnableRedirection); // z powrotem wlaczamy przekierowywania
   }
}
//---------------------------------------------------------------------------
void __fastcall TForm3::Button8Click(TObject *Sender)
{
String value;
if (Pos(" table found",Label9->Caption)>0)
 value="PP_PhmSoftPowerPlayTable";
JumpTo_RegeditExeKey("HKEY_LOCAL_MACHINE\\"+Const.RegKey, value);
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

#ifndef ChangeFriendlyNameH
#define ChangeFriendlyNameH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Grids.hpp>
#include <Vcl.ValEdit.hpp>

//---------------------------------------------------------------------------
const int USER_EDITLISTVIEW = WM_USER + 666;


class TForm4 : public TForm
{
__published:	// IDE-managed Components
	TValueListEditor *ValueListEditor1;
	TLabel *Label1;
	TButton *Button1;
	TButton *Button2;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);


private:	// User declarations
TEdit *ListViewEditor;
TListItem *LItem;
//MESSAGE void __fastcall UserEditListView( TMessage& Message ) USER_EDITLISTVIEW ;
public:		// User declarations
	__fastcall TForm4(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm4 *Form4;
//---------------------------------------------------------------------------
#endif

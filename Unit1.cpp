//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Settings.h"
#include "PowerPlay.h"
#include "ChangeFriendlyName.h"
#include "Reorder.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

//---------------------------------------------------------------------------
const String appversion="0.2.9 beta1";
const TColor ChangedColor=(TColor)0x00D5FFFF;
//---------------------------------------------------------------------------

TForm1 *Form1;

typedef struct OptionSet {
   bool UseI2C;
   bool PNPString;
   bool BusNumber;
   bool Adapter;
   bool FriendlyName;
   bool RegistryKey;
   bool DoNotListUnsupported;
   bool IgnoreError8;
   String FanDeactivatedStr;
   bool IniBackup_AutoCreate;
   bool IniBackup_AutoRestore;
   int  IniBackup_Count;
   bool IniBackup_NoConfirmOnRestore;
   bool IniBackup_LimitNumOfBackups;
   bool CheckValuesMismatch;
   bool AutoReset;
   bool FanResetToAutoMode;
} OptionSet;

OptionSet Options;

bool FanDeactivatedExists=false;     //czy kt�ry� GPU ma zdeaktywowany fan

typedef struct iI2C {
   int Line;
   int Address;
   bool devicefound;
   int typ;    //typ 0 - IR3567R | typ 1 - up9505
} iI2C;

typedef struct pLimits {
 int MinVoltage;
 int MaxVoltage;
 int MaxMemTiming;
} pLimits;

typedef struct GPUItem {
   DynamicArray <int> AdIndexes;
   String DisplayName;
   String PNPString;
   String BusString;
   String IDString;
   String DriverPath;
   String DevID;
   String FriendlyName;
   bool APISupported;
   int GPULevels;
   int MemoryLevels;
   int MaxLevels;
   bool FanEnabled;
   iI2C I2C;
   pLimits Limits;
   ADLODNExtSettings ExtSettingsDefault;
   ADLOD8SingleInitSetting Od8Default[OD8_COUNT];
   bool Od8Caps[OD8_COUNT];
   int APIVersion;
} GPUItem;

DynamicArray <GPUItem> GPU;

typedef struct ProfileItem {
   String Name;
   DynamicArray <int> GPU_Clock;
   DynamicArray <int> GPU_Vcc;
   DynamicArray <bool> GPU_Enabled;
   DynamicArray <int> Mem_Clock;
   DynamicArray <int> Mem_Vcc;
   DynamicArray <bool> Mem_Enabled;
   int Fan_Min;
   int Fan_Max;
   int Fan_Target;
   int Fan_Acoustic;
   int Power_Temp;
   int Power_Target;
   int Offset;               //I2C voltage offset
   int LLC;                 //I2C LLC         1-on 0-off
   String PhaseGain;
   String CurrentScale;
   ADLODNFanCurvePoint FanCurvePoints[5];
   int Mem_TimingLevel;
   int Fan_ZeroRPM;
   int GPU_Min;
   int GPU_Max;
   int Mem_Max;
   int APIVer;
} ProfileItem;

DynamicArray <ProfileItem> Profiles;

ProfileItem CurrentValues;

String INI::inifile;
String INI::backupinifile;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
String AdlErrCodeToString(int ErrCode)
{
switch (ErrCode)
  {
    case 4:   return "ADL_OK_WAIT - All OK, but need to wait";
    case 3:   return "ADL_OK_RESTART - All OK, but need restart";
    case 2:   return "ADL_OK_MODE_CHANGE - All OK, but need mode change";
    case 1:   return "ADL_OK_WARNING - All OK, but with warning";
    case -1:  return "";
    case -2:  return "ADL_ERR_NOT_INIT - ADL not initialized";
    case -3:  return "ADL_ERR_INVALID_PARAM - One of the parameter passed is invalid";
    case -4:  return "ADL_ERR_INVALID_PARAM_SIZE - One of the parameter size is invalid";
    case -5:  return "ADL_ERR_INVALID_ADL_IDX - Invalid ADL index passed";
    case -6:  return "ADL_ERR_INVALID_CONTROLLER_IDX - Invalid controller index passed";
    case -7:  return "ADL_ERR_INVALID_DIPLAY_IDX - Invalid display index passed";
    case -8:  return "ADL_ERR_NOT_SUPPORTED - Function not supported by the driver";
    case -9:  return "ADL_ERR_NULL_POINTER - Null Pointer error";
    case -10: return "ADL_ERR_DISABLED_ADAPTER - Call can't be made due to disabled adapter";
    case -11: return "ADL_ERR_INVALID_CALLBACK - Invalid Callback";
    case -12: return "ADL_ERR_RESOURCE_CONFLICT - Display Resource conflict";
    case -20: return "ADL_ERR_SET_INCOMPLETE - Failed to update some of the values. Can be returned by set request that include multiple values if not all values were successfully committed";
    case -21: return "ADL_ERR_NO_XDISPLAY - There's no Linux XDisplay in Linux Console environment";
  }
return "";
}
//---------------------------------------------------------------------------
String FormatErrorMessage(String msg, int ErrCode, bool WindowsError)
{
if (ErrCode!=NULL)
  {
    String str;
    if (WindowsError)
      {
       Char* lpMsgBuf=NULL;
       FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, ErrCode, LANG_NEUTRAL | (SUBLANG_DEFAULT << 10), (PChar)(&lpMsgBuf), 0, NULL);
       str=lpMsgBuf;
       LocalFree(lpMsgBuf);
       if (str!="")
         {
           str=StringReplace(str, '\x0d', "", TReplaceFlags() << rfReplaceAll);
           str=StringReplace(str, '\x0a', "", TReplaceFlags() << rfReplaceAll);
         }
      } else
         {
           str=AdlErrCodeToString(ErrCode);
         }
    if (str=="")
     {
       msg=msg+" (ErrorCode: "+IntToStr(ErrCode)+")";
     } else
        {
           msg=msg+" (ErrorCode: "+IntToStr(ErrCode)+") ("+str+")";
        }
  }
return msg;
}

//---------------------------------------------------------------------------
void blad(String msg, int ErrCode, bool WindowsError)
{
if ((Options.IgnoreError8) && (ErrCode==ADL_ERR_NOT_SUPPORTED) && (WindowsError==false))
 {
   return;
 }
msg=FormatErrorMessage(msg, ErrCode, WindowsError);
MessageBox(0, msg.w_str(), L"OverdriveNTool", MB_ICONERROR|MB_OK|MB_TOPMOST);
}
//---------------------------------------------------------------------------
//0-error  //1-info
bool LogWrite(String msg, int ErrCode, bool WindowsError, int typ)
{
if (!FileExists("OverdriveNTool.log"))
 {
  TFileStream *fs=new TFileStream("OverdriveNTool.log", fmCreate);
  delete fs;
 }

if (typ==0)
 {
   msg=FormatErrorMessage(msg, ErrCode, WindowsError);
   msg="ERROR: "+msg;
 }

TFileStream *fs=new TFileStream("OverdriveNTool.log", fmOpenWrite | fmShareDenyNone);
try
 {
   msg=DateToStr(Date())+" "+TimeToStr(Time())+" - "+msg;
   fs->Seek(0,soFromEnd);
   AnsiString st=msg+"\x0d\x0a";
   fs->Write((void*)st.c_str(),st.Length());
 }
  __finally
  {
    delete fs;
  }

return true;
}

//---------------------------------------------------------------------------

bool LogWriteSimple(String msg)
{
if (!FileExists("OverdriveNTool.log"))
 {
  TFileStream *fs=new TFileStream("OverdriveNTool.log", fmCreate);
  delete fs;
 }
TFileStream *fs=new TFileStream("OverdriveNTool.log", fmOpenWrite | fmShareDenyNone);
try
 {
   fs->Seek(0,soFromEnd);
   AnsiString st=msg+"\x0d\x0a";
   fs->Write((void*)st.c_str(),st.Length());
 }
  __finally
  {
    delete fs;
  }

return true;
}

//---------------------------------------------------------------------------

typedef int (*ADL2_MAIN_CONTROL_CREATE)(ADL_MAIN_MALLOC_CALLBACK, int, ADL_CONTEXT_HANDLE*);
typedef int (*ADL2_MAIN_CONTROL_DESTROY)(ADL_CONTEXT_HANDLE);
typedef int (*ADL2_ADAPTER_NUMBEROFADAPTERS_GET)(ADL_CONTEXT_HANDLE, int*);
typedef int (*ADL2_ADAPTER_ADAPTERINFO_GET)(ADL_CONTEXT_HANDLE, LPAdapterInfo, int );
typedef int (*ADL2_OVERDRIVEN_CAPABILITIESX2_GET)(ADL_CONTEXT_HANDLE, int, ADLODNCapabilitiesX2*);
typedef int(*ADL2_OVERDRIVE_CAPS) (ADL_CONTEXT_HANDLE, int, int*, int*, int*);

typedef int (*ADL2_OVERDRIVEN_SYSTEMCLOCKSX2_GET)(ADL_CONTEXT_HANDLE, int, ADLODNPerformanceLevelsX2*);
typedef int (*ADL2_OVERDRIVEN_SYSTEMCLOCKSX2_SET)(ADL_CONTEXT_HANDLE, int, ADLODNPerformanceLevelsX2*);
typedef int (*ADL2_OVERDRIVEN_MEMORYCLOCKSX2_GET)(ADL_CONTEXT_HANDLE, int, ADLODNPerformanceLevelsX2*);
typedef int (*ADL2_OVERDRIVEN_MEMORYCLOCKSX2_SET)(ADL_CONTEXT_HANDLE, int, ADLODNPerformanceLevelsX2*);

typedef int (*ADL2_OVERDRIVEN_FANCONTROL_GET)(ADL_CONTEXT_HANDLE, int, ADLODNFanControl*);
typedef int (*ADL2_OVERDRIVEN_FANCONTROL_SET)(ADL_CONTEXT_HANDLE, int, ADLODNFanControl*);
typedef int (*ADL2_OVERDRIVEN_POWERLIMIT_GET)(ADL_CONTEXT_HANDLE, int, ADLODNPowerLimitSetting*);
typedef int (*ADL2_OVERDRIVEN_POWERLIMIT_SET)(ADL_CONTEXT_HANDLE, int, ADLODNPowerLimitSetting*);
typedef int (*ADL2_DISPLAY_WRITEANDREADI2C) (ADL_CONTEXT_HANDLE, int i, ADLI2C * plI2C);
//typedef int ( *ADL2_GRAPHICS_VERSIONX2_GET)(ADL_CONTEXT_HANDLE context, ADLVersionsInfoX2 * lpVersionsInfo);
typedef int (*ADL2_ADAPTER_ACTIVE_GET ) (ADL_CONTEXT_HANDLE, int, int* );

typedef int (*ADL2_OVERDRIVEN_SETTINGSEXT_GET) (ADL_CONTEXT_HANDLE , int, int*, int *, ADLODNExtSingleInitSetting**, int**);
typedef int (*ADL2_OVERDRIVEN_SETTINGSEXT_SET) (ADL_CONTEXT_HANDLE, int, int, int*, int*);

typedef int(*ADL2_OVERDRIVE8_SETTING_SET) (ADL_CONTEXT_HANDLE, int, ADLOD8SetSetting*, ADLOD8CurrentSetting*);
typedef int(*ADL2_OVERDRIVE8_INIT_SETTINGX2_GET) (ADL_CONTEXT_HANDLE context, int iAdapterIndex, int* lpOverdrive8Capabilities, int *lpNumberOfFeatures, ADLOD8SingleInitSetting** lppInitSettingList);
typedef int(*ADL2_OVERDRIVE8_CURRENT_SETTINGX2_GET) (ADL_CONTEXT_HANDLE context, int iAdapterIndex, int *lpNumberOfFeatures, int** lppCurrentSettingList);

//---

HINSTANCE hDLL=NULL;
ADL_CONTEXT_HANDLE context = NULL;

ADL2_MAIN_CONTROL_CREATE	   ADL2_Main_Control_Create = NULL;
ADL2_MAIN_CONTROL_DESTROY	   ADL2_Main_Control_Destroy = NULL;
ADL2_ADAPTER_NUMBEROFADAPTERS_GET  ADL2_Adapter_NumberOfAdapters_Get = NULL;
ADL2_ADAPTER_ADAPTERINFO_GET       ADL2_Adapter_AdapterInfo_Get = NULL;
ADL2_OVERDRIVEN_CAPABILITIESX2_GET ADL2_OverdriveN_CapabilitiesX2_Get = NULL;
ADL2_OVERDRIVE_CAPS              ADL2_Overdrive_Caps = NULL;

ADL2_OVERDRIVEN_SYSTEMCLOCKSX2_GET ADL2_OverdriveN_SystemClocksX2_Get = NULL;
ADL2_OVERDRIVEN_SYSTEMCLOCKSX2_SET ADL2_OverdriveN_SystemClocksX2_Set = NULL;
ADL2_OVERDRIVEN_MEMORYCLOCKSX2_GET ADL2_OverdriveN_MemoryClocksX2_Get = NULL;
ADL2_OVERDRIVEN_MEMORYCLOCKSX2_GET ADL2_OverdriveN_MemoryClocksX2_Set = NULL;

ADL2_OVERDRIVEN_FANCONTROL_GET     ADL2_OverdriveN_FanControl_Get =NULL;
ADL2_OVERDRIVEN_FANCONTROL_SET     ADL2_OverdriveN_FanControl_Set=NULL;
ADL2_OVERDRIVEN_POWERLIMIT_GET     ADL2_OverdriveN_PowerLimit_Get =NULL;
ADL2_OVERDRIVEN_POWERLIMIT_SET     ADL2_OverdriveN_PowerLimit_Set=NULL;
ADL2_DISPLAY_WRITEANDREADI2C       ADL2_Display_WriteAndReadI2C = NULL;
//ADL2_GRAPHICS_VERSIONX2_GET        ADL2_Graphics_VersionsX2_Get = NULL;
ADL2_ADAPTER_ACTIVE_GET		   ADL2_Adapter_Active_Get=NULL;

ADL2_OVERDRIVEN_SETTINGSEXT_GET ADL2_OverdriveN_SettingsExt_Get=NULL;
ADL2_OVERDRIVEN_SETTINGSEXT_SET ADL2_OverdriveN_SettingsExt_Set=NULL;


ADL2_OVERDRIVE8_SETTING_SET ADL2_Overdrive8_Setting_Set = NULL;
ADL2_OVERDRIVE8_INIT_SETTINGX2_GET ADL2_Overdrive8_Init_SettingX2_Get = NULL;
ADL2_OVERDRIVE8_CURRENT_SETTINGX2_GET ADL2_Overdrive8_Current_SettingX2_Get = NULL;

//---
//---------------------------------------------------------------------------
//Memory allocation Callback
void* __stdcall ADL_Main_Memory_Alloc (int iSize)
{
void* lpBuffer = malloc(iSize);
return lpBuffer;
}
//---------------------------------------------------------------------------
// Optional Memory de-allocation function
void __stdcall ADL_Main_Memory_Free(void** lpBuffer)
{
    if (NULL != *lpBuffer)
    {
        free(*lpBuffer);
        *lpBuffer = NULL;
    };
}
//---------------------------------------------------------------------------
bool InitializeADL()
{
hDLL=LoadLibrary(L"atiadlxx.dll");
if (hDLL==NULL)
 {
   hDLL=LoadLibrary(L"atiadlxy.dll");
 }

if (hDLL==NULL)
 {
   blad("Failed to load ADL library", GetLastError(), true);
   return false;
 }
ADL2_Main_Control_Create = (ADL2_MAIN_CONTROL_CREATE) GetProcAddress(hDLL,"ADL2_Main_Control_Create");
ADL2_Main_Control_Destroy = (ADL2_MAIN_CONTROL_DESTROY) GetProcAddress(hDLL,"ADL2_Main_Control_Destroy");
ADL2_Adapter_NumberOfAdapters_Get = (ADL2_ADAPTER_NUMBEROFADAPTERS_GET) GetProcAddress(hDLL,"ADL2_Adapter_NumberOfAdapters_Get");
ADL2_Adapter_AdapterInfo_Get = (ADL2_ADAPTER_ADAPTERINFO_GET) GetProcAddress(hDLL,"ADL2_Adapter_AdapterInfo_Get");
ADL2_OverdriveN_CapabilitiesX2_Get = (ADL2_OVERDRIVEN_CAPABILITIESX2_GET) GetProcAddress (hDLL, "ADL2_OverdriveN_CapabilitiesX2_Get");
ADL2_Overdrive_Caps = (ADL2_OVERDRIVE_CAPS)GetProcAddress(hDLL, "ADL2_Overdrive_Caps");

ADL2_OverdriveN_SystemClocksX2_Get = (ADL2_OVERDRIVEN_SYSTEMCLOCKSX2_GET) GetProcAddress (hDLL, "ADL2_OverdriveN_SystemClocksX2_Get");
ADL2_OverdriveN_SystemClocksX2_Set = (ADL2_OVERDRIVEN_SYSTEMCLOCKSX2_SET) GetProcAddress (hDLL, "ADL2_OverdriveN_SystemClocksX2_Set");
ADL2_OverdriveN_MemoryClocksX2_Get = (ADL2_OVERDRIVEN_MEMORYCLOCKSX2_GET) GetProcAddress (hDLL, "ADL2_OverdriveN_MemoryClocksX2_Get");
ADL2_OverdriveN_MemoryClocksX2_Set = (ADL2_OVERDRIVEN_MEMORYCLOCKSX2_SET) GetProcAddress (hDLL, "ADL2_OverdriveN_MemoryClocksX2_Set");

ADL2_OverdriveN_FanControl_Get = (ADL2_OVERDRIVEN_FANCONTROL_GET) GetProcAddress (hDLL, "ADL2_OverdriveN_FanControl_Get");
ADL2_OverdriveN_FanControl_Set = (ADL2_OVERDRIVEN_FANCONTROL_SET) GetProcAddress (hDLL, "ADL2_OverdriveN_FanControl_Set");
ADL2_OverdriveN_PowerLimit_Get = (ADL2_OVERDRIVEN_POWERLIMIT_GET) GetProcAddress (hDLL, "ADL2_OverdriveN_PowerLimit_Get");
ADL2_OverdriveN_PowerLimit_Set = (ADL2_OVERDRIVEN_POWERLIMIT_SET) GetProcAddress (hDLL, "ADL2_OverdriveN_PowerLimit_Set");
ADL2_Display_WriteAndReadI2C = (ADL2_DISPLAY_WRITEANDREADI2C) GetProcAddress(hDLL,"ADL2_Display_WriteAndReadI2C");
//ADL2_Graphics_VersionsX2_Get = (ADL2_GRAPHICS_VERSIONX2_GET)GetProcAddress(hDLL, "ADL2_Graphics_VersionsX2_Get");
ADL2_Adapter_Active_Get = (ADL2_ADAPTER_ACTIVE_GET) GetProcAddress(hDLL, "ADL2_Adapter_Active_Get");

ADL2_OverdriveN_SettingsExt_Get=(ADL2_OVERDRIVEN_SETTINGSEXT_GET) GetProcAddress(hDLL, "ADL2_OverdriveN_SettingsExt_Get");
ADL2_OverdriveN_SettingsExt_Set=(ADL2_OVERDRIVEN_SETTINGSEXT_SET) GetProcAddress(hDLL, "ADL2_OverdriveN_SettingsExt_Set");

ADL2_Overdrive8_Setting_Set = (ADL2_OVERDRIVE8_SETTING_SET)GetProcAddress(hDLL, "ADL2_Overdrive8_Setting_Set");
ADL2_Overdrive8_Init_SettingX2_Get = (ADL2_OVERDRIVE8_INIT_SETTINGX2_GET)GetProcAddress(hDLL, "ADL2_Overdrive8_Init_SettingX2_Get");
ADL2_Overdrive8_Current_SettingX2_Get = (ADL2_OVERDRIVE8_CURRENT_SETTINGX2_GET)GetProcAddress(hDLL, "ADL2_Overdrive8_Current_SettingX2_Get");

//---

if (   NULL == ADL2_Main_Control_Create ||
       NULL == ADL2_Main_Control_Destroy ||
       NULL == ADL2_Adapter_NumberOfAdapters_Get||
       NULL == ADL2_Adapter_AdapterInfo_Get ||
       NULL == ADL2_OverdriveN_CapabilitiesX2_Get ||
       NULL == ADL2_Overdrive_Caps ||

       NULL == ADL2_OverdriveN_SystemClocksX2_Get ||
       NULL == ADL2_OverdriveN_SystemClocksX2_Set ||
       NULL == ADL2_OverdriveN_MemoryClocksX2_Get ||
       NULL == ADL2_OverdriveN_MemoryClocksX2_Set ||

       NULL == ADL2_OverdriveN_FanControl_Get ||
       NULL == ADL2_OverdriveN_FanControl_Set ||
       NULL == ADL2_OverdriveN_PowerLimit_Get ||
       NULL == ADL2_OverdriveN_PowerLimit_Set ||
       //NULL == ADL2_Graphics_VersionsX2_Get ||
       NULL == ADL2_Display_WriteAndReadI2C ||
       NULL == ADL2_Adapter_Active_Get ||

       NULL == ADL2_OverdriveN_SettingsExt_Get ||
       NULL == ADL2_OverdriveN_SettingsExt_Set ||

       NULL == ADL2_Overdrive8_Setting_Set ||
       NULL == ADL2_Overdrive8_Init_SettingX2_Get ||
       NULL == ADL2_Overdrive8_Current_SettingX2_Get
       //---
   )
    {
     if (NULL == ADL2_Main_Control_Create || NULL == ADL2_Main_Control_Destroy)  //General error
      {
        blad("Failed to get ADL function pointers", GetLastError(), true);
      } else                                                                     //Missing functions
       {
         String str;
         if (NULL == ADL2_Main_Control_Create) str+="ADL2_Main_Control_Create\n";
         if (NULL == ADL2_Main_Control_Destroy) str+="ADL2_Main_Control_Destroy\n";
         if (NULL == ADL2_Adapter_NumberOfAdapters_Get) str+="ADL2_Adapter_NumberOfAdapters_Get\n";
         if (NULL == ADL2_Adapter_AdapterInfo_Get) str+="ADL2_Adapter_AdapterInfo_Get\n";
         if (NULL == ADL2_OverdriveN_CapabilitiesX2_Get) str+="ADL2_OverdriveN_CapabilitiesX2_Get\n";
         if (NULL == ADL2_Overdrive_Caps) str+="ADL2_Overdrive_Caps\n";

         if (NULL == ADL2_OverdriveN_SystemClocksX2_Get) str+="ADL2_OverdriveN_SystemClocksX2_Get (17.7.2 driver or newer)\n";     //17.7.2 required
         if (NULL == ADL2_OverdriveN_SystemClocksX2_Set) str+="ADL2_OverdriveN_SystemClocksX2_Set (17.7.2 driver or newer)\n";     //17.7.2 required
         if (NULL == ADL2_OverdriveN_MemoryClocksX2_Get) str+="ADL2_OverdriveN_MemoryClocksX2_Get (17.7.2 driver or newer)\n";     //17.7.2 required
         if (NULL == ADL2_OverdriveN_MemoryClocksX2_Set) str+="ADL2_OverdriveN_MemoryClocksX2_Set (17.7.2 driver or newer)\n";     //17.7.2 required

         if (NULL == ADL2_OverdriveN_FanControl_Get) str+="ADL2_OverdriveN_FanControl_Get\n";
         if (NULL == ADL2_OverdriveN_FanControl_Set) str+="ADL2_OverdriveN_FanControl_Set\n";
         if (NULL == ADL2_OverdriveN_PowerLimit_Get) str+="ADL2_OverdriveN_PowerLimit_Get\n";
         if (NULL == ADL2_OverdriveN_PowerLimit_Set) str+="ADL2_OverdriveN_PowerLimit_Set\n";
       //  if (NULL == ADL2_Graphics_VersionsX2_Get) str+="ADL2_Graphics_VersionsX2_Get\n";
         if (NULL == ADL2_Display_WriteAndReadI2C) str+="ADL2_Display_WriteAndReadI2C\n";
         if (NULL == ADL2_Adapter_Active_Get) str+="ADL2_Adapter_Active_Get\n";

         if (NULL == ADL2_OverdriveN_SettingsExt_Get) str+="ADL2_OverdriveN_SettingsExt_Get (18.12.2 driver or newer)\n";         //18.12.2 required
         if (NULL == ADL2_OverdriveN_SettingsExt_Set) str+="ADL2_OverdriveN_SettingsExt_Set (18.12.2 driver or newer)\n";         //18.12.2 required

         if (NULL == ADL2_Overdrive8_Setting_Set) str+="ADL2_Overdrive8_Setting_Set\n";
         if (NULL == ADL2_Overdrive8_Init_SettingX2_Get) str+="ADL2_Overdrive8_Init_SettingX2_Get\n";
         if (NULL == ADL2_Overdrive8_Current_SettingX2_Get) str+="ADL2_Overdrive8_Current_SettingX2_Get\n";

         blad("Failed to get ADL function pointers.\nMissing functions:\n"+str, GetLastError(), true);
       }
     FreeLibrary(hDLL);
     return false;
    }
int an=ADL2_Main_Control_Create(ADL_Main_Memory_Alloc, 1, &context);
if (ADL_OK != an)
 {
   blad("Failed to initialize ADL context", an, false);
   FreeLibrary(hDLL);
   return false;
 }

return true;
}
//---------------------------------------------------------------------------
void DeinitializeADL()
{
ADL2_Main_Control_Destroy(context);
FreeLibrary(hDLL);
//hDLL=NULL;
}
//---------------------------------------------------------------------------
//0-ok, -1 inicjalizacja ADL si� nie powiod�a, -2 stary driver (przed 17.7.2),
//-3 brak plik�w atiadlxx.dll atiadlxy.dll
int VerifyADL_Library()
{
hDLL=LoadLibrary(L"atiadlxx.dll");
if (hDLL==NULL)
 hDLL=LoadLibrary(L"atiadlxy.dll");
if (hDLL==NULL)
  {
    TCHAR infoBuf[MAX_PATH];
    if (GetSystemDirectory(infoBuf, MAX_PATH)>3)
      {
        String str=String(infoBuf)+"\\atiadlxx.dll";
        bool isWow64 = false;
        if (IsWow64Process(GetCurrentProcess(),(int*)&isWow64))
         {
            if (isWow64)
             {
               str=StringReplace(str, "\\System32", "\\SysWOW64", TReplaceFlags() << rfIgnoreCase);
             }
            str=StringReplace(str, "\\windows", "\\Windows",TReplaceFlags() << rfReplaceAll );
            void* rd;
            Wow64DisableWow64FsRedirection(&rd);
            if (FileExists(str)==false)
             {
               blad("Required driver file: "+str+" not found.", NULL, false);
               Wow64RevertWow64FsRedirection(rd);
               return -3;
             }
            Wow64RevertWow64FsRedirection(rd);
         }  else
           {
             blad("IsWow64Process function failed", GetLastError(), true);
             return -1;
           }
      } else
        {
           blad("GetSystemDirectory function failed", GetLastError(), true);
           return -1;
        }
    return -1;
  } else
    {
       if (NULL==GetProcAddress (hDLL, "ADL2_OverdriveN_SettingsExt_Get"))
         {
           FreeLibrary(hDLL);
           return -2;
         } else
           {
             FreeLibrary(hDLL);
           }
    }
return 0;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//sprawdza czy zainstalowano sterownik, wymaga minimum windows 7;
bool CheckIfDriverInstalled()
{
//82: atikmpag.sys
//83: atikmdag.sys
LPVOID drivers[1024];
DWORD cbNeeded;
if (EnumDeviceDrivers(drivers, sizeof(drivers), &cbNeeded) && (cbNeeded < sizeof(drivers)))
   {
     TCHAR szDriver[128];
     int cDrivers = cbNeeded / sizeof(drivers[0]);
     const int szSize=sizeof(szDriver) / sizeof(szDriver[0]);
     for (int i=0; i < cDrivers; i++ )               // atikmpag.sys - AMD multi-vendor Miniport Driver
       {                                             // atikmdag.sys - ATI Radeon Kernel Mode Driver //ten sprawdzamy
         if (12==GetDeviceDriverBaseName(drivers[i], szDriver, szSize))  //"atikmdag.sys" ma 12 znak�w
          {
            if (((String)szDriver=="atikmdag.sys") || ((String)szDriver=="amdkmdag.sys"))
             return true;  //sterownik odnaleziony
          }
       }
   } else
      {
        blad("EnumDeviceDrivers function failed", GetLastError(), true);
        return true;
      }

return false;      //nie odnaleziono
}
//---------------------------------------------------------------------------
String Odnt::FormatGPUDisplayName(int GpuInd, bool IncludeFrName)
{
String str=GPU[GpuInd].IDString;
try
 {
  if (IncludeFrName)
   {
     str=str+"  ("+GPU[GpuInd].FriendlyName+")";
   }
  if (Options.BusNumber || Options.Adapter || Options.RegistryKey)
   {
     str=str+"  (";
     String sub="";
     if (Options.BusNumber)
      {
       if (Options.PNPString)
        {
          sub="Bus:"+GPU[GpuInd].BusString;
        } else
          {
            sub="Bus: "+GPU[GpuInd].BusString;
          }
      }
     if (Options.Adapter)
      {
       if (Options.PNPString)
         {
           if (sub!="")
            {
              sub=sub+"|";
            }
           sub=sub+"Adapter:"+IntToStr(GPU[GpuInd].AdIndexes[0]);
         } else
            {
              if (sub!="")
               {
                sub=sub+" | ";
               }
              sub=sub+"Adapter: "+IntToStr(GPU[GpuInd].AdIndexes[0]);
            }
      }
     if (Options.RegistryKey)
      {
       String drv=GPU[GpuInd].DriverPath;
       drv=drv.Delete(1,Pos("}\\",drv)+1);
       if (Options.PNPString)
         {
           if (sub!="")
            {
              sub=sub+"|";
            }
           sub=sub+"RegKey:"+drv;
         } else
            {
              if (sub!="")
               {
                 sub=sub+" | ";
               }
              sub=sub+"RegKey: "+drv;
            }
      }
     str=str+sub+")";
     if (Options.PNPString)
      {
        str=str+" "+ GPU[GpuInd].PNPString;
      }
   } else
     {
      if (Options.PNPString)
        {
          str=str+"  ("+ GPU[GpuInd].PNPString+")";
        }
     }
 }
 __except(EXCEPTION_EXECUTE_HANDLER)
  {
    return GPU[GpuInd].IDString;
  }
return str;
}
//---------------------------------------------------------------------------
struct busitem {
  DynamicArray <int> adapters;
  int PciLoc;
  String name;
  String subsys;
  String devid;
  int VendorId;
  String DriverPath;
  String BusStr;
};
#define AMDVENDORID 1002
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

bool FindGPUs()
{

if (InitializeADL())
 {
  int iNumberAdapters;
  int an=ADL2_Adapter_NumberOfAdapters_Get(context, &iNumberAdapters);
  if (ADL_OK != an)
    {
      blad("Failed to get adapters count", an, false);
      DeinitializeADL();
      return false;
    }
  //############################################################################
  if (iNumberAdapters>0)
    {
     LPAdapterInfo lpAdapterInfo = (LPAdapterInfo)malloc(sizeof(AdapterInfo)* iNumberAdapters);
     memset(lpAdapterInfo, '\0', sizeof (AdapterInfo)* iNumberAdapters);
     an=ADL2_Adapter_AdapterInfo_Get(context, lpAdapterInfo, sizeof(AdapterInfo)* iNumberAdapters);
     if (ADL_OK != an)
      {
        blad("Failed to get adapter info", an, false);
        DeinitializeADL();
        return false;
      }

     //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     //Set Pci location for each adapter
     DynamicArray <u_int> PCI_Loc;
     for (int i = 0; i < iNumberAdapters; i++)
       {
        if (lpAdapterInfo[i].iBusNumber>-1)
          {

             PCI_Loc.Length++;
             PCI_Loc[i]=(lpAdapterInfo[i].iBusNumber << 16) | (lpAdapterInfo[i].iDeviceNumber << 8) | lpAdapterInfo[i].iFunctionNumber;
          }
       }

     //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

     DynamicArray <busitem> BusGpu;
     String PhysicalAdapters=";";
     for (int i = 0; i < iNumberAdapters; i++)
      {
       if (lpAdapterInfo[i].iBusNumber>-1)
         {
             if (Pos(";"+UIntToStr(PCI_Loc[i])+";",PhysicalAdapters)==0)
              {
                 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                 //tu kod po znalezieniu adaptera
                 PhysicalAdapters=PhysicalAdapters+UIntToStr(PCI_Loc[i])+";";
                 //*******************************
                 //szukamy pierwszego aktywnego adaptera:
                 DynamicArray <int> iAdapters;

                 iAdapters.Length++;
                 iAdapters[iAdapters.Length-1]=lpAdapterInfo[i].iAdapterIndex;
                 //*******************************

                 //insertujemy w odpowienie miejsce (Sortowanie po PCI location)-++++
                 int position=0;
                 for (int k = 0; k < BusGpu.Length; k++)
                  {
                   if (PCI_Loc[i]>BusGpu[k].PciLoc) position=k+1;
                  }
                 int count=BusGpu.Length;
                 BusGpu.Length++;
                 for (int m = count; m > position; m--)
                  {
                   BusGpu[m]= BusGpu[m-1];
                  }
                 BusGpu[position].PciLoc=PCI_Loc[i];
                 BusGpu[position].adapters=iAdapters;
                 BusGpu[position].name=lpAdapterInfo[i].strAdapterName;
                 BusGpu[position].VendorId=lpAdapterInfo[i].iVendorID;
                 BusGpu[position].devid=lpAdapterInfo[i].strPNPString;
                 BusGpu[position].subsys=lpAdapterInfo[i].strPNPString;
                 BusGpu[position].subsys.Delete(1,Pos(("&SUBSYS"),BusGpu[position].subsys));
                 BusGpu[position].DriverPath=lpAdapterInfo[i].strDriverPathExt;
                 BusGpu[position].BusStr=IntToStr(lpAdapterInfo[i].iBusNumber)+":"+IntToStr(lpAdapterInfo[i].iDeviceNumber)+":"+IntToStr(lpAdapterInfo[i].iFunctionNumber);
                 //insertujemy w odpowiednie miejsce ----------------------------++++
              }
         }
      }
     free(lpAdapterInfo);
     //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     for (int i = 0; i < BusGpu.Length; i++)
       {
         //*******************************
         GPU.Length++;
         GPU[GPU.Length-1].AdIndexes.Length=BusGpu[i].adapters.Length;
         GPU[GPU.Length-1].AdIndexes=BusGpu[i].adapters;
         GPU[GPU.Length-1].IDString=IntToStr(GPU.Length-1)+": "+BusGpu[i].name;
         GPU[GPU.Length-1].PNPString= BusGpu[i].subsys;
         GPU[GPU.Length-1].BusString=BusGpu[i].BusStr;
         GPU[GPU.Length-1].FriendlyName="";
         GPU[GPU.Length-1].DriverPath=BusGpu[i].DriverPath;
         GPU[GPU.Length-1].DevID=BusGpu[i].devid;
         GPU[GPU.Length-1].DisplayName=Odnt.FormatGPUDisplayName(GPU.Length-1, false);
         GPU[GPU.Length-1].APISupported=false;
         GPU[GPU.Length-1].APIVersion=0;
         GPU[GPU.Length-1].GPULevels=0;
         GPU[GPU.Length-1].MemoryLevels=0;
         GPU[GPU.Length-1].MaxLevels=0;
         GPU[GPU.Length-1].FanEnabled=true;
         GPU[GPU.Length-1].I2C.devicefound=false;
         GPU[GPU.Length-1].I2C.Line=-1;
         GPU[GPU.Length-1].I2C.Address=-1;
         GPU[GPU.Length-1].I2C.typ=-1;
         GPU[GPU.Length-1].Limits.MaxMemTiming=0;
         if (Options.FanDeactivatedStr!="")
          {
            if (Pos(";"+IntToStr(GPU.Length-1)+";",Options.FanDeactivatedStr)>0)
             {
               GPU[GPU.Length-1].FanEnabled=false;
               FanDeactivatedExists=true;
             }
          }




         //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         //Detect API supported and API Version
         try
          {
           if (BusGpu[i].VendorId==AMDVENDORID)
             {

               //===============================================================
               //API 7 (OverdriveN)=============================================
               //===============================================================
               ADLODNCapabilitiesX2 overdriveCapabilities;
               memset(&overdriveCapabilities, 0, sizeof(ADLODNCapabilitiesX2));
               if (ADL_OK == ADL2_OverdriveN_CapabilitiesX2_Get(context, GPU[GPU.Length-1].AdIndexes[0], &overdriveCapabilities))
                {
                  GPU[GPU.Length-1].MaxLevels=overdriveCapabilities.iMaximumNumberOfPerformanceLevels;
                  GPU[GPU.Length-1].Limits.MinVoltage=overdriveCapabilities.svddcRange.iMin;
                  GPU[GPU.Length-1].Limits.MaxVoltage=overdriveCapabilities.svddcRange.iMax;

                  if (GPU[GPU.Length-1].MaxLevels>0)
                    {
                      //GPU
                      const int size=sizeof(ADLODNPerformanceLevelsX2) + sizeof(ADLODNPerformanceLevelX2)*(GPU[GPU.Length-1].MaxLevels - 1);
                      ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
                      pL->iSize = size;
                      pL->iNumberOfPerformanceLevels = GPU[GPU.Length-1].MaxLevels;
                      if (ADL_OK == ADL2_OverdriveN_SystemClocksX2_Get(context, GPU[GPU.Length-1].AdIndexes[0], pL))
                        {
                          for (int j=pL->iNumberOfPerformanceLevels; j > 0; j--)
                            {
                              if (((pL->aLevels[j-1].iClock/100)>0) && (((pL->aLevels[j-1].iVddc)>0)))
                                {
                                  GPU[GPU.Length-1].GPULevels=j;
                                  break;
                                }
                            }
                          //Memory
                          memset(pL, 0, size);
                          pL->iSize = size;
                          pL->iNumberOfPerformanceLevels = GPU[GPU.Length-1].MaxLevels;
                          if (ADL_OK == ADL2_OverdriveN_MemoryClocksX2_Get(context, GPU[GPU.Length-1].AdIndexes[0], pL))
                           {
                             for (int j=0; j < pL->iNumberOfPerformanceLevels; j++)
                              {
                                if (((pL->aLevels[j].iClock/100)>0) && (((pL->aLevels[j].iVddc)>0)))
                                  {
                                    GPU[GPU.Length-1].MemoryLevels=j+1;
                                  } else break;
                              }
                             if ((GPU[GPU.Length-1].GPULevels>0) && (GPU[GPU.Length-1].MemoryLevels>0))
                              {
                                GPU[GPU.Length-1].APISupported=true;
                                GPU[GPU.Length-1].APIVersion=7;

                                //===========
                                //OverdriveN SettingsExt default:
                                ADLODNExtSettingsInfo *ExtInfo=new  ADLODNExtSettingsInfo;
                                if (OverdriveN.ExtSettings_Get(GPU.Length-1, ExtInfo, NULL, false))
                                 {
                                  GPU[GPU.Length-1].Limits.MaxMemTiming=ExtInfo->MemTimingLevel.maxValue;
                                  //Fan curve default:
                                  for (int i = 0; i < 5; i++)
                                   {
                                    GPU[GPU.Length-1].ExtSettingsDefault.Point[i].Temp=ExtInfo->Point[i].Temp.defaultValue;
                                    GPU[GPU.Length-1].ExtSettingsDefault.Point[i].Percentage=ExtInfo->Point[i].Percentage.defaultValue;
                                   }
                                  GPU[GPU.Length-1].ExtSettingsDefault.MemTimingLevel=ExtInfo->MemTimingLevel.defaultValue;
                                  GPU[GPU.Length-1].ExtSettingsDefault.ZeroRPM=ExtInfo->ZeroRPM.defaultValue;
                                 }
                                delete ExtInfo;
                                //===========
                              }
                           }
                        }
                      free(pL);
                    }
                }

               //===============================================================
               //API 8 (Overdrive8)=============================================
               //===============================================================


               if (GPU[GPU.Length-1].APISupported==false)
                {
                 int iSupported=0;
                 int iEnabled=0;
                 int iVersion=0;
                 if (ADL_OK == ADL2_Overdrive_Caps(context, GPU[GPU.Length-1].AdIndexes[0], &iSupported, &iEnabled, &iVersion))
                  {
                   if (iVersion==8)
                    {
                     int numberOfFeaturesCurrent = OD8_COUNT;
                     int* lpCurrentSettingList = NULL;
                     if (ADL_OK == ADL2_Overdrive8_Current_SettingX2_Get(context, GPU[GPU.Length-1].AdIndexes[0], &numberOfFeaturesCurrent, &lpCurrentSettingList))
                      {
                       if (lpCurrentSettingList!=NULL)
                        {
                          GPU[GPU.Length-1].APISupported=true;
                          GPU[GPU.Length-1].APIVersion=8;
                          GPU[GPU.Length-1].GPULevels=3;
                          GPU[GPU.Length-1].MemoryLevels=0;
                          GPU[GPU.Length-1].MaxLevels=3;

                          ADL_Main_Memory_Free((void**)&lpCurrentSettingList);

                          //Overdrive8 defaults & MemTimingLevel max value
                          if (Overdrive8.GetValues(GPU.Length-1, (ADLOD8SingleInitSetting*)&GPU[GPU.Length-1].Od8Default, NULL, false))
                           {
                            GPU[GPU.Length-1].Limits.MaxMemTiming=GPU[GPU.Length-1].Od8Default[OD8_AC_TIMING].maxValue;
                           }
                        }
                      }
                    }
                  }
                }
               //===============================================================
             }
          } __except(EXCEPTION_EXECUTE_HANDLER)
              {
               //do nothing\unsupported
              }
         //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





         //do not list unsupported GPUs:
         if (GPU[GPU.Length-1].APISupported==false)
          {
           if (Options.DoNotListUnsupported)
            {
              GPU.Length--;
            }
          }
         //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       }
    }
  //############################################################################
  DeinitializeADL();
 } else
  {
    return false;
  }
return true;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::ShowHideI2CControls(int GpuInd)
{
if (ComboBox1->ItemIndex!=-1)
   {
     if (GPU[GpuInd].APISupported)
        {
          GroupBox6->Visible=true;
        }
     if (GPU[GpuInd].I2C.devicefound==true)
       {
         GroupBox6->Caption="I2C: "+I2cDB[GPU[GpuInd].I2C.typ].Name;
         I2C_Offset->Visible=true;
         Label4->Visible=true;
         Label6->Visible=true;
         if (I2cDB[GPU[GpuInd].I2C.typ].LLC_Enabled)
          {
            Label5->Visible=true;
            Label7->Visible=true;
            I2C_LLC->Visible=true;
          } else
           {
             Label5->Visible=false;
             Label7->Visible=false;
             I2C_LLC->Visible=false;
           }
         if (I2cDB[GPU[GpuInd].I2C.typ].Phase_Enabled)
          {
             Label9->Visible=true;
             Edit1->Visible=true;
          } else
           {
             Label9->Visible=false;
             Edit1->Visible=false;
           }
         if (I2cDB[GPU[GpuInd].I2C.typ].Scale_Enabled)
          {
             Label10->Visible=true;
             Edit2->Visible=true;
          } else
           {
             Label10->Visible=false;
             Edit2->Visible=false;
           }
         Label8->Visible=false;  //Not Available
       } else
           {
            GroupBox6->Caption="I2C";
            I2C_Offset->Visible=false;
            I2C_LLC->Visible=false;
            Edit1->Visible=false;
            Edit2->Visible=false;
            Label4->Visible=false;
            Label6->Visible=false;
            Label5->Visible=false;
            Label7->Visible=false;
            Label9->Visible=false;
            Label10->Visible=false;
            Label8->Visible=true;    //Not Available
          }
    } else
       {
         GroupBox6->Visible=false;
       }

}
// ---------------------------------------------------------------------------
void __fastcall TForm1::SetBoxesNotVisible()
{
GroupBox1->Visible=false;
GroupBox2->Visible=false;
GroupBox3->Visible=false;
GroupBox4->Visible=false;
GroupBox5->Visible=false;
Button1->Visible=false;
Button4->Visible=false;
}
//---------------------------------------------------------------------------
//pokazujemy lub chowamy kontrolki
void __fastcall TForm1::ShowHideControls(int GpuInd)
{
if (GPU[GpuInd].APISupported)
 {
   if (InfoLabel->Visible)
    {
     ShowHideInfoLabel(false, 0);
    }
   GroupBox1->Visible=true;
   GroupBox2->Visible=true;
   GroupBox3->Visible=true;
   GroupBox4->Visible=true;
   GroupBox5->Visible=true;
   Button1->Visible=true;
   Button4->Visible=true;
  // EnableMenuItem(GetSystemMenu(Handle, false), ((Word)-6),MF_BYCOMMAND | MF_ENABLED);
   //============
   //GPU
   if (GPU[GpuInd].GPULevels>=1)
    {
     GPU_P0->Visible=true;
     GPU_Clock_0->Visible=true;
     GPU_Vcc_0->Visible=true;
    } else
       {
        GPU_P0->Visible=false;
        GPU_Clock_0->Visible=false;
        GPU_Vcc_0->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=2)
    {
     GPU_P1->Visible=true;
     GPU_Clock_1->Visible=true;
     GPU_Vcc_1->Visible=true;
    } else
       {
        GPU_P1->Visible=false;
        GPU_Clock_1->Visible=false;
        GPU_Vcc_1->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=3)
    {
     GPU_P2->Visible=true;
     GPU_Clock_2->Visible=true;
     GPU_Vcc_2->Visible=true;
    } else
       {
        GPU_P2->Visible=false;
        GPU_Clock_2->Visible=false;
        GPU_Vcc_2->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=4)
    {
     GPU_P3->Visible=true;
     GPU_Clock_3->Visible=true;
     GPU_Vcc_3->Visible=true;
    } else
       {
        GPU_P3->Visible=false;
        GPU_Clock_3->Visible=false;
        GPU_Vcc_3->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=5)
    {
     GPU_P4->Visible=true;
     GPU_Clock_4->Visible=true;
     GPU_Vcc_4->Visible=true;
    } else
       {
        GPU_P4->Visible=false;
        GPU_Clock_4->Visible=false;
        GPU_Vcc_4->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=6)
    {
     GPU_P5->Visible=true;
     GPU_Clock_5->Visible=true;
     GPU_Vcc_5->Visible=true;
    } else
       {
        GPU_P5->Visible=false;
        GPU_Clock_5->Visible=false;
        GPU_Vcc_5->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=7)
    {
     GPU_P6->Visible=true;
     GPU_Clock_6->Visible=true;
     GPU_Vcc_6->Visible=true;
    } else
       {
        GPU_P6->Visible=false;
        GPU_Clock_6->Visible=false;
        GPU_Vcc_6->Visible=false;
       }
   if (GPU[GpuInd].GPULevels>=8)
    {
     GPU_P7->Visible=true;
     GPU_Clock_7->Visible=true;
     GPU_Vcc_7->Visible=true;
    } else
       {
        GPU_P7->Visible=false;
        GPU_Clock_7->Visible=false;
        GPU_Vcc_7->Visible=false;
       }
   //============
   //Memory
   if (GPU[GpuInd].MemoryLevels>=1)
    {
     Mem_P0->Visible=true;
     Mem_Clock_0->Visible=true;
     Mem_Vcc_0->Visible=true;
    } else
       {
        Mem_P0->Visible=false;
        Mem_Clock_0->Visible=false;
        Mem_Vcc_0->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=2)
    {
     Mem_P1->Visible=true;
     Mem_Clock_1->Visible=true;
     Mem_Vcc_1->Visible=true;
    } else
       {
        Mem_P1->Visible=false;
        Mem_Clock_1->Visible=false;
        Mem_Vcc_1->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=3)
    {
     Mem_P2->Visible=true;
     Mem_Clock_2->Visible=true;
     Mem_Vcc_2->Visible=true;
    } else
       {
        Mem_P2->Visible=false;
        Mem_Clock_2->Visible=false;
        Mem_Vcc_2->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=4)
    {
     Mem_P3->Visible=true;
     Mem_Clock_3->Visible=true;
     Mem_Vcc_3->Visible=true;
    } else
       {
        Mem_P3->Visible=false;
        Mem_Clock_3->Visible=false;
        Mem_Vcc_3->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=5)
    {
     Mem_P4->Visible=true;
     Mem_Clock_4->Visible=true;
     Mem_Vcc_4->Visible=true;
    } else
       {
        Mem_P4->Visible=false;
        Mem_Clock_4->Visible=false;
        Mem_Vcc_4->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=6)
    {
     Mem_P5->Visible=true;
     Mem_Clock_5->Visible=true;
     Mem_Vcc_5->Visible=true;
    } else
       {
        Mem_P5->Visible=false;
        Mem_Clock_5->Visible=false;
        Mem_Vcc_5->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=7)
    {
     Mem_P6->Visible=true;
     Mem_Clock_6->Visible=true;
     Mem_Vcc_6->Visible=true;
    } else
       {
        Mem_P6->Visible=false;
        Mem_Clock_6->Visible=false;
        Mem_Vcc_6->Visible=false;
       }
   if (GPU[GpuInd].MemoryLevels>=8)
    {
     Mem_P7->Visible=true;
     Mem_Clock_7->Visible=true;
     Mem_Vcc_7->Visible=true;
    } else
       {
        Mem_P7->Visible=false;
        Mem_Clock_7->Visible=false;
        Mem_Vcc_7->Visible=false;
       }
   //============
   //Fan disabled
   if (FanDeactivatedExists)
    {
     EnableDisableFanSection(GPU[GpuInd].FanEnabled);
    }
   //============
   //MemTimingLevel
   if (Mem_TimingBox->Items->Count!=(GPU[GpuInd].Limits.MaxMemTiming+1))
    {
     Mem_TimingBox->Items->Clear();
     Mem_TimingBox->Items->Add("0 - Auto");
     for (int i = 1; i <= GPU[GpuInd].Limits.MaxMemTiming; i++)
      {
       Mem_TimingBox->Items->Add(IntToStr(i)+" - Level "+IntToStr(i));
      }
    }
   //============
   //Overdrive8 controls:
   if (GPU[GpuInd].APIVersion==8)
    {
      Label12->Visible=false;
      Label13->Visible=false;
      GPU_Min_Label->Visible=true;
      GPU_Max_Label->Visible=true;
      GPU_Min->Visible=true;
      GPU_Max->Visible=true;
      Mem_Max_Label->Visible=true;
      Mem_Max->Visible=true;

      GPU_Clock_0->Enabled=true;
      GPU_Vcc_0->Enabled=true;
      GPU_Clock_1->Enabled=true;
      GPU_Vcc_1->Enabled=true;
      GPU_Clock_2->Enabled=true;
      GPU_Vcc_2->Enabled=true;
      GPU_P0->Font->Color=clWindowText;
      GPU_P1->Font->Color=clWindowText;
      GPU_P2->Font->Color=clWindowText;

     // Label20->Top=GPU_P2->Top;
     // Mem_TimingBox->Top=GPU_Clock_2->Top;

    } else
     {
      Label12->Visible=true;
      Label13->Visible=true;
      GPU_Min_Label->Visible=false;
      GPU_Max_Label->Visible=false;
      GPU_Min->Visible=false;
      GPU_Max->Visible=false;
      Mem_Max_Label->Visible=false;
      Mem_Max->Visible=false;

     // Label20->Top=232;
     // Mem_TimingBox->Top=229;

     }
   //===========

 } else
   {
     GroupBox1->Visible=false;
     GroupBox2->Visible=false;
     GroupBox3->Visible=false;
     GroupBox4->Visible=false;
     GroupBox5->Visible=false;
     GroupBox6->Visible=false;
     Button1->Visible=false;
     Button4->Visible=false;
     ShowHideInfoLabel(true, 0);
    // EnableMenuItem(GetSystemMenu(Handle, false), ((Word)-6),MF_BYCOMMAND | MF_DISABLED);
   }
//I2C
if (Options.UseI2C)
  {
    ShowHideI2CControls(GpuInd);
  }

}
//---------------------------------------------------------------------------
bool EqualValues(const ProfileItem *p1, const ProfileItem *p2, int GpuInd)
{
 if (p1->GPU_Clock.Length==p2->GPU_Clock.Length)
   {
     for (int j = 0; j < p1->GPU_Clock.Length; j++)
      {
       if (p1->GPU_Clock[j]!=p2->GPU_Clock[j])
        return false;
      }
     if (GPU[GpuInd].APIVersion==7)
      {
       for (int j = 0; j < p1->GPU_Clock.Length; j++)
        {
         if (p1->GPU_Enabled[j]!=p2->GPU_Enabled[j])
          return false;
        }
      }
   } else
      return false;

 if (p1->GPU_Vcc.Length==p2->GPU_Vcc.Length)
  {
    for (int j = 0; j < p1->GPU_Vcc.Length; j++)
     {
      if (p1->GPU_Vcc[j]!=p2->GPU_Vcc[j])
       return false;
     }
  } else
     return false;

 if (p1->Mem_Clock.Length==p2->Mem_Clock.Length)
  {
    for (int j = 0; j < p1->Mem_Clock.Length; j++)  //For Overdrive8 will be skipped
     {
      if (p1->Mem_Clock[j]!=p2->Mem_Clock[j] || p1->Mem_Enabled[j]!=p2->Mem_Enabled[j])
       return false;
     }
  } else
     return false;


 if (p1->Mem_Vcc.Length==p2->Mem_Vcc.Length)
  {
    for (int j = 0; j < p1->Mem_Vcc.Length; j++) //For Overdrive8 will be skipped
     {
      if (p1->Mem_Vcc[j]!=p2->Mem_Vcc[j])
       return false;
     }
  } else
     return false;

 if (GPU[GpuInd].FanEnabled)
   {
    // if (p1->Fan_Min!=p2->Fan_Min) return false;          //obsolete in 18.12.2 driver
    // if (p1->Fan_Max!=p2->Fan_Max) return false;          //obsolete in 18.12.2 driver
    // if (p1->Fan_Target!=p2->Fan_Target) return false;    //obsolete in 18.12.2 driver
     if (p1->Fan_Acoustic!=p2->Fan_Acoustic) return false;

     for (int i = 0; i < 5; i++)
      {
       if (p1->FanCurvePoints[i].Temp!=p2->FanCurvePoints[i].Temp) return false;
       if (p1->FanCurvePoints[i].Percentage!=p2->FanCurvePoints[i].Percentage) return false;
      }
     if (p1->Fan_ZeroRPM!=p2->Fan_ZeroRPM) return false;
   }
 if (p1->Mem_TimingLevel!=p2->Mem_TimingLevel) return false;


 //if (p1->Power_Temp!=p2->Power_Temp) return false;   //obsolete in 18.12.2 driver
 if (p1->Power_Target!=p2->Power_Target) return false;

 if (GPU[GpuInd].APIVersion==8)
  {
   if (p1->GPU_Min!=p2->GPU_Min) return false;
   if (p1->GPU_Max!=p2->GPU_Max) return false;
   if (p1->Mem_Max!=p2->Mem_Max) return false;
  }

 //I2C
 if (Options.UseI2C)
   {
    if (GPU[GpuInd].I2C.devicefound)
     {
       if ((p1->Offset!=-500))
        {
         if (p1->Offset!=p2->Offset) return false;
        }
       if ((p1->LLC!=-500) && (p2->LLC!=-500))
        {
         if (p1->LLC!=p2->LLC) return false;
        }
       if ((p1->PhaseGain!="NULL") && (p2->PhaseGain!="NULL"))
        {
         if (p1->PhaseGain!=p2->PhaseGain) return false;
        }
       if ((p1->CurrentScale!="NULL") && (p2->CurrentScale!="NULL"))
        {
         if (p1->CurrentScale!=p2->CurrentScale) return false;
        }
     }
   }

 //r�wne :
 return true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ReadCurrent(int GpuInd)
{
if (GPU[GpuInd].APISupported==false)
 {
   return;
 }
if (InitializeADL())
 {
  CurrentValues.GPU_Clock.Length=GPU[GpuInd].GPULevels;
  CurrentValues.GPU_Vcc.Length=GPU[GpuInd].GPULevels;
  CurrentValues.GPU_Enabled.Length=GPU[GpuInd].GPULevels;
  CurrentValues.Mem_Clock.Length=GPU[GpuInd].MemoryLevels;
  CurrentValues.Mem_Vcc.Length=GPU[GpuInd].MemoryLevels;
  CurrentValues.Mem_Enabled.Length=GPU[GpuInd].MemoryLevels;
  //****************************************************************************
  //OverdriveN
  if (GPU[GpuInd].APIVersion==7)
   {
    //GPU
    const int size = sizeof(ADLODNPerformanceLevelsX2) + sizeof(ADLODNPerformanceLevelX2)* (GPU[GpuInd].MaxLevels - 1);
    ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
    pL->iSize = size;
    pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;

    int an=ADL2_OverdriveN_SystemClocksX2_Get(context, GPU[GpuInd].AdIndexes[0], pL);

    if (ADL_OK == an)
     {
       if (GPU[GpuInd].GPULevels>=1)
        {
         GPU_Clock_0->Value=pL->aLevels[0].iClock/100;
         GPU_Vcc_0->Value=pL->aLevels[0].iVddc;

         CurrentValues.GPU_Clock[0]=pL->aLevels[0].iClock/100;
         CurrentValues.GPU_Vcc[0]=pL->aLevels[0].iVddc;
         GPU_Clock_0->Color=clWindow;
         GPU_Vcc_0->Color=clWindow;
         EnableDisablePState((pL->aLevels[0].iEnabled>0),GPU_P0,GPU_Clock_0,GPU_Vcc_0);
         CurrentValues.GPU_Enabled[0]=(pL->aLevels[0].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=2)
        {
         GPU_Clock_1->Value=pL->aLevels[1].iClock/100;
         GPU_Vcc_1->Value=pL->aLevels[1].iVddc;

         CurrentValues.GPU_Clock[1]=pL->aLevels[1].iClock/100;
         CurrentValues.GPU_Vcc[1]=pL->aLevels[1].iVddc;
         GPU_Clock_1->Color=clWindow;
         GPU_Vcc_1->Color=clWindow;
         EnableDisablePState((pL->aLevels[1].iEnabled>0),GPU_P1,GPU_Clock_1,GPU_Vcc_1);
         CurrentValues.GPU_Enabled[1]=(pL->aLevels[1].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=3)
        {
         GPU_Clock_2->Value=pL->aLevels[2].iClock/100;
         GPU_Vcc_2->Value=pL->aLevels[2].iVddc;

         CurrentValues.GPU_Clock[2]=pL->aLevels[2].iClock/100;
         CurrentValues.GPU_Vcc[2]=pL->aLevels[2].iVddc;
         GPU_Clock_2->Color=clWindow;
         GPU_Vcc_2->Color=clWindow;
         EnableDisablePState((pL->aLevels[2].iEnabled>0),GPU_P2,GPU_Clock_2,GPU_Vcc_2);
         CurrentValues.GPU_Enabled[2]=(pL->aLevels[2].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=4)
        {
         GPU_Clock_3->Value=pL->aLevels[3].iClock/100;
         GPU_Vcc_3->Value=pL->aLevels[3].iVddc;

         CurrentValues.GPU_Clock[3]=pL->aLevels[3].iClock/100;
         CurrentValues.GPU_Vcc[3]=pL->aLevels[3].iVddc;
         GPU_Clock_3->Color=clWindow;
         GPU_Vcc_3->Color=clWindow;
         EnableDisablePState((pL->aLevels[3].iEnabled>0),GPU_P3,GPU_Clock_3,GPU_Vcc_3);
         CurrentValues.GPU_Enabled[3]=(pL->aLevels[3].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=5)
        {
         GPU_Clock_4->Value=pL->aLevels[4].iClock/100;
         GPU_Vcc_4->Value=pL->aLevels[4].iVddc;

         CurrentValues.GPU_Clock[4]=pL->aLevels[4].iClock/100;
         CurrentValues.GPU_Vcc[4]=pL->aLevels[4].iVddc;
         GPU_Clock_4->Color=clWindow;
         GPU_Vcc_4->Color=clWindow;
         EnableDisablePState((pL->aLevels[4].iEnabled>0),GPU_P4,GPU_Clock_4,GPU_Vcc_4);
         CurrentValues.GPU_Enabled[4]=(pL->aLevels[4].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=6)
        {
         GPU_Clock_5->Value=pL->aLevels[5].iClock/100;
         GPU_Vcc_5->Value=pL->aLevels[5].iVddc;

         CurrentValues.GPU_Clock[5]=pL->aLevels[5].iClock/100;
         CurrentValues.GPU_Vcc[5]=pL->aLevels[5].iVddc;
         GPU_Clock_5->Color=clWindow;
         GPU_Vcc_5->Color=clWindow;
         EnableDisablePState((pL->aLevels[5].iEnabled>0),GPU_P5,GPU_Clock_5,GPU_Vcc_5);
         CurrentValues.GPU_Enabled[5]=(pL->aLevels[5].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=7)
        {
         GPU_Clock_6->Value=pL->aLevels[6].iClock/100;
         GPU_Vcc_6->Value=pL->aLevels[6].iVddc;

         CurrentValues.GPU_Clock[6]=pL->aLevels[6].iClock/100;
         CurrentValues.GPU_Vcc[6]=pL->aLevels[6].iVddc;
         GPU_Clock_6->Color=clWindow;
         GPU_Vcc_6->Color=clWindow;
         EnableDisablePState((pL->aLevels[6].iEnabled>0),GPU_P6,GPU_Clock_6,GPU_Vcc_6);
         CurrentValues.GPU_Enabled[6]=(pL->aLevels[6].iEnabled>0);
        }
       if (GPU[GpuInd].GPULevels>=8)
        {
         GPU_Clock_7->Value=pL->aLevels[7].iClock/100;
         GPU_Vcc_7->Value=pL->aLevels[7].iVddc;

         CurrentValues.GPU_Clock[7]=pL->aLevels[7].iClock/100;
         CurrentValues.GPU_Vcc[7]=pL->aLevels[7].iVddc;
         GPU_Clock_7->Color=clWindow;
         GPU_Vcc_7->Color=clWindow;
         EnableDisablePState((pL->aLevels[7].iEnabled>0),GPU_P7,GPU_Clock_7,GPU_Vcc_7);
         CurrentValues.GPU_Enabled[7]=(pL->aLevels[7].iEnabled>0);
        }
       //Memory
       memset(pL, 0, size);
       pL->iSize = size;
       pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;
       an=ADL2_OverdriveN_MemoryClocksX2_Get(context, GPU[GpuInd].AdIndexes[0], pL);
       if (ADL_OK == an)
        {
          if (GPU[GpuInd].MemoryLevels>=1)
           {
            Mem_Clock_0->Value=pL->aLevels[0].iClock/100;
            Mem_Vcc_0->Value=pL->aLevels[0].iVddc;

            CurrentValues.Mem_Clock[0]=pL->aLevels[0].iClock/100;
            CurrentValues.Mem_Vcc[0]=pL->aLevels[0].iVddc;
            Mem_Clock_0->Color=clWindow;
            Mem_Vcc_0->Color=clWindow;
            EnableDisablePState((pL->aLevels[0].iEnabled>0),Mem_P0,Mem_Clock_0,Mem_Vcc_0);
            CurrentValues.Mem_Enabled[0]=(pL->aLevels[0].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=2)
           {
            Mem_Clock_1->Value=pL->aLevels[1].iClock/100;
            Mem_Vcc_1->Value=pL->aLevels[1].iVddc;

            CurrentValues.Mem_Clock[1]=pL->aLevels[1].iClock/100;
            CurrentValues.Mem_Vcc[1]=pL->aLevels[1].iVddc;
            Mem_Clock_1->Color=clWindow;
            Mem_Vcc_1->Color=clWindow;
            EnableDisablePState((pL->aLevels[1].iEnabled>0),Mem_P1,Mem_Clock_1,Mem_Vcc_1);
            CurrentValues.Mem_Enabled[1]=(pL->aLevels[1].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=3)
           {
            Mem_Clock_2->Value=pL->aLevels[2].iClock/100;
            Mem_Vcc_2->Value=pL->aLevels[2].iVddc;

            CurrentValues.Mem_Clock[2]=pL->aLevels[2].iClock/100;
            CurrentValues.Mem_Vcc[2]=pL->aLevels[2].iVddc;
            Mem_Clock_2->Color=clWindow;
            Mem_Vcc_2->Color=clWindow;
            EnableDisablePState((pL->aLevels[2].iEnabled>0),Mem_P2,Mem_Clock_2,Mem_Vcc_2);
            CurrentValues.Mem_Enabled[2]=(pL->aLevels[2].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=4)
           {
            Mem_Clock_3->Value=pL->aLevels[3].iClock/100;
            Mem_Vcc_3->Value=pL->aLevels[3].iVddc;

            CurrentValues.Mem_Clock[3]=pL->aLevels[3].iClock/100;
            CurrentValues.Mem_Vcc[3]=pL->aLevels[3].iVddc;
            Mem_Clock_3->Color=clWindow;
            Mem_Vcc_3->Color=clWindow;
            EnableDisablePState((pL->aLevels[3].iEnabled>0),Mem_P3,Mem_Clock_3,Mem_Vcc_3);
            CurrentValues.Mem_Enabled[3]=(pL->aLevels[3].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=5)
           {
            Mem_Clock_4->Value=pL->aLevels[4].iClock/100;
            Mem_Vcc_4->Value=pL->aLevels[4].iVddc;

            CurrentValues.Mem_Clock[4]=pL->aLevels[4].iClock/100;
            CurrentValues.Mem_Vcc[4]=pL->aLevels[4].iVddc;
            Mem_Clock_4->Color=clWindow;
            Mem_Vcc_4->Color=clWindow;
            EnableDisablePState((pL->aLevels[4].iEnabled>0),Mem_P4,Mem_Clock_4,Mem_Vcc_4);
            CurrentValues.Mem_Enabled[4]=(pL->aLevels[4].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=6)
           {
            Mem_Clock_5->Value=pL->aLevels[5].iClock/100;
            Mem_Vcc_5->Value=pL->aLevels[5].iVddc;

            CurrentValues.Mem_Clock[5]=pL->aLevels[5].iClock/100;
            CurrentValues.Mem_Vcc[5]=pL->aLevels[5].iVddc;
            Mem_Clock_5->Color=clWindow;
            Mem_Vcc_5->Color=clWindow;
            EnableDisablePState((pL->aLevels[5].iEnabled>0),Mem_P5,Mem_Clock_5,Mem_Vcc_5);
            CurrentValues.Mem_Enabled[5]=(pL->aLevels[5].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=7)
           {
            Mem_Clock_6->Value=pL->aLevels[6].iClock/100;
            Mem_Vcc_6->Value=pL->aLevels[6].iVddc;

            CurrentValues.Mem_Clock[6]=pL->aLevels[6].iClock/100;
            CurrentValues.Mem_Vcc[6]=pL->aLevels[6].iVddc;
            Mem_Clock_6->Color=clWindow;
            Mem_Vcc_6->Color=clWindow;
            EnableDisablePState((pL->aLevels[6].iEnabled>0),Mem_P6,Mem_Clock_6,Mem_Vcc_6);
            CurrentValues.Mem_Enabled[6]=(pL->aLevels[6].iEnabled>0);
           }
          if (GPU[GpuInd].MemoryLevels>=8)
           {
            Mem_Clock_7->Value=pL->aLevels[7].iClock/100;
            Mem_Vcc_7->Value=pL->aLevels[7].iVddc;

            CurrentValues.Mem_Clock[7]=pL->aLevels[7].iClock/100;
            CurrentValues.Mem_Vcc[7]=pL->aLevels[7].iVddc;
            Mem_Clock_7->Color=clWindow;
            Mem_Vcc_7->Color=clWindow;
            EnableDisablePState((pL->aLevels[7].iEnabled>0),Mem_P7,Mem_Clock_7,Mem_Vcc_7);
            CurrentValues.Mem_Enabled[7]=(pL->aLevels[7].iEnabled>0);
           }
         } else
           blad("MemoryClocks_Get function failed",an,false);
       free(pL);
       //Fan
       ADLODNFanControl odNFanControl;
       memset(&odNFanControl, 0, sizeof(ADLODNFanControl));
       an=ADL2_OverdriveN_FanControl_Get(context,GPU[GpuInd].AdIndexes[0], &odNFanControl);
       if (ADL_OK == an)
        {
         Fan_Min->Value=odNFanControl.iMinFanLimit;
         CurrentValues.Fan_Min=odNFanControl.iMinFanLimit;

         Fan_Max->Value=odNFanControl.iTargetFanSpeed;
         CurrentValues.Fan_Max=odNFanControl.iTargetFanSpeed;

         Fan_Target->Value=odNFanControl.iTargetTemperature;
         CurrentValues.Fan_Target=odNFanControl.iTargetTemperature;

         Fan_Acoustic->Value=odNFanControl.iMinPerformanceClock;
         CurrentValues.Fan_Acoustic=odNFanControl.iMinPerformanceClock;
         if (GPU[GpuInd].FanEnabled)
          {
            Fan_Min->Color=clWindow;
            Fan_Max->Color=clWindow;
            Fan_Target->Color=clWindow;
            Fan_Acoustic->Color=clWindow;
          }

        } else
           blad("FanControl_Get function failed",an,false);
       //Power
       ADLODNPowerLimitSetting odNPowerControl;
       memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
       an=ADL2_OverdriveN_PowerLimit_Get(context,GPU[GpuInd].AdIndexes[0], &odNPowerControl);
       if (an == ADL_OK)
        {
         Power_Temp->Value=odNPowerControl.iMaxOperatingTemperature;
         CurrentValues.Power_Temp=odNPowerControl.iMaxOperatingTemperature;
         Power_Temp->Color=clWindow;

         Power_Target->Value=odNPowerControl.iTDPLimit;
         CurrentValues.Power_Target=odNPowerControl.iTDPLimit;
         Power_Target->Color=clWindow;
        } else
           blad("PowerLimit_Get function failed",an,false);

       //Fan Curve, Zero RPM, Mem Timing
       ADLODNExtSettings *ExtCurrent= new ADLODNExtSettings;
       if (OverdriveN.ExtSettings_Get(GpuInd, NULL, ExtCurrent, false))
        {
         Fan_Temp_0->Value=ExtCurrent->Point[0].Temp;
         Fan_Perc_0->Value=ExtCurrent->Point[0].Percentage;
         Fan_Temp_1->Value=ExtCurrent->Point[1].Temp;
         Fan_Perc_1->Value=ExtCurrent->Point[1].Percentage;
         Fan_Temp_2->Value=ExtCurrent->Point[2].Temp;
         Fan_Perc_2->Value=ExtCurrent->Point[2].Percentage;
         Fan_Temp_3->Value=ExtCurrent->Point[3].Temp;
         Fan_Perc_3->Value=ExtCurrent->Point[3].Percentage;
         Fan_Temp_4->Value=ExtCurrent->Point[4].Temp;
         Fan_Perc_4->Value=ExtCurrent->Point[4].Percentage;

         for (int i = 0; i < 5; i++)
          {
           CurrentValues.FanCurvePoints[i].Temp=ExtCurrent->Point[i].Temp;
           CurrentValues.FanCurvePoints[i].Percentage=ExtCurrent->Point[i].Percentage;
          }

         if (GPU[GpuInd].FanEnabled)
          {
           Fan_Temp_0->Color=clWindow;
           Fan_Perc_0->Color=clWindow;
           Fan_Temp_1->Color=clWindow;
           Fan_Perc_1->Color=clWindow;
           Fan_Temp_2->Color=clWindow;
           Fan_Perc_2->Color=clWindow;
           Fan_Temp_3->Color=clWindow;
           Fan_Perc_3->Color=clWindow;
           Fan_Temp_4->Color=clWindow;
           Fan_Perc_4->Color=clWindow;
          }

         ZeroRPMBox->Checked=ExtCurrent->ZeroRPM;
         CurrentValues.Fan_ZeroRPM=ExtCurrent->ZeroRPM;
         ZeroRPMBox->Color=clBtnFace;

         if (ExtCurrent->MemTimingLevel<Mem_TimingBox->Items->Count)
          Mem_TimingBox->ItemIndex=ExtCurrent->MemTimingLevel;
         CurrentValues.Mem_TimingLevel=ExtCurrent->MemTimingLevel;
         Mem_TimingBox->Color=clWindow;
        }
       delete ExtCurrent;
     } else
      blad("SystemClocks_Get function failed", an, false);
   } else
  //****************************************************************************
    //Overdrive8
    if (GPU[GpuInd].APIVersion==8)
     {
      Odn8Settings Od8Current;
      if (Overdrive8.GetValues(GpuInd, NULL, &Od8Current, false))
       {
         //GPU
         GPU_Clock_0->Value=Od8Current.GPU_P[0].Clock;
         GPU_Vcc_0->Value=Od8Current.GPU_P[0].Voltage;
         GPU_Clock_1->Value=Od8Current.GPU_P[1].Clock;
         GPU_Vcc_1->Value=Od8Current.GPU_P[1].Voltage;
         GPU_Clock_2->Value=Od8Current.GPU_P[2].Clock;
         GPU_Vcc_2->Value=Od8Current.GPU_P[2].Voltage;
         GPU_Min->Value=Od8Current.GPU_Min;
         GPU_Max->Value=Od8Current.GPU_Max;

         for (int i = 0; i < 3; i++)
          {
           CurrentValues.GPU_Clock[i]=Od8Current.GPU_P[i].Clock;
           CurrentValues.GPU_Vcc[i]=Od8Current.GPU_P[i].Voltage;
           CurrentValues.GPU_Enabled[i]=true;
          }
         CurrentValues.GPU_Min=Od8Current.GPU_Min;
         CurrentValues.GPU_Max=Od8Current.GPU_Max;

         GPU_Clock_0->Color=clWindow;
         GPU_Vcc_0->Color=clWindow;
         GPU_Clock_1->Color=clWindow;
         GPU_Vcc_1->Color=clWindow;
         GPU_Clock_2->Color=clWindow;
         GPU_Vcc_2->Color=clWindow;
         GPU_Min->Color=clWindow;
         GPU_Max->Color=clWindow;

         //Memory
         Mem_Max->Value=Od8Current.Mem_Max;
         CurrentValues.Mem_Max=Od8Current.Mem_Max;
         Mem_Max->Color=clWindow;

         //Fan Curve, Zero RPM, Mem Timing
         Fan_Temp_0->Value=Od8Current.ExtSettings.Point[0].Temp;
         Fan_Perc_0->Value=Od8Current.ExtSettings.Point[0].Percentage;
         Fan_Temp_1->Value=Od8Current.ExtSettings.Point[1].Temp;
         Fan_Perc_1->Value=Od8Current.ExtSettings.Point[1].Percentage;
         Fan_Temp_2->Value=Od8Current.ExtSettings.Point[2].Temp;
         Fan_Perc_2->Value=Od8Current.ExtSettings.Point[2].Percentage;
         Fan_Temp_3->Value=Od8Current.ExtSettings.Point[3].Temp;
         Fan_Perc_3->Value=Od8Current.ExtSettings.Point[3].Percentage;
         Fan_Temp_4->Value=Od8Current.ExtSettings.Point[4].Temp;
         Fan_Perc_4->Value=Od8Current.ExtSettings.Point[4].Percentage;

         for (int i = 0; i < 5; i++)
          {
           CurrentValues.FanCurvePoints[i].Temp=Od8Current.ExtSettings.Point[i].Temp;
           CurrentValues.FanCurvePoints[i].Percentage=Od8Current.ExtSettings.Point[i].Percentage;
          }

         ZeroRPMBox->Checked=Od8Current.ExtSettings.ZeroRPM;
         CurrentValues.Fan_ZeroRPM=Od8Current.ExtSettings.ZeroRPM;
         ZeroRPMBox->Color=clBtnFace;

         if (Od8Current.ExtSettings.MemTimingLevel<Mem_TimingBox->Items->Count)
          Mem_TimingBox->ItemIndex=Od8Current.ExtSettings.MemTimingLevel;
         CurrentValues.Mem_TimingLevel=Od8Current.ExtSettings.MemTimingLevel;
         Mem_TimingBox->Color=clWindow;

         //Acoustic Limit
         Fan_Acoustic->Value=Od8Current.Fan_Acoustic;
         CurrentValues.Fan_Acoustic=Od8Current.Fan_Acoustic;


         if (GPU[GpuInd].FanEnabled)
          {
           Fan_Temp_0->Color=clWindow;
           Fan_Perc_0->Color=clWindow;
           Fan_Temp_1->Color=clWindow;
           Fan_Perc_1->Color=clWindow;
           Fan_Temp_2->Color=clWindow;
           Fan_Perc_2->Color=clWindow;
           Fan_Temp_3->Color=clWindow;
           Fan_Perc_3->Color=clWindow;
           Fan_Temp_4->Color=clWindow;
           Fan_Perc_4->Color=clWindow;
           Fan_Acoustic->Color=clWindow;
          }


         //Power
         Power_Target->Value=Od8Current.Power_Target;
         CurrentValues.Power_Target=Od8Current.Power_Target;
         Power_Target->Color=clWindow;
       }
     }
  //****************************************************************************

 DeinitializeADL();
 }
return;
}
//---------------------------------------------------------------------------
bool OverdriveN::CheckIfLimitsExceed(int GpuInd, ADLODNPerformanceLevelsX2 *pLevels, ADLODNFanControl* fan, ADLODNPowerLimitSetting* power, String* msg, bool SetToGpu)
{
String str;
bool above=false;
bool below=false;
bool ret=false;

ADLODNCapabilitiesX2 caps;
memset(&caps, 0, sizeof(ADLODNCapabilitiesX2));
if (ADL_OK == ADL2_OverdriveN_CapabilitiesX2_Get(context,GPU[GPU.Length-1].AdIndexes[0], &caps))
  {
   String infomsg;
   if (pLevels!=NULL)
    {
     int len;
     if (SetToGpu)
      len=GPU[GpuInd].GPULevels; else
       len=GPU[GpuInd].MemoryLevels;

     for (int i = 0; i < len; i++)
      {
       if (SetToGpu)
         {
           if (pLevels->aLevels[i].iClock>caps.sEngineClockRange.iMax || pLevels->aLevels[i].iVddc>caps.svddcRange.iMax)
            {
             above=true;
             ret=true;
            }
           if (pLevels->aLevels[i].iClock<caps.sEngineClockRange.iMin || pLevels->aLevels[i].iVddc<caps.svddcRange.iMin)
            {
              below=true;
              ret=true;
            }
         } else
           {
             if (pLevels->aLevels[i].iClock>caps.sMemoryClockRange.iMax || pLevels->aLevels[i].iVddc>caps.svddcRange.iMax)
              {
               above=true;
               ret=true;
              }
             if (pLevels->aLevels[i].iClock<caps.sMemoryClockRange.iMin || pLevels->aLevels[i].iVddc<caps.svddcRange.iMin)
              {
                below=true;
                ret=true;
              }
           }
      }
     if (ret)
      {
        if (SetToGpu)
         {
          infomsg=IntToStr(caps.sEngineClockRange.iMin/100)+" Mhz < GPU clock < "+IntToStr(caps.sEngineClockRange.iMax/100)+" MHz\n"
                  +IntToStr(caps.svddcRange.iMin)+" mV < GPU voltage < "+IntToStr(caps.svddcRange.iMax)+" mV";
         } else
           {
            infomsg=IntToStr(caps.sMemoryClockRange.iMin/100)+" Mhz < Memory clock < "+IntToStr(caps.sMemoryClockRange.iMax/100)+" MHz\n"
                    +IntToStr(caps.svddcRange.iMin)+" mV < Memory voltage < "+IntToStr(caps.svddcRange.iMax)+" mV";
           }
      }
    } else
     if (fan!=NULL)
      {
        if (fan->iTargetTemperature<caps.fanTemperature.iMin || fan->iMinPerformanceClock<caps.minimumPerformanceClock.iMin ||
            fan->iMinFanLimit<caps.fanSpeed.iMin || fan->iTargetFanSpeed<caps.fanSpeed.iMin)
             {
              below=true;
              ret=true;
             }
        if (fan->iTargetTemperature>caps.fanTemperature.iMax || fan->iMinPerformanceClock>caps.minimumPerformanceClock.iMax ||
            fan->iMinFanLimit>caps.fanSpeed.iMax || fan->iTargetFanSpeed>caps.fanSpeed.iMax)
             {
              above=true;
              ret=true;
             }
        if (ret)
         infomsg=IntToStr(caps.minimumPerformanceClock.iMin)+" MHz < Fan Acoustic Limit < "+IntToStr(caps.minimumPerformanceClock.iMax)+" MHz\n";
        /*  
         infomsg=IntToStr(caps.fanSpeed.iMin)+" RPM < Fan Minimum < "+IntToStr(caps.fanSpeed.iMax)+" RPM\n"
                 +IntToStr(caps.fanSpeed.iMin)+" RPM < Fan Maximum < "+IntToStr(caps.fanSpeed.iMax)+" RPM\n"
                 +IntToStr(caps.fanTemperature.iMin)+" �C < Fan Target Temp. < "+IntToStr(caps.fanTemperature.iMax)+" �C\n"
                 +IntToStr(caps.minimumPerformanceClock.iMin)+" MHz < Fan Acoustic Limit < "+IntToStr(caps.minimumPerformanceClock.iMax)+" MHz\n";
                  */
     /*  
	     if ((fan->iTargetTemperature<caps.fanTemperature.iMin || fan->iTargetTemperature>caps.fanTemperature.iMax) ||  //Target temp
           (fan->iMinPerformanceClock<caps.minimumPerformanceClock.iMin || fan->iMinPerformanceClock>caps.minimumPerformanceClock.iMax) || //Acoustic limit
           (fan->iMinFanLimit<caps.fanSpeed.iMin || fan->iMinFanLimit>caps.fanSpeed.iMax) || //Fan speed minimum rpm
           (fan->iTargetFanSpeed<caps.fanSpeed.iMin || fan->iTargetFanSpeed>caps.fanSpeed.iMax))  //Fan speed maximum rpm
             ret=true;  */
      } else
       if (power!=NULL)
        {
         if (power->iMaxOperatingTemperature<caps.powerTuneTemperature.iMin || power->iTDPLimit<caps.power.iMin)
          {
           below=true;
           ret=true;
          }
         if (power->iMaxOperatingTemperature>caps.powerTuneTemperature.iMax || power->iTDPLimit>caps.power.iMax)
          {
           above=true;
           ret=true;
          }
         if (ret)
          infomsg=IntToStr(caps.power.iMin)+" % < Power Target < "+IntToStr(caps.power.iMax)+" %\n";
         /* 
          infomsg=IntToStr(caps.powerTuneTemperature.iMin)+" �C < Power Max Temp. < "+IntToStr(caps.powerTuneTemperature.iMax)+" �C\n"
                  +IntToStr(caps.power.iMin)+" % < Power Target < "+IntToStr(caps.power.iMax)+" %\n";
                  */
        /* 
		   if ((power->iMaxOperatingTemperature<caps.powerTuneTemperature.iMin || power->iMaxOperatingTemperature>caps.powerTuneTemperature.iMax) || //Power max temp.
             (power->iTDPLimit<caps.power.iMin || power->iTDPLimit>caps.power.iMax))  //Power target
               ret=true;   */
        }
   if (ret)
    {
     String str;
     if (above==below)
      str="above/below"; else
       if (above)
        str="above"; else
         str="below";

      *msg=" (ErrorCode: -1) (Values are "+str+" driver limits)\n"+"Current limits for GPU "+IntToStr(GpuInd)+":\n"+infomsg;
    }
  }
return ret;
}
//---------------------------------------------------------------------------
bool OverdriveN::SetClocks(int GpuInd, bool SetToGpu, ADLODNPerformanceLevelsX2 *pLevels, const int size, String* errmsg, bool CmdMsg, bool silent)
{
bool retcode=true;
ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)malloc(size);
memcpy(pL, pLevels, size);
int an;
if (SetToGpu)
 an=ADL2_OverdriveN_SystemClocksX2_Set(context, GPU[GpuInd].AdIndexes[0], pL); else
  an=ADL2_OverdriveN_MemoryClocksX2_Set(context, GPU[GpuInd].AdIndexes[0], pL);
if (ADL_OK!=an && silent==false)
 {
  String str=*errmsg;
  if (*errmsg=="")
   if (SetToGpu)
    str="Failed to set GPU values"; else
     str="Failed to set Memory values";
  String msg;
  if (an==-1)
   {
    if (CheckIfLimitsExceed(GpuInd, pLevels, NULL, NULL, &msg, SetToGpu))
     {
      str+=msg;
      an=0;
     }
   }
  if (CmdMsg)
   Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
    blad(str, an, false);
  retcode=false;
  free(pL);
 }
free(pL);

return retcode;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
bool OverdriveN::SetValues_GPUMem(int GpuInd, bool SetToGpu, ADLODNPerformanceLevelsX2 *pLevels, const int size, String errmsg, bool CmdMsg)
{

pLevels->iNumberOfPerformanceLevels=GPU[GpuInd].MaxLevels;
pLevels->iMode=ADLODNControlType::ODNControlType_Manual;

ADLODNPerformanceLevelsX2 *Wanted;
if (Options.CheckValuesMismatch)
 {
  Wanted=(ADLODNPerformanceLevelsX2*)malloc(size);
  memcpy(Wanted, pLevels, size);
 }

bool retcode=SetClocks(GpuInd, SetToGpu, pLevels, size, &errmsg, CmdMsg, false);

//====== auto reset
if (Options.AutoReset && retcode)
 {
   ADLODNPerformanceLevelsX2 *resLevels=(ADLODNPerformanceLevelsX2*)malloc(size);
   memcpy(resLevels, pLevels, size);
   resLevels->iMode=ADLODNControlType::ODNControlType_Default;
   if (SetToGpu)
    ADL2_OverdriveN_SystemClocksX2_Set(context, GPU[GpuInd].AdIndexes[0], resLevels); else
     ADL2_OverdriveN_MemoryClocksX2_Set(context, GPU[GpuInd].AdIndexes[0], resLevels);
   free(resLevels);
   retcode=SetClocks(GpuInd, SetToGpu, pLevels, size, &errmsg, CmdMsg, false);
 }

if (retcode && Options.CheckValuesMismatch)
 {
  ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
  pL->iSize = size;
  pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;

  int ret=-1;
  if (SetToGpu)
   ret=ADL2_OverdriveN_SystemClocksX2_Get(context, GPU[GpuInd].AdIndexes[0], pL); else
    ret=ADL2_OverdriveN_MemoryClocksX2_Get(context, GPU[GpuInd].AdIndexes[0], pL);

  if (ADL_OK==ret)
   {
    String str;
    String prefix;
    int len;
    if (SetToGpu)
     {
       prefix="GPU";
       len=GPU[GpuInd].GPULevels;
     } else
      {
        prefix="Memory";
        len=GPU[GpuInd].MemoryLevels;
      }

    for (int i = 0; i < len; i++)
     {
      if (Wanted->aLevels[i].iClock!=pL->aLevels[i].iClock)
       {
        str+=prefix+" P"+IntToStr(i)+": got '"+IntToStr(pL->aLevels[i].iClock/100)+" MHz' instead of '"+IntToStr(Wanted->aLevels[i].iClock/100)+" MHz'\n";
       }
      if (Wanted->aLevels[i].iVddc!=pL->aLevels[i].iVddc)
       {
        str+=prefix+" P"+IntToStr(i)+": got '"+IntToStr(pL->aLevels[i].iVddc)+" mV' instead of '"+IntToStr(Wanted->aLevels[i].iVddc)+" mV'\n";
       }
      if ((Wanted->aLevels[i].iEnabled>0)!=(pL->aLevels[i].iEnabled>0))
       {
        String wantstr, currstr="'enabled'";
        if (Wanted->aLevels[i].iEnabled==0)
         wantstr="'disabled'";
        if (pL->aLevels[i].iEnabled==0)
         currstr="'disabled'";
        str+=prefix+" P"+IntToStr(i)+": got "+currstr+" instead of "+wantstr+"\n";
       }
     }
   if (str!="")
    {
     str=prefix+ " values mismatch for GPU "+IntToStr(GpuInd)+".\n"+str;
     if (CmdMsg)
      Cmd.ErrorInfoShow(str, CmdMsg, 0, 0, false); else
       blad(str, 0, false);
    }
   }
  free(pL);
 }
if (Options.CheckValuesMismatch)
 free(Wanted);

return retcode;
}
//---------------------------------------------------------------------------
bool OverdriveN::SetValues_Fan(int GpuInd, bool ReplaceObsolete, int Fan_Min, int Fan_Max, int Fan_Target, int Fan_Acoustic, String errmsg, bool CmdMsg)
{
bool retcode=true;

//====== auto reset
if (Options.AutoReset)
 {
   ADLODNFanControl odNFanControl;
   odNFanControl.iMinFanLimit=Fan_Min;
   odNFanControl.iTargetFanSpeed=Fan_Max;
   odNFanControl.iTargetTemperature=Fan_Target;
   odNFanControl.iMinPerformanceClock=Fan_Acoustic;
   odNFanControl.iMode = ADLODNControlType::ODNControlType_Manual;
   if (ADL_OK == ADL2_OverdriveN_FanControl_Set(context, GPU[GpuInd].AdIndexes[0], &odNFanControl))
    {
     memset(&odNFanControl, 0, sizeof(ADLODNFanControl));
     odNFanControl.iMode = ADLODNControlType::ODNControlType_Default;
     ADL2_OverdriveN_FanControl_Set(context, GPU[GpuInd].AdIndexes[0], &odNFanControl);
    }
 }
//====
ADLODNFanControl FC;
memset(&FC, 0, sizeof(ADLODNFanControl));
FC.iMinFanLimit=Fan_Min;
FC.iTargetFanSpeed=Fan_Max;
FC.iTargetTemperature=Fan_Target;
FC.iMinPerformanceClock=Fan_Acoustic;
FC.iMode = ADLODNControlType::ODNControlType_Manual;
//====
// Replace obsolete values since driver 18.12.2
if (ReplaceObsolete)
 {
  ADLODNFanControl odNFanControl;
  memset(&odNFanControl, 0, sizeof(ADLODNFanControl));
  if (ADL_OK==ADL2_OverdriveN_FanControl_Get(context, GPU[GpuInd].AdIndexes[0], &odNFanControl))
   {
    FC.iMinFanLimit=odNFanControl.iMinFanLimit;
    FC.iTargetFanSpeed=odNFanControl.iTargetFanSpeed;
    FC.iTargetTemperature=odNFanControl.iTargetTemperature;
   }
 }
 //====
int an = ADL2_OverdriveN_FanControl_Set(context, GPU[GpuInd].AdIndexes[0], &FC);
if (ADL_OK != an)
 {
  String str=errmsg;
  if (errmsg=="")
   str="Failed to set Fan values";
  String msg;
  if (an==-1)
   {
    if (CheckIfLimitsExceed(GpuInd, NULL, &FC, NULL, &msg, false))
     {
      str+=msg;
      an=0;
     }
   }
  if (CmdMsg)
   Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
    blad(str, an, false);
  retcode=false;
 }


if (retcode && Options.CheckValuesMismatch)
 {
  ADLODNFanControl Current;
  memset(&Current, 0, sizeof(ADLODNFanControl));
  int ret=ADL2_OverdriveN_FanControl_Get(context,GPU[GpuInd].AdIndexes[0], &Current);
  if (ADL_OK==ret)
   {
    String str;
    // Obsolete in 18.12.2 //////
    if (FC.iMinFanLimit!=Current.iMinFanLimit)
     str+="Minimum (RPM): got '"+IntToStr(Current.iMinFanLimit)+" RPM' instead of '"+IntToStr(FC.iMinFanLimit)+" RPM'\n";
    if (FC.iTargetFanSpeed!=Current.iTargetFanSpeed)
     str+="Maximum (RPM): got '"+IntToStr(Current.iTargetFanSpeed)+" RPM' instead of '"+IntToStr(FC.iTargetFanSpeed)+" RPM'\n";
    if (FC.iTargetTemperature!=Current.iTargetTemperature)
     str+="Target Temp.: (�C): got '"+IntToStr(Current.iTargetTemperature)+" �C' instead of '"+IntToStr(FC.iTargetTemperature)+" �C'\n";
    /////////////////////////////
    if (FC.iMinPerformanceClock!=Current.iMinPerformanceClock)
     str+="Acoustic Limit (MHz): got '"+IntToStr(Current.iMinPerformanceClock)+" MHz' instead of '"+IntToStr(FC.iMinPerformanceClock)+" MHz'\n";

    if (str!="")
     {
      str="Fan values mismatch for GPU "+IntToStr(GpuInd)+".\n"+str;
      if (CmdMsg)
       Cmd.ErrorInfoShow(str, CmdMsg, 0, 0, false); else
       blad(str, 0, false);
     }
   }
 }

return retcode;
}
//---------------------------------------------------------------------------
bool OverdriveN::SetValues_Power(int GpuInd, bool ReplaceObsolete, int Power_Temp, int Power_Target, String errmsg, bool CmdMsg)
{
bool retcode=true;
//====== auto reset
if (Options.AutoReset)
 {
  ADLODNPowerLimitSetting odNPowerControl;
  memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
  odNPowerControl.iMaxOperatingTemperature=Power_Temp;
  odNPowerControl.iTDPLimit=Power_Target;
  odNPowerControl.iMode=ADLODNControlType::ODNControlType_Manual;
  if (ADL_OK==ADL2_OverdriveN_PowerLimit_Set(context, GPU[GpuInd].AdIndexes[0], &odNPowerControl))
   {
    memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
    odNPowerControl.iMaxOperatingTemperature=-1;
    odNPowerControl.iMode=ADLODNControlType::ODNControlType_Default;
    ADL2_OverdriveN_PowerLimit_Set(context, GPU[GpuInd].AdIndexes[0], &odNPowerControl);
   }
 }
//====
ADLODNPowerLimitSetting PC;
memset(&PC, 0, sizeof(ADLODNPowerLimitSetting));
PC.iMaxOperatingTemperature=Power_Temp;
PC.iTDPLimit=Power_Target;
PC.iMode=ADLODNControlType::ODNControlType_Manual;
//====
// Replace obsolete values since driver 18.12.2
if (ReplaceObsolete)
 {
  ADLODNPowerLimitSetting odNPowerControl;
  memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
  if (ADL_OK==ADL2_OverdriveN_PowerLimit_Get(context,GPU[GpuInd].AdIndexes[0], &odNPowerControl))
   {
    PC.iMaxOperatingTemperature=odNPowerControl.iMaxOperatingTemperature;
   }
 }
//====
int an=ADL2_OverdriveN_PowerLimit_Set(context, GPU[GpuInd].AdIndexes[0], &PC);
if (ADL_OK != an)
 {
  String str=errmsg;
  if (errmsg=="")
   str="Failed to set Power values";
  String msg;
  if (an==-1)
   {
    if (CheckIfLimitsExceed(GpuInd, NULL, NULL, &PC, &msg, false))
     {
      str+=msg;
      an=0;
     }
   }
  if (CmdMsg)
   Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
    blad(str, an, false);
  retcode=false;
 }


if (retcode && Options.CheckValuesMismatch)
 {
  ADLODNPowerLimitSetting odNPowerControl;
  memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
  int ret=ADL2_OverdriveN_PowerLimit_Get(context, GPU[GpuInd].AdIndexes[0], &odNPowerControl);
  if (ADL_OK==ret)
   {
    String str;
     //Obsolete in 18.12.2 ///
    if (PC.iMaxOperatingTemperature!=odNPowerControl.iMaxOperatingTemperature)
     str+="Max Temp. (�C): got '"+IntToStr(odNPowerControl.iMaxOperatingTemperature)+" �C' instead of '"+IntToStr(PC.iMaxOperatingTemperature)+" �C'\n";
    //////////////////////////
    if (PC.iTDPLimit!=odNPowerControl.iTDPLimit)
     str+="Power Target (%): got '"+IntToStr(odNPowerControl.iTDPLimit)+" %' instead of '"+IntToStr(PC.iTDPLimit)+" %'\n";
    if (str!="")
     {
      str="Power values mismatch for GPU "+IntToStr(GpuInd)+".\n"+str;
      if (CmdMsg)
       Cmd.ErrorInfoShow(str, CmdMsg, 0, 0, false); else
        blad(str, 0, false);
     }
   }
 }

return retcode;
}
//---------------------------------------------------------------------------
bool OverdriveN::ExtSettings_Get(int GpuInd, ADLODNExtSettingsInfo *ExtInfo, ADLODNExtSettings *ExtCurrent, bool CmdMsg)
{
bool retcode=true;
int overdriveNExtCapabilities;
int numberOfODNExtFeatures = ODN_COUNT;
ADLODNExtSingleInitSetting* lpInitSettingList = NULL;
int* lpCurrentSettingList = NULL;
int an=ADL2_OverdriveN_SettingsExt_Get(context, GPU[GpuInd].AdIndexes[0], &overdriveNExtCapabilities, &numberOfODNExtFeatures, &lpInitSettingList, &lpCurrentSettingList);
if (ADL_OK == an && lpInitSettingList!=NULL && lpCurrentSettingList!=NULL)
 {
  if (ExtCurrent!=NULL)
   memcpy(ExtCurrent, lpCurrentSettingList, sizeof(ADLODNExtSettings));
  if (ExtInfo!=NULL)
   memcpy(ExtInfo, lpInitSettingList, sizeof(ADLODNExtSettingsInfo));
  ADL_Main_Memory_Free((void**)&lpInitSettingList);
  ADL_Main_Memory_Free((void**)&lpCurrentSettingList);
 } else
  {
   String str="Failed to read Fan curve, Zero RPM and Memory Timing Level for GPU "+IntToStr(GpuInd);
   if (CmdMsg)
    Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
     blad(str, an, false);
   retcode=false;
  }
return retcode;
}
//---------------------------------------------------------------------------
bool OverdriveN::ExtSettings_Set(int GpuInd, ADLODNExtSettings *ExtCurrent, bool CmdMsg)
{
bool retcode=true;

ADLODNExtSettings ValidList={1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
if (GPU[GpuInd].FanEnabled==false)
 {
  for (int i = 0; i < 5; i++)
   {
    ValidList.Point[i].Temp=0;
    ValidList.Point[i].Percentage=0;
   }
  ValidList.ZeroRPM=0;
 } else
  {
   if (memcmp(&GPU[GpuInd].ExtSettingsDefault.Point, ExtCurrent->Point, sizeof(ADLODNFanCurvePoint[5]))==0)
    {
     for (int i = 0; i < 5; i++)
      {
       ExtCurrent->Point[i].Temp=-1;
       ExtCurrent->Point[i].Percentage=-1;
      }
    }
  }

//========================
//Prze��czamy auto OC/UV na Manual je�eli nie Reset
if (ExtCurrent->AutoUVEngine!=-1)
 {
  ExtCurrent->AutoUVEngine=0;
  ExtCurrent->AutoOCEngine=0;
  ExtCurrent->AutoOCMemory=0;
 }
//========================

int sSize=ODN_COUNT;
int an= ADL2_OverdriveN_SettingsExt_Set(context, GPU[GpuInd].AdIndexes[0], sSize, (int*)&ValidList, (int*)ExtCurrent);

if (ADL_OK != an)
 {
  String str="Failed to set Fan curve for GPU "+IntToStr(GpuInd);
  if (CmdMsg)
   Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
     blad(str, an, false);
  retcode=false;
 }


if (retcode && Options.CheckValuesMismatch && ExtCurrent->AutoUVEngine!=-1) //nie reset
 {
  ADLODNExtSettings ReadValues;
  if (ExtSettings_Get(GpuInd, NULL, &ReadValues, CmdMsg))
   {
    String str="";
    if (ExtCurrent->Point[0].Temp!=-1)
     {
      for (int i = 0; i < 5; i++)
       {
        if (ExtCurrent->Point[i].Temp!=ReadValues.Point[i].Temp)
         str+="Fan P"+IntToStr(i)+": got '"+IntToStr(ReadValues.Point[i].Temp)+" �C' instead of '"+IntToStr(ExtCurrent->Point[i].Temp)+" �C'\n";
        if (ExtCurrent->Point[i].Percentage!=ReadValues.Point[i].Percentage)
         str+="Fan P"+IntToStr(i)+": got '"+IntToStr(ReadValues.Point[i].Percentage)+" %' instead of '"+IntToStr(ExtCurrent->Point[i].Percentage)+" %'\n";
       }
     }
    if (ExtCurrent->MemTimingLevel!=ReadValues.MemTimingLevel)
     str+="Memory Timing Level: got '"+IntToStr(ReadValues.MemTimingLevel)+"' instead of '"+IntToStr(ExtCurrent->MemTimingLevel)+"'\n";
    if (ExtCurrent->ZeroRPM!=ReadValues.ZeroRPM)
     str+=String("Fan ZeroRPM: got '")+(ReadValues.ZeroRPM==1 ? "ON" : "OFF")+"' instead of '"+(ExtCurrent->ZeroRPM==1 ? "ON" : "OFF")+"'\n";
    if (str!="")
     {
      str=String("Fan")+(Pos("Memory",str)>0 ? "/Memory" : "") +" values mismatch for GPU "+IntToStr(GpuInd)+".\n"+str;
      if (CmdMsg)
       Cmd.ErrorInfoShow(str, CmdMsg, 0, 0, false); else
        blad(str, 0, false);
     }
   }
 }

return retcode;
}
//---------------------------------------------------------------------------
bool OverdriveN::ExtSettings_Reset(int GpuInd, bool CmdMsg)
{
ADLODNExtSettings ExtCurrent={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
return ExtSettings_Set(GpuInd, &ExtCurrent, CmdMsg);
}
//---------------------------------------------------------------------------
bool Overdrive8::GetValues(int GpuInd, ADLOD8SingleInitSetting* Default, Odn8Settings* Current, bool CmdMsg)
{




bool retcode=true;
if (Default!=NULL)
 {
  int overdrive8Capabilities;
  int numberOfFeatures = OD8_COUNT;
  ADLOD8SingleInitSetting* lpInitSettingList = NULL;
  int an=ADL2_Overdrive8_Init_SettingX2_Get(context, GPU[GpuInd].AdIndexes[0], &overdrive8Capabilities, &numberOfFeatures, &lpInitSettingList);
  if (ADL_OK == an && lpInitSettingList!=NULL)
   {
    memcpy(Default, lpInitSettingList, sizeof(ADLOD8SingleInitSetting)*OD8_COUNT);

    //==========
    //Capabilites
      /* 
    for (int i = OD8_GFXCLK_FREQ1; i <= OD8_GFXCLK_VOLTAGE3; i++)
     {
      GPU[GpuInd].Od8Caps[i]= ((overdrive8Capabilities & ADL_OD8_GFXCLK_CURVE)==ADL_OD8_GFXCLK_CURVE);
     }
    GPU[GpuInd].Od8Caps[OD8_GFXCLK_FMAX]=((overdrive8Capabilities & ADL_OD8_GFXCLK_LIMITS)==ADL_OD8_GFXCLK_LIMITS);
    GPU[GpuInd].Od8Caps[OD8_GFXCLK_FMIN]=((overdrive8Capabilities & ADL_OD8_GFXCLK_LIMITS)==ADL_OD8_GFXCLK_LIMITS);
    GPU[GpuInd].Od8Caps[OD8_UCLK_FMAX]=((overdrive8Capabilities & ADL_OD8_UCLK_MAX)==ADL_OD8_UCLK_MAX);

    GPU[GpuInd].Od8Caps[OD8_POWER_PERCENTAGE]=((overdrive8Capabilities & ADL_OD8_POWER_LIMIT)==ADL_OD8_POWER_LIMIT);
    GPU[GpuInd].Od8Caps[OD8_FAN_ACOUSTIC_LIMIT]=((overdrive8Capabilities & ADL_OD8_ACOUSTIC_LIMIT_SCLK)==ADL_OD8_ACOUSTIC_LIMIT_SCLK);
    GPU[GpuInd].Od8Caps[OD8_AC_TIMING]=((overdrive8Capabilities & ADL_OD8_MEMORY_TIMING_TUNE)==ADL_OD8_MEMORY_TIMING_TUNE);
    GPU[GpuInd].Od8Caps[OD8_FAN_ZERORPM_CONTROL]=((overdrive8Capabilities & ADL_OD8_FAN_ZERO_RPM_CONTROL)==ADL_OD8_FAN_ZERO_RPM_CONTROL);
    for (int i = OD8_FAN_CURVE_TEMPERATURE_1; i <= OD8_FAN_CURVE_SPEED_5; i++)
     {
      GPU[GpuInd].Od8Caps[i]= ((overdrive8Capabilities & ADL_OD8_FAN_CURVE)==ADL_OD8_FAN_CURVE);
     }

    //Excluded (not used) Capabilites
    GPU[GpuInd].Od8Caps[OD8_FAN_MIN_SPEED]=false;
    GPU[GpuInd].Od8Caps[OD8_FAN_TARGET_TEMP]=false;
    GPU[GpuInd].Od8Caps[OD8_OPERATING_TEMP_MAX]=false;
          */
    //Zamiennik powy�szych
    for (int i = 0; i < OD8_COUNT; i++)
     {
      GPU[GpuInd].Od8Caps[i]=((lpInitSettingList[i].featureID>0) && ((overdrive8Capabilities & lpInitSettingList[i].featureID)==lpInitSettingList[i].featureID));
     }
    //==========


    ADL_Main_Memory_Free((void**)&lpInitSettingList);
   } else
    {
      String str="ADL2_Overdrive8_Init_SettingX2_Get function failed for GPU "+IntToStr(GpuInd);
      if (CmdMsg)
       Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
        blad(str, an, false);
      retcode=false;
    }
 }
if (Current!=NULL)
 {
  int numberOfFeaturesCurrent = OD8_COUNT;
  int* lpCurrentSettingList = NULL;
  int an=ADL2_Overdrive8_Current_SettingX2_Get(context, GPU[GpuInd].AdIndexes[0], &numberOfFeaturesCurrent, &lpCurrentSettingList);
  if (ADL_OK == an && lpCurrentSettingList!=NULL)
   {
    memcpy(Current, lpCurrentSettingList, sizeof(Odn8Settings));


    ADL_Main_Memory_Free((void**)&lpCurrentSettingList);
   } else
    {
      String str="ADL2_Overdrive8_Current_SettingX2_Get function failed for GPU "+IntToStr(GpuInd);
      if (CmdMsg)
       Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
        blad(str, an, false);
      retcode=false;
    }
 }
return retcode;
}
//---------------------------------------------------------------------------
bool Overdrive8::SetValues(int GpuInd, Odn8Settings* Current, bool CmdMsg)
{
//====== auto reset
if (Options.AutoReset)
 Od8Set(GpuInd, true, NULL, CmdMsg);
//=================

return Od8Set(GpuInd, false, Current, CmdMsg);
}
//---------------------------------------------------------------------------
bool Overdrive8::Reset(int GpuInd, bool CmdMsg)
{
return Od8Set(GpuInd, true, NULL, CmdMsg);
}
//---------------------------------------------------------------------------
bool Overdrive8::Od8Set(int GpuInd, bool Reset, Odn8Settings* Current, bool CmdMsg)
{
bool retcode=true;

ADLOD8SetSetting odSetSetting;
memset(&odSetSetting, 0, sizeof(ADLOD8SetSetting));
odSetSetting.count = OD8_COUNT;

int TempIn[OD8_COUNT];
if (Reset==false)
 memcpy(&TempIn, Current, sizeof(Odn8Settings));

//========================
//Prze��czamy auto OC/UV na Manual
TempIn[OD8_AUTO_UV_ENGINE_CONTROL]=0;
TempIn[OD8_AUTO_OC_ENGINE_CONTROL]=0;
TempIn[OD8_AUTO_OC_MEMORY_CONTROL]=0;
//========================

for (int i = 0; i < OD8_COUNT; i++)
 {
   //excluded features=======
   if ((i==OD8_FAN_MIN_SPEED) ||            //Fan_Min
       (i==OD8_FAN_TARGET_TEMP) ||          //Fan_Target
       (i==OD8_OPERATING_TEMP_MAX) ||       //Power_Temp
       (GPU[GpuInd].FanEnabled==false && (i==OD8_FAN_ZERORPM_CONTROL || i==OD8_FAN_ACOUSTIC_LIMIT || i>=OD8_FAN_CURVE_TEMPERATURE_1))) //FanEnabled
        {
         continue;
        }
   //========================
   odSetSetting.od8SettingTable[i].requested=1;
   odSetSetting.od8SettingTable[i].reset=Reset;
   if (Reset==false)
    {
     odSetSetting.od8SettingTable[i].value=TempIn[i];
    } else
     {
      odSetSetting.od8SettingTable[i].value=GPU[GpuInd].Od8Default[i].defaultValue;
     }
   //========================
 }


ADLOD8CurrentSetting odCurrentSetting;
memset(&odCurrentSetting, 0, sizeof(ADLOD8CurrentSetting));
odCurrentSetting.count=OD8_COUNT;

int an=ADL2_Overdrive8_Setting_Set(context, GPU[GpuInd].AdIndexes[0], &odSetSetting, &odCurrentSetting);
if (ADL_OK != an)
 {
  String str;
  if (Reset)
   str="Failed to reset values for GPU "+IntToStr(GpuInd); else
    str="Failed to set values for GPU "+IntToStr(GpuInd);
  if (CmdMsg)
   Cmd.ErrorInfoShow(str, CmdMsg, an, 0, false); else
    blad(str, an, false);
  retcode=false;
 }

if (retcode && Options.CheckValuesMismatch && Reset==false)
 {
  Odn8Settings Od8Out;
  memcpy(&Od8Out, &odCurrentSetting.Od8SettingTable, sizeof(Odn8Settings));

  String str="";
  for (int i = 0; i < 3; i++)
   {
    if (Od8Out.GPU_P[i].Clock!=Current->GPU_P[i].Clock)
     str+="GPU P"+IntToStr(i)+": got '"+IntToStr(Od8Out.GPU_P[i].Clock)+" MHz' instead of '"+IntToStr(Current->GPU_P[i].Clock)+" MHz'\n";
    if (Od8Out.GPU_P[i].Voltage!=Current->GPU_P[i].Voltage)
     str+="GPU P"+IntToStr(i)+": got '"+IntToStr(Od8Out.GPU_P[i].Voltage)+" mV' instead of '"+IntToStr(Current->GPU_P[i].Voltage)+" mV'\n";
   }

  if (Od8Out.GPU_Min!=Current->GPU_Min)
   str+="GPU Minimum: got '"+IntToStr(Od8Out.GPU_Min)+" MHz' instead of '"+IntToStr(Current->GPU_Min)+" MHz'\n";
  if (Od8Out.GPU_Max!=Current->GPU_Max)
   str+="GPU Maximum: got '"+IntToStr(Od8Out.GPU_Max)+" MHz' instead of '"+IntToStr(Current->GPU_Max)+" MHz'\n";
  if (Od8Out.Mem_Max!=Current->Mem_Max)
   str+="Memory Maximum: got '"+IntToStr(Od8Out.Mem_Max)+" MHz' instead of '"+IntToStr(Current->Mem_Max)+" MHz'\n";

  if (Od8Out.ExtSettings.MemTimingLevel!=Current->ExtSettings.MemTimingLevel)
   str+="Memory Timing Level: got '"+IntToStr(Od8Out.ExtSettings.MemTimingLevel)+"' instead of '"+IntToStr(Current->ExtSettings.MemTimingLevel)+"'\n";

  for (int i = 0; i < 5; i++)
   {
    if (Od8Out.ExtSettings.Point[i].Temp!=Current->ExtSettings.Point[i].Temp)
     str+="Fan P"+IntToStr(i)+": got '"+IntToStr(Od8Out.ExtSettings.Point[i].Temp)+" �C' instead of '"+IntToStr(Current->ExtSettings.Point[i].Temp)+" �C'\n";
    if (Od8Out.ExtSettings.Point[i].Percentage!=Current->ExtSettings.Point[i].Percentage)
     str+="Fan P"+IntToStr(i)+": got '"+IntToStr(Od8Out.ExtSettings.Point[i].Percentage)+" %' instead of '"+IntToStr(Current->ExtSettings.Point[i].Percentage)+" %'\n";
   }

  if (Od8Out.ExtSettings.ZeroRPM!=Current->ExtSettings.ZeroRPM)
   str+=String("Fan ZeroRPM: got '")+(Od8Out.ExtSettings.ZeroRPM==1 ? "ON" : "OFF")+"' instead of '"+(Current->ExtSettings.ZeroRPM==1 ? "ON" : "OFF")+"'\n";
  if (Od8Out.Fan_Acoustic!=Current->Fan_Acoustic)
   str+="Fan Acoustic Limit: got '"+IntToStr(Od8Out.Fan_Acoustic)+" MHz' instead of '"+IntToStr(Current->Fan_Acoustic)+" MHz'\n";

  if (Od8Out.Power_Target!=Current->Power_Target)
  str+="Power Target: got '"+IntToStr(Od8Out.Power_Target)+" %' instead of '"+IntToStr(Current->Power_Target)+" %'\n";

  if (str!="")
   {
    str="Values mismatch for GPU "+IntToStr(GpuInd)+".\n"+str;
    if (CmdMsg)
     Cmd.ErrorInfoShow(str, CmdMsg, 0, 0, false); else
      blad(str, 0, false);
   }
 }

return retcode;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void __fastcall TForm1::SetCurrent(int GpuInd)
{
if (GPU[GpuInd].APISupported==false)
 {
   return;
 }

if (InitializeADL())
 {
  //**************************************
  //OverdriveN
  if (GPU[GpuInd].APIVersion==7)
   {
    const int size = sizeof(ADLODNPerformanceLevelsX2) + (sizeof(ADLODNPerformanceLevelX2)*(GPU[GpuInd].MaxLevels-1));
    ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
    pL->iSize=size;
    //GPU
    if (GPU[GpuInd].GPULevels>=1)
        {
         if (GPU_Clock_0->Value==0)
          GPU_Clock_0->Value=1;
         if (GPU_Vcc_0->Value==0)
          GPU_Vcc_0->Value=1;
         pL->aLevels[0].iClock=GPU_Clock_0->Value*100;
         pL->aLevels[0].iVddc=GPU_Vcc_0->Value;
         pL->aLevels[0].iEnabled=GPU_Clock_0->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=2)
        {
         if (GPU_Clock_1->Value==0)
          GPU_Clock_1->Value=1;
         if (GPU_Vcc_1->Value==0)
          GPU_Vcc_1->Value=1;
         pL->aLevels[1].iClock=GPU_Clock_1->Value*100;
         pL->aLevels[1].iVddc=GPU_Vcc_1->Value;
         pL->aLevels[1].iEnabled=GPU_Clock_1->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=3)
        {
         if (GPU_Clock_2->Value==0)
          GPU_Clock_2->Value=1;
         if (GPU_Vcc_2->Value==0)
          GPU_Vcc_2->Value=1;
         pL->aLevels[2].iClock=GPU_Clock_2->Value*100;
         pL->aLevels[2].iVddc=GPU_Vcc_2->Value;
         pL->aLevels[2].iEnabled=GPU_Clock_2->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=4)
        {
         if (GPU_Clock_3->Value==0)
          GPU_Clock_3->Value=1;
         if (GPU_Vcc_3->Value==0)
          GPU_Vcc_3->Value=1;
         pL->aLevels[3].iClock=GPU_Clock_3->Value*100;
         pL->aLevels[3].iVddc=GPU_Vcc_3->Value;
         pL->aLevels[3].iEnabled=GPU_Clock_3->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=5)
        {
         if (GPU_Clock_4->Value==0)
          GPU_Clock_4->Value=1;
         if (GPU_Vcc_4->Value==0)
          GPU_Vcc_4->Value=1;

         pL->aLevels[4].iClock=GPU_Clock_4->Value*100;
         pL->aLevels[4].iVddc=GPU_Vcc_4->Value;
         pL->aLevels[4].iEnabled=GPU_Clock_4->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=6)
        {
         if (GPU_Clock_5->Value==0)
          GPU_Clock_5->Value=1;
         if (GPU_Vcc_5->Value==0)
          GPU_Vcc_5->Value=1;
         pL->aLevels[5].iClock=GPU_Clock_5->Value*100;
         pL->aLevels[5].iVddc=GPU_Vcc_5->Value;
         pL->aLevels[5].iEnabled=GPU_Clock_5->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=7)
        {
         if (GPU_Clock_6->Value==0)
          GPU_Clock_6->Value=1;
         if (GPU_Vcc_6->Value==0)
          GPU_Vcc_6->Value=1;
         pL->aLevels[6].iClock=GPU_Clock_6->Value*100;
         pL->aLevels[6].iVddc=GPU_Vcc_6->Value;
         pL->aLevels[6].iEnabled=GPU_Clock_6->Enabled;
        }
    if (GPU[GpuInd].GPULevels>=8)
        {
         if (GPU_Clock_7->Value==0)
          GPU_Clock_7->Value=1;
         if (GPU_Vcc_7->Value==0)
          GPU_Vcc_7->Value=1;
         pL->aLevels[7].iClock=GPU_Clock_7->Value*100;
         pL->aLevels[7].iVddc=GPU_Vcc_7->Value;
         pL->aLevels[7].iEnabled=GPU_Clock_7->Enabled;
        }
    OverdriveN.SetValues_GPUMem(GpuInd, true, pL, size, "Failed to set GPU values", false);
    //Memory
    memset(pL, 0, size);
    pL->iSize=size;
    if (GPU[GpuInd].MemoryLevels>=1)
        {
         if (Mem_Clock_0->Value==0)
          Mem_Clock_0->Value=1;
         if (Mem_Vcc_0->Value==0)
          Mem_Vcc_0->Value=1;
         pL->aLevels[0].iClock=Mem_Clock_0->Value*100;
         pL->aLevels[0].iVddc=Mem_Vcc_0->Value;
         pL->aLevels[0].iEnabled=Mem_Clock_0->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=2)
        {
         if (Mem_Clock_1->Value==0)
          Mem_Clock_1->Value=1;
         if (Mem_Vcc_1->Value==0)
          Mem_Vcc_1->Value=1;
         pL->aLevels[1].iClock=Mem_Clock_1->Value*100;
         pL->aLevels[1].iVddc=Mem_Vcc_1->Value;
         pL->aLevels[1].iEnabled=Mem_Clock_1->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=3)
        {
         if (Mem_Clock_2->Value==0)
          Mem_Clock_2->Value=1;
         if (Mem_Vcc_2->Value==0)
          Mem_Vcc_2->Value=1;
         pL->aLevels[2].iClock=Mem_Clock_2->Value*100;
         pL->aLevels[2].iVddc=Mem_Vcc_2->Value;
         pL->aLevels[2].iEnabled=Mem_Clock_2->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=4)
        {
         if (Mem_Clock_3->Value==0)
          Mem_Clock_3->Value=1;
         if (Mem_Vcc_3->Value==0)
          Mem_Vcc_3->Value=1;
         pL->aLevels[3].iClock=Mem_Clock_3->Value*100;
         pL->aLevels[3].iVddc=Mem_Vcc_3->Value;
         pL->aLevels[3].iEnabled=Mem_Clock_3->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=5)
        {
         if (Mem_Clock_4->Value==0)
          Mem_Clock_4->Value=1;
         if (Mem_Vcc_4->Value==0)
          Mem_Vcc_4->Value=1;
         pL->aLevels[4].iClock=Mem_Clock_4->Value*100;
         pL->aLevels[4].iVddc=Mem_Vcc_4->Value;
         pL->aLevels[4].iEnabled=Mem_Clock_4->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=6)
        {
         if (Mem_Clock_5->Value==0)
          Mem_Clock_5->Value=1;
         if (Mem_Vcc_5->Value==0)
          Mem_Vcc_5->Value=1;
         pL->aLevels[5].iClock=Mem_Clock_5->Value*100;
         pL->aLevels[5].iVddc=Mem_Vcc_5->Value;
         pL->aLevels[5].iEnabled=Mem_Clock_5->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=7)
        {
         if (Mem_Clock_6->Value==0)
          Mem_Clock_6->Value=1;
         if (Mem_Vcc_6->Value==0)
          Mem_Vcc_6->Value=1;
         pL->aLevels[6].iClock=Mem_Clock_6->Value*100;
         pL->aLevels[6].iVddc=Mem_Vcc_6->Value;
         pL->aLevels[6].iEnabled=Mem_Clock_6->Enabled;
        }
    if (GPU[GpuInd].MemoryLevels>=8)
        {
         if (Mem_Clock_7->Value==0)
          Mem_Clock_7->Value=1;
         if (Mem_Vcc_7->Value==0)
          Mem_Vcc_7->Value=1;
         pL->aLevels[7].iClock=Mem_Clock_7->Value*100;
         pL->aLevels[7].iVddc=Mem_Vcc_7->Value;
         pL->aLevels[7].iEnabled=Mem_Clock_7->Enabled;
        }
    OverdriveN.SetValues_GPUMem(GpuInd, false, pL, size, "Failed to set Memory values", false);
    free(pL);
    //Fan
    if (GPU[GpuInd].FanEnabled)
     {
      OverdriveN.SetValues_Fan(GpuInd, true, Fan_Min->Value, Fan_Max->Value, Fan_Target->Value, Fan_Acoustic->Value, "Failed to set Fan values", false);
     }
    //Fan Curve, Zero RPM, Mem Timing

    ADLODNExtSettings *ExtCurrent= new ADLODNExtSettings;
    memset(ExtCurrent, 0, sizeof(ADLODNExtSettings));
    ExtCurrent->Point[0].Temp=Fan_Temp_0->Value;
    ExtCurrent->Point[0].Percentage=Fan_Perc_0->Value;
    ExtCurrent->Point[1].Temp=Fan_Temp_1->Value;
    ExtCurrent->Point[1].Percentage=Fan_Perc_1->Value;
    ExtCurrent->Point[2].Temp=Fan_Temp_2->Value;
    ExtCurrent->Point[2].Percentage=Fan_Perc_2->Value;
    ExtCurrent->Point[3].Temp=Fan_Temp_3->Value;
    ExtCurrent->Point[3].Percentage=Fan_Perc_3->Value;
    ExtCurrent->Point[4].Temp=Fan_Temp_4->Value;
    ExtCurrent->Point[4].Percentage=Fan_Perc_4->Value;

    ExtCurrent->MemTimingLevel=Mem_TimingBox->ItemIndex;
    ExtCurrent->ZeroRPM=ZeroRPMBox->Checked;

    OverdriveN.ExtSettings_Set(GpuInd, ExtCurrent, false);
    delete ExtCurrent;
    //Power
    OverdriveN.SetValues_Power(GpuInd, true, Power_Temp->Value, Power_Target->Value, "Failed to set Power values", false);
    //=====
   } else
  //**************************************
    //Overdrive8
    if (GPU[GpuInd].APIVersion==8)
     {
      Odn8Settings Od8Current;
      memset(&Od8Current, 0, sizeof(Odn8Settings));
      //GPU
      Od8Current.GPU_P[0].Clock=GPU_Clock_0->Value;
      Od8Current.GPU_P[0].Voltage=GPU_Vcc_0->Value;
      Od8Current.GPU_P[1].Clock=GPU_Clock_1->Value;
      Od8Current.GPU_P[1].Voltage=GPU_Vcc_1->Value;
      Od8Current.GPU_P[2].Clock=GPU_Clock_2->Value;
      Od8Current.GPU_P[2].Voltage=GPU_Vcc_2->Value;
      Od8Current.GPU_Min=GPU_Min->Value;
      Od8Current.GPU_Max=GPU_Max->Value;
      //Memory
      Od8Current.Mem_Max=Mem_Max->Value;
      //Fan Curve, Zero RPM, Mem Timing
      Od8Current.ExtSettings.Point[0].Temp=Fan_Temp_0->Value;
      Od8Current.ExtSettings.Point[0].Percentage=Fan_Perc_0->Value;
      Od8Current.ExtSettings.Point[1].Temp=Fan_Temp_1->Value;
      Od8Current.ExtSettings.Point[1].Percentage=Fan_Perc_1->Value;
      Od8Current.ExtSettings.Point[2].Temp=Fan_Temp_2->Value;
      Od8Current.ExtSettings.Point[2].Percentage=Fan_Perc_2->Value;
      Od8Current.ExtSettings.Point[3].Temp=Fan_Temp_3->Value;
      Od8Current.ExtSettings.Point[3].Percentage=Fan_Perc_3->Value;
      Od8Current.ExtSettings.Point[4].Temp=Fan_Temp_4->Value;
      Od8Current.ExtSettings.Point[4].Percentage=Fan_Perc_4->Value;
      Od8Current.ExtSettings.ZeroRPM=ZeroRPMBox->Checked;
      Od8Current.ExtSettings.MemTimingLevel=Mem_TimingBox->ItemIndex;
      //Acoustic Limit
      Od8Current.Fan_Acoustic=Fan_Acoustic->Value;
      //Power
      Od8Current.Power_Target=Power_Target->Value;

      Overdrive8.SetValues(GpuInd, &Od8Current, false);
     }
  //**************************************
  DeinitializeADL();
 }
return;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
bool Cmd::AttachToConsole()
{
if (CosoleAttached)
 return true;

if (!AttachConsole(ATTACH_PARENT_PROCESS))
  {
   DWORD err=GetLastError();
   //ERROR_GEN_FAILURE-brak parenta, ERROR_INVALID_HANDLE - parent nie posiada konsoli, ERROR_ACCESS_DENIED - proces ma ju� konsol�
   if (err==ERROR_ACCESS_DENIED) //already has a console
     {
       CosoleAttached=true;
       return true;
     } else
      {
       LogWrite("AttachConsole function failed", err, true, 0);
       return false;
      }
  } else
    {
      HANDLE h=GetStdHandle(STD_OUTPUT_HANDLE);
      if (h!=INVALID_HANDLE_VALUE)
       {
         int osh=_open_osfhandle((__int32)h, 0);
         if (osh!=-1)
          {
           if (dup2(osh, 1)==ERROR_SUCCESS)
            {
              CosoleAttached=true;
              return true;
            }
          }
       } else
        {
          LogWrite("GetStdHandle function failed", GetLastError(), true, 0);
          return false;
        }
    }
return false;
}
//---------------------------------------------------------------------------
//typ: 0 error 1-OK message 2 -Info message 3-warning
void Cmd::ErrorInfoShow(const String msg, bool console, int ErrCode, int typ, bool WindowsError)
{
if ((Options.IgnoreError8) && (ErrCode==ADL_ERR_NOT_SUPPORTED) && (WindowsError==false))
 {
   return;
 }
//console message
if (console)
 {
   if (Cmd::AttachToConsole())
     {
       if (msg!="")        //pusty message oznacza �e tylko zapisujemy tylko now� lini�
        {
         String str=msg;    
         if (typ==0)
          {
            str="ERROR: "+FormatErrorMessage(str, ErrCode, WindowsError);
            SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);   //kolorujemy na czerwono
          } else
           if ((typ==1) || (typ==2))
             {
              str="INFO: "+str;
              SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);   //kolorujemy na jasno-zielono
             } else
              if (typ==3)
               {
                str="WARNING: "+str;
                SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);   //kolorujemy na ��to
               }
         str=StringReplace(str, '\x0d', "", TReplaceFlags() << rfReplaceAll);
         str=StringReplace(str, '\x0a', "", TReplaceFlags() << rfReplaceAll);
         str+="\n";
         wprintf(str.w_str());
        }
      // wprintf(L"\n");
       SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);     //kolorujemy na bia�o //standardowy kolor
     } else
       {
         console=false;
         if (msg=="") return;
       }
 }
//Win32 GUI message
if (console==false)
 {
  if (typ==0)    //b��d
    {
      blad(msg, ErrCode, WindowsError);
    } else
      {
        if (typ==1)    //OK_message
         {
          MessageBox(0, msg.w_str(), L"OverdriveNTool", MB_OK);
         } else
           {
            if (typ==2)    //Info message
             {
              MessageBox(0, msg.w_str(), L"OverdriveNTool", MB_ICONINFORMATION|MB_OK);
             } else
              if (typ==3)    //Warning
                {
                  MessageBox(0, msg.w_str(), L"OverdriveNTool", MB_ICONWARNING|MB_OK);
                }
           }
      }
 }
return;
}
//---------------------------------------------------------------------------
void Cmd::ShowGUI()
{
GPU.Length=0;
Profiles.Length=0;
}
//---------------------------------------------------------------------------
void Cmd::PrintCurrentValues()
{
Cmd::AttachToConsole();
for (int i = 0; i < GPU.Length; i++)
 {
   if (!GPU[i].APISupported)
    continue;
   if (!Cmd::ReadCurrent(i, true))
    {
     Cmd::ErrorInfoShow("Could not get current values for GPU "+IntToStr(i), true, NULL, 0, false);
     continue;
    }
   String str=GPU[i].IDString;
   String s;
   for (int j = 0; j < CurrentValues.GPU_Clock.Length; j++)
    {
      s=IntToStr(CurrentValues.GPU_Clock[j])+";"+IntToStr(CurrentValues.GPU_Vcc[j]);
      if (CurrentValues.GPU_Enabled[j]==false && GPU[i].APIVersion==7)
       s+=";0";
      str+="|GPU_P"+IntToStr(j)+"="+s;
    }
   for (int j = 0; j < CurrentValues.Mem_Clock.Length; j++)
    {
     s=IntToStr(CurrentValues.Mem_Clock[j])+";"+IntToStr(CurrentValues.Mem_Vcc[j]);
     if (CurrentValues.Mem_Enabled[j]==false && GPU[i].APIVersion==7)
      s+=";0";
     str+="|Mem_P"+IntToStr(j)+"="+s;
    }
  // str+="|Fan_Min="+IntToStr(CurrentValues.Fan_Min);             //Obsoltete in 18.12.2
 //  str+="|Fan_Max="+IntToStr(CurrentValues.Fan_Max);             //Obsoltete in 18.12.2
 //  str+="|Fan_Target="+IntToStr(CurrentValues.Fan_Target);       //Obsoltete in 18.12.2
   str+="|Fan_Acoustic="+IntToStr(CurrentValues.Fan_Acoustic);
 //  str+="|Power_Temp="+IntToStr(CurrentValues.Power_Temp);       //Obsoltete in 18.12.2
   str+="|Power_Target="+IntToStr(CurrentValues.Power_Target);
   for (int j = 0; j < 5; j++)
    {
     s=IntToStr(CurrentValues.FanCurvePoints[j].Temp)+";"+IntToStr(CurrentValues.FanCurvePoints[j].Percentage);
     str+="|Fan_P"+IntToStr(j)+"="+s;
    }
   str+="|Fan_ZeroRPM="+IntToStr(CurrentValues.Fan_ZeroRPM);
   str+="|Mem_TimingLevel="+IntToStr(CurrentValues.Mem_TimingLevel);
   if (GPU[i].APIVersion==8)
    {
     str+="|GPU_Min="+IntToStr(CurrentValues.GPU_Min);
     str+="|GPU_Max="+IntToStr(CurrentValues.GPU_Max);
     str+="|Mem_Max="+IntToStr(CurrentValues.Mem_Max);
    }
   str+="\n";
   wprintf(str.w_str());
 //  wprintf(L"\n");
 }
}
//---------------------------------------------------------------------------
bool Cmd::SetCustomToGPU(String GpuStr, String cm, const String OryginalCommand, bool CmdMsg)
{
bool retcode=true;
bool AffectsAllGPUs=false;
int GpuInd=-1;
int GPUsLength=1;
if (GpuStr=="*")
 {
  AffectsAllGPUs=true;
  GPUsLength=GPU.Length;
 } else
  {
   if (!TryStrToInt(GpuStr, GpuInd))
    {
     Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+GpuStr+"\" cannot be used as [gpu_id]", CmdMsg, NULL, 0, false);
     return false;
    }
   if (!CorrectGPU(GpuInd))
    {
     Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"GPU with id="+GpuStr+" not found", CmdMsg, NULL, 0, false);
     return false;
    }
   if (!SupportedGPU(GpuInd))
    {
     Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"GPU with id="+GpuStr+" is not supported", CmdMsg, NULL, 0, false);
     return false;
    }
  }

TStringList* command=new TStringList;
command->StrictDelimiter=true;
command->Delimiter=Char(30);
command->QuoteChar=Char(0);
command->DelimitedText=cm;

String ConstantFanSpeed;
String PowerEfficiency;
String Chill;

for (int m = 0; m < GPUsLength; m++)
 {
    if (AffectsAllGPUs)
     {
       if (!SupportedGPU(m)) continue;
       GpuInd=m;
     }
    //pierw odczyt aktualnych warto�ci
    if (!Cmd::ReadCurrent(GpuInd, CmdMsg))
     {
      Cmd::ErrorInfoShow("Failed to apply command: \""+ OryginalCommand+"\"\x0d\x0a"+"Could not get current values for GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
      return false;
     }
    try
     {
      //Parsowanie
      String str;
      String CurrCmd;
      int index;
      for (int i = 0; i < command->Count; i++)
       {
        CurrCmd=LowerCase(command->Strings[i]);
        str=CurrCmd;
        if ((Pos("gpu_p", CurrCmd)==1) || (Pos("mem_p", CurrCmd)==1))
         {
          if (str.SubString(6,1)=="#")     //Apply to highest PState
           {
            if (Pos("gpu_p", str)==1)
             index=CurrentValues.GPU_Clock.Length-1; else
              index=CurrentValues.Mem_Clock.Length-1;
           } else
            {
              if (!TryStrToInt(str.SubString(6,1), index))
               {
                Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+str.SubString(6,1)+"\" is not valid Pstate number", CmdMsg, NULL, 0, false);
                return false;
               }
            }
          int ps;
          if (Pos("gpu_p", str)==1)
           ps=CurrentValues.GPU_Clock.Length-1; else
            ps=CurrentValues.Mem_Clock.Length-1;

          if ((index<0) || (index>ps))
           {
            Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+IntToStr(index)+"\" exceeds Pstates number for GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
            return false;
           }

          str=str.Delete(1,7);    //jest 7 znak�w pocz�tkowych w��cznie z =, bo ilo�� Pstat�w nie przekracza 10 (jest r�wna 8)
          if (Pos(";", str)>0)
           {
             String s=str.SubString(1,Pos(";",str)-1);
             if (s!="*")            //skip value
              {
               if (Pos("gpu_p", CurrCmd)==1)
                CurrentValues.GPU_Clock[index]=StrToInt(s); else
                 CurrentValues.Mem_Clock[index]=StrToInt(s);
              }
             str.Delete(1,Pos(";",str));
             if (Pos(";0",str)>0)
              {
               if (Pos("gpu_p", CurrCmd)==1)
                CurrentValues.GPU_Enabled[index]=false; else
                 CurrentValues.Mem_Enabled[index]=false;
               str=StringReplace(str, ";0", "", TReplaceFlags() << rfIgnoreCase);
              } else
                {
                 if (Pos("gpu_p", CurrCmd)==1)
                  CurrentValues.GPU_Enabled[index]=true; else
                   CurrentValues.Mem_Enabled[index]=true;
                }
             if (str!="*")            //skip value
              {
               if (Pos("gpu_p", CurrCmd)==1)
                CurrentValues.GPU_Vcc[index]=StrToInt(str); else
                 CurrentValues.Mem_Vcc[index]=StrToInt(str);
              }
           } else
            {
             Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+command->Strings[i]+"\" is not valid parameter", CmdMsg, NULL, 0, false);
             return false;
            }
         } else
          if (Pos("fan_min=", str)==1)
           {
            str=StringReplace(str, "fan_min=", "", TReplaceFlags() << rfIgnoreCase);
            CurrentValues.Fan_Min=StrToInt(str);
           } else
            if (Pos("fan_max=", str)==1)
             {
              str=StringReplace(str, "fan_max=", "", TReplaceFlags() << rfIgnoreCase);
              CurrentValues.Fan_Max=StrToInt(str);
             } else
              if (Pos("fan_target=", str)==1)
               {
                str=StringReplace(str, "fan_target=", "", TReplaceFlags() << rfIgnoreCase);
                CurrentValues.Fan_Target=StrToInt(str);
               } else
                if (Pos("fan_acoustic=", str)==1)
                 {
                  str=StringReplace(str, "fan_acoustic=", "", TReplaceFlags() << rfIgnoreCase);
                  CurrentValues.Fan_Acoustic=StrToInt(str);
                 } else
                  if (Pos("power_temp=", str)==1)
                   {
                    str=StringReplace(str, "power_temp=", "", TReplaceFlags() << rfIgnoreCase);
                    CurrentValues.Power_Temp=StrToInt(str);
                   } else
                    if (Pos("power_target=", str)==1)
                     {
                      str=StringReplace(str, "power_target=", "", TReplaceFlags() << rfIgnoreCase);
                      CurrentValues.Power_Target=StrToInt(str);
                     } else
                      if (Pos("offset=", str)==1)
                       {
                        str=StringReplace(str, "offset=", "", TReplaceFlags() << rfIgnoreCase);
                        CurrentValues.Offset=StrToInt(str);
                       } else
                         if (Pos("llc=", str)==1)
                          {
                           str=StringReplace(str, "llc=", "", TReplaceFlags() << rfIgnoreCase);
                           CurrentValues.LLC=StrToInt(str);
                          } else
                           if (Pos("phasegain=", str)==1)
                            {
                             CurrentValues.PhaseGain=StringReplace(str, "phasegain=", "", TReplaceFlags() << rfIgnoreCase);
                            } else
                             if (Pos("currentscale=", str)==1)
                              {
                               CurrentValues.CurrentScale=StringReplace(str, "currentscale=", "", TReplaceFlags() << rfIgnoreCase);
                              } else
                               if (Pos("setconstantfanspeed=", str)==1)
                                {
                                 ConstantFanSpeed=StringReplace(str, "setconstantfanspeed=", "", TReplaceFlags() << rfIgnoreCase);
                                } else
                                 if (Pos("setpowerefficiency=", str)==1)
                                  {
                                   PowerEfficiency=StringReplace(str, "setpowerefficiency=", "", TReplaceFlags() << rfIgnoreCase);
                                  } else
                                   if (Pos("setchill=", str)==1)
                                    {
                                     Chill=StringReplace(str, "setchill=", "", TReplaceFlags() << rfIgnoreCase);
                                    } else    //fan_curve
                                      if (Pos("fan_p", str)==1)
                                       {
                                        if (!TryStrToInt(str.SubString(6,1), index))
                                         {
                                           Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+str.SubString(6,1)+"\" is not valid Pstate number", CmdMsg, NULL, 0, false);
                                           return false;
                                         }
                                        if ((index<0) || (index>4))
				         {
     					  Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+IntToStr(index)+"\" exceeds Fan Pstates number (0-4) "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
        				  return false;
         			         }
                                        str=str.Delete(1,7);    //jest 7 znak�w pocz�tkowych w��cznie z =, bo ilo�� Pstat�w r�wna si� 5
                                        if (Pos(";", str)>0)
                                         {
                                          CurrentValues.FanCurvePoints[index].Temp=StrToInt(str.SubString(1,Pos(";",str)-1));
                                          str.Delete(1,Pos(";",str));
                                          CurrentValues.FanCurvePoints[index].Percentage=StrToInt(str);
                                         }
                                       } else
                                        if (Pos("fan_zerorpm=", str)==1)
                                          {
                			   str=StringReplace(str, "fan_zerorpm=", "", TReplaceFlags() << rfIgnoreCase);
                 		       	   CurrentValues.Fan_ZeroRPM=StrToInt(str);
                		          } else
                                            if (Pos("mem_timinglevel=", str)==1)
                                             {
                			      str=StringReplace(str, "mem_timinglevel=", "", TReplaceFlags() << rfIgnoreCase);
                 		       	      CurrentValues.Mem_TimingLevel=StrToInt(str);
                		             } else
                                              if (Pos("gpu_min=", str)==1)
                                               {
                			        str=StringReplace(str, "gpu_min=", "", TReplaceFlags() << rfIgnoreCase);
                 		       	        CurrentValues.GPU_Min=StrToInt(str);
                		               } else
                                                 if (Pos("gpu_max=", str)==1)
                                                  {
                			           str=StringReplace(str, "gpu_max=", "", TReplaceFlags() << rfIgnoreCase);
                 		       	           CurrentValues.GPU_Max=StrToInt(str);
                		                  } else
                                                   if (Pos("mem_max=", str)==1)
                                                    {
                			             str=StringReplace(str, "mem_max=", "", TReplaceFlags() << rfIgnoreCase);
                 		       	             CurrentValues.Mem_Max=StrToInt(str);
                		                    } else
                                                     {
                                                      Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\x0d\x0a"+"\""+command->Strings[i]+"\" is not valid parameter", CmdMsg, NULL, 0, false);
                                                      return false;
                                                     }
       }
     } catch(Exception& E)
       {
         Cmd::ErrorInfoShow("Failed to parse command: \""+ OryginalCommand+"\"\x0d\x0a"+E.Message, CmdMsg, NULL, 0, false);
         return false;
       }
    //=====
    //Aplikujemy
    if (InitializeADL())
     {
      if (GPU[GpuInd].APIVersion==7)
       {
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //GPU
        if (Pos("gpu_p", LowerCase(cm))>0)
         {
           const int size = sizeof(ADLODNPerformanceLevelsX2) + (sizeof(ADLODNPerformanceLevelX2)*(GPU[GpuInd].MaxLevels-1));
           ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
           pL->iSize=size;
           for (int i = 0; i < GPU[GpuInd].GPULevels; i++)
            {
             pL->aLevels[i].iClock=CurrentValues.GPU_Clock[i]*100;
             pL->aLevels[i].iVddc=CurrentValues.GPU_Vcc[i];
             pL->aLevels[i].iEnabled=CurrentValues.GPU_Enabled[i];
            }
           retcode=OverdriveN.SetValues_GPUMem(GpuInd, true, pL, size, "Failed to set GPU clocks for GPU "+IntToStr(GpuInd), CmdMsg);
           free(pL);
         }
        //============================================================================
        //Memory
        if (Pos("mem_p", LowerCase(cm))>0)
         {
           const int size = sizeof(ADLODNPerformanceLevelsX2) + (sizeof(ADLODNPerformanceLevelX2)*(GPU[GpuInd].MaxLevels-1));
           ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
           pL->iSize=size;
           for (int i = 0; i < GPU[GpuInd].MemoryLevels; i++)
            {
             pL->aLevels[i].iClock=CurrentValues.Mem_Clock[i]*100;
             pL->aLevels[i].iVddc=CurrentValues.Mem_Vcc[i];
             pL->aLevels[i].iEnabled=CurrentValues.Mem_Enabled[i];
            }
           retcode=OverdriveN.SetValues_GPUMem(GpuInd, false, pL, size, "Failed to set Memory clocks for GPU "+IntToStr(GpuInd), CmdMsg);

           free(pL);
         }
        //============================================================================
        //Fan
        if ((Pos("fan_acoustic", LowerCase(cm))>0) || (Pos("fan_target", LowerCase(cm))>0) || (Pos("fan_m", LowerCase(cm))>0))
         {
          retcode=OverdriveN.SetValues_Fan(GpuInd, false, CurrentValues.Fan_Min, CurrentValues.Fan_Max, CurrentValues.Fan_Target, CurrentValues.Fan_Acoustic, "Failed to set Fan values for GPU "+IntToStr(GpuInd), CmdMsg);
         }
        //============================================================================
        //Fan curve, Zero RPM, Timing Level
        if ((Pos("fan_p", LowerCase(cm))>0) || (Pos("fan_zerorpm", LowerCase(cm))>0) || (Pos("mem_timinglevel", LowerCase(cm))>0))
         {
          ADLODNExtSettings *ExtCurrent= new ADLODNExtSettings;
          memset(ExtCurrent, 0, sizeof(ADLODNExtSettings));
          memcpy(ExtCurrent->Point, &CurrentValues.FanCurvePoints, sizeof(ADLODNFanCurvePoint)*5);
          ExtCurrent->MemTimingLevel=CurrentValues.Mem_TimingLevel;
          ExtCurrent->ZeroRPM=CurrentValues.Fan_ZeroRPM;
          retcode=OverdriveN.ExtSettings_Set(GpuInd, ExtCurrent, CmdMsg);
          delete ExtCurrent;
         }
        //============================================================================
        //Power
        if (Pos("power_", LowerCase(cm))>0)
         {
          retcode=OverdriveN.SetValues_Power(GpuInd, false, CurrentValues.Power_Temp, CurrentValues.Power_Target, "Failed to set Power values for GPU "+IntToStr(GpuInd), CmdMsg);
         }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       } else
        if (GPU[GpuInd].APIVersion==8)
         {
          Odn8Settings Od8Current;
          memset(&Od8Current, 0, sizeof(Odn8Settings));
          //GPU
          for (int i = 0; i < 3; i++)
           {
            Od8Current.GPU_P[i].Clock=CurrentValues.GPU_Clock[i];
            Od8Current.GPU_P[i].Voltage=CurrentValues.GPU_Vcc[i];
           }
          Od8Current.GPU_Min=CurrentValues.GPU_Min;
          Od8Current.GPU_Max=CurrentValues.GPU_Max;
          //Memory
          Od8Current.Mem_Max=CurrentValues.Mem_Max;
          //Fan Curve, Zero RPM, Mem Timing
          for (int i = 0; i < 5; i++)
           {
            Od8Current.ExtSettings.Point[i].Temp=CurrentValues.FanCurvePoints[i].Temp;
            Od8Current.ExtSettings.Point[i].Percentage=CurrentValues.FanCurvePoints[i].Percentage;
           }
          Od8Current.ExtSettings.ZeroRPM=CurrentValues.Fan_ZeroRPM;
          Od8Current.ExtSettings.MemTimingLevel=CurrentValues.Mem_TimingLevel;
          //Acoustic Limit
          Od8Current.Fan_Acoustic=CurrentValues.Fan_Acoustic;
          //Power
          Od8Current.Power_Target=CurrentValues.Power_Target;

          retcode=Overdrive8.SetValues(GpuInd, &Od8Current, CmdMsg);
         }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      DeinitializeADL();
     }
   //===============
   //Aplikujemy I2C
   if (Options.UseI2C)
    {
      if ((Pos("offset=", LowerCase(cm))>0) ||
          (Pos("llc=", LowerCase(cm))>0) ||
          (Pos("phasegain=", LowerCase(cm))>0) ||
          (Pos("currentscale=", LowerCase(cm))>0))
            {
             Cmd::InitializeI2C();
             I2C_.WriteSettings(GpuInd, false, CurrentValues.Offset, CurrentValues.LLC, CurrentValues.PhaseGain, CurrentValues.CurrentScale);
            }
    }
   //=============== other, no checking API version
   //Constant FanSpeed
   if (Pos("setconstantfanspeed=", LowerCase(cm))>0)
    {
     int val;
     if (LowerCase(ConstantFanSpeed)=="auto")
      {
        val=-1;
      } else
       {
        if (TryStrToInt(ConstantFanSpeed, val)==false)
          {
            Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\nPut fan speed value between 0 and 100", CmdMsg, NULL, 0, false);
            continue;
          }
        if ((val<0) || (val>100))
          {
            Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\nPut fan speed value between 0 and 100", CmdMsg, NULL, 0, false);
            continue;
          }
       }
      SetFansPercentage(GpuInd, val);
    }
   //===============
   //Power Efficiency
   if (Pos("setpowerefficiency=", LowerCase(cm))>0)
    {
     bool val;
     if ((LowerCase(PowerEfficiency)=="on") || (LowerCase(PowerEfficiency)=="1"))
      {
        val=true;
      } else
       if ((LowerCase(PowerEfficiency)=="off") || (LowerCase(PowerEfficiency)=="0"))
        {
          val=false;
        } else
         {
           Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\nPut correct Power Efficiency value: on/off (1/0) ", CmdMsg, NULL, 0, false);
           continue;
         }
     bool Current=false;
     SetGetChillPowerEfficiency(false, GpuInd, false, val, &Current);
    }
   //===============
   //Chill
   if (Pos("setchill=", LowerCase(cm))>0)
    {
     bool val;
     if ((LowerCase(Chill)=="on") || (LowerCase(Chill)=="1"))
      {
        val=true;
      } else
       if ((LowerCase(Chill)=="off") || (LowerCase(Chill)=="0"))
        {
          val=false;
        } else
         {
           Cmd::ErrorInfoShow("Wrong command: "+ OryginalCommand+"\nPut correct Chill value: on/off (1/0) ", CmdMsg, NULL, 0, false);
           continue;
         }
     bool Current=false;
     SetGetChillPowerEfficiency(true, GpuInd, false, val, &Current);
    }
 }
delete command;
return retcode;
}
//--------------------------------------------------------------------------
bool Cmd::SetProfileToGPU(int prof,int GpuInd, bool CmdMsg)
{
/*   
if (GPU[GpuInd].APIVersion!=Profiles[prof].APIVer)
 {
  Cmd::ErrorInfoShow("Profile \""+Profiles[prof].Name+"\" is not compatible with GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
  return false;
 }
*/

if (GPU[GpuInd].APIVersion==7)
 {
  //Sprawdzamy czy niema mniej leveli w profilu ni� w gpu
  if ((Profiles[prof].GPU_Clock.Length < GPU[GpuInd].GPULevels) || (Profiles[prof].Mem_Clock.Length<GPU[GpuInd].MemoryLevels))
    {
      Cmd::ErrorInfoShow("Warning: profile \""+Profiles[prof].Name+"\" contains less PStates than GPU "+IntToStr(GpuInd)+" has.", CmdMsg, NULL, 3, false);
      if (Cmd::ReadCurrent(GpuInd, CmdMsg)==false)
       {
         return false;
       }
    }
 }

bool retcode=true;
if (InitializeADL())
 {
  //******************************************************
  if (GPU[GpuInd].APIVersion==7)
   {
    const int size = sizeof(ADLODNPerformanceLevelsX2) + (sizeof(ADLODNPerformanceLevelX2)*(GPU[GpuInd].MaxLevels-1));
    ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
    pL->iSize=size;
    //GPU
    for (int i = 0; i < GPU[GpuInd].GPULevels; i++)
        {
         if (Profiles[prof].GPU_Clock.Length>i)
         {
           pL->aLevels[i].iClock=Profiles[prof].GPU_Clock[i]*100;
           pL->aLevels[i].iVddc=Profiles[prof].GPU_Vcc[i];
           pL->aLevels[i].iEnabled=Profiles[prof].GPU_Enabled[i];
         } else
           {
             pL->aLevels[i].iClock=CurrentValues.GPU_Clock[i]*100;
             pL->aLevels[i].iVddc=CurrentValues.GPU_Vcc[i];
             pL->aLevels[i].iEnabled=CurrentValues.GPU_Enabled[i];
           }
        }
    retcode=OverdriveN.SetValues_GPUMem(GpuInd, true, pL, size, "Failed to set GPU values for GPU "+IntToStr(GpuInd), CmdMsg);
    //Memory
    memset(pL, 0, size);
    pL->iSize=size;
    for (int i = 0; i < GPU[GpuInd].MemoryLevels; i++)
        {
         if (Profiles[prof].Mem_Clock.Length>i)
         {
           pL->aLevels[i].iClock=Profiles[prof].Mem_Clock[i]*100;
           pL->aLevels[i].iVddc=Profiles[prof].Mem_Vcc[i];
           pL->aLevels[i].iEnabled=Profiles[prof].Mem_Enabled[i];
         } else
            {
              pL->aLevels[i].iClock=CurrentValues.Mem_Clock[i]*100;
              pL->aLevels[i].iVddc=CurrentValues.Mem_Vcc[i];
              pL->aLevels[i].iEnabled=CurrentValues.Mem_Enabled[i];
            }
        }
    retcode=OverdriveN.SetValues_GPUMem(GpuInd, false, pL, size, "Failed to set Memory values for GPU "+IntToStr(GpuInd), CmdMsg);
    free(pL);
    //Fan
    if (GPU[GpuInd].FanEnabled)
     {
      retcode=OverdriveN.SetValues_Fan(GpuInd, true, Profiles[prof].Fan_Min, Profiles[prof].Fan_Max, Profiles[prof].Fan_Target, Profiles[prof].Fan_Acoustic, "Failed to set Fan values for GPU "+IntToStr(GpuInd), CmdMsg);
     }
    //Fan Curve, Zero RPM, Memory timing level
    ADLODNExtSettings *ExtCurrent= new ADLODNExtSettings;
    memset(ExtCurrent, 0, sizeof(ADLODNExtSettings));

    if ((Profiles[prof].Mem_TimingLevel==-500) || (Profiles[prof].Fan_ZeroRPM==-500))
     {
      OverdriveN.ExtSettings_Get(GpuInd, NULL, ExtCurrent, CmdMsg);
      Profiles[prof].Mem_TimingLevel=ExtCurrent->MemTimingLevel;
      Profiles[prof].Fan_ZeroRPM=ExtCurrent->ZeroRPM;
      memset(ExtCurrent, 0, sizeof(ADLODNExtSettings));
     }
    if (GPU[GpuInd].FanEnabled)
     {
      for (int i = 0; i < 5; i++)
       {
        if (Profiles[prof].FanCurvePoints[i].Temp==-500 || Profiles[prof].FanCurvePoints[i].Percentage==-500)
         {
          OverdriveN.ExtSettings_Get(GpuInd, NULL, ExtCurrent, CmdMsg);
          memcpy(&Profiles[prof].FanCurvePoints, ExtCurrent->Point, sizeof(ADLODNFanCurvePoint)*5);
          memset(ExtCurrent, 0, sizeof(ADLODNExtSettings));
          break;
         }
        }
      for (int i = 0; i < 5; i++)
       {
        ExtCurrent->Point[i].Temp=Profiles[prof].FanCurvePoints[i].Temp;
        ExtCurrent->Point[i].Percentage=Profiles[prof].FanCurvePoints[i].Percentage;
       }
     }
    ExtCurrent->MemTimingLevel=Profiles[prof].Mem_TimingLevel;
    ExtCurrent->ZeroRPM=Profiles[prof].Fan_ZeroRPM;
    retcode=OverdriveN.ExtSettings_Set(GpuInd, ExtCurrent, CmdMsg);
    delete ExtCurrent;
    //Power
    retcode=OverdriveN.SetValues_Power(GpuInd, true, Profiles[prof].Power_Temp, Profiles[prof].Power_Target, "Failed to set Power values for GPU "+IntToStr(GpuInd), CmdMsg);
   } else
   //******************************************************
    if (GPU[GpuInd].APIVersion==8)
     {
       bool fullprofile=true;

       for (int i = 0; i < 3; i++)
        {
         if (Profiles[prof].GPU_Clock[i]==-500 || Profiles[prof].GPU_Vcc[i]==-500)
          {
            fullprofile=false;
            break;
          }
        }
       if (Profiles[prof].GPU_Min==-500 || Profiles[prof].GPU_Max==-500 ||
           Profiles[prof].Mem_Max==-500 || Profiles[prof].Mem_TimingLevel==-500 ||
           Profiles[prof].Fan_ZeroRPM==-500 || Profiles[prof].Fan_Acoustic==-500)
            {
             fullprofile=false;
            }
       for (int i = 0; i < 5; i++)
        {
         if (Profiles[prof].FanCurvePoints[i].Temp==-500 || Profiles[prof].FanCurvePoints[i].Percentage==-500)
          {
            fullprofile=false;
            break;
          }
        }
       Odn8Settings Od8Current;
       memset(&Od8Current, 0, sizeof(Odn8Settings));
       if (!fullprofile)
        {
         if (Overdrive8.GetValues(GpuInd, NULL, &Od8Current, CmdMsg))
          fullprofile=true;
        }
       if (fullprofile)
        {
         //GPU
         for (int i = 0; i < 3; i++)
          {
           Od8Current.GPU_P[i].Clock=Profiles[prof].GPU_Clock[i];
           Od8Current.GPU_P[i].Voltage=Profiles[prof].GPU_Vcc[i];
          }
         Od8Current.GPU_Min=Profiles[prof].GPU_Min;
         Od8Current.GPU_Max=Profiles[prof].GPU_Max;
         //Memory
         Od8Current.Mem_Max=Profiles[prof].Mem_Max;
         //Fan Curve, Zero RPM, Mem Timing
         for (int i = 0; i < 5; i++)
          {
           Od8Current.ExtSettings.Point[i].Temp=Profiles[prof].FanCurvePoints[i].Temp;
           Od8Current.ExtSettings.Point[i].Percentage=Profiles[prof].FanCurvePoints[i].Percentage;
          }
         Od8Current.ExtSettings.ZeroRPM=Profiles[prof].Fan_ZeroRPM;
         Od8Current.ExtSettings.MemTimingLevel=Profiles[prof].Mem_TimingLevel;
         //Acoustic Limit
         Od8Current.Fan_Acoustic=Profiles[prof].Fan_Acoustic;
         //Power
         Od8Current.Power_Target=Profiles[prof].Power_Target;

         retcode=Overdrive8.SetValues(GpuInd, &Od8Current, CmdMsg);
        } else
         {
          Cmd::ErrorInfoShow("Profile \""+Profiles[prof].Name+"\" is not complete, cannot apply it to GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
          retcode=false;
         }
     }
  //******************************************************
  DeinitializeADL();

  //============================================================================
  //I2C
  if (Options.UseI2C)
   {
     I2C_.WriteSettings(GpuInd,false,Profiles[prof].Offset,Profiles[prof].LLC,Profiles[prof].PhaseGain,Profiles[prof].CurrentScale);
   }
  //============================================================================
 } else
  {
   retcode=false;
  }

return retcode;
}
//---------------------------------------------------------------------------
bool Cmd::IsHighGPUCount()
{
return (GPU.Length>10);
}
//---------------------------------------------------------------------------
int Cmd::GetGPULength()
{
return GPU.Length;
}
//---------------------------------------------------------------------------
void Cmd::InitializeI2C()
{
if (Options.UseI2C)
 {
  if (I2C_.Scanned==false)
   {
     I2C_.Scan(I2C_.DoFullScan);
   }
 }
}
//---------------------------------------------------------------------------
bool Cmd::RestartGPU(int GpuInd, bool CmdMsg, bool WaitForEnd)
{
if (Odnt.CheckIfAdmin())
  {
    if (!ppRestartGPU(GPU[GpuInd].DevID))
      {
        Cmd::ErrorInfoShow("Failed to restart GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
        return false;
      } else
       {
         if (WaitForEnd && GPU[GpuInd].APISupported)
          {
             ADLODNPowerLimitSetting odNPowerControl;
             memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
             for (int i = 0; i < 7; i++)  //max 7 seconds of waiting
              {
               Sleep(1000);
               if (InitializeADL())
                {
                  if (GPU[GpuInd].APIVersion==7)
                   {
                    if (ADL_OK==ADL2_OverdriveN_PowerLimit_Get(context, GPU[GpuInd].AdIndexes[0], &odNPowerControl))
                     {
                      DeinitializeADL();
                      break;
                     }
                   } else
                    if (GPU[GpuInd].APIVersion==8)
                     {
                       int numberOfFeaturesCurrent = OD8_COUNT;
		       int* lpCurrentSettingList = NULL;
		       int an=ADL2_Overdrive8_Current_SettingX2_Get(context, GPU[GpuInd].AdIndexes[0], &numberOfFeaturesCurrent, &lpCurrentSettingList);
 		       if (ADL_OK == an && lpCurrentSettingList!=NULL)
                        {
                         DeinitializeADL();
                         break;
                        }
                     }
                  DeinitializeADL();
                }
              }
          }
       }
  } else
    {
      Cmd::ErrorInfoShow("-t command requires administrator rights, failed to restart GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
      return false;
    }
return true;
}
//---------------------------------------------------------------------------
bool Cmd::ReadCurrent(int GpuInd, bool CmdMsg)
{
if (InitializeADL())
 {
  int an;
  CurrentValues.GPU_Clock.Length=GPU[GpuInd].GPULevels;
  CurrentValues.GPU_Vcc.Length=GPU[GpuInd].GPULevels;
  CurrentValues.GPU_Enabled.Length=GPU[GpuInd].GPULevels;
  CurrentValues.Mem_Clock.Length=GPU[GpuInd].MemoryLevels;
  CurrentValues.Mem_Vcc.Length=GPU[GpuInd].MemoryLevels;
  CurrentValues.Mem_Enabled.Length=GPU[GpuInd].MemoryLevels;

  //OverdriveN=============================
  if (GPU[GpuInd].APIVersion==7)
   {
    const int size = sizeof(ADLODNPerformanceLevelsX2) + sizeof(ADLODNPerformanceLevelX2)* (GPU[GpuInd].MaxLevels - 1);
    //GPU
    ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
    pL->iSize = size;
    pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;
    an=ADL2_OverdriveN_SystemClocksX2_Get(context, GPU[GpuInd].AdIndexes[0], pL);
    if (ADL_OK == an)
     {
       for (int i = 0; i < GPU[GpuInd].GPULevels; i++)
        {
          CurrentValues.GPU_Clock[i]=pL->aLevels[i].iClock/100;
          CurrentValues.GPU_Vcc[i]=pL->aLevels[i].iVddc;
          CurrentValues.GPU_Enabled[i]=(pL->aLevels[i].iEnabled>0);
        }
     } else
      {
        Cmd::ErrorInfoShow("SystemClocks_Get function failed", CmdMsg, an, 0, false);
        DeinitializeADL();
        return false;
      }

    //Memory
    memset(pL, 0, size);
    pL->iSize = size;
    pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;
    an=ADL2_OverdriveN_MemoryClocksX2_Get(context, GPU[GpuInd].AdIndexes[0], pL);
    if (ADL_OK == an)
     {
       for (int i = 0; i < GPU[GpuInd].MemoryLevels; i++)
        {
         CurrentValues.Mem_Clock[i]=pL->aLevels[i].iClock/100;
         CurrentValues.Mem_Vcc[i]=pL->aLevels[i].iVddc;
         CurrentValues.Mem_Enabled[i]=(pL->aLevels[i].iEnabled>0);
        }
     } else
      {
        Cmd::ErrorInfoShow("MemoryClocks_Get function failed", CmdMsg, an, 0, false);
        DeinitializeADL();
        return false;
      }
    free(pL);
    //Fan
    ADLODNFanControl odNFanControl;
    memset(&odNFanControl, 0, sizeof(ADLODNFanControl));
    an=ADL2_OverdriveN_FanControl_Get(context,GPU[GpuInd].AdIndexes[0], &odNFanControl);
    if (ADL_OK == an)
     {
       CurrentValues.Fan_Min=odNFanControl.iMinFanLimit;
       CurrentValues.Fan_Max=odNFanControl.iTargetFanSpeed;
       CurrentValues.Fan_Target=odNFanControl.iTargetTemperature;
       CurrentValues.Fan_Acoustic=odNFanControl.iMinPerformanceClock;
     } else
      {
        Cmd::ErrorInfoShow("FanControl_Get function failed", CmdMsg, an, 0, false);
        DeinitializeADL();
        return false;
      }
     //Fan curve, Zero RPM, Memory Timings
     ADLODNExtSettings *ExtCurrent= new ADLODNExtSettings;
     if (OverdriveN.ExtSettings_Get(GpuInd, NULL, ExtCurrent, CmdMsg))
      {
       for (int i = 0; i < 5; i++)
        {
         CurrentValues.FanCurvePoints[i].Temp=ExtCurrent->Point[i].Temp;
         CurrentValues.FanCurvePoints[i].Percentage=ExtCurrent->Point[i].Percentage;
        }
       CurrentValues.Mem_TimingLevel=ExtCurrent->MemTimingLevel;
       CurrentValues.Fan_ZeroRPM=ExtCurrent->ZeroRPM;
       delete ExtCurrent;
      } else
       {
        delete ExtCurrent;
        DeinitializeADL();
        return false;
       }
    //Power
    ADLODNPowerLimitSetting odNPowerControl;
    memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
    an= ADL2_OverdriveN_PowerLimit_Get(context,GPU[GpuInd].AdIndexes[0], &odNPowerControl);
    if (ADL_OK == an)
     {
      CurrentValues.Power_Temp=odNPowerControl.iMaxOperatingTemperature;
      CurrentValues.Power_Target=odNPowerControl.iTDPLimit;
     } else
       {
        Cmd::ErrorInfoShow("PowerLimit_Get function failed", CmdMsg, an, 0, false);
        DeinitializeADL();
        return false;
       }
   } else
  //Overdrive8=============================
     if (GPU[GpuInd].APIVersion==8)
      {
       Odn8Settings Od8Current;
       if (Overdrive8.GetValues(GpuInd, NULL, &Od8Current, CmdMsg))
        {
         //GPU
         for (int i = 0; i < 3; i++)
          {
           CurrentValues.GPU_Clock[i]=Od8Current.GPU_P[i].Clock;
           CurrentValues.GPU_Vcc[i]=Od8Current.GPU_P[i].Voltage;
           CurrentValues.GPU_Enabled[i]=true;
          }
         CurrentValues.GPU_Min=Od8Current.GPU_Min;
         CurrentValues.GPU_Max=Od8Current.GPU_Max;

         //Memory
         CurrentValues.Mem_Max=Od8Current.Mem_Max;

         //Fan Curve, Zero RPM, Mem Timing
         for (int i = 0; i < 5; i++)
          {
           CurrentValues.FanCurvePoints[i].Temp=Od8Current.ExtSettings.Point[i].Temp;
           CurrentValues.FanCurvePoints[i].Percentage=Od8Current.ExtSettings.Point[i].Percentage;
          }
         CurrentValues.Fan_ZeroRPM=Od8Current.ExtSettings.ZeroRPM;
         CurrentValues.Mem_TimingLevel=Od8Current.ExtSettings.MemTimingLevel;
         //Acoustic Limit
         CurrentValues.Fan_Acoustic=Od8Current.Fan_Acoustic;
         //Power
         CurrentValues.Power_Target=Od8Current.Power_Target;
        } else
         {
          DeinitializeADL();
          return false;
         }
      }
  //=======================================
  DeinitializeADL();
 } else
  {
   return false;
  }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

if (Options.UseI2C)
 {
  CurrentValues.Offset=-500;
  CurrentValues.LLC=-500;
  CurrentValues.PhaseGain="NULL";
  CurrentValues.CurrentScale="NULL";
  I2C_.ReadSettings(GpuInd,false);
 }

return true;
}
//---------------------------------------------------------------------------
bool Cmd::CompareProfileWithGPU(int prof,int GpuInd, String atype, bool CmdMsg)
{
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
 //pierw odczytujemy
 if (Cmd::ReadCurrent(GpuInd, CmdMsg)==false)
   {
     return false;
   }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (!EqualValues(&CurrentValues, &Profiles[prof], GpuInd))
  {
   if (atype=="cp")  //silent apply
    {
      if (Cmd::SetProfileToGPU(prof, GpuInd, CmdMsg)==false)
       {
        Cmd::ErrorInfoShow("Failed to apply profile \""+Profiles[prof].Name+"\"", CmdMsg, NULL, 0, false);
       }
    } else
     if (atype=="cm")  //with message and apply
      {
        if (Cmd::SetProfileToGPU(prof, GpuInd, CmdMsg))
         {
           Cmd::ErrorInfoShow("GPU "+IntToStr(GpuInd)+" values were not equal with profile \""+Profiles[prof].Name+"\"."+"\x0d\x0a"+ "Succesfully applied this profile.", CmdMsg, NULL, 2, false);
         } else
          {
            Cmd::ErrorInfoShow("Failed to apply profile \""+Profiles[prof].Name+"\" to GPU "+IntToStr(GpuInd), CmdMsg, NULL, 0, false);
          }
      } else
       if (atype=="co")  //with message only
         {
           Cmd::ErrorInfoShow("GPU "+IntToStr(GpuInd)+" values are not equal with profile \""+Profiles[prof].Name+"\".", CmdMsg, NULL, 2, false);
         }
  }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
return true;
}
//---------------------------------------------------------------------------
void Reset(int GpuInd, bool CmdMsg, bool from_cmd)
{
if (InitializeADL())
 {
  //***************************
  if (GPU[GpuInd].APIVersion==7)
   {
    int an;
    //GPU
    const int size = sizeof(ADLODNPerformanceLevelsX2) + (sizeof(ADLODNPerformanceLevelX2)*(GPU[GpuInd].MaxLevels-1));
    ADLODNPerformanceLevelsX2 *pL=(ADLODNPerformanceLevelsX2*)calloc(1, size);
    pL->iSize=size;
    pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;
    pL->iMode = ADLODNControlType::ODNControlType_Default;
    an=ADL2_OverdriveN_SystemClocksX2_Set(context,GPU[GpuInd].AdIndexes[0], pL);
    if (ADL_OK != an)
     {
      Cmd.ErrorInfoShow("Failed to reset GPU values", CmdMsg, an, 0, false);
     }
    //Memory
    memset(pL, 0, size);
    pL->iSize=size;
    pL->iNumberOfPerformanceLevels = GPU[GpuInd].MaxLevels;
    pL->iMode = ADLODNControlType::ODNControlType_Default;
    an=ADL2_OverdriveN_MemoryClocksX2_Set(context,GPU[GpuInd].AdIndexes[0], pL);
    if (ADL_OK != an)
     {
      Cmd.ErrorInfoShow("Failed to reset Memory values", CmdMsg, an, 0, false);
     }
    free(pL);
    //Fan
    if (GPU[GpuInd].FanEnabled)
     {
       //Fan Values
       ADLODNFanControl odNFanControl;
       memset(&odNFanControl, 0, sizeof(ADLODNFanControl));
       odNFanControl.iMode = ADLODNControlType::ODNControlType_Default;
       an=ADL2_OverdriveN_FanControl_Set(context,GPU[GpuInd].AdIndexes[0], &odNFanControl);
       if (ADL_OK != an)
        {
         Cmd.ErrorInfoShow("Failed to reset Fan values", CmdMsg, an, 0, false);
        }
     }
    //Fan Curve, Zero RPM, Memory timing level
    if (!OverdriveN.ExtSettings_Reset(GpuInd, CmdMsg))
     Cmd.ErrorInfoShow("Failed to reset Fan curve, Zero RPM and Memory timing level", CmdMsg, 0, 0, false);
    //Power
    ADLODNPowerLimitSetting odNPowerControl;
    memset(&odNPowerControl, 0, sizeof(ADLODNPowerLimitSetting));
    odNPowerControl.iMaxOperatingTemperature=-1;
    odNPowerControl.iMode=ADLODNControlType::ODNControlType_Default;
    an=ADL2_OverdriveN_PowerLimit_Set(context,GPU[GpuInd].AdIndexes[0], &odNPowerControl);
    if (ADL_OK != an)
     {
      Cmd.ErrorInfoShow("Failed to reset Power values", CmdMsg, an, 0, false);
     }
   } else
  //***************************
    if (GPU[GpuInd].APIVersion==8)
     {
      Overdrive8.Reset(GpuInd, CmdMsg);
     }
  //***************************
  DeinitializeADL();
 }

if (Options.UseI2C)              
 {
  if (from_cmd==false)
   {                         //GUI
    I2C_.WriteSettings(GpuInd,true,Form1->I2C_Offset->Value,Form1->I2C_LLC->Checked,Form1->Edit1->Text,Form1->Edit2->Text);
   } else
     {              //linia komend
      I2C_.WriteSettings(GpuInd,true,0,false,"NULL","NULL");
     }

 }
return;
}
//---------------------------------------------------------------------------
//return =true if something was changed
bool __fastcall TForm1::CurrentToProfile(int i, int GpuInd)
{
bool changed=false;
if ((Profiles[i].GPU_Clock.Length!=GPU[GpuInd].GPULevels) ||
    (Profiles[i].GPU_Vcc.Length!=GPU[GpuInd].GPULevels) ||
    (Profiles[i].GPU_Enabled.Length!=GPU[GpuInd].GPULevels) ||
    (Profiles[i].Mem_Clock.Length!=GPU[GpuInd].MemoryLevels) ||
    (Profiles[i].Mem_Vcc.Length!=GPU[GpuInd].MemoryLevels) ||
    (Profiles[i].Mem_Enabled.Length!=GPU[GpuInd].MemoryLevels))
     {
      changed=true;
     }

Profiles[i].GPU_Clock.Length=GPU[GpuInd].GPULevels;
Profiles[i].GPU_Vcc.Length=GPU[GpuInd].GPULevels;
Profiles[i].GPU_Enabled.Length=GPU[GpuInd].GPULevels;
Profiles[i].Mem_Clock.Length=GPU[GpuInd].MemoryLevels;
Profiles[i].Mem_Vcc.Length=GPU[GpuInd].MemoryLevels;
Profiles[i].Mem_Enabled.Length=GPU[GpuInd].MemoryLevels;

if (Profiles[i].GPU_Clock.Length>=1)
 {
   if ((Profiles[i].GPU_Clock[0]!=GPU_Clock_0->Value) ||
      (Profiles[i].GPU_Vcc[0]!=GPU_Vcc_0->Value) ||
      (Profiles[i].GPU_Enabled[0]!=GPU_Clock_0->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[0]=GPU_Clock_0->Value;
   Profiles[i].GPU_Vcc[0]=GPU_Vcc_0->Value;
   Profiles[i].GPU_Enabled[0]=GPU_Clock_0->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=2)
 {
   if ((Profiles[i].GPU_Clock[1]!=GPU_Clock_1->Value) ||
      (Profiles[i].GPU_Vcc[1]!=GPU_Vcc_1->Value) ||
      (Profiles[i].GPU_Enabled[1]!=GPU_Clock_1->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[1]=GPU_Clock_1->Value;
   Profiles[i].GPU_Vcc[1]=GPU_Vcc_1->Value;
   Profiles[i].GPU_Enabled[1]=GPU_Clock_1->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=3)
 {
   if ((Profiles[i].GPU_Clock[2]!=GPU_Clock_2->Value) ||
      (Profiles[i].GPU_Vcc[2]!=GPU_Vcc_2->Value) ||
      (Profiles[i].GPU_Enabled[2]!=GPU_Clock_2->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[2]=GPU_Clock_2->Value;
   Profiles[i].GPU_Vcc[2]=GPU_Vcc_2->Value;
   Profiles[i].GPU_Enabled[2]=GPU_Clock_2->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=4)
 {
   if ((Profiles[i].GPU_Clock[3]!=GPU_Clock_3->Value) ||
      (Profiles[i].GPU_Vcc[3]!=GPU_Vcc_3->Value) ||
      (Profiles[i].GPU_Enabled[3]!=GPU_Clock_3->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[3]=GPU_Clock_3->Value;
   Profiles[i].GPU_Vcc[3]=GPU_Vcc_3->Value;
   Profiles[i].GPU_Enabled[3]=GPU_Clock_3->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=5)
 {
   if ((Profiles[i].GPU_Clock[4]!=GPU_Clock_4->Value) ||
      (Profiles[i].GPU_Vcc[4]!=GPU_Vcc_4->Value) ||
      (Profiles[i].GPU_Enabled[4]!=GPU_Clock_4->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[4]=GPU_Clock_4->Value;
   Profiles[i].GPU_Vcc[4]=GPU_Vcc_4->Value;
   Profiles[i].GPU_Enabled[4]=GPU_Clock_4->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=6)
 {
   if ((Profiles[i].GPU_Clock[5]!=GPU_Clock_5->Value) ||
      (Profiles[i].GPU_Vcc[5]!=GPU_Vcc_5->Value) ||
      (Profiles[i].GPU_Enabled[5]!=GPU_Clock_5->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[5]=GPU_Clock_5->Value;
   Profiles[i].GPU_Vcc[5]=GPU_Vcc_5->Value;
   Profiles[i].GPU_Enabled[5]=GPU_Clock_5->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=7)
 {
   if ((Profiles[i].GPU_Clock[6]!=GPU_Clock_6->Value) ||
      (Profiles[i].GPU_Vcc[6]!=GPU_Vcc_6->Value) ||
      (Profiles[i].GPU_Enabled[6]!=GPU_Clock_6->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[6]=GPU_Clock_6->Value;
   Profiles[i].GPU_Vcc[6]=GPU_Vcc_6->Value;
   Profiles[i].GPU_Enabled[6]=GPU_Clock_6->Enabled;
 }

if (Profiles[i].GPU_Clock.Length>=8)
 {
   if ((Profiles[i].GPU_Clock[7]!=GPU_Clock_7->Value) ||
      (Profiles[i].GPU_Vcc[7]!=GPU_Vcc_7->Value) ||
      (Profiles[i].GPU_Enabled[7]!=GPU_Clock_7->Enabled))
       {
        changed=true;
       }
   Profiles[i].GPU_Clock[7]=GPU_Clock_7->Value;
   Profiles[i].GPU_Vcc[7]=GPU_Vcc_7->Value;
   Profiles[i].GPU_Enabled[7]=GPU_Clock_7->Enabled;
 }

if (Profiles[i].Mem_Clock.Length>=1)
 {
   if ((Profiles[i].Mem_Clock[0]!=Mem_Clock_0->Value) ||
      (Profiles[i].Mem_Vcc[0]!=Mem_Vcc_0->Value) ||
      (Profiles[i].Mem_Enabled[0]!=Mem_Clock_0->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[0]=Mem_Clock_0->Value;
   Profiles[i].Mem_Vcc[0]=Mem_Vcc_0->Value;
   Profiles[i].Mem_Enabled[0]=Mem_Clock_0->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=2)
 {
   if ((Profiles[i].Mem_Clock[1]!=Mem_Clock_1->Value) ||
      (Profiles[i].Mem_Vcc[1]!=Mem_Vcc_1->Value) ||
      (Profiles[i].Mem_Enabled[1]!=Mem_Clock_1->Enabled))
       {
        changed=true;
       }

   Profiles[i].Mem_Clock[1]=Mem_Clock_1->Value;
   Profiles[i].Mem_Vcc[1]=Mem_Vcc_1->Value;
   Profiles[i].Mem_Enabled[1]=Mem_Clock_1->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=3)
 {
   if ((Profiles[i].Mem_Clock[2]!=Mem_Clock_2->Value) ||
      (Profiles[i].Mem_Vcc[2]!=Mem_Vcc_2->Value) ||
      (Profiles[i].Mem_Enabled[2]!=Mem_Clock_2->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[2]=Mem_Clock_2->Value;
   Profiles[i].Mem_Vcc[2]=Mem_Vcc_2->Value;
   Profiles[i].Mem_Enabled[2]=Mem_Clock_2->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=4)
 {
   if ((Profiles[i].Mem_Clock[3]!=Mem_Clock_3->Value) ||
      (Profiles[i].Mem_Vcc[3]!=Mem_Vcc_3->Value) ||
      (Profiles[i].Mem_Enabled[3]!=Mem_Clock_3->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[3]=Mem_Clock_3->Value;
   Profiles[i].Mem_Vcc[3]=Mem_Vcc_3->Value;
   Profiles[i].Mem_Enabled[3]=Mem_Clock_3->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=5)
 {
   if ((Profiles[i].Mem_Clock[4]!=Mem_Clock_4->Value) ||
      (Profiles[i].Mem_Vcc[4]!=Mem_Vcc_4->Value) ||
      (Profiles[i].Mem_Enabled[4]!=Mem_Clock_4->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[4]=Mem_Clock_4->Value;
   Profiles[i].Mem_Vcc[4]=Mem_Vcc_4->Value;
   Profiles[i].Mem_Enabled[4]=Mem_Clock_4->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=6)
 {
   if ((Profiles[i].Mem_Clock[5]!=Mem_Clock_5->Value) ||
      (Profiles[i].Mem_Vcc[5]!=Mem_Vcc_5->Value) ||
      (Profiles[i].Mem_Enabled[5]!=Mem_Clock_5->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[5]=Mem_Clock_5->Value;
   Profiles[i].Mem_Vcc[5]=Mem_Vcc_5->Value;
   Profiles[i].Mem_Enabled[5]=Mem_Clock_5->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=7)
 {
   if ((Profiles[i].Mem_Clock[6]!=Mem_Clock_6->Value) ||
      (Profiles[i].Mem_Vcc[6]!=Mem_Vcc_6->Value) ||
      (Profiles[i].Mem_Enabled[6]!=Mem_Clock_6->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[6]=Mem_Clock_6->Value;
   Profiles[i].Mem_Vcc[6]=Mem_Vcc_6->Value;
   Profiles[i].Mem_Enabled[6]=Mem_Clock_6->Enabled;
 }
if (Profiles[i].Mem_Clock.Length>=8)
 {
   if ((Profiles[i].Mem_Clock[7]!=Mem_Clock_7->Value) ||
      (Profiles[i].Mem_Vcc[7]!=Mem_Vcc_7->Value) ||
      (Profiles[i].Mem_Enabled[7]!=Mem_Clock_7->Enabled))
       {
        changed=true;
       }
   Profiles[i].Mem_Clock[7]=Mem_Clock_7->Value;
   Profiles[i].Mem_Vcc[7]=Mem_Vcc_7->Value;
   Profiles[i].Mem_Enabled[7]=Mem_Clock_7->Enabled;
 }
//==========
if ((Profiles[i].GPU_Min!=GPU_Min->Value) ||
    (Profiles[i].GPU_Max!=GPU_Max->Value) ||
    (Profiles[i].Mem_Max!=Mem_Max->Value))
     {
      changed=true;
     }
if (GPU[GpuInd].APIVersion==8)
 {
  Profiles[i].GPU_Min=GPU_Min->Value;
  Profiles[i].GPU_Max=GPU_Max->Value;
  Profiles[i].Mem_Max=Mem_Max->Value;
 } else
  if (GPU[GpuInd].APIVersion==7)
   {
    Profiles[i].GPU_Min=-500;
    Profiles[i].GPU_Max=-500;
    Profiles[i].Mem_Max=-500;
   }
//=========
if (Profiles[i].APIVer!=GPU[GpuInd].APIVersion)
 changed=true;
Profiles[i].APIVer=GPU[GpuInd].APIVersion;
//=========
if (//(Profiles[i].Fan_Min!=Fan_Min->Value) ||             //Obsolete in 18.12.2
   //(Profiles[i].Fan_Max!=Fan_Max->Value) ||              //Obsolete in 18.12.2
   //(Profiles[i].Fan_Target!=Fan_Target->Value) ||        //Obsolete in 18.12.2
   (Profiles[i].Fan_Acoustic!=Fan_Acoustic->Value) ||
 //  (Profiles[i].Power_Temp!=Power_Temp->Value) ||        //Obsolete in 18.12.2
   (Profiles[i].Power_Target!=Power_Target->Value))
     {
       changed=true;
     }
//Profiles[i].Fan_Min=Fan_Min->Value;                       //Obsolete in 18.12.2
//Profiles[i].Fan_Max=Fan_Max->Value;                       //Obsolete in 18.12.2
//Profiles[i].Fan_Target=Fan_Target->Value;                 //Obsolete in 18.12.2
Profiles[i].Fan_Acoustic=Fan_Acoustic->Value;
//Profiles[i].Power_Temp=Power_Temp->Value;                 //Obsolete in 18.12.2
Profiles[i].Power_Target=Power_Target->Value;

//=========
if ((Profiles[i].FanCurvePoints[0].Temp!=Fan_Temp_0->Value) ||
     (Profiles[i].FanCurvePoints[0].Percentage!=Fan_Perc_0->Value) ||
     (Profiles[i].FanCurvePoints[1].Temp!=Fan_Temp_1->Value) ||
     (Profiles[i].FanCurvePoints[1].Percentage!=Fan_Perc_1->Value) ||
     (Profiles[i].FanCurvePoints[2].Temp!=Fan_Temp_2->Value) ||
     (Profiles[i].FanCurvePoints[2].Percentage!=Fan_Perc_2->Value) ||
     (Profiles[i].FanCurvePoints[3].Temp!=Fan_Temp_3->Value) ||
     (Profiles[i].FanCurvePoints[3].Percentage!=Fan_Perc_3->Value) ||
     (Profiles[i].FanCurvePoints[4].Temp!=Fan_Temp_4->Value) ||
     (Profiles[i].FanCurvePoints[4].Percentage!=Fan_Perc_4->Value))
       {
         changed=true;
       }

Profiles[i].FanCurvePoints[0].Temp=Fan_Temp_0->Value;
Profiles[i].FanCurvePoints[0].Percentage=Fan_Perc_0->Value;
Profiles[i].FanCurvePoints[1].Temp=Fan_Temp_1->Value;
Profiles[i].FanCurvePoints[1].Percentage=Fan_Perc_1->Value;
Profiles[i].FanCurvePoints[2].Temp=Fan_Temp_2->Value;
Profiles[i].FanCurvePoints[2].Percentage=Fan_Perc_2->Value;
Profiles[i].FanCurvePoints[3].Temp=Fan_Temp_3->Value;
Profiles[i].FanCurvePoints[3].Percentage=Fan_Perc_3->Value;
Profiles[i].FanCurvePoints[4].Temp=Fan_Temp_4->Value;
Profiles[i].FanCurvePoints[4].Percentage=Fan_Perc_4->Value;
//=========
if (Mem_TimingBox->ItemIndex!=-1)
 {
  if (Profiles[i].Mem_TimingLevel!=Mem_TimingBox->ItemIndex)
   {
    changed=true;
   }
  Profiles[i].Mem_TimingLevel=Mem_TimingBox->ItemIndex;
 }
//=========
if (Profiles[i].Fan_ZeroRPM!=ZeroRPMBox->Checked)
 {
  changed=true;
 }
Profiles[i].Fan_ZeroRPM=ZeroRPMBox->Checked;
//=========
//I2C
if (Options.UseI2C)
 {
  if (GPU[GpuInd].I2C.devicefound)
   {
    if (Profiles[i].Offset!=I2C_Offset->Value)
     {
       Profiles[i].Offset=I2C_Offset->Value;
       changed=true;
     }
    if (I2C_LLC->Visible)
      {
        if (Profiles[i].LLC!=I2C_LLC->Checked)
         {
           Profiles[i].LLC=I2C_LLC->Checked;
           changed=true;
         }
      } else
         {
          if (Profiles[i].LLC!=-500)
           {
             Profiles[i].LLC=-500;
             changed=true;
           }
         }
    if (Edit1->Visible)
      {
        if (Profiles[i].PhaseGain!=Edit1->Text)
         {
           Profiles[i].PhaseGain=Edit1->Text;
           changed=true;
         }
      } else
        {
         if (Profiles[i].PhaseGain!="NULL")
          {
            Profiles[i].PhaseGain="NULL";
            changed=true;
          }
        }
    if (Edit2->Visible)
      {
        if (Profiles[i].CurrentScale!=Edit2->Text)
         {
           Profiles[i].CurrentScale=Edit2->Text;
           changed=true;
         }
      } else
         {
          if (Profiles[i].CurrentScale!="NULL")
           {
             Profiles[i].CurrentScale="NULL";
             changed=true;
           }
         }
   } else
    {
      if ((Profiles[i].Offset!=-500) ||
          (Profiles[i].LLC!=-500) ||
          (Profiles[i].PhaseGain!="NULL") ||
          (Profiles[i].CurrentScale!="NULL"))
           {
            changed=true;
           }

      Profiles[i].Offset=-500;
      Profiles[i].LLC=-500;
      Profiles[i].PhaseGain="NULL";
      Profiles[i].CurrentScale="NULL";
    }
 }
return changed;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::ProfileToCurrent(int i, int GpuInd)
{
if (GPU[GpuInd].APIVersion==7)
 {
  if ((Profiles[i].GPU_Clock.Length>=1) && (GPU[GpuInd].GPULevels>=1))
   {
     GPU_Clock_0->Value=Profiles[i].GPU_Clock[0];
     GPU_Vcc_0->Value=Profiles[i].GPU_Vcc[0];
     EnableDisablePState(Profiles[i].GPU_Enabled[0],GPU_P0,GPU_Clock_0,GPU_Vcc_0);
     GPU_Clock_0Change(NULL);
     GPU_Vcc_0Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=2) && (GPU[GpuInd].GPULevels>=2))
   {
     GPU_Clock_1->Value=Profiles[i].GPU_Clock[1];
     GPU_Vcc_1->Value=Profiles[i].GPU_Vcc[1];
     EnableDisablePState(Profiles[i].GPU_Enabled[1],GPU_P1,GPU_Clock_1,GPU_Vcc_1);
     GPU_Clock_1Change(NULL);
     GPU_Vcc_1Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=3) && (GPU[GpuInd].GPULevels>=3))
   {
     GPU_Clock_2->Value=Profiles[i].GPU_Clock[2];
     GPU_Vcc_2->Value=Profiles[i].GPU_Vcc[2];
     EnableDisablePState(Profiles[i].GPU_Enabled[2],GPU_P2,GPU_Clock_2,GPU_Vcc_2);
     GPU_Clock_2Change(NULL);
     GPU_Vcc_2Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=4) && (GPU[GpuInd].GPULevels>=4))
   {
     GPU_Clock_3->Value=Profiles[i].GPU_Clock[3];
     GPU_Vcc_3->Value=Profiles[i].GPU_Vcc[3];
     EnableDisablePState(Profiles[i].GPU_Enabled[3],GPU_P3,GPU_Clock_3,GPU_Vcc_3);
     GPU_Clock_3Change(NULL);
     GPU_Vcc_3Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=5) && (GPU[GpuInd].GPULevels>=5))
   {
     GPU_Clock_4->Value=Profiles[i].GPU_Clock[4];
     GPU_Vcc_4->Value=Profiles[i].GPU_Vcc[4];
     EnableDisablePState(Profiles[i].GPU_Enabled[4],GPU_P4,GPU_Clock_4,GPU_Vcc_4);
     GPU_Clock_4Change(NULL);
     GPU_Vcc_4Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=6) && (GPU[GpuInd].GPULevels>=6))
   {
     GPU_Clock_5->Value=Profiles[i].GPU_Clock[5];
     GPU_Vcc_5->Value=Profiles[i].GPU_Vcc[5];
     EnableDisablePState(Profiles[i].GPU_Enabled[5],GPU_P5,GPU_Clock_5,GPU_Vcc_5);
     GPU_Clock_5Change(NULL);
     GPU_Vcc_5Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=7) && (GPU[GpuInd].GPULevels>=7))
   {
     GPU_Clock_6->Value=Profiles[i].GPU_Clock[6];
     GPU_Vcc_6->Value=Profiles[i].GPU_Vcc[6];
     EnableDisablePState(Profiles[i].GPU_Enabled[6],GPU_P6,GPU_Clock_6,GPU_Vcc_6);
     GPU_Clock_6Change(NULL);
     GPU_Vcc_6Change(NULL);
   }

  if ((Profiles[i].GPU_Clock.Length>=8) && (GPU[GpuInd].GPULevels>=8))
   {
     GPU_Clock_7->Value=Profiles[i].GPU_Clock[7];
     GPU_Vcc_7->Value=Profiles[i].GPU_Vcc[7];
     EnableDisablePState(Profiles[i].GPU_Enabled[7],GPU_P7,GPU_Clock_7,GPU_Vcc_7);
     GPU_Clock_7Change(NULL);
     GPU_Vcc_7Change(NULL);
   }

  if ((Profiles[i].Mem_Clock.Length>=1) && (GPU[GpuInd].MemoryLevels>=1))
   {
     Mem_Clock_0->Value=Profiles[i].Mem_Clock[0];
     Mem_Vcc_0->Value=Profiles[i].Mem_Vcc[0];
     EnableDisablePState(Profiles[i].Mem_Enabled[0],Mem_P0,Mem_Clock_0,Mem_Vcc_0);
     Mem_Clock_0Change(NULL);
     Mem_Vcc_0Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=2) && (GPU[GpuInd].MemoryLevels>=2))
   {
     Mem_Clock_1->Value=Profiles[i].Mem_Clock[1];
     Mem_Vcc_1->Value=Profiles[i].Mem_Vcc[1];
     EnableDisablePState(Profiles[i].Mem_Enabled[1],Mem_P1,Mem_Clock_1,Mem_Vcc_1);
     Mem_Clock_1Change(NULL);
     Mem_Vcc_1Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=3) && (GPU[GpuInd].MemoryLevels>=3))
   {
     Mem_Clock_2->Value=Profiles[i].Mem_Clock[2];
     Mem_Vcc_2->Value=Profiles[i].Mem_Vcc[2];
     EnableDisablePState(Profiles[i].Mem_Enabled[2],Mem_P2,Mem_Clock_2,Mem_Vcc_2);
     Mem_Clock_2Change(NULL);
     Mem_Vcc_2Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=4) && (GPU[GpuInd].MemoryLevels>=4))
   {
     Mem_Clock_3->Value=Profiles[i].Mem_Clock[3];
     Mem_Vcc_3->Value=Profiles[i].Mem_Vcc[3];
     EnableDisablePState(Profiles[i].Mem_Enabled[3],Mem_P3,Mem_Clock_3,Mem_Vcc_3);
     Mem_Clock_3Change(NULL);
     Mem_Vcc_3Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=5) && (GPU[GpuInd].MemoryLevels>=5))
   {
     Mem_Clock_4->Value=Profiles[i].Mem_Clock[4];
     Mem_Vcc_4->Value=Profiles[i].Mem_Vcc[4];
     EnableDisablePState(Profiles[i].Mem_Enabled[4],Mem_P4,Mem_Clock_4,Mem_Vcc_4);
     Mem_Clock_4Change(NULL);
     Mem_Vcc_4Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=6) && (GPU[GpuInd].MemoryLevels>=6))
   {
     Mem_Clock_5->Value=Profiles[i].Mem_Clock[5];
     Mem_Vcc_5->Value=Profiles[i].Mem_Vcc[5];
     EnableDisablePState(Profiles[i].Mem_Enabled[5],Mem_P5,Mem_Clock_5,Mem_Vcc_5);
     Mem_Clock_5Change(NULL);
     Mem_Vcc_5Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=7) && (GPU[GpuInd].MemoryLevels>=7))
   {
     Mem_Clock_6->Value=Profiles[i].Mem_Clock[6];
     Mem_Vcc_6->Value=Profiles[i].Mem_Vcc[6];
     EnableDisablePState(Profiles[i].Mem_Enabled[6],Mem_P6,Mem_Clock_6,Mem_Vcc_6);
     Mem_Clock_6Change(NULL);
     Mem_Vcc_6Change(NULL);
   }
  if ((Profiles[i].Mem_Clock.Length>=8) && (GPU[GpuInd].MemoryLevels>=8))
   {
     Mem_Clock_7->Value=Profiles[i].Mem_Clock[7];
     Mem_Vcc_7->Value=Profiles[i].Mem_Vcc[7];
     EnableDisablePState(Profiles[i].Mem_Enabled[7],Mem_P7,Mem_Clock_7,Mem_Vcc_7);
     Mem_Clock_7Change(NULL);
     Mem_Vcc_7Change(NULL);
   }
 } else
  if (GPU[GpuInd].APIVersion==8)
   {
    GPU_Clock_0->Value=Profiles[i].GPU_Clock[0];
    GPU_Vcc_0->Value=Profiles[i].GPU_Vcc[0];
    GPU_Clock_0Change(NULL);
    GPU_Vcc_0Change(NULL);
    GPU_Clock_1->Value=Profiles[i].GPU_Clock[1];
    GPU_Vcc_1->Value=Profiles[i].GPU_Vcc[1];
    GPU_Clock_1Change(NULL);
    GPU_Vcc_1Change(NULL);
    GPU_Clock_2->Value=Profiles[i].GPU_Clock[2];
    GPU_Vcc_2->Value=Profiles[i].GPU_Vcc[2];
    GPU_Clock_2Change(NULL);
    GPU_Vcc_2Change(NULL);

    if (Profiles[i].GPU_Min!=-500)
     GPU_Min->Value=Profiles[i].GPU_Min;
    if (Profiles[i].GPU_Max!=-500)
     GPU_Max->Value=Profiles[i].GPU_Max;
    if (Profiles[i].Mem_Max!=-500)
     Mem_Max->Value=Profiles[i].Mem_Max;
   }
//////////////////////////////////////////
//Both APIs:
if (GPU[GpuInd].FanEnabled)
 {
   Fan_Min->Value=Profiles[i].Fan_Min;
   Fan_Max->Value=Profiles[i].Fan_Max;
   Fan_Target->Value=Profiles[i].Fan_Target;
   Fan_Acoustic->Value=Profiles[i].Fan_Acoustic;

   if (Profiles[i].FanCurvePoints[0].Temp!=-500)
    Fan_Temp_0->Value=Profiles[i].FanCurvePoints[0].Temp;
   if (Profiles[i].FanCurvePoints[0].Percentage!=-500)
    Fan_Perc_0->Value=Profiles[i].FanCurvePoints[0].Percentage;
   if (Profiles[i].FanCurvePoints[1].Temp!=-500)
    Fan_Temp_1->Value=Profiles[i].FanCurvePoints[1].Temp;
   if (Profiles[i].FanCurvePoints[1].Percentage!=-500)
    Fan_Perc_1->Value=Profiles[i].FanCurvePoints[1].Percentage;
   if (Profiles[i].FanCurvePoints[2].Temp!=-500)
    Fan_Temp_2->Value=Profiles[i].FanCurvePoints[2].Temp;
   if (Profiles[i].FanCurvePoints[2].Percentage!=-500)
    Fan_Perc_2->Value=Profiles[i].FanCurvePoints[2].Percentage;
   if (Profiles[i].FanCurvePoints[3].Temp!=-500)
    Fan_Temp_3->Value=Profiles[i].FanCurvePoints[3].Temp;
   if (Profiles[i].FanCurvePoints[3].Percentage!=-500)
    Fan_Perc_3->Value=Profiles[i].FanCurvePoints[3].Percentage;
   if (Profiles[i].FanCurvePoints[4].Temp!=-500)
    Fan_Temp_4->Value=Profiles[i].FanCurvePoints[4].Temp;
   if (Profiles[i].FanCurvePoints[4].Percentage!=-500)
    Fan_Perc_4->Value=Profiles[i].FanCurvePoints[4].Percentage;

   if (Profiles[i].Fan_ZeroRPM!=-500)
    ZeroRPMBox->Checked=Profiles[i].Fan_ZeroRPM;
 }

if ((Profiles[i].Mem_TimingLevel!=-500) && (Mem_TimingBox->Items->Count>Profiles[i].Mem_TimingLevel))
 {
  Mem_TimingBox->ItemIndex=Profiles[i].Mem_TimingLevel;
  Mem_TimingBoxChange(NULL);
 }


Power_Temp->Value=Profiles[i].Power_Temp;
Power_Target->Value=Profiles[i].Power_Target;
/////////////////////////////////////

//I2C
if (Options.UseI2C)
 {
  if (GPU[GpuInd].I2C.devicefound)
   {
    if (Profiles[i].Offset!=-500)
     {
      I2C_Offset->Value=Profiles[i].Offset;
      I2C_OffsetChange(NULL);
     }

    if (Profiles[i].LLC!=-500)
     {
       if (Profiles[i].LLC==1)
        {
          I2C_LLC->Checked=true;
        } else
          {
            I2C_LLC->Checked=false;
          }
     }

    if (Profiles[i].PhaseGain!="NULL")
     {
      Edit1->Text=Profiles[i].PhaseGain;
     }

    if (Profiles[i].CurrentScale!="NULL")
     {
      Edit2->Text=Profiles[i].CurrentScale ;
     }
   }
 }
}
//---------------------------------------------------------------------------
                          // 11111    
void INI::FlushFileBuffer(String ini_name)
{
void* iFile=CreateFile(ini_name.w_str(),
                       GENERIC_WRITE,
                       0,
                       NULL,
                       OPEN_EXISTING,
                       FILE_ATTRIBUTE_NORMAL | FILE_FLAG_NO_BUFFERING | FILE_FLAG_WRITE_THROUGH,
                       NULL);

if (iFile!=INVALID_HANDLE_VALUE)
    {
      try
       {
         if (FlushFileBuffers(iFile)==0)
          {
           LogWrite("FlushFileBuffers function failed", GetLastError(), true, 0);
           #ifdef  _DEBUG
           blad("FlushFileBuffers function failed", GetLastError(), true);
           #endif
          }
       }
      __finally
        {
          CloseHandle(iFile);
        }
    } else
      {
         LogWrite("CreateFile function failed", GetLastError(), true, 0);
         #ifdef  _DEBUG
         blad("CreateFile function failed", GetLastError(), true);
         #endif
      }

return;
}
//---------------------------------------------------------------------------
int INI::SaveProfiles()
{
TMemIniFile* ini = new TMemIniFile(inifile, TEncoding::Unicode);
try
 {
 // profiles cleanup (kasujemy wpisy profili):
   int all=0;
   int i=-1;
   while (1)
    {
      if (all>10) break;
      i++;
      if (ini->SectionExists("Profile_"+IntToStr(i)))
         {
           ini->EraseSection("Profile_"+IntToStr(i));
         } else
           {
             all++;
           }
    }
  //===========================
  String str;
  for (i = 0; i < Profiles.Length; i++)
   {
    ini->WriteString("Profile_"+IntToStr(i),"Name",Profiles[i].Name);
    for (int j = 0; j < Profiles[i].GPU_Clock.Length; j++)
     {
      str=IntToStr(Profiles[i].GPU_Clock[j])+";"+IntToStr(Profiles[i].GPU_Vcc[j]);
      if (Profiles[i].GPU_Enabled[j]==false)
       {
         str=str+";0";
       }
      ini->WriteString("Profile_"+IntToStr(i),"GPU_P"+IntToStr(j),str);
     }
    if (Profiles[i].GPU_Min!=-500)
     ini->WriteInteger("Profile_"+IntToStr(i),"GPU_Min", Profiles[i].GPU_Min);
    if (Profiles[i].GPU_Max!=-500)
     ini->WriteInteger("Profile_"+IntToStr(i),"GPU_Max", Profiles[i].GPU_Max);
    for (int j = 0; j < Profiles[i].Mem_Clock.Length; j++)
     {
      str=IntToStr(Profiles[i].Mem_Clock[j])+";"+IntToStr(Profiles[i].Mem_Vcc[j]);
      if (Profiles[i].Mem_Enabled[j]==false)
       {
         str=str+";0";
       }
      ini->WriteString("Profile_"+IntToStr(i),"Mem_P"+IntToStr(j),str);
     }
    if (Profiles[i].Mem_Max!=-500)
     ini->WriteInteger("Profile_"+IntToStr(i),"Mem_Max", Profiles[i].Mem_Max);
    if (Profiles[i].Mem_TimingLevel!=-500)
     ini->WriteInteger("Profile_"+IntToStr(i),"Mem_TimingLevel", Profiles[i].Mem_TimingLevel);

  

    for (int j = 0; j < 5; j++)
     {
      if ((Profiles[i].FanCurvePoints[j].Temp==-500) || (Profiles[i].FanCurvePoints[j].Percentage==-500))
       break;
      str=IntToStr(Profiles[i].FanCurvePoints[j].Temp)+";"+IntToStr(Profiles[i].FanCurvePoints[j].Percentage);
      ini->WriteString("Profile_"+IntToStr(i), "Fan_P"+IntToStr(j), str);
     }

    if (Profiles[i].Fan_ZeroRPM!=-500)
     ini->WriteBool("Profile_"+IntToStr(i),"Fan_ZeroRPM", Profiles[i].Fan_ZeroRPM);
    // ini->WriteInteger("Profile_"+IntToStr(i),"Fan_Min", Profiles[i].Fan_Min);          //Obsolete in 18.12.2
    // ini->WriteInteger("Profile_"+IntToStr(i),"Fan_Max", Profiles[i].Fan_Max);          //Obsolete in 18.12.2
    // ini->WriteInteger("Profile_"+IntToStr(i),"Fan_Target", Profiles[i].Fan_Target);    //Obsolete in 18.12.2
    ini->WriteInteger("Profile_"+IntToStr(i),"Fan_Acoustic", Profiles[i].Fan_Acoustic);
    // ini->WriteInteger("Profile_"+IntToStr(i),"Power_Temp", Profiles[i].Power_Temp);    //Obsolete in 18.12.2
    ini->WriteInteger("Profile_"+IntToStr(i),"Power_Target", Profiles[i].Power_Target);
    //I2C
    if (Options.UseI2C)
      {
        if (Profiles[i].Offset!=-500)
         {
           ini->WriteInteger("Profile_"+IntToStr(i),"Offset",Profiles[i].Offset);
         }
        if (Profiles[i].LLC!=-500)
         {
           ini->WriteInteger("Profile_"+IntToStr(i),"LLC",Profiles[i].LLC);
         }
        if (Profiles[i].PhaseGain!="NULL")
         {
           ini->WriteString("Profile_"+IntToStr(i),"PhaseGain",Profiles[i].PhaseGain);
         }
        if (Profiles[i].CurrentScale!="NULL")
         {
           ini->WriteString("Profile_"+IntToStr(i),"CurrentScale",Profiles[i].CurrentScale);
         }
      }
   }
  ini->UpdateFile();
  INI::FlushFileBuffer(inifile);
 }
__finally
   {
     delete ini;
   }
if (Options.IniBackup_AutoCreate)
 {
  INI::MakeIniBackup(false);
 }
return 0;
}

//---------------------------------------------------------------------------
void INI::LoadOptions(bool from_cmd)
{
if (from_cmd)
  {
    I2C_.UsingCommandLine=true;
    I2C_.Scanned=false;
    Options.BusNumber=false;
    Options.PNPString=false;
    Options.Adapter=false;
    Options.FriendlyName=false;
    Options.RegistryKey=false;
  } else
     {
       I2C_.UsingCommandLine=false;
       I2C_.Scanned=false;
     }

if (FileExists(inifile))
 {
   TIniFile* ini = new TIniFile(inifile);
   try
    {
      if (from_cmd==false)
        {
           Options.BusNumber=ini->ReadBool("General","ShowBusNumber",false);
           Options.PNPString=ini->ReadBool("General","ShowPnpString",false);
           Options.Adapter=ini->ReadBool("General","ShowAdapterIndex",false);
           Options.FriendlyName=ini->ReadBool("General","ShowFriendlyName",false);
           Options.RegistryKey=ini->ReadBool("General","ShowRegistryKey",false);
           //Dodatkowe mena===================
           if (ini->ReadBool("General", "DisplayFanMenu", false)==true)
            Form1->GroupBox3->PopupMenu=Form1->FanTestMenu;
           if (ini->ReadBool("General", "DisplayPowerMenu", false)==true)
            Form1->GroupBox4->PopupMenu=Form1->PowerMenu;
           //===========================
        }
      Options.FanDeactivatedStr=ini->ReadString("General", "FanDeactivatedGpus", "");
      if (Options.FanDeactivatedStr!="")
       Options.FanDeactivatedStr=";"+Options.FanDeactivatedStr+";";
      Options.IgnoreError8=ini->ReadBool("General","IgnoreError-8",false);
      Options.AutoReset=ini->ReadBool("General","ResetBeforeApply",false);
      Options.CheckValuesMismatch=ini->ReadBool("General","CheckValuesMismatch",false);
      Options.DoNotListUnsupported=ini->ReadBool("General","DoNotListUnsupported",false);
      Options.UseI2C=ini->ReadBool("General","EnableI2c",false);
      if (Options.UseI2C)
       {
         if (ini->SectionExists("I2C"))
          {
            I2C_.DoFullScan=false;
          } else
            {
              I2C_.DoFullScan=true;
            }
       }
    }
    __finally
     {
       delete ini;
     }
 } else
   {
      Options.BusNumber=false;
      Options.PNPString=false;
      Options.Adapter=false;
      Options.FriendlyName=false;
      Options.RegistryKey=false;
      Options.DoNotListUnsupported=false;
      Options.UseI2C=false;
      I2C_.DoFullScan=false;
      Options.IgnoreError8=false;
      Options.FanDeactivatedStr="";
      Options.AutoReset=false;
      Options.CheckValuesMismatch=false;
   }

if (FileExists(backupinifile))
 {
   TIniFile* ini = new TIniFile(backupinifile);
   try
    {
      Options.IniBackup_AutoCreate=ini->ReadBool("General", "CreateAutomatically", false);
      Options.IniBackup_AutoRestore=ini->ReadBool("General", "RestoreAutomatically", false);
      Options.IniBackup_NoConfirmOnRestore=ini->ReadBool("General", "DoNotConfirmOnRestore", false);
      Options.IniBackup_LimitNumOfBackups=ini->ReadBool("General", "LimitNumberOfBackups", false);
      Options.IniBackup_Count=ini->ReadInteger("General", "NumberOfBackups", 10);
    } __finally
     {
       delete ini;
     }
 } else
  {
   Options.IniBackup_AutoCreate=false;
   Options.IniBackup_AutoRestore=false;
   Options.IniBackup_Count=10;
   Options.IniBackup_NoConfirmOnRestore=false;
   Options.IniBackup_LimitNumOfBackups=false;
  }

return;
}
//---------------------------------------------------------------------------
void INI::SaveOptions()
{
TMemIniFile* ini = new TMemIniFile(inifile, TEncoding::Unicode);
try
 {
   bool changed=false;
   if (ini->ReadBool("General","ShowBusNumber",0)!=Form2->CheckBox3->Checked)
    {
      ini->WriteBool("General","ShowBusNumber",Form2->CheckBox3->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","ShowPnpString",0)!=Form2->CheckBox1->Checked)
    {
      ini->WriteBool("General","ShowPnpString",Form2->CheckBox1->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","ShowAdapterIndex",0)!=Form2->CheckBox4->Checked)
    {
      ini->WriteBool("General","ShowAdapterIndex",Form2->CheckBox4->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","ShowFriendlyName",0)!=Form2->CheckBox6->Checked)
    {
      ini->WriteBool("General","ShowFriendlyName",Form2->CheckBox6->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","ShowRegistryKey",0)!=Form2->CheckBox7->Checked)
    {
      ini->WriteBool("General","ShowRegistryKey",Form2->CheckBox7->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","EnableI2c",0)!=Form2->CheckBox2->Checked)
    {
      ini->WriteBool("General","EnableI2c",Form2->CheckBox2->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","DoNotListUnsupported",0)!=Form2->CheckBox5->Checked)
    {
      ini->WriteBool("General","DoNotListUnsupported",Form2->CheckBox5->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","IgnoreError-8",0)!=Form2->CheckBox8->Checked)
    {
      ini->WriteBool("General","IgnoreError-8",Form2->CheckBox8->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","ResetBeforeApply",0)!=Form2->CheckBox15->Checked)
    {
      ini->WriteBool("General","ResetBeforeApply", Form2->CheckBox15->Checked);
      changed=true;
    }
   if (ini->ReadBool("General","CheckValuesMismatch",0)!=Form2->CheckBox14->Checked)
    {
      ini->WriteBool("General","CheckValuesMismatch", Form2->CheckBox14->Checked);
      changed=true;
    }

   if (Form2->CheckBox2->Checked==false)   //dodatkowa kasujemy sekcj� I2C w ini gdy I2C jest disabled
    {
      if (ini->SectionExists("I2C"))
        {
          ini->EraseSection("I2C");
          changed=true;
        }
    }
   if (changed)
    {
     ini->UpdateFile();
     INI::FlushFileBuffer(inifile);
    }
 }
   __finally
   {
     delete ini;
   }

TIniFile* bini = new TIniFile(backupinifile);
try
 {
  if ((bini->ReadBool("General", "CreateAutomatically", false)!=Form2->CheckBox10->Checked) ||
     (bini->ReadBool("General", "RestoreAutomatically", false)!=Form2->CheckBox9->Checked) ||
     (bini->ReadBool("General", "LimitNumberOfBackups", false)!=Form2->CheckBox12->Checked) ||
     (bini->ReadBool("General", "DoNotConfirmOnRestore", false)!=Form2->CheckBox11->Checked) ||
     (bini->ReadInteger("General", "NumberOfBackups", 10)!=Form2->SpinEdit1->Value))
      {
        if (!DirectoryExists(ExtractFilePath(Application->ExeName)+"ini_backup"))
         {
           int ret=SHCreateDirectory(NULL,(ExtractFilePath(Application->ExeName)+"ini_backup").w_str());
           if (ret!=0)
            {
             blad("Failed to create directory \""+ExtractFilePath(Application->ExeName)+"ini_backup\"", ret, true);
             return;
            }
         }
        //INI_SetReadOnly(backupinifile, false);
        bini->WriteBool("General","CreateAutomatically", Form2->CheckBox10->Checked);
        bini->WriteBool("General","RestoreAutomatically", Form2->CheckBox9->Checked);
        bini->WriteBool("General","LimitNumberOfBackups", Form2->CheckBox12->Checked);
        bini->WriteBool("General","DoNotConfirmOnRestore", Form2->CheckBox11->Checked);
        bini->WriteInteger("General","NumberOfBackups", Form2->SpinEdit1->Value);
        INI::FlushFileBuffer(backupinifile);
        //INI_SetReadOnly(backupinifile, true);
      }
 } __finally
   {
     delete bini;
   }
return;
}
//---------------------------------------------------------------------------
void INI::EraseI2CSection()
{
if (FileExists(inifile))
 {
   TMemIniFile* ini = new TMemIniFile(inifile, TEncoding::Unicode);
   try
    {
      if (ini->SectionExists("I2C"))
        {
          ini->EraseSection("I2C");
          ini->UpdateFile();
          INI::FlushFileBuffer(inifile);
        };
    }  __finally
     {
       delete ini;
     }
 }
return;
}

//---------------------------------------------------------------------------
int INI::LoadProfiles()
{
if (FileExists(inifile))
 {
   TIniFile* ini = new TIniFile(inifile);
   try
    {
     //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
     //Profile
     int m=-1;
     int err=0;
     while (1)    //never ending loop
      {
       String str;
       m++;
       const String s="Profile_"+IntToStr(m);
       if (ini->ValueExists(s,"Name"))
        {
         Profiles.Length++;
         Profiles[Profiles.Length-1].APIVer=0;
         try
          {
            Profiles[Profiles.Length-1].Name=ini->ReadString(s,"Name","");
            for (int i = 0; i < 8; i++)
             {
              str=ini->ReadString(s,"GPU_P"+IntToStr(i),"");
              str=str.Trim();
              if (Pos(";",str)>0)
        	 {
                  Profiles[Profiles.Length-1].GPU_Enabled.Length=Profiles[Profiles.Length-1].GPU_Enabled.Length+1;
                  Profiles[Profiles.Length-1].GPU_Clock.Length=Profiles[Profiles.Length-1].GPU_Clock.Length+1;
                  Profiles[Profiles.Length-1].GPU_Clock[i]=StrToInt(str.SubString(1,Pos(";",str)-1));
                  str.Delete(1,Pos(";",str));
                  if (Pos(";0",str)>0)
                   {
                     Profiles[Profiles.Length-1].GPU_Enabled[i]=false;
                     str=StringReplace(str, ";0", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);
                   } else
                    {
                     Profiles[Profiles.Length-1].GPU_Enabled[i]=true;
                    }
                  Profiles[Profiles.Length-1].GPU_Vcc.Length=Profiles[Profiles.Length-1].GPU_Vcc.Length+1;
                  Profiles[Profiles.Length-1].GPU_Vcc[i]=StrToInt(str);
                 } else
                    break;
             }
            for (int i = 0; i < 8; i++)
             {
              str=ini->ReadString(s,"Mem_P"+IntToStr(i),"");
              str=str.Trim();
              if (Pos(";",str)>0)
        	 {
                  Profiles[Profiles.Length-1].Mem_Enabled.Length=Profiles[Profiles.Length-1].Mem_Enabled.Length+1;
                  Profiles[Profiles.Length-1].Mem_Clock.Length=Profiles[Profiles.Length-1].Mem_Clock.Length+1;
                  Profiles[Profiles.Length-1].Mem_Clock[i]=StrToInt(str.SubString(1,Pos(";",str)-1));
                  str.Delete(1,Pos(";",str));
                  if (Pos(";0",str)>0)
                   {
                     Profiles[Profiles.Length-1].Mem_Enabled[i]=false;
                     str=StringReplace(str, ";0", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);
                   } else
                    {
                     Profiles[Profiles.Length-1].Mem_Enabled[i]=true;
                    }
                  Profiles[Profiles.Length-1].Mem_Vcc.Length=Profiles[Profiles.Length-1].Mem_Vcc.Length+1;
                  Profiles[Profiles.Length-1].Mem_Vcc[i]=StrToInt(str);
                 } else
                    break;
             }
            //Profiles[Profiles.Length-1].Fan_Min=ini->ReadInteger(s,"Fan_Min",0);         //Obsolete in 18.12.2
            //Profiles[Profiles.Length-1].Fan_Max=ini->ReadInteger(s,"Fan_Max",0);         //Obsolete in 18.12.2
            //Profiles[Profiles.Length-1].Fan_Target=ini->ReadInteger(s,"Fan_Target",0);   //Obsolete in 18.12.2
            Profiles[Profiles.Length-1].Fan_Acoustic=ini->ReadInteger(s,"Fan_Acoustic",0);
            //Profiles[Profiles.Length-1].Power_Temp=ini->ReadInteger(s,"Power_Temp",0);   //Obsolete in 18.12.2
            Profiles[Profiles.Length-1].Power_Target=ini->ReadInteger(s,"Power_Target",0);
            Profiles[Profiles.Length-1].Offset=ini->ReadInteger(s,"Offset",-500);
            Profiles[Profiles.Length-1].LLC=ini->ReadInteger(s,"LLC",-500);
            Profiles[Profiles.Length-1].PhaseGain=ini->ReadString(s,"PhaseGain","NULL");
            Profiles[Profiles.Length-1].CurrentScale=ini->ReadString(s,"CurrentScale","NULL");
            for (int i = 0; i < 5; i++)
             {
              str=ini->ReadString(s,"Fan_P"+IntToStr(i),"");
              str=str.Trim();
              if (Pos(";",str)>1)
               {
                Profiles[Profiles.Length-1].FanCurvePoints[i].Temp=StrToInt(str.SubString(1,Pos(";",str)-1));
                str.Delete(1,Pos(";",str));
                Profiles[Profiles.Length-1].FanCurvePoints[i].Percentage=StrToInt(str);
               } else
                {
                 Profiles[Profiles.Length-1].FanCurvePoints[i].Temp=-500;
                 Profiles[Profiles.Length-1].FanCurvePoints[i].Percentage=-500;
                }
             }
            Profiles[Profiles.Length-1].Mem_TimingLevel=ini->ReadInteger(s, "Mem_TimingLevel", -500);
            Profiles[Profiles.Length-1].Fan_ZeroRPM=ini->ReadInteger(s, "Fan_ZeroRPM", -500);
            Profiles[Profiles.Length-1].GPU_Min=ini->ReadInteger(s, "GPU_Min", -500);
            Profiles[Profiles.Length-1].GPU_Max=ini->ReadInteger(s, "GPU_Max", -500);
            Profiles[Profiles.Length-1].Mem_Max=ini->ReadInteger(s, "Mem_Max", -500);

            if (Profiles[Profiles.Length-1].Mem_Clock.Length>0)
             Profiles[Profiles.Length-1].APIVer=7;
            if ((Profiles[Profiles.Length-1].GPU_Min!=-500) &&
               (Profiles[Profiles.Length-1].GPU_Max!=-500) &&
               (Profiles[Profiles.Length-1].Mem_Max!=-500))
                 Profiles[Profiles.Length-1].APIVer=8;


          }
          __except(EXCEPTION_EXECUTE_HANDLER)
           {
            blad("Failed to load profile \""+Profiles[Profiles.Length-1].Name+"\" from .ini", NULL, false);
            Profiles.Length--;
            continue;
           }
        } else
          {
            if (err>1) break;
            err++;
          }
      }
     //&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
    }
   __finally
    {
      delete ini;
    }
 }
return Profiles.Length;
}
//---------------------------------------------------------------------------
int Cmd::FindProfileByName(String Name)
{
for (int i=0; i < Profiles.Length; i++)
  {
    if (Profiles[i].Name==Name)
     {
       return i;
     }
  }
return -1;
}
//---------------------------------------------------------------------------
bool Cmd::CorrectGPU(int GpuInd)
{
return !(GpuInd>(GPU.Length-1));
}
//---------------------------------------------------------------------------
bool Cmd::SupportedGPU(int GpuInd)
{
return (GPU[GpuInd].APISupported);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)  //RESET
{

if ((GetKeyState(VK_CONTROL) & 0x8000))   //ctrl pressed for multigpu
 {
  for (int i = 0; i < GPU.Length; i++)
   {
    if ((GPU[i].APISupported))
     Reset(i, false, false);
   }
 } else
  {
   Reset(ComboBox1->ItemIndex, false, false);
  }

ReadCurrent(ComboBox1->ItemIndex);

if (Options.UseI2C)
 {
   Sleep(10);
   I2C_.ReadSettings(ComboBox1->ItemIndex,true);
 }

}
//---------------------------------------------------------------------------
//pozycj About..., Settings... w menu systemowym
void __fastcall TForm1::wmSysCommand(TMessage& Message)
{
if (Message.WParamLo==(Word)-1)     //About...
  {
    String str;
    str="OverdriveNTool v"+appversion;
    str=str+"\x0d\x0a";
    str=str+ "Author: tede @forums guru3d";
    MessageBox(0, str.c_str(), L"About", MB_OK);

  } else
  if (Message.WParamLo==(Word)-4)      //Settings...
   {
    Form2->Show();
   } else
    if (Message.WParamLo==(Word)-6)    //SoftPowerPlayTable Editor
     {
      OpenPowerPlayEditor();
     }
TForm::Dispatch(&Message);
}
//---------------------------------------------------------------------------
void INI::SetIniFileName()
{
//inifile=ExtractFilePath(Application->ExeName)+(StringReplace(ExtractFileName(Application->ExeName),".exe",".ini", TReplaceFlags() << rfIgnoreCase));
inifile=ExtractFilePath(Application->ExeName)+"OverdriveNTool.ini";
backupinifile=ExtractFilePath(Application->ExeName)+"ini_backup\\_ini_backup.ini";
}
// ---------------------------------------------------------------------------
void Odnt::ProfilesCompareAtStartup()
{
if (Profiles.Length>0)
 {
  for (int i = 0; i < Profiles.Length; i++)
   {
     if (EqualValues(&CurrentValues, &Profiles[i], Form1->ComboBox1->ItemIndex))
      {
        Form1->ComboBox2->ItemIndex=i;
        break;
      }
   }
 }
}

// ---------------------------------------------------------------------------
typedef struct fsItem {
 String DevID;
 String FriendlyName;
} fsItem;
// ---------------------------------------------------------------------------
bool Odnt::ChangeGPUFriendlyName(int GpuInd, String NewName)
{
GUID hidGuid= StringToGUID("{4d36e968-e325-11ce-bfc1-08002be10318}");

HDEVINFO hDevInfo = SetupDiGetClassDevs(&hidGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_PROFILE);
if (INVALID_HANDLE_VALUE == hDevInfo)
  {
    blad( "SetupDiGetClassDevs function failed",GetLastError(),true);
    return false;
  }

SP_DEVINFO_DATA* pspDevInfoData = (SP_DEVINFO_DATA*)HeapAlloc(GetProcessHeap(), 0, sizeof(SP_DEVINFO_DATA));
pspDevInfoData->cbSize = sizeof(SP_DEVINFO_DATA);
pspDevInfoData->ClassGuid=hidGuid;

for (int i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, pspDevInfoData); i++)
 {
   DWORD nSize = 0;
   TCHAR buf[MAX_PATH];
   if (!SetupDiGetDeviceInstanceId(hDevInfo, pspDevInfoData, buf, sizeof(buf), &nSize))
    {
       blad("SetupDiGetDeviceInstanceId function failed",GetLastError(),true);
       return false;
    } else
     {
       String str=buf;
       if (str==GPU[GpuInd].DevID)
        {
         if (NewName=="")    //Kasowanie
           {
              DWORD nSize = 0;
              TCHAR buf[MAX_PATH];
              DWORD DataT;
              //pierw sprawdzamy czy istnieje
              if (SetupDiGetDeviceRegistryProperty(hDevInfo, pspDevInfoData, SPDRP_FRIENDLYNAME, &DataT, (PBYTE)buf, sizeof(buf), &nSize))
               {
                  if  (SetupDiSetDeviceRegistryProperty(hDevInfo, pspDevInfoData, SPDRP_FRIENDLYNAME, NULL, 0))
                   {
                     return true;
                   } else
                     {
                       blad("SetupDiSetDeviceRegistryProperty function failed",GetLastError(),true);
                       return false;
                     }
               } else
                 {
                   return true;
                 }
           } else
             {
               TCHAR buf[MAX_PATH];
               wcsncpy(buf,NewName.w_str(),MAX_PATH);
               if  (SetupDiSetDeviceRegistryProperty(hDevInfo, pspDevInfoData, SPDRP_FRIENDLYNAME, (PBYTE)buf, sizeof(buf)))
                {
                  return true;
                } else
                  {
                    blad("SetupDiSetDeviceRegistryProperty function failed",GetLastError(),true);
                    return false;
                  }
             }
        }
     }
 }

return false;
}
// ---------------------------------------------------------------------------
bool Odnt::ReadGPUFriendlyName(bool fromsettings)
{
//&&&&&&&&&&&&&&&&&&&&&&&
GUID hidGuid= StringToGUID("{4d36e968-e325-11ce-bfc1-08002be10318}");

HDEVINFO hDevInfo = SetupDiGetClassDevs(&hidGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_PROFILE);
if (INVALID_HANDLE_VALUE == hDevInfo)
  {
    blad( "SetupDiGetClassDevs function failed", GetLastError(), true);
    return false;
  }

SP_DEVINFO_DATA* pspDevInfoData = (SP_DEVINFO_DATA*)HeapAlloc(GetProcessHeap(), 0, sizeof(SP_DEVINFO_DATA));
pspDevInfoData->cbSize = sizeof(SP_DEVINFO_DATA);
pspDevInfoData->ClassGuid=hidGuid;

DynamicArray <fsItem> fItems;

for (int i = 0; SetupDiEnumDeviceInfo(hDevInfo, i, pspDevInfoData); i++)
 {
   DWORD nSize = 0;
   TCHAR buf[MAX_PATH];
   if (!SetupDiGetDeviceInstanceId(hDevInfo, pspDevInfoData, buf, sizeof(buf), &nSize))
    {
       blad("SetupDiGetDeviceInstanceId function failed", GetLastError(), true);
       return false;
    }
   String str=buf;
   DWORD DataT;

                                                                 // SPDRP_BUSNUMBER
   if (SetupDiGetDeviceRegistryProperty(hDevInfo, pspDevInfoData, SPDRP_FRIENDLYNAME, &DataT, (PBYTE)buf, sizeof(buf), &nSize))
    {
      fItems.Length++;
      fItems[fItems.Length-1].FriendlyName=buf;
      fItems[fItems.Length-1].DevID=str;
    }
 }
//&&&&&&&&&&&&&&&&&&&&&&&
if ((fItems.Length>0) || (fromsettings))
 {
  for (int i = 0; i < GPU.Length; i++)
   {
     GPU[i].FriendlyName="";
     for (int j = 0; j < fItems.Length; j++)
      {
       if (fItems[j].DevID==GPU[i].DevID)
         {
           GPU[i].FriendlyName=fItems[j].FriendlyName;
           break;
         }
      }
     if (GPU[i].FriendlyName!="")
       {
         GPU[i].DisplayName=FormatGPUDisplayName(i, true);
       }
   }
 }
return true;       Profiles.Length=0;
}
// ---------------------------------------------------------------------------
//typ: 0-not supported, 1-driver not installed, 2-old driver
void __fastcall TForm1::ShowHideInfoLabel(bool visible, int typ)
{
//InfoLabel->Top=196;
if (visible)
   {
      switch (typ)
         {
           case 0:
             InfoLabel->Left=DpiMultiplerPix*214;
             InfoLabel->Caption="This GPU is not supported";
             break;
           case 1:
             InfoLabel->Left=DpiMultiplerPix*175;
             InfoLabel->Caption="Driver is detected as not installed/loaded";
             break;
           case 2:
             InfoLabel->Left=DpiMultiplerPix*137;
             InfoLabel->Caption="This application requires driver 18.12.2 or newer";
             break;
           default:
             InfoLabel->Caption="";
             break;
         }
   } else
      {
        InfoLabel->Caption="";
      }
InfoLabel->Visible=visible;
InfoLabel->Repaint();
Form1->Repaint();
}
// ---------------------------------------------------------------------------

//pozycje w g��wnym menu systemowym
inline void __fastcall TForm1::AddSystemMemuEntries()
{
HMENU sysMenu=GetSystemMenu(Handle, false);
//pozycja "Settings":
AppendMenu(sysMenu, MF_SEPARATOR, (Word)-3, L"");
AppendMenu(sysMenu, MF_BYPOSITION, (Word)-4, L"Settings");

//pozycja "SoftPowerPlayTable Editor":
AppendMenu(sysMenu, MF_SEPARATOR, (Word)-5, L"");
AppendMenu(sysMenu, MF_BYPOSITION, (Word)-6, L"PPTable editor");
//EnableMenuItem(sysMenu, ((Word)-6),MF_BYCOMMAND | MF_DISABLED);

//pozycja "About...":
AppendMenu(sysMenu, MF_SEPARATOR, (Word)-2, L"");
AppendMenu(sysMenu, MF_BYPOSITION, (Word)-1, L"About...");
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
Form1->Caption="OverdriveNTool "+appversion;
#ifdef  _DEBUG
Form1->Caption=Form1->Caption+ " > DEBUG VERSION <";
#endif
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
INI.LoadOptions(false);
if (INI.LoadProfiles()==0)
 INI.RestoreFromBackup(false);
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//ustawiamy form� gdy I2C jest disabled
if (Options.UseI2C==false)
 {
  DpiMultiplerPix=1;
  if (Form1->Monitor->PixelsPerInch!=96)  //fix dla innych DPI
   DpiMultiplerPix =1 + ((Form1->Monitor->PixelsPerInch-96) / double(96));

  Form1->Height=DpiMultiplerPix*456;
  GroupBox6->Visible=false;
  GroupBox5->Top=DpiMultiplerPix*314;
  Button1->Top=DpiMultiplerPix*388;
  Button2->Top=DpiMultiplerPix*388;
 }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//ustawienia formy na ostatni� pozycj�
INI.SetMainFormPosition();
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//SetBoxesNotVisible();
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//pozycje w g��wnym menu systemowym
AddSystemMemuEntries();
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (CheckIfDriverInstalled()==false)
 {
   ShowHideInfoLabel(true, 1);
   SetBoxesNotVisible();
   return;
 }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (FindGPUs()==false)
  {
   SetBoxesNotVisible();
   if (VerifyADL_Library()==-2)
     {
       ShowHideInfoLabel(true, 2);
     }
   return;
  }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (Options.FriendlyName)
 {
  if (GPU.Length>0)
   {
    try
     {
       Odnt.ReadGPUFriendlyName(false);
       ChangeFriendlyName1->Visible=true;
     }
     __except(EXCEPTION_EXECUTE_HANDLER)
       {
         SetBoxesNotVisible();
       }
   }
 }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (GPU.Length>0)
 {
  //********************************************
  //Gdy jest wi�cej ni� 10 GPU to dodajemy "0" na pocz�tku gpu_id dla gpu 0-9
  //wy��czone - chyba zrezygnuj� z tego
  /*  
  if (GPU.Length>10)
   {
     for (int i = 0; i < 10; i++)
       {
         GPU[i].DisplayName="0"+GPU[i].DisplayName;
         GPU[i].IDString="0"+GPU[i].IDString;
       }
   }
  */
  //********************************************
 // bool api7only=true;
  for (int i = 0; i < GPU.Length; i++)
   {
     ComboBox1->Items->Add(GPU[i].DisplayName);
   //  if (GPU[i].APIVersion==8)
   //   api7only=false;
   }
  //Make main form smaller size if only api7 exists

  if (Options.UseI2C)
   {
     I2C_.Linescanning=true;
   }
  //********************************************
  //Ustawiamy GPU na 0 lub pierwsze supportowane
  if ((GPU.Length==1) || (GPU[0].APISupported))
   {
     ComboBox1->ItemIndex=0;
   } else
    {
      int StartupGPU=0;
      for (int i = 0; i < GPU.Length; i++)
        {
         if (GPU[i].APISupported)
           {
             StartupGPU=i;
             break;
           }
        }
      ComboBox1->ItemIndex=StartupGPU;
    }
  ComboBox1Change(Sender);
  //********************************************
 } else
   {
    SetBoxesNotVisible();
   }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//Fan deactivated
if (FanDeactivatedExists)
 EnableDisableFanSection(GPU[ComboBox1->ItemIndex].FanEnabled);
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (Profiles.Length>0)
 {
  for (int i = 0; i < Profiles.Length; i++)
   {
    ComboBox2->Items->Add(Profiles[i].Name);
   }
 }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (Options.UseI2C)
 {
  if(GPU.Length>0)
   {
     I2C_.Linescanning=true;
     I2C_.Scan(I2C_.DoFullScan);
   }
 } else
   {
     Odnt.ProfilesCompareAtStartup();
   }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ComboBox1Change(TObject *Sender)
{
if (ComboBox1->ItemIndex!=-1)
 {
  ShowHideControls(ComboBox1->ItemIndex);
  ReadCurrent(ComboBox1->ItemIndex);
  if (Options.UseI2C)
   {
    if (I2C_.Linescanning==false)
     {
       I2C_.ReadSettings(ComboBox1->ItemIndex,true);
     }
   }
 }

}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{
Button2->SetFocus();

Form2->CheckBox3->Checked=Options.BusNumber;
Form2->CheckBox1->Checked=Options.PNPString;
Form2->CheckBox4->Checked=Options.Adapter;
Form2->CheckBox6->Checked=Options.FriendlyName;
Form2->CheckBox7->Checked=Options.RegistryKey;
Form2->CheckBox2->Checked=Options.UseI2C;
Form2->CheckBox5->Checked=Options.DoNotListUnsupported;
Form2->CheckBox8->Checked=Options.IgnoreError8;
Form2->CheckBox14->Checked=Options.CheckValuesMismatch;
Form2->CheckBox15->Checked=Options.AutoReset;

Form2->CheckBox10->Checked=Options.IniBackup_AutoCreate;
Form2->CheckBox12->Checked=Options.IniBackup_LimitNumOfBackups;
Form2->CheckBox9->Checked=Options.IniBackup_AutoRestore;
Form2->CheckBox11->Checked=Options.IniBackup_NoConfirmOnRestore;
Form2->SpinEdit1->Value=Options.IniBackup_Count;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button10Click(TObject *Sender)
{
if (ComboBox1->ItemIndex==-1)
 {
   return;
 }
ReadCurrent(ComboBox1->ItemIndex);
if (Options.UseI2C)
 {
  I2C_.ReadSettings(ComboBox1->ItemIndex,true);
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)    //APPLY
{
bool CtrlPressed=GetKeyState(VK_CONTROL) & 0x8000;    //ctrl pressed for multigpu
if (CtrlPressed)
 {
  for (int i = 0; i < GPU.Length; i++)
   {
    if ((GPU[i].APISupported) && (GPU[i].APIVersion==GPU[ComboBox1->ItemIndex].APIVersion))
     SetCurrent(i);
   }
 } else
  {
   SetCurrent(ComboBox1->ItemIndex);
  }

ReadCurrent(ComboBox1->ItemIndex);
if (Options.UseI2C)
 {
  if (CtrlPressed)
   {
    for (int i = 0; i < GPU.Length; i++)
     {
      if ((GPU[i].APISupported) && (GPU[i].APIVersion==GPU[ComboBox1->ItemIndex].APIVersion))
       I2C_.WriteSettings(i, false, I2C_Offset->Value, I2C_LLC->Checked, Edit1->Text,Edit2->Text);
     }
   } else
    {
     I2C_.WriteSettings(ComboBox1->ItemIndex, false, I2C_Offset->Value, I2C_LLC->Checked, Edit1->Text, Edit2->Text);
    }

  Sleep(10);
  I2C_.ReadSettings(ComboBox1->ItemIndex,true);
 }

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
Form1->Close();
}
//---------------------------------------------------------------------------
//Save
void __fastcall TForm1::Button9Click(TObject *Sender)
{
if (ComboBox2->ItemIndex==-1)
 {
  Button6->Click();
 }
if ((Profiles.Length==0) || (ComboBox2->ItemIndex==-1))
{
  return;
}
if (CurrentToProfile(ComboBox2->ItemIndex, ComboBox1->ItemIndex)) //ogranicz zapis do ini
 INI.SaveProfiles();
}
//---------------------------------------------------------------------------
//New
void __fastcall TForm1::Button6Click(TObject *Sender)
{
String str="";
if (!InputQuery("New","New profile name:",str)) return;

str=str.Trim();
if (str=="")
{
    ShowMessage("Profile name cannot be empty");
    return;
}
str=StringReplace(str, "\"", "", TReplaceFlags() << rfReplaceAll << rfIgnoreCase);
Profiles.Length=Profiles.Length+1;
Profiles[Profiles.Length-1].Name=str;
//I2C
Profiles[Profiles.Length-1].Offset=-500;
Profiles[Profiles.Length-1].LLC=-500;
Profiles[Profiles.Length-1].PhaseGain="NULL";
Profiles[Profiles.Length-1].CurrentScale="NULL";
//-------
ComboBox2->Items->Add(str);
ComboBox2->ItemIndex=Profiles.Length-1;

Button9->Click();
}
//---------------------------------------------------------------------------
//Load
void __fastcall TForm1::Button8Click(TObject *Sender)
{
if ((ComboBox2->ItemIndex==-1) || (Profiles.Length==0) || (ComboBox1->ItemIndex==-1))
 {
  return;
 }
ProfileToCurrent(ComboBox2->ItemIndex, ComboBox1->ItemIndex);
}
//---------------------------------------------------------------------------
//Rename
void __fastcall TForm1::Button5Click(TObject *Sender)
{
if ((ComboBox2->ItemIndex==-1) || (Profiles.Length==0))
 {
  return;
 }

String str=Profiles[ComboBox2->ItemIndex].Name;

if (!InputQuery("Rename","New name:",str)) return;

str=str.Trim();
if (str=="")
{
    ShowMessage("Profile name cannot be empty");
    return;
}
int i;
i=ComboBox2->ItemIndex;
if (Profiles[i].Name==str)
 return;    //ogranicz zapis do ini
ComboBox2->Items->Strings[i]=str;
Profiles[i].Name=str;
ComboBox2->ItemIndex=i;

INI.SaveProfiles();

}
//---------------------------------------------------------------------------
//Delete
void __fastcall TForm1::Button7Click(TObject *Sender)
{
if ((ComboBox2->ItemIndex==-1) || (Profiles.Length==0))
 {
  return;
 }

String str="Are you sure to delete profile: "+ Profiles[ComboBox2->ItemIndex].Name+"?";
if (MessageBox(0, str.w_str(), L"Delete", MB_ICONQUESTION|MB_YESNO)==7) return;


int m=ComboBox2->ItemIndex;
for (int i = m; i < Profiles.Length-1; i++)
    {
     Profiles[i]=Profiles[i+1];
    }

Profiles.Length--;

ComboBox2->Items->Delete(m);

ComboBox2->ItemIndex=m-1;
if ((ComboBox2->ItemIndex==-1) && (Profiles.Length>0))
 {
  ComboBox2->ItemIndex=0;
 }
ComboBox2->Refresh();

INI.SaveProfiles();

}                         //  dotad
//---------------------------------------------------------------------------
//typ - 0 pstates, 1 fan+ power + I2C
void __fastcall TForm1::ValueChange(Byte typ, bool enabled,int value, TSpinEdit* s)
{
if (typ==0)
 {
  if ((value!=s->Value) || (enabled!=s->Enabled))
   {
    s->Color=ChangedColor;
   } else
      s->Color=clWindow;
 } else
  {
    if (value!=s->Value)
     {
      s->Color=ChangedColor;
     } else
         s->Color=clWindow;
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_7Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[7], CurrentValues.GPU_Clock[7],GPU_Clock_7);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_6Change(TObject *Sender)
{

ValueChange(0,CurrentValues.GPU_Enabled[6],CurrentValues.GPU_Clock[6],GPU_Clock_6);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_5Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[5],CurrentValues.GPU_Clock[5],GPU_Clock_5);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_4Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[4],CurrentValues.GPU_Clock[4],GPU_Clock_4);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_3Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[3],CurrentValues.GPU_Clock[3],GPU_Clock_3);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_2Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[2],CurrentValues.GPU_Clock[2],GPU_Clock_2);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_1Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[1],CurrentValues.GPU_Clock[1],GPU_Clock_1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Clock_0Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[0], CurrentValues.GPU_Clock[0],GPU_Clock_0);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::GPU_Vcc_0Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[0], CurrentValues.GPU_Vcc[0],GPU_Vcc_0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_1Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[1],CurrentValues.GPU_Vcc[1],GPU_Vcc_1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_2Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[2],CurrentValues.GPU_Vcc[2],GPU_Vcc_2);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_3Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[3],CurrentValues.GPU_Vcc[3],GPU_Vcc_3);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_4Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[4],CurrentValues.GPU_Vcc[4],GPU_Vcc_4);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_5Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[5],CurrentValues.GPU_Vcc[5],GPU_Vcc_5);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_6Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[6],CurrentValues.GPU_Vcc[6],GPU_Vcc_6);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_Vcc_7Change(TObject *Sender)
{
ValueChange(0,CurrentValues.GPU_Enabled[7],CurrentValues.GPU_Vcc[7],GPU_Vcc_7);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_0Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[0],CurrentValues.Mem_Clock[0],Mem_Clock_0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_1Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[1],CurrentValues.Mem_Clock[1],Mem_Clock_1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_2Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[2],CurrentValues.Mem_Clock[2],Mem_Clock_2);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_3Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[3],CurrentValues.Mem_Clock[3],Mem_Clock_3);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_4Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[4],CurrentValues.Mem_Clock[4],Mem_Clock_4);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_5Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[5],CurrentValues.Mem_Clock[5],Mem_Clock_5);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_6Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[6],CurrentValues.Mem_Clock[6],Mem_Clock_6);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Clock_7Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[7],CurrentValues.Mem_Clock[7],Mem_Clock_7);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_0Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[0],CurrentValues.Mem_Vcc[0],Mem_Vcc_0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_1Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[1],CurrentValues.Mem_Vcc[1],Mem_Vcc_1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_2Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[2],CurrentValues.Mem_Vcc[2],Mem_Vcc_2);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_3Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[3],CurrentValues.Mem_Vcc[3],Mem_Vcc_3);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_4Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[4],CurrentValues.Mem_Vcc[4],Mem_Vcc_4);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_5Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[5],CurrentValues.Mem_Vcc[5],Mem_Vcc_5);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_6Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[6],CurrentValues.Mem_Vcc[6],Mem_Vcc_6);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_Vcc_7Change(TObject *Sender)
{
ValueChange(0,CurrentValues.Mem_Enabled[7],CurrentValues.Mem_Vcc[7],Mem_Vcc_7);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_MinChange(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1,false,CurrentValues.Fan_Min,Fan_Min);
 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_MaxChange(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1,false,CurrentValues.Fan_Max,Fan_Max);
 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_TargetChange(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1,false,CurrentValues.Fan_Target,Fan_Target);
 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_AcousticChange(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1,false,CurrentValues.Fan_Acoustic,Fan_Acoustic);
 }

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Power_TempChange(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1,false,CurrentValues.Power_Temp,Power_Temp);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Power_TargetChange(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1,false,CurrentValues.Power_Target,Power_Target);
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Fan_Temp_0Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[0].Temp, Fan_Temp_0);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Temp_1Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[1].Temp, Fan_Temp_1);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Temp_2Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[2].Temp, Fan_Temp_2);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Temp_3Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[3].Temp, Fan_Temp_3);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Temp_4Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[4].Temp, Fan_Temp_4);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Perc_0Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[0].Percentage, Fan_Perc_0);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Perc_1Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[1].Percentage, Fan_Perc_1);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Perc_2Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[2].Percentage, Fan_Perc_2);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Perc_3Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[3].Percentage, Fan_Perc_3);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Fan_Perc_4Change(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].FanEnabled)
 {
  ValueChange(1, false , CurrentValues.FanCurvePoints[4].Percentage, Fan_Perc_4);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
//ew zapisujemy po�o�enie formy gdy si� zmieni�o
if ((StartupPosLeft!=Form1->Left) || (StartupPosTop!=Form1->Top))
 INI.SaveMainFormPosition();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::EnableDisablePState(bool typ, TLabel* L,TSpinEdit* S1, TSpinEdit* S2)
{
if (typ==false)    //disable
 {
   S1->Enabled=false;
   S2->Enabled=false;
   L->Font->Color=clGrayText;
 } else
   {
     S1->Enabled=true;
     S2->Enabled=true;
     L->Font->Color=clWindowText;
   }
}
//---------------------------------------------------------------------------
int I2C_::RegisterRead(int GpuInd, int iSize, int iLine, int iAddress, int iOffset)
{
  ADLI2C i2c;
  i2c.iSpeed = 5;
  i2c.iAction = ADL_DL_I2C_ACTIONREAD_REPEATEDSTART;

  i2c.iLine = iLine;
  i2c.iAddress = iAddress << 1;

  i2c.iOffset = iOffset;

  i2c.iDataSize = iSize;
  if (iSize==1)
    {
      char pdata[1]={0x00};
      i2c.pcData=pdata;
    } else
      {
       char pdata[2]={0x00,0x00};
       i2c.pcData=pdata;
      }
 i2c.iSize = sizeof(ADLI2C);

 if (ADL_OK==ADL2_Display_WriteAndReadI2C(context, GPU[GpuInd].AdIndexes[0], &i2c))
  {
    if (iSize==1)
      {
        return Byte(i2c.pcData[0]);
      } else
        {
         return (((Byte(i2c.pcData[1])) << 8) | Byte(i2c.pcData[0]));
        }
  } else
   {
     return -1;
   }
}
//---------------------------------------------------------------------------
int I2C_::RegisterWrite(int GpuInd, int iSize, int iLine, int iAddress, int iOffset, int Value)
{
  ADLI2C i2c;
  i2c.iSpeed = 5;
  i2c.iAction = ADL_DL_I2C_ACTIONWRITE;

  i2c.iLine = iLine;
  i2c.iAddress = iAddress << 1;

  i2c.iOffset = iOffset;

  i2c.iDataSize = iSize;
  if (iSize==1)
    {
     char pdata[1];
     pdata[0]=Value;
     i2c.pcData=pdata;
    } else
      {
       char pdata[2];
       pdata[0]=(Value >> (8*0)) & 0xFF;
       pdata[1]=(Value >> (8*1)) & 0xFF;
       i2c.pcData=pdata;
      }

 i2c.iSize = sizeof(ADLI2C);
 return ADL2_Display_WriteAndReadI2C(context, GPU[GpuInd].AdIndexes[0], &i2c);

}
//---------------------------------------------------------------------------
int INI::LoadI2CSettings()
{
if (FileExists(inifile))
 {
  TIniFile* ini = new TIniFile(inifile);
  try
   {
     for (int i = 0; i < GPU.Length; i++)
     {
       int k=ini->ReadInteger("I2C",IntToStr(i),0);
       if (k!=0)
        {
          GPU[i].I2C.Line=(k>>8) & 0xFF;
          GPU[i].I2C.Address=(k>>16) & 0xFF;
          GPU[i].I2C.typ=(k>>24) & 0xFF;
        } else
          {
            GPU[i].I2C.Line=-1;
            GPU[i].I2C.Address=-1;
            GPU[i].I2C.typ=-1;
          }
     }
   }
   __finally
    {
      delete ini;
    }
 }

return 0;
}

//---------------------------------------------------------------------------
int INI::SaveI2CSettings()
{
if (FileExists(inifile))
 {
  TMemIniFile* ini = new TMemIniFile(inifile, TEncoding::Unicode);
  try
   {
    for (int i = 0; i < GPU.Length; i++)
     {
      int k=0;
      if ((GPU[i].I2C.Line!=-1) && (GPU[i].I2C.Address!=-1)) //je�eli znaleziono adresy
       {
        k=k ^ GPU[i].I2C.Line<<8; //8*1
        k=k ^ GPU[i].I2C.Address<<16; //8*2
        k=k ^ GPU[i].I2C.typ<<24;   //8*3
       }
      ini->WriteString("I2C",IntToStr(i),k);
     }

   ini->UpdateFile();
   INI::FlushFileBuffer(inifile);
   }
  __finally
     {
       delete ini;
     }
 }

return 0;
}

//---------------------------------------------------------------------------
bool I2C_::FindDevice(int GpuInd, int Line, int Address, int typ)
{
int answ=RegisterRead(GpuInd, I2cDB[typ].IdentSize, Line, Address, I2cDB[typ].IdentOffset1);
if (answ!=-1)
 {
  if  (answ==I2cDB[typ].IdentData1)
   {
    if (I2cDB[typ].IdentOffset2!=NULL)
     {
      int answ=RegisterRead(GpuInd, I2cDB[typ].IdentSize, Line, Address, I2cDB[typ].IdentOffset2);
      if (answ!=-1)
       {
         if (I2cDB[typ].IdentData21!=NULL)
          {
            if  ((answ==I2cDB[typ].IdentData2) || (answ==I2cDB[typ].IdentData21))
             {
              return true;
             } 
          } else
            {
              if (answ==I2cDB[typ].IdentData2)
               {
                 return true;
               }
            }
       }
     } else
        {
          return true;
        }
   }
 }
return false;
}

//---------------------------------------------------------------------------
void I2C_::Scan(bool FullScan)
{
if (InitializeADL())
 {
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if (FullScan==false)   //tylko ini
    {
     INI.LoadI2CSettings();
     for (int i = 0; i < GPU.Length; i++)
      {
       if (GPU[i].APISupported==false)
        {
          continue;
        }
       if ((GPU[i].I2C.Line!=-1) && (GPU[i].I2C.Address!=-1))   // mamy adresy z ini
        {
          if (FindDevice(i, GPU[i].I2C.Line, GPU[i].I2C.Address, GPU[i].I2C.typ))
            {
               GPU[i].I2C.devicefound=true;
               continue;
            } else
              {
                GPU[i].I2C.Line=-1;
                GPU[i].I2C.Address=-1;
                GPU[i].I2C.typ=-1;
                GPU[i].I2C.devicefound=false;
              }
        }
      }
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    } else         //Pe�ny skan
     {
       for (int i = 0; i < GPU.Length; i++)
        {
         if (GPU[i].APISupported==false)
          {
            continue;
          }

         //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
         //fixed addresses
         bool found=false;
         for ( int k = 0; k < I2cDBLength; k++)
          {
           if (I2cDB[k].FixedLine!=NULL)
             {
              if (FindDevice(i, I2cDB[k].FixedLine, I2cDB[k].FixedAddress, k))
               {
                 found=true;
                 GPU[i].I2C.Line=I2cDB[k].FixedLine;
                 GPU[i].I2C.Address=I2cDB[k].FixedAddress;
                 GPU[i].I2C.typ=k;
                 GPU[i].I2C.devicefound=true;
                 break;
               }
             }
          }
         if (found) continue;
         //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
         //all addresses
         for (int m = 1; m < 7; m++)  // po ka�dej linii        
          {
           if (found) break;
           if (m==2) continue;
           for ( int j = 0; j < 0x7F; j++)     //po ka�dym adresie
             {
               // pierwszy bajt odczytany musi mie� offset 0x01 lub wi�kszy inaczej I2C si� zawiesza przez inne urz�dzenia
               if (RegisterRead(i, 1, m, j, 0xFF) != -1)     //znaleziono jakie� urz�dzenie
                {
                 //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                 for ( int k = 0; k < I2cDBLength; k++)     //po ka�dym typie
                  {
                    if (I2cDB[k].FixedLine==NULL)
                     {
                       if (FindDevice(i, m, j, k))
                        {
                          found=true;
                   	  GPU[i].I2C.Line=m;
                   	  GPU[i].I2C.Address=j;
                   	  GPU[i].I2C.typ=k;
                   	  GPU[i].I2C.devicefound=true;
                   	  break;
                        }
                     }
                  }
                 if (found) break;
                 //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                }
             }
          }
         //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        }
       INI.SaveI2CSettings();
     }
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  DeinitializeADL();
 }

Linescanning=false;
Scanned=true;

if (UsingCommandLine==false)
 {
  Form1->ShowHideI2CControls(Form1->ComboBox1->ItemIndex);
  if (GPU[Form1->ComboBox1->ItemIndex].I2C.devicefound==true)
    {
      ReadSettings(Form1->ComboBox1->ItemIndex,true);
    } else
     {
      CurrentValues.Offset=-500;
      CurrentValues.LLC=-500;
      CurrentValues.PhaseGain="NULL";
      CurrentValues.CurrentScale="NULL";
     }
  Odnt.ProfilesCompareAtStartup();
 }

return;
}
//---------------------------------------------------------------------------
byte I2C_::ConvertVoltageWrite(int GpuInd, int value)
{
if (I2cDB[GPU[GpuInd].I2C.typ].ConvertVoltage)
 {
   if (value>=0)          //liczba dodatnia
    {
       return value;
    } else                //liczba ujemna
     {
       return Byte(abs(value) | 0x80);
     }

 } else
   {
      return value;
   }
}           
//---------------------------------------------------------------------------
int I2C_::ConvertVoltageRead(int GpuInd, int value)
{
if (I2cDB[GPU[GpuInd].I2C.typ].ConvertVoltage)
 {
  if ((value>>7)==0)         //liczba dodatnia
    {
       return value;
    } else                  //liczba ujemna
     {
       return (-(Byte(value) ^ 0x80));
     }
 } else
   {
     if (value<0x80)         //liczba dodatnia
      {
        return value;
      } else                  //liczba ujemna
       {
         return (value | 0xFFFFFF00);
       }
   }
}
//---------------------------------------------------------------------------
void I2C_::WriteSettings(int GpuInd, bool reset,int offset,int llc, String phasegain, String CurrentScale)
{
if (GPU[GpuInd].I2C.devicefound==false)
 {
    return;  // wychodzimy gdy niesupportowane
 }
if (InitializeADL())
 {
   ADLI2C i2c;
   i2c.iAction=ADL_DL_I2C_ACTIONWRITE;
   i2c.iSpeed=10;
   i2c.pcData=new char[1];
   i2c.iDataSize=1;
   i2c.iSize=sizeof(ADLI2C);
   i2c.iLine=GPU[GpuInd].I2C.Line;
   i2c.iAddress=GPU[GpuInd].I2C.Address << 1;
   int an;
   //=====================================================
   //Voltage offset
   if (offset!=-500)
    {
     if ((offset>=-48) && (offset<=48))
      {
        if (I2cDB[GPU[GpuInd].I2C.typ].VoltageAddRegOffset!=NULL)
         {
          i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].VoltageAddRegOffset;
          *i2c.pcData=I2cDB[GPU[GpuInd].I2C.typ].VoltageAddRegData;
          an=ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c);
          if (ADL_OK!=an)
           blad("Failed to set I2C Offset", an, false);
         }
        i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].VoltageOffset1;
        if (reset)
         *i2c.pcData=0; else
           *i2c.pcData=ConvertVoltageWrite(GpuInd, offset);
        an=ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c);
        if (ADL_OK!=an)
         blad("Failed to set I2C Offset", an, false);
      } else
       blad("For security reason I2C offset must be between -48 (-300mV) and 48 (+300mV)", NULL, false);
    }
   //=====================================================
   //LLC
   if (I2cDB[GPU[GpuInd].I2C.typ].LLC_Enabled)
    {
      if (llc!=-500)
       {
        i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].LLCOffset;
        if (reset)  
         {
          *i2c.pcData=I2cDB[GPU[GpuInd].I2C.typ].LLCdataOFF;
         } else
           {
            if (llc)
             *i2c.pcData=I2cDB[GPU[GpuInd].I2C.typ].LLCdataON; else
               *i2c.pcData=I2cDB[GPU[GpuInd].I2C.typ].LLCdataOFF;
           }
        an=ADL2_Display_WriteAndReadI2C(context, GPU[GpuInd].AdIndexes[0], &i2c);
        if (ADL_OK!=an)
         {
           blad("Failed to set I2C LLC", an, false);
         }
       }
    }
   //=====================================================
   //Phase gain
   if (I2cDB[GPU[GpuInd].I2C.typ].Phase_Enabled)
    {
     if (phasegain!="NULL")
      {
       if (phasegain.Length()!=6)
        {
          blad("Wrong data is entered in Phase gain, must be in 3 bytes length hexadecimal format", NULL, false);
        } else
         {
           if (reset==false)         //nie resetujemy tego bo jest r�ne na r�nych maszynach
            {
              i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].PhaseGainOffset1;         //bajt nr 1
              *i2c.pcData=StrToInt("0x"+phasegain.SubString(1,2));
              an=ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c);
              if (ADL_OK==an)
               {
                i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].PhaseGainOffset2;       //bajt nr 2
                *i2c.pcData=StrToInt("0x"+phasegain.SubString(3,2));
                an=ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c);
                if (ADL_OK==an)
                  {
                    i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].PhaseGainOffset3;   //bajt nr 3
                    *i2c.pcData=StrToInt("0x"+phasegain.SubString(5,2));
                    an=ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c);
                    if (ADL_OK!=an)
                     {
                       blad("Failed to set I2C Phase Gain", an, false);
                     }
                  } else blad("Failed to set I2C Phase Gain", an, false);
               } else blad("Failed to set I2C Phase Gain", an, false);
            }
         }
      }
    }
   //=====================================================
   //Current Scale
   if (I2cDB[GPU[GpuInd].I2C.typ].Scale_Enabled)
    {
     if (CurrentScale!="NULL")
      {
       if (CurrentScale.Length()!=2)
        {
          blad("Wrong data is entered in Current scale, must be in 1 byte hexadecimal format",NULL,false);
        } else
         {
          if (reset==false)         //nie resetujemy tego bo jest r�ne na r�nych maszynach
           {
             i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].CurrentScaleOffset;
             *i2c.pcData=StrToInt("0x"+CurrentScale);
             an=ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c);
             if (ADL_OK!=an)
              {
                blad("Failed to set I2C Current Scale", an, false);
              }
           }
         }
      }
    }
   delete[] i2c.pcData;
   //=====================================================
   DeinitializeADL();
 }
return;
}
//---------------------------------------------------------------------------
void I2C_::ReadSettings(int GpuInd,bool ShowOnForm)
{
if (GPU[GpuInd].I2C.devicefound==false) return;  // wychodzimy gdy niesupportowane
if (InitializeADL())
 {
    ADLI2C i2c;
    i2c.iAction=ADL_DL_I2C_ACTIONREAD_REPEATEDSTART;
    i2c.iSpeed=15;

    char pdata[1]={0x00};
    i2c.iDataSize=1;
    i2c.pcData=pdata;
    i2c.iSize=sizeof(ADLI2C);
    i2c.iLine=GPU[GpuInd].I2C.Line;
    i2c.iAddress=GPU[GpuInd].I2C.Address << 1;

    //=====================================================
    //Voltage offset
    bool readoffset=true;
    if (I2cDB[GPU[GpuInd].I2C.typ].VoltageAddRegOffset!=NULL)
     {
       i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].VoltageAddRegOffset;
       if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
        {
           if (i2c.pcData[0]!=I2cDB[GPU[GpuInd].I2C.typ].VoltageAddRegData)
            {
              readoffset=false;
              CurrentValues.Offset=0;
              if (ShowOnForm)
                {
                  Form1->I2C_Offset->Value=0;
                  Form1->Label4->Caption="0mV";
                }
            }
        } else
          {
            GPU[GpuInd].I2C.devicefound=false;
            CurrentValues.Offset=-500;
          }

     }
    if (readoffset)
      {
        i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].VoltageOffset1;
        if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
         {
          int k=ConvertVoltageRead(GpuInd, i2c.pcData[0]);
          if (ShowOnForm)
           {
            Form1->I2C_Offset->Value=k;
            if (k>0)
             Form1->Label4->Caption="+"+FloatToStr(k*6.25)+"mV"; else
              Form1->Label4->Caption=FloatToStr(k*6.25)+"mV";
           }
        CurrentValues.Offset=k;
        }
         else
          {
           GPU[GpuInd].I2C.devicefound=false;
           CurrentValues.Offset=-500;
          }
      }
    if (ShowOnForm)
     {
       Form1->I2C_Offset->Color=clWindow;
     }
    //=====================================================
    //LLC
    if (I2cDB[GPU[GpuInd].I2C.typ].LLC_Enabled)
     {
      i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].LLCOffset;
      if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
       {
        int k=Byte(i2c.pcData[0]);     //funkcja Byte zamienia signed na unsigned char

        if (k==I2cDB[GPU[GpuInd].I2C.typ].LLCdataOFF)
         {
          i2c.pcData=pdata;
          if (ShowOnForm) Form1->I2C_LLC->Checked=false;
          CurrentValues.LLC=0;
         } else
          if (k==I2cDB[GPU[GpuInd].I2C.typ].LLCdataON)
           {
             if (ShowOnForm) Form1->I2C_LLC->Checked=true;
             CurrentValues.LLC=1;
           } else
              blad("I2C LLC has wrong value=0x"+IntToHex(k,2), NULL, false);
       }
       else
         {
          GPU[GpuInd].I2C.devicefound=false;
          CurrentValues.LLC=-500;
         }
       if (ShowOnForm) Form1->I2C_LLC->Color=clBtnFace;
     } else
      {
        CurrentValues.LLC=-500;
      }
    //=====================================================
    //Phase gain
    if (I2cDB[GPU[GpuInd].I2C.typ].Phase_Enabled)
     {
      i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].PhaseGainOffset1;
      i2c.pcData=pdata;
      AnsiString str;
      AnsiString s;
      if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
       {
         str=IntToHex(Byte(i2c.pcData[0]),2);
         i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].PhaseGainOffset2;
         if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
          {
           s=IntToHex(Byte(i2c.pcData[0]),2);
           str=str+s;
           i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].PhaseGainOffset3;
           if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
            {
              s=IntToHex(Byte(i2c.pcData[0]),2);
              str=str+s;
              CurrentValues.PhaseGain=str;
              if (ShowOnForm) Form1->Edit1->Text=str;
            } else
              {
                GPU[GpuInd].I2C.devicefound=false;
                CurrentValues.PhaseGain="NULL";
              }
          } else
           {
             GPU[GpuInd].I2C.devicefound=false;
             CurrentValues.PhaseGain="NULL";
           }
       } else
           {
             GPU[GpuInd].I2C.devicefound=false;
             CurrentValues.PhaseGain="NULL";
           }
      if (ShowOnForm) Form1->Edit1->Color=clWindow;
     } else
       {
        CurrentValues.PhaseGain="NULL";
       }
    //=====================================================
    //CurrentScale
    if (I2cDB[GPU[GpuInd].I2C.typ].Scale_Enabled)
     {
      i2c.iOffset=I2cDB[GPU[GpuInd].I2C.typ].CurrentScaleOffset;
      if (ADL_OK==ADL2_Display_WriteAndReadI2C (context, GPU[GpuInd].AdIndexes[0], &i2c))
       {
        AnsiString s=IntToHex(Byte(i2c.pcData[0]),2);
        CurrentValues.CurrentScale=s;
        if (ShowOnForm) Form1->Edit2->Text=s;
       } else
          {
            GPU[GpuInd].I2C.devicefound=false;
            CurrentValues.CurrentScale="NULL";
          }
      if (ShowOnForm) Form1->Edit2->Color=clWindow;
     } else
       {
         CurrentValues.CurrentScale="NULL";
       }
    //=====================================================
    DeinitializeADL();
 }
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::I2C_OffsetChange(TObject *Sender)
{

ValueChange(1,true,CurrentValues.Offset,I2C_Offset);
if (I2C_Offset->Value>0)
 Form1->Label4->Caption="+"+FloatToStr(I2C_Offset->Value*6.25)+"mV"; else
  Form1->Label4->Caption=FloatToStr(I2C_Offset->Value*6.25)+"mV";

}
//---------------------------------------------------------------------------

void __fastcall TForm1::I2C_LLCClick(TObject *Sender)
{
if (I2C_LLC->Checked)
 {
    if (CurrentValues.LLC==0)
     {
      I2C_LLC->Color=ChangedColor;
     } else
         I2C_LLC->Color=clBtnFace;

   Label7->Caption="ON";
 }
  else
   {
    if (CurrentValues.LLC==1)
     {
      I2C_LLC->Color=ChangedColor;
     } else
         I2C_LLC->Color=clBtnFace;


    Label7->Caption="OFF";
   }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::GPU_P0Click(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].APIVersion==7)
 {
  EnableDisablePState(!GPU_Clock_0->Enabled,GPU_P0,GPU_Clock_0,GPU_Vcc_0);
  GPU_Clock_0Change(Sender);
  GPU_Vcc_0Change(Sender);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_P1Click(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].APIVersion==7)
 {
  EnableDisablePState(!GPU_Clock_1->Enabled,GPU_P1,GPU_Clock_1,GPU_Vcc_1);
  GPU_Clock_1Change(Sender);
  GPU_Vcc_1Change(Sender);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_P2Click(TObject *Sender)
{
if (GPU[ComboBox1->ItemIndex].APIVersion==7)
 {
  EnableDisablePState(!GPU_Clock_2->Enabled,GPU_P2,GPU_Clock_2,GPU_Vcc_2);
  GPU_Clock_2Change(Sender);
  GPU_Vcc_2Change(Sender);
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_P3Click(TObject *Sender)
{
EnableDisablePState(!GPU_Clock_3->Enabled,GPU_P3,GPU_Clock_3,GPU_Vcc_3);
GPU_Clock_3Change(Sender);
GPU_Vcc_3Change(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::GPU_P4Click(TObject *Sender)
{
EnableDisablePState(!GPU_Clock_4->Enabled,GPU_P4,GPU_Clock_4,GPU_Vcc_4);
GPU_Clock_4Change(Sender);
GPU_Vcc_4Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_P5Click(TObject *Sender)
{
EnableDisablePState(!GPU_Clock_5->Enabled,GPU_P5,GPU_Clock_5,GPU_Vcc_5);
GPU_Clock_5Change(Sender);
GPU_Vcc_5Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_P6Click(TObject *Sender)
{
EnableDisablePState(!GPU_Clock_6->Enabled,GPU_P6,GPU_Clock_6,GPU_Vcc_6);
GPU_Clock_6Change(Sender);
GPU_Vcc_6Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_P7Click(TObject *Sender)
{
EnableDisablePState(!GPU_Clock_7->Enabled,GPU_P7,GPU_Clock_7,GPU_Vcc_7);
GPU_Clock_7Change(Sender);
GPU_Vcc_7Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P0Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_0->Enabled,Mem_P0,Mem_Clock_0,Mem_Vcc_0);
Mem_Clock_0Change(Sender);
Mem_Vcc_0Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P1Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_1->Enabled,Mem_P1,Mem_Clock_1,Mem_Vcc_1);
Mem_Clock_1Change(Sender);
Mem_Vcc_1Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P2Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_2->Enabled,Mem_P2,Mem_Clock_2,Mem_Vcc_2);
Mem_Clock_2Change(Sender);
Mem_Vcc_2Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P3Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_3->Enabled,Mem_P3,Mem_Clock_3,Mem_Vcc_3);
Mem_Clock_3Change(Sender);
Mem_Vcc_3Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P4Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_4->Enabled,Mem_P4,Mem_Clock_4,Mem_Vcc_4);
Mem_Clock_4Change(Sender);
Mem_Vcc_4Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P5Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_5->Enabled,Mem_P5,Mem_Clock_5,Mem_Vcc_5);
Mem_Clock_5Change(Sender);
Mem_Vcc_5Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P6Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_6->Enabled,Mem_P6,Mem_Clock_6,Mem_Vcc_6);
Mem_Clock_6Change(Sender);
Mem_Vcc_6Change(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_P7Click(TObject *Sender)
{
EnableDisablePState(!Mem_Clock_7->Enabled,Mem_P7,Mem_Clock_7,Mem_Vcc_7);
Mem_Clock_7Change(Sender);
Mem_Vcc_7Change(Sender);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1KeyPress(TObject *Sender, System::WideChar &Key)
{
const String str="0123456789abcdefABCDEF";
if ((Pos(Key,str)>0))
 {
   Key=toupper(Key);
 } else
  if (Key!=VK_BACK)
    {
      Key=0;
    }
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit2KeyPress(TObject *Sender, System::WideChar &Key)
{
const String str="0123456789abcdefABCDEF";
if (Pos(Key,str)>0)
 {
  Key=toupper(Key);
 } else
  if (Key!=VK_BACK)
    {
      Key=0;
    }
return;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Edit1Change(TObject *Sender)
{

if (CurrentValues.PhaseGain!=Edit1->Text)
  {
   Edit1->Color=ChangedColor;

  } else
   {
     Edit1->Color=clWindow;
   }
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Edit2Change(TObject *Sender)
{

if (CurrentValues.CurrentScale!=Edit2->Text)
  {
   Edit2->Color=ChangedColor;

  } else
   {
     Edit2->Color=clWindow;
   }
return;
}

//---------------------------------------------------------------------------
void Odnt::ChangeGPUDescription(bool FromSettings)
{
if ((Form1->ComboBox1->ItemIndex==-1) || (GPU.Length==0))
  {
    return;
  }

if (FromSettings)
 {
    Options.BusNumber=Form2->CheckBox3->Checked;
    Options.PNPString=Form2->CheckBox1->Checked;
    Options.Adapter=Form2->CheckBox4->Checked;
    Options.FriendlyName=Form2->CheckBox6->Checked;
    Options.RegistryKey=Form2->CheckBox7->Checked;
    Options.IgnoreError8=Form2->CheckBox8->Checked;
    Options.CheckValuesMismatch=Form2->CheckBox14->Checked;
    Options.AutoReset=Form2->CheckBox15->Checked;

    Options.IniBackup_AutoCreate=Form2->CheckBox10->Checked;
    Options.IniBackup_LimitNumOfBackups=Form2->CheckBox12->Checked;
    Options.IniBackup_AutoRestore=Form2->CheckBox9->Checked;
    Options.IniBackup_NoConfirmOnRestore=Form2->CheckBox11->Checked;
    Options.IniBackup_Count=Form2->SpinEdit1->Value;
 }
bool frname=Options.FriendlyName;

if (frname)
 {
  if (Odnt::ReadGPUFriendlyName(true)==false)
   {
     frname=false;
   }
 }
if (frname)
 {
  Form1->ChangeFriendlyName1->Visible=true;
 } else
   {
    Form1->ChangeFriendlyName1->Visible=false;
   }

int i=Form1->ComboBox1->ItemIndex;
Form1->ComboBox1->Clear();
String str;
for (int i = 0; i < GPU.Length; i++)
  {
    GPU[i].DisplayName=FormatGPUDisplayName(i, (GPU[i].FriendlyName!="") && Options.FriendlyName);
    Form1->ComboBox1->Items->Add(GPU[i].DisplayName);
  }
Form1->ComboBox1->ItemIndex=i;

//szeroko�� itemu w dropliscie
//SendMessage(Form1->ComboBox1->Handle, CB_SETDROPPEDWIDTH, 590, 0);       
return;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::EnableDisableFanSection(bool enabled)
{
if (enabled)
 {
   GroupBox3->Font->Color=clWindowText;
   Fan_Min->Enabled=true;
   Fan_Max->Enabled=true;
   Fan_Target->Enabled=true;
   Fan_Acoustic->Enabled=true;
   Fan_Min->Color=clWindow;
   Fan_Max->Color=clWindow;
   Fan_Target->Color=clWindow;
   Fan_Acoustic->Color=clWindow;
   ZeroRPMBox->Enabled=true;
 } else
   {
     GroupBox3->Font->Color=clGrayText;
     Fan_Min->Value=CurrentValues.Fan_Min;
     Fan_Max->Value=CurrentValues.Fan_Max;
     Fan_Target->Value=CurrentValues.Fan_Target;
     Fan_Acoustic->Value=CurrentValues.Fan_Acoustic;
     Fan_Min->Enabled=false;
     Fan_Max->Enabled=false;
     Fan_Target->Enabled=false;
     Fan_Acoustic->Enabled=false;
     Fan_Min->Color=clInactiveCaption;
     Fan_Max->Color=clInactiveCaption;
     Fan_Target->Color=clInactiveCaption;
     Fan_Acoustic->Color=clInactiveCaption;
     ZeroRPMBox->Enabled=false;
     ZeroRPMBox->Color=clBtnFace;
   }

for (int i = 0; i < 5; i++)
 {
  TSpinEdit* Spin1=(TSpinEdit*)Form1->FindComponent("Fan_Temp_"+IntToStr(i));
  TSpinEdit* Spin2=(TSpinEdit*)Form1->FindComponent("Fan_Perc_"+IntToStr(i));

  Spin1->Enabled=enabled;
  Spin2->Enabled=enabled;
  if (enabled)
   {
    Spin1->Color=clWindow;
    Spin2->Color=clWindow;
   } else
    {
     Spin1->Color=clInactiveCaption;
     Spin2->Color=clInactiveCaption;
    }
 }

return;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::GroupBox3DblClick(TObject *Sender)
{
//Wymaga wci�ni�tego przycisku Ctrl
if ((GetKeyState(VK_CONTROL) & 0x8000)==false)
{
    return;
}
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
if (GPU[ComboBox1->ItemIndex].FanEnabled)
  {
    EnableDisableFanSection(false);
    GPU[ComboBox1->ItemIndex].FanEnabled=false;
  } else
     {
       EnableDisableFanSection(true);
       GPU[ComboBox1->ItemIndex].FanEnabled=true;
     }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//zapis do ini tylko tutaj
INI.SaveFanDeactivated();
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
return;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::OpenPowerPlayEditor()
{
if (Form3->Visible) return;

if (ComboBox1->ItemIndex==-1)   //brak kart, by� mo�e niezainstalowany sterownik
 {
   InitializePowerPlay("{4d36e968-e325-11ce-bfc1-08002be10318}\\0000", "none", 0, 99999, "");
   Form3->Show();
   return;
 }

if (GPU[ComboBox1->ItemIndex].APISupported==false)
 {
   InitializePowerPlay(GPU[ComboBox1->ItemIndex].DriverPath, ComboBox1->Items->Strings[ComboBox1->ItemIndex],0, 99999, GPU[ComboBox1->ItemIndex].DevID);
   Form3->Show();
   return;
 }


//je�eli s� jakie� zera b�d� mniejsze ni� aktualne to ignorujemy warto�ci

int MinVoltage=GPU[ComboBox1->ItemIndex].Limits.MinVoltage;
int MaxVoltage=GPU[ComboBox1->ItemIndex].Limits.MaxVoltage;

if (MinVoltage<600)
 MinVoltage=600;
if (MaxVoltage<1000)
 MaxVoltage=1000;

InitializePowerPlay(GPU[ComboBox1->ItemIndex].DriverPath, ComboBox1->Items->Strings[ComboBox1->ItemIndex],MinVoltage,MaxVoltage,GPU[ComboBox1->ItemIndex].DevID);
Form3->Show();
return;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ChangeFriendlyName1Click(TObject *Sender)
{
if ((ComboBox1->ItemIndex==-1) || (GPU.Length==0)) return;

if (Odnt.CheckIfAdmin()==false)
 {
    blad("Administrator rights are required to change friendly name", NULL, false);
    return;
 }

bool SingleGPU=false;
if (GPU.Length==1)
 {
   SingleGPU=true;
 } else
  {
    if (GPU.Length==2)
     {
      if ((GPU[0].APISupported==false) || (GPU[1].APISupported==false))
       {
        SingleGPU=true;
       }
     }
  }

if (SingleGPU)    //SingleGPU
 {
   String str=GPU[ComboBox1->ItemIndex].FriendlyName;
   if (!InputQuery("Change friendly name","(empty will delete it)", str)) return;
   str=str.Trim();
   if (Odnt.ChangeGPUFriendlyName(ComboBox1->ItemIndex, str)==false)
    {
      blad("Failed to change friendly name", NULL, false);
    } else
      {
        GPU[ComboBox1->ItemIndex].FriendlyName=str;
        Odnt.ChangeGPUDescription(false);
      }

 } else      //MultiGPU
   {
     if (Form1->Form4_Created==false)     //dynamicznie kreowana forma Form4
      {
         Application->CreateForm(__classid(TForm4), &Form4);
      }
     Form4->ValueListEditor1->Strings->Clear();
     for (int i = 0; i < GPU.Length; i++)
      {
       String fname="";
       if (GPU[i].FriendlyName!="")
        {
         fname="  ("+GPU[i].FriendlyName+")";
        }
       String drv=GPU[i].DriverPath;
       drv.Delete(1,Pos("}\\",drv)+1);
       String str=GPU[i].IDString+fname+" | Bus:Device:Function: "+ GPU[i].BusString+" | PNP string: "+GPU[i].DevID +
       +" | Adapter index: "+GPU[i].AdIndexes[0]+" | Registry key: "+ drv;
       Form4->ValueListEditor1->InsertRow(str,GPU[i].FriendlyName,true);
      }
     Form4->Show();
   }
return;
}
//---------------------------------------------------------------------------
bool Odnt::CheckIfAdmin()
{
bool b;
SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
PSID AdministratorsGroup;
b = AllocateAndInitializeSid(&NtAuthority,2,SECURITY_BUILTIN_DOMAIN_RID,DOMAIN_ALIAS_RID_ADMINS,0, 0, 0, 0, 0, 0,&AdministratorsGroup);
if (b)
{
  if (!CheckTokenMembership( NULL, AdministratorsGroup, (int*)&b))
    {
     b=false;
    }
  FreeSid(AdministratorsGroup);
}
return b;
}
//---------------------------------------------------------------------------
void Odnt::ChangeMultiGpuFriendlyNames()
{
for (int i = 0; i < GPU.Length; i++)
 {
  String str=Form4->ValueListEditor1->Cells[1][i+1];
  str=str.Trim();
  if (str!=GPU[i].FriendlyName)
   {
    if (Odnt::ChangeGPUFriendlyName(i, str)==false)
     {
       blad("Failed to change friendly name for GPU "+IntToStr(i), NULL, false);
     } else
      {
        GPU[i].FriendlyName=str;
      }
   }
 }
ChangeGPUDescription(false);
return;
}
//---------------------------------------------------------------------------
//Value -1 reset to default
void SetFansPercentage(int GpuInd, int Value)
{
if (InitializeADL())
 {
   //Inicjalizacja nowych funkcji
   typedef int (*ADL2_OVERDRIVE5_FANSPEED_SET )(ADL_CONTEXT_HANDLE, int iAdapterIndex, int iThermalControllerIndex, ADLFanSpeedValue *lpFanSpeedValue);
   typedef int (*ADL2_OVERDRIVE5_FANSPEEDTODEFAULT_SET )(ADL_CONTEXT_HANDLE, int iAdapterIndex, int iThermalControllerIndex);

   typedef int ( *ADL2_OVERDRIVE6_FANSPEED_SET )(ADL_CONTEXT_HANDLE, int iAdapterIndex, ADLOD6FanSpeedValue *lpFanSpeedValue);
   typedef int ( *ADL2_OVERDRIVE6_FANSPEED_RESET )(ADL_CONTEXT_HANDLE, int iAdapterIndex);

   ADL2_OVERDRIVE5_FANSPEED_SET ADL2_Overdrive5_FanSpeed_Set = (ADL2_OVERDRIVE5_FANSPEED_SET)GetProcAddress (hDLL, "ADL2_Overdrive5_FanSpeed_Set");
   ADL2_OVERDRIVE5_FANSPEEDTODEFAULT_SET ADL2_Overdrive5_FanSpeedToDefault_Set = (ADL2_OVERDRIVE5_FANSPEEDTODEFAULT_SET) GetProcAddress (hDLL, "ADL2_Overdrive5_FanSpeedToDefault_Set");


   ADL2_OVERDRIVE6_FANSPEED_SET ADL2_Overdrive6_FanSpeed_Set  = (ADL2_OVERDRIVE6_FANSPEED_SET)GetProcAddress(hDLL, "ADL2_Overdrive6_FanSpeed_Set");
   ADL2_OVERDRIVE6_FANSPEED_RESET ADL2_Overdrive6_FanSpeed_Reset = (ADL2_OVERDRIVE6_FANSPEED_RESET) GetProcAddress (hDLL, "ADL2_Overdrive6_FanSpeed_Reset");

   if (NULL == ADL2_Overdrive5_FanSpeed_Set ||
       NULL == ADL2_Overdrive5_FanSpeedToDefault_Set ||
       NULL == ADL2_Overdrive6_FanSpeed_Set ||
       NULL == ADL2_Overdrive6_FanSpeed_Reset)
    {
     blad("Failed to get ADL function pointers", GetLastError(), true);
     FreeLibrary(hDLL);
     return;
    }
   int an;
   if (Value==-1)     //automode
     {
       an=ADL2_Overdrive5_FanSpeedToDefault_Set(context, GPU[GpuInd].AdIndexes[0], 0);
       if (ADL_OK!=an)
        {
         if (ADL_OK!=ADL2_Overdrive6_FanSpeed_Reset(context, GPU[GpuInd].AdIndexes[0]))
          {
           bool ignore8=Options.IgnoreError8;
           Options.IgnoreError8=false;
           blad("Failed to set Fan to automode", an, false);
           Options.IgnoreError8=ignore8;
          }
        }
       if (GPU[GpuInd].FanEnabled)
        {
         int Ad_Length=GPU[GpuInd].AdIndexes.Length;
         ADLODNFanControl odNFanControl;
         memset(&odNFanControl, 0, sizeof(ADLODNFanControl));
         int an=ADL2_OverdriveN_FanControl_Get(context, GPU[GpuInd].AdIndexes[0], &odNFanControl);
         if (ADL_OK==an)
          {
            odNFanControl.iMode = ADLODNControlType::ODNControlType_Manual;
            for (int i=0; i < Ad_Length; i++)
             {
              ADLODNFanControl FC=odNFanControl;
              an= ADL2_OverdriveN_FanControl_Set(context,GPU[GpuInd].AdIndexes[i], &FC);
              if (ADL_OK != an)
               {
                 blad("Failed to set Fan parameters", an, false);
                 break;
               }
             }
          } else
           {
             blad("Failed to get Fan parameters", an, false);
           }
        }
     } else
      {
        ADLFanSpeedValue FanSpeed = {0};
        FanSpeed.iFanSpeed = Value;
        FanSpeed.iFlags=1;
        FanSpeed.iSpeedType=ADL_DL_FANCTRL_SPEED_TYPE_PERCENT;
        an=ADL2_Overdrive5_FanSpeed_Set(context, GPU[GpuInd].AdIndexes[0], 0, &FanSpeed);
        if (ADL_OK != an)
         {
          ADLOD6FanSpeedValue fanSpeed = {0};
          fanSpeed.iSpeedType=ADL_OD6_FANSPEED_TYPE_PERCENT;
          fanSpeed.iFanSpeed=Value;
          if (ADL_OK != ADL2_Overdrive6_FanSpeed_Set (context, GPU[GpuInd].AdIndexes[0], &fanSpeed))
           {
             bool ignore8=Options.IgnoreError8;
             Options.IgnoreError8=false;
             blad("Failed to set constant fan speed", an, false);
             Options.IgnoreError8=ignore8;
           }
         }
      }
   DeinitializeADL();
 }
return;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SettoAutoMode1Click(TObject *Sender)
{
SetFansPercentage(ComboBox1->ItemIndex, -1);
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SetFansto01Click(TObject *Sender)
{
SetFansPercentage(ComboBox1->ItemIndex, 0);
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SetFansto02Click(TObject *Sender)
{
SetFansPercentage(ComboBox1->ItemIndex, 20);
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SetFansto301Click(TObject *Sender)
{
SetFansPercentage(ComboBox1->ItemIndex, 30);
return;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Setmanualto701Click(TObject *Sender)
{
SetFansPercentage(ComboBox1->ItemIndex, 70);
return;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SetFansto1001Click(TObject *Sender)
{
SetFansPercentage(ComboBox1->ItemIndex, 100);
return;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Setcustomspeed1Click(TObject *Sender)
{
String str="";
if (!InputQuery("","Set speed (%):", str)) return;
str=str.Trim();
if (str=="")
 {
  ShowMessage("Put value between 0 and 100");
  return;
 }
int speed;
if (!TryStrToInt(str, speed))
 {
  ShowMessage("Put value between 0 and 100");
  return;
 }
if ((speed<0) || (speed>100))
 {
   ShowMessage("Put value between 0 and 100");
   return;
 }
SetFansPercentage(ComboBox1->ItemIndex, speed);
}
//---------------------------------------------------------------------------
void INI::MakeIniBackup(bool DisplayConfirmMsg)
{
if (!FileExists(inifile))
 {
   if (DisplayConfirmMsg)
    ShowMessage("\"OverdriveNTool.ini\" does not exist");
   return;
 }

if (DisplayConfirmMsg)    //Robimy z przycisku w opcjach, wi�c aktualizujemy
 {
   Options.IniBackup_AutoCreate=Form2->CheckBox10->Checked;
   Options.IniBackup_LimitNumOfBackups=Form2->CheckBox12->Checked;
   Options.IniBackup_Count=Form2->SpinEdit1->Value;
 }

String dir=ExtractFilePath(Application->ExeName)+"ini_backup\\";
if (!DirectoryExists(dir))
 {
   int ret=SHCreateDirectory(NULL,dir.w_str());
   if (ret!=0)
    {
     blad("Failed to create directory \""+dir+"\"", ret, true);
     return;
    }
 }
TFormatSettings FormatSettings;
FormatSettings.Create(GetThreadLocale());
FormatSettings.ShortDateFormat="yyyy-mm-dd";
FormatSettings.DateSeparator=*L"-";
String fname=DateToStr(Date(), FormatSettings)+" "+FormatDateTime("hh.nn.ss", Now())+" OverdriveNTool.ini";
if (!CopyFileW(("\\\\?\\"+ExtractFilePath(Application->ExeName)+"OverdriveNTool.ini").w_str(), ("\\\\?\\"+dir+fname).w_str(), 0))
 {
   blad("Failed to copy \"OverdriveNTool.ini\" file", GetLastError(), true);
   return;
 }
INI::FlushFileBuffer(dir+fname);

if ((Options.IniBackup_LimitNumOfBackups) && (Options.IniBackup_Count>0) && (Options.IniBackup_AutoCreate))
 {
   TStringList *files=new TStringList;
   TSearchRec rec;
   int found=FindFirst(dir+"2???-??-?? ??.??.?? OverdriveNTool.ini", fileAnything, rec);
   while (found==0)
    {
      if ((rec.Attr & faDirectory)==0)
       files->Add(rec.Name);
     found=FindNext(rec);
    }
   FindClose(rec);
   files->Sort();
   while (files->Count>Options.IniBackup_Count)
    {
     if (files->Count==0) break;
     if (!DeleteFile(dir+files->Strings[0]))
      {
        FileSetAttr(dir+files->Strings[0], 0);
        if (!DeleteFile(dir+files->Strings[0]))
         LogWrite("Failed to delete backup file: \""+files->Strings[0]+"\"",0,false,1);
      }
     files->Delete(0);
    }
 }
if (DisplayConfirmMsg)
 ShowMessage("File saved as \""+fname+"\"");

return;
}
//---------------------------------------------------------------------------
inline void INI::SetMainFormPosition()
{
TIniFile* ini = new TIniFile(inifile);
try
 {
  Form1->Left= ini->ReadInteger("General","MainWindowLeft",0);
  Form1->Top= ini->ReadInteger("General","MainWindowTop",0);
  if ((Form1->Left==0) && (Form1->Top==0))
   {
     Form1->Position=poScreenCenter;   //pierwsze uruchomienie programu
   }                                   //istnieje bug gdy u�yjemy zmiany pozycji to znika custom menu z systemmenu
 }                                     //je�eli system memu adding by� u�yty wcze�niej AddSystemMemuEntries(); musi by� potem
__finally
  {
    delete ini;
  }
}
//---------------------------------------------------------------------------
void INI::SaveMainFormPosition()
{
if (FileExists(inifile))
 {
   TMemIniFile* ini = new TMemIniFile(inifile, TEncoding::Unicode);
   try
    {
     try
      {
        ini->WriteInteger("General","MainWindowLeft", Form1->Left);
        ini->WriteInteger("General","MainWindowTop", Form1->Top);
        ini->UpdateFile();
        INI::FlushFileBuffer(inifile);
      }  catch(Exception& E)
          {
            if ((GetFileAttributes(inifile.w_str()) & faReadOnly)==0) //Check if user set ini file to readOnly
             {
               blad(E.Message, E.HelpContext, true);
             }
          }
    }
   __finally
    {
      delete ini;
    }
 }
}
//---------------------------------------------------------------------------
void INI::SaveFanDeactivated()
{
String str="";
for (int i = 0; i < GPU.Length; i++)
 {
  if (GPU[i].FanEnabled==false)
    {
     if (str=="")
      {
        str=str+IntToStr(i);
      }  else
        {
          str=str+";"+IntToStr(i);
        }
    }
 }
TMemIniFile* ini = new TMemIniFile(inifile, TEncoding::Unicode);
try
 {
   if (str=="")
     {
       FanDeactivatedExists=false;
       if (ini->ValueExists("General","FanDeactivatedGpus"))
         {
           ini->DeleteKey("General","FanDeactivatedGpus");
         }
     } else
       {
         FanDeactivatedExists=true;
         ini->WriteString("General","FanDeactivatedGpus", str);
       }
   ini->UpdateFile();
   INI::FlushFileBuffer(inifile);
 }
   __finally
   {
    delete ini;
   }
}
//---------------------------------------------------------------------------
void INI::RestoreFromBackup(bool from_cmd)
{
if (Options.IniBackup_AutoRestore==false) return;
String dir=ExtractFilePath(Application->ExeName)+"ini_backup\\";

if (DirectoryExists(dir))
 {
   TStringList *files=new TStringList;
   TSearchRec rec;
   int found=FindFirst(dir+"2???-??-?? ??.??.?? OverdriveNTool.ini", fileAnything, rec);
   while (found==0)
    {
      if ((rec.Attr & faDirectory)==0)
       files->Add(rec.Name);
     found=FindNext(rec);
    }
   FindClose(rec);
   if (files->Count>0)
    {
     int IndexToRestore=-1;
     files->Sort();
     for (int i=files->Count-1; i>=0; i--)
      {
       TIniFile* ini = new TIniFile(dir+files->Strings[i]);
       if (ini->ValueExists("Profile_0","Name"))
        {
         if (ini->ReadString("Profile_0","Name","")!="")
          {
            IndexToRestore=i;
            delete ini;
            break;
          }
        }
       delete ini;
      }
     if (IndexToRestore!=-1)    //plik backupu zawiera jakie� profile
      {
       if (Options.IniBackup_NoConfirmOnRestore==false)
        {
          String s="";
          if (IndexToRestore!=files->Count-1)
           s=" that contains profiles (not the last one)";
          String str="OverdriveNTool.ini contains 0 profiles. Restore it from last backup"+s+" \""+files->Strings[IndexToRestore]+"\"?";
          if (MessageBox(0, str.w_str(), L"Restore from backup", MB_ICONQUESTION|MB_YESNO)==7)
            return;
        }
       //1. przywracamy plik backupu
       DeleteFileW(("\\\\?\\"+ExtractFilePath(Application->ExeName)+"OverdriveNTool.ini").w_str());
       if (!CopyFileW( ("\\\\?\\"+dir+files->Strings[IndexToRestore]).w_str(), ("\\\\?\\"+ExtractFilePath(Application->ExeName)+"OverdriveNTool.ini").w_str(), 0))
        {
         blad("Failed to restore \"OverdriveNTool.ini\" file", GetLastError(), true);
         return;
        }
       //Odczytujemy opcje i profile
       INI::LoadOptions(from_cmd);
       INI::LoadProfiles();
       if (Options.IniBackup_NoConfirmOnRestore)
        LogWrite("\"OverdriveNTool.ini\" was restored from \""+files->Strings[IndexToRestore]+"\"", 0 , false, 1);
      }
    }
 }
return;
}
//---------------------------------------------------------------------------
/*  
void INI::SetReadOnly(String filename, bool readonly)
{
int currattr =GetFileAttributes(filename.w_str());
if (readonly)
 {
   SetFileAttributes(filename.w_str(), currattr | faReadOnly);
 } else
  {
   SetFileAttributes(filename.w_str(), currattr & ~faReadOnly);
  }
}  */
//---------------------------------------------------------------------------
bool SetGetChillPowerEfficiency(bool Chill, int GpuInd, bool silent, int NewValue, bool* CurrentValue)
{
if (InitializeADL())
 {
   //Inicjalizacja nowych funkcji
   typedef int (*FUNC_GET )(ADL_CONTEXT_HANDLE, int iAdapterIndex, int* Value);
   typedef int (*FUNC_SET )(ADL_CONTEXT_HANDLE, int iAdapterIndex, int Value);

   AnsiString GetFunc="ADL2_PPW_Status_Get";
   AnsiString SetFunc="ADL2_PPW_Status_Set";
   if (Chill)
    {
      GetFunc="ADL2_Chill_Settings_Get";
      SetFunc="ADL2_Chill_Settings_Set";
    }

   FUNC_GET Func_Get = (FUNC_GET) GetProcAddress (hDLL, GetFunc.c_str());
   FUNC_SET Func_Set = (FUNC_SET) GetProcAddress (hDLL, SetFunc.c_str());

   if (NULL == Func_Get || NULL == Func_Set)
    {
     if (!silent)
      blad("Failed to get "+GetFunc+" function pointers", GetLastError(), true);
     FreeLibrary(hDLL);
     return false;
    }
   int an;

   if (NewValue!=-1)   //Set
    {
     an=Func_Set(context, GPU[GpuInd].AdIndexes[0], NewValue);
     if (ADL_OK != an)
      {
       if (!silent)
        blad(SetFunc+" function failed", an, false);
       return false;
      }
    }
   an=Func_Get(context, GPU[GpuInd].AdIndexes[0], (int*)CurrentValue);
   if (ADL_OK != an)
    {
     if (!silent)
      blad(GetFunc+" function failed", an, false);
     return false;
    }
   if (NewValue!=-1)
    {
      if (NewValue!=*CurrentValue)
       {
         if (!silent)
          {
           if (Chill)
            blad("Failed to set Chill settings", 0, false); else
             blad("Failed to set Power Efficiency", 0, false);
          }
         return false;
       }
    }
   //===========================================
   DeinitializeADL();
 }
return true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PowerEfficiency1Chill1DisplayStatus(bool Chill, bool Current)
{
String str="Power Efficiency:";
TMenuItem* mi=PowerEfficiency1;
if (Chill)
 {
   str="Chill:";
   mi=Chill1;
 }
if (Current)
   {
    mi->Caption=str+" ON";
    mi->Checked=true;
   } else
     {
      mi->Caption=str+" OFF";
      mi->Checked=false;
     }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PowerMenuPopup(TObject *Sender)
{
bool Current=false;
if (SetGetChillPowerEfficiency(false, ComboBox1->ItemIndex, true, -1, &Current))
 PowerEfficiency1Chill1DisplayStatus(false, Current); else
  PowerEfficiency1->Visible=false;

if (SetGetChillPowerEfficiency(true, ComboBox1->ItemIndex, true, -1, &Current))
 PowerEfficiency1Chill1DisplayStatus(true, Current); else
  Chill1->Visible=false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::PowerEfficiency1Click(TObject *Sender)
{
int NewValue=(!PowerEfficiency1->Checked);

bool Current;
if (SetGetChillPowerEfficiency(false, ComboBox1->ItemIndex, false, NewValue, &Current))
 PowerEfficiency1Chill1DisplayStatus(false, Current);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Chill1Click(TObject *Sender)
{
int NewValue=(!Chill1->Checked);

bool Current;
if (SetGetChillPowerEfficiency(true, ComboBox1->ItemIndex, false, NewValue, &Current))
 PowerEfficiency1Chill1DisplayStatus(true, Current);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::ZeroRPMBoxClick(TObject *Sender)
{
if (ZeroRPMBox->Checked)
 {
    if (CurrentValues.Fan_ZeroRPM==false)
     ZeroRPMBox->Color=ChangedColor; else
      ZeroRPMBox->Color=clBtnFace;

   ZeroRPMBox->Caption="ON";
 }
  else
   {
    if (CurrentValues.Fan_ZeroRPM==true)
     ZeroRPMBox->Color=ChangedColor;
      else
       ZeroRPMBox->Color=clBtnFace;

    ZeroRPMBox->Caption="OFF";
   }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Mem_TimingBoxChange(TObject *Sender)
{
if (CurrentValues.Mem_TimingLevel!=Mem_TimingBox->ItemIndex)
 Mem_TimingBox->Color=ChangedColor; else
  Mem_TimingBox->Color=clWindow;
}
//---------------------------------------------------------------------------


void __fastcall TForm1::GPU_MinChange(TObject *Sender)
{
if (CurrentValues.GPU_Min!=GPU_Min->Value)
 GPU_Min->Color=ChangedColor; else
  GPU_Min->Color=clWindow;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GPU_MaxChange(TObject *Sender)
{
if (CurrentValues.GPU_Max!=GPU_Max->Value)
 GPU_Max->Color=ChangedColor; else
  GPU_Max->Color=clWindow;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Mem_MaxChange(TObject *Sender)
{
if (CurrentValues.Mem_Max!=Mem_Max->Value)
 Mem_Max->Color=ChangedColor; else
  Mem_Max->Color=clWindow;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
if (Profiles.Length==0)
 return;

Form5->ListView1->Items->Clear();

for (int i = 0; i < Profiles.Length; i++)
 {
  TListItem* li=Form5->ListView1->Items->Add();
  li->Caption=Profiles[i].Name;
  li->SubItems->Add(i);
 }
Form5->ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ReorderProfiles()
{
if (Profiles.Length==0)
 {
  return;
 }

DynamicArray <ProfileItem> tmp;
tmp.Length=Profiles.Length;

tmp=Profiles;
Profiles.Length=0;
Profiles.Length=tmp.Length;

int itemindex=ComboBox2->ItemIndex;

try
 {
  for (int i = 0; i < Profiles.Length; i++)
   {
    Profiles[i]=tmp[StrToInt(Form5->ListView1->Items->Item[i]->SubItems->Strings[0])];
   }

  for (int i = 0; i < Form5->ListView1->Items->Count; i++)
   {
    if (StrToInt(Form5->ListView1->Items->Item[i]->SubItems->Strings[0])==itemindex)
     {
      itemindex=i;
      break;
     }
   }
 } __except(EXCEPTION_EXECUTE_HANDLER)
  {
   Profiles=tmp;
   return;
  }

ComboBox2->Clear();
tmp.Length=0;

for (int i = 0; i < Profiles.Length; i++)
 {
  ComboBox2->Items->Add(Profiles[i].Name);
 }
ComboBox2->ItemIndex=itemindex;
INI.SaveProfiles();
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "Unit1.h"

#include "ChangeFriendlyName.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm4 *Form4;

//---------------------------------------------------------------------------
__fastcall TForm4::TForm4(TComponent* Owner)
	: TForm(Owner)
{
Form1->Form4_Created=true;
}
//---------------------------------------------------------------------------

void __fastcall TForm4::Button1Click(TObject *Sender)
{
Odnt.ChangeMultiGpuFriendlyNames();
Form4->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm4::Button2Click(TObject *Sender)
{
Form4->Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm4::FormResize(TObject *Sender)
{
ValueListEditor1->ColWidths[0]=Form4->Width-(180*Form1->DpiMultiplerPix);
}
//---------------------------------------------------------------------------

